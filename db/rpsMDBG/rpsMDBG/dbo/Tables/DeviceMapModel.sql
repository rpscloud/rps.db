﻿CREATE TABLE [dbo].[DeviceMapModel] (
    [DeviceId]        UNIQUEIDENTIFIER NOT NULL,
    [MapId]           UNIQUEIDENTIFIER NOT NULL,
    [Name]            NVARCHAR (MAX)   NULL,
    [X]               DECIMAL (18, 2)  NOT NULL,
    [Y]               DECIMAL (18, 2)  NOT NULL,
    [W]               INT              NOT NULL,
    [H]               INT              NOT NULL,
    [R]               DECIMAL (18, 2)  NOT NULL,
    [SVG]             NVARCHAR (MAX)   NULL,
    [Image]           NVARCHAR (MAX)   NULL,
    [S]               DECIMAL (18, 2)  NOT NULL,
    [ChangedDateTime] DATETIME         NULL,
    [Deleted]         BIT              NOT NULL,
    [_IsDeleted]      BIT              NOT NULL,
    [Device_Id]       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_dbo.DeviceMapModel] PRIMARY KEY CLUSTERED ([DeviceId] ASC),
    CONSTRAINT [FK_dbo.DeviceMapModel_dbo.DeviceModel_Device_Id] FOREIGN KEY ([Device_Id]) REFERENCES [dbo].[DeviceModel] ([Id]),
    CONSTRAINT [FK_dbo.DeviceMapModel_dbo.MapModel_MapId] FOREIGN KEY ([MapId]) REFERENCES [dbo].[MapModel] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_MapId]
    ON [dbo].[DeviceMapModel]([MapId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Device_Id]
    ON [dbo].[DeviceMapModel]([Device_Id] ASC);

