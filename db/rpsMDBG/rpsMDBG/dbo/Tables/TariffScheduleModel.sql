﻿CREATE TABLE [dbo].[TariffScheduleModel] (
    [Id]         UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Name]       NVARCHAR(MAX)    NULL,
    [_IsDeleted] BIT              NOT NULL,
    [Sync]       DATETIME         NULL,
    [IdFC]       TINYINT          NULL,
    CONSTRAINT [PK_dbo.TariffScheduleModel] PRIMARY KEY CLUSTERED ([Id] ASC),
);
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TariffScheduleModel_idFc]
    ON [dbo].[TariffScheduleModel]([IdFC] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffScheduleModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наименование', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffScheduleModel', @level2type = N'COLUMN', @level2name = N'Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffScheduleModel', @level2type = N'COLUMN', @level2name = N'Sync';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД для карты', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffScheduleModel', @level2type = N'COLUMN', @level2name = N'IdFC';

GO
CREATE TRIGGER [dbo].[SyncTariffScheduleModel]
ON [dbo].[TariffScheduleModel]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [TariffScheduleModel] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='TariffScheduleModel'
end
