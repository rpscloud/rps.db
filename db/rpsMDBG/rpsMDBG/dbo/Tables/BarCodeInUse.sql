﻿CREATE TABLE [dbo].[BarCodeInUse] (
    [Id]               UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [IdBCM]            UNIQUEIDENTIFIER NULL,
    [OperId]           UNIQUEIDENTIFIER NULL,
    [CompanyId]        UNIQUEIDENTIFIER NULL,
    [IdTP]             TINYINT          NULL,
    [IdTS]             TINYINT          NULL,
    [DiscountAmount]   INT              NULL,
    [GroupIdFC]        TINYINT          NULL,
    [UsageTime]        DATETIME         DEFAULT (getdate()) NULL,
    [Sync]             DATETIME         NULL,
    [GuestPlateNumber] NVARCHAR (MAX)   NULL,
    [GuestCarModel]    NVARCHAR (MAX)   NULL,
    [GuestName]        NVARCHAR (MAX)   NULL,
    [GuestEmail]       NVARCHAR (MAX)   NULL,
    [ParkingTime]      INT              NULL,
    [Comment]          NVARCHAR (MAX)   NULL,
    [CreatedAt]        DATETIME         NULL,
    [EnterId]          UNIQUEIDENTIFIER NULL,
    [ExitId]           UNIQUEIDENTIFIER NULL,
    [IdTPForCount]     UNIQUEIDENTIFIER NULL,
    [IdTSForCount]     UNIQUEIDENTIFIER NULL,
    [CardId]           BIGINT           NULL,
    [Ammount]          INT              NULL,
    [Used] BIT NULL, 
    [ActivatedAt] DATETIME NULL, 
    [ParkingEnterTime] DATETIME         NULL,
    [TPForRent] UNIQUEIDENTIFIER NULL, 
    [TSForRent] UNIQUEIDENTIFIER NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [BarCodeInUse_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[CompanyModel] ([Id]),
    CONSTRAINT [BarCodeInUse_Group] FOREIGN KEY ([GroupIdFC]) REFERENCES [dbo].[Group] ([idFC]),
    CONSTRAINT [BarCodeInUse_Tariffs] FOREIGN KEY ([IdTP]) REFERENCES [dbo].[Tariffs] ([IdFC]),
    CONSTRAINT [BarCodeInUse_TariffScheduleModel] FOREIGN KEY ([IdTS]) REFERENCES [dbo].[TariffScheduleModel] ([IdFC])
);


GO
ALTER TABLE [dbo].[BarCodeInUse] NOCHECK CONSTRAINT [BarCodeInUse_Tariffs];


GO
ALTER TABLE [dbo].[BarCodeInUse] NOCHECK CONSTRAINT [BarCodeInUse_TariffScheduleModel];




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = 'Sync';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время активации штрихкода', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'UsageTime';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Группа для BCF=3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'GroupIdFC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма для BCF=2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'DiscountAmount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТC для BCF=1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'IdTS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТП для BCF=1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'IdTP';



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Компания - заказчик штрихкода', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'CompanyId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Оператор - заказчик штрихкода', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'OperId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Штрихкод', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'IdBCM';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Гос номер', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'GuestPlateNumber';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Модель ТС гостя', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'GuestCarModel';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ФИО Гостя', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'GuestName';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Почта гостя', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'GuestEmail';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время парковки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'ParkingTime';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id транзакции въезда', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'EnterId';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id транзакции выезда', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'ExitId';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТП для расчета задолженности арендатора перед УК', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'IdTPForCount';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТС для расчета задолженности арендатора перед УК', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'IdTSForCount';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Номер выданной по штрих коду карты', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'CardId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма задолженности арендатора перед УК', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'Ammount';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Признак наличия активации', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'Used';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время активации', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'ActivatedAt';

GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТП для расчета задолженности арендатора перед УК, если есть места', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'TPForRent';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТС для расчета задолженности арендатора перед УК, если есть места', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeInUse', @level2type = N'COLUMN', @level2name = N'TSForRent';

GO
CREATE TRIGGER [dbo].[SyncBarCodeInUse]
ON [dbo].[BarCodeInUse]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [BarCodeInUse] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='BarCodeInUse'
 END

GO
