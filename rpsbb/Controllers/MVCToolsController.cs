﻿using ASE.Data;
using rps.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace rpsbb.Controllers
{
    public class MVCToolsController : Controller
    {
        // GET: ASPMVC
        public ActionResult Index()
        {
            return View();
        }

        private void ClearDB(DB db)
        {
            if (db == null)
                return;

            foreach (var item in db.UserParkingRoles.ToList())
                db.UserParkingRoles.Remove(item);
            foreach (var item in db.Roles.ToList())
                db.Roles.Remove(item);
            foreach (var item in db.RightsInRoles.ToList())
                db.RightsInRoles.Remove(item);
            foreach (var item in db.Rights.ToList())
                db.Rights.Remove(item);
            foreach (var item in db.OrganizationParkings.ToList())
                db.OrganizationParkings.Remove(item);
            foreach (var item in db.Organizations.ToList())
                db.Organizations.Remove(item);
            foreach (var item in db.Users.ToList())
                db.Users.Remove(item);
            foreach (var item in db.Parkings.ToList())
                db.Parkings.Remove(item);
            db.SaveChanges();
        }

        private void InitDBs(string con)
        {
            DB db = new DB(con);
            db.Database.CreateIfNotExists();
            db.Database.Initialize(true);
            db.Maps.FirstOrDefault();
        }

        public ActionResult FillDB()
        {
            //InitDBs("ConnectionString");
            //InitDBs("ConnectionStringL0");
            //InitDBs("ConnectionStringL1");

            System.Data.Entity.Database.SetInitializer<DB>(new ASE.EF.AutoMigrationInit<DB>());
            DB db = new DB("ConnectionString");
            DB dbL0 = null;
            DB dbL1 = null;
            //dbL0 = new DB("ConnectionStringL0");
            //dbL1 = new DB("ConnectionStringL1");

            ClearDB(db);
            ClearDB(dbL0);
            ClearDB(dbL1);

            //OrganizationModel
            var organizations = new List<OrganizationModel>();
            for (int i = 0; i < 5; i++)
            {
                organizations.Add(new OrganizationModel { 
                    Id = Guid.NewGuid(),
                    Name = "Oraganization " + i,
                    ChangedDateTime = DateTime.Now
                });
            }
            foreach (var item in organizations)
                db.Organizations.Add(item);

            if (dbL0 != null)
            {
                dbL0.Organizations.Add(organizations[0]);
                dbL0.Organizations.Add(organizations[1]);
                dbL1.Organizations.Add(organizations[2]);
            }

            //ParkingModel
            var parkings = new List<ParkingModel>();
            for (int i = 0; i < 2; i++)
            {
                parkings.Add(new ParkingModel { 
                    Id = Guid.NewGuid(),
                    Name = "Parking " + i,
                    ChangedDateTime = DateTime.Now,
                    S2Sql = "localhost",
                    S2Db = String.Format("rpsR{0}", i - 5),
                    S2User = "sa",
                    S2Password = "111",

                    S3Sql = "localhost",
                    S3Db = String.Format("rps", i - 5),
                    S3User = "sa",
                    S3Password = "111",
                });
            }
            foreach (var item in parkings)
                db.Parkings.Add(item);
            if (dbL0 != null)
            {
                dbL0.Parkings.Add(parkings[0]);
                dbL1.Parkings.Add(parkings[1]);
            }

            //OrganizationParkings
            var organizationParkings = new List<OrganizationParkingModel>();
            organizationParkings.Add(new OrganizationParkingModel
            {
                Id = Guid.NewGuid(),
                Active = true,
                ChangedDateTime = DateTime.Now,
                ParkingId = parkings[0].Id,
                OrganizationId = organizations[0].Id,
            });
            organizationParkings.Add(new OrganizationParkingModel
            {
                Id = Guid.NewGuid(),
                Active = true,
                ChangedDateTime = DateTime.Now,
                ParkingId = parkings[0].Id,
                OrganizationId = organizations[1].Id,
            });
            organizationParkings.Add(new OrganizationParkingModel
            {
                Id = Guid.NewGuid(),
                Active = true,
                ChangedDateTime = DateTime.Now,
                ParkingId = parkings[1].Id,
                OrganizationId = organizations[2].Id,
            });
            foreach (var item in organizationParkings)
                db.OrganizationParkings.Add(item);
            if (dbL0 != null)
            {
                dbL0.OrganizationParkings.Add(organizationParkings[0]);
                dbL0.OrganizationParkings.Add(organizationParkings[1]);
                dbL1.OrganizationParkings.Add(organizationParkings[2]);
            }

            //UserModel
            var users = new List<UserModel>();
            for (int i = 0; i < 15; i++)
            {
                users.Add(new UserModel
                {
                    Id = Guid.NewGuid(),
                    Name = "User " + i,
                    Email = String.Format("user{0}@domain.com", i),
                    LoginName = "u" + i,
                    Password = "p" + i,
                    ChangedDateTime = DateTime.Now,
                });
            }
            foreach (var item in users)
                db.Users.Add(item);
            if (dbL0 != null)
            {
                dbL0.Users.Add(users[0]);
                dbL0.Users.Add(users[1]);
                dbL0.Users.Add(users[2]);
                dbL0.Users.Add(users[3]);
                dbL1.Users.Add(users[0]);
                dbL1.Users.Add(users[4]);
            }

            //RightModel
            var rights = new List<RightModel>();
            for (int i = 0; i < 15; i++)
            {
                rights.Add(new RightModel
                {
                    Id = Guid.NewGuid(),
                    Name = "Root Right " + i,
                });
                rights.Add(new RightModel
                {
                    Id = Guid.NewGuid(),
                    Name = String.Format("Right {0} {1}", i / 2, i),
                    ParentId = rights[i / 2].Id
                });
                rights.Add(new RightModel
                {
                    Id = Guid.NewGuid(),
                    Name = String.Format("Right {0} {1}", i / 2, i),
                    ParentId = rights[i / 2].Id,
                    ChangedDateTime = DateTime.Now,
                });
            }
            foreach (var item in rights)
            {
                db.Rights.Add(item);
                if (dbL0 != null)
                {

                    dbL0.Rights.Add(item);
                    dbL1.Rights.Add(item);
                }
            }

            //RoleModel
            var roles = new List<RoleModel>();
            for (int i = 0; i < 6; i++)
            {
                roles.Add(new RoleModel
                {
                    Id = Guid.NewGuid(),
                    Name = "Role " + i,
                    ParkingId = parkings[i < 3 ? 0 : 1].Id,
                    Active = true,
                    ChangedDateTime = DateTime.Now,
                });
            }
            foreach (var item in roles)
                db.Roles.Add(item);
            if (dbL0 != null)
            {

                dbL0.Roles.Add(roles[0]);
                dbL0.Roles.Add(roles[1]);
                dbL0.Roles.Add(roles[2]);
                dbL1.Roles.Add(roles[3]);
                dbL1.Roles.Add(roles[4]);
                dbL1.Roles.Add(roles[5]);
            }

            //UserRoleParkings
            var userParkingRoles = new List<UserParkingRoleModel>();
            userParkingRoles.Add(new UserParkingRoleModel 
            {
                Id = Guid.NewGuid(),
                Active = true,
                ChangedDateTime = DateTime.Now,
                ParkingId = parkings[0].Id,
                UserId = users[0].Id,
                RoleId = roles[0].Id,
            });
            userParkingRoles.Add(new UserParkingRoleModel
            {
                Id = Guid.NewGuid(),
                Active = true,
                ChangedDateTime = DateTime.Now,
                ParkingId = parkings[0].Id,
                UserId = users[1].Id,
                //RoleId = roles[1].Id,
            });
            userParkingRoles.Add(new UserParkingRoleModel
            {
                Id = Guid.NewGuid(),
                Active = true,
                ChangedDateTime = DateTime.Now,
                ParkingId = parkings[0].Id,
                UserId = users[2].Id,
                RoleId = roles[0].Id,
            });
            userParkingRoles.Add(new UserParkingRoleModel
            {
                Id = Guid.NewGuid(),
                Active = true,
                ChangedDateTime = DateTime.Now,
                ParkingId = parkings[0].Id,
                UserId = users[3].Id,
                RoleId = roles[0].Id,
            });
            userParkingRoles.Add(new UserParkingRoleModel
            {
                Id = Guid.NewGuid(),
                Active = true,
                ChangedDateTime = DateTime.Now,
                ParkingId = parkings[1].Id,
                UserId = users[0].Id,
                RoleId = roles[3].Id,
            });
            userParkingRoles.Add(new UserParkingRoleModel
            {
                Id = Guid.NewGuid(),
                Active = true,
                ChangedDateTime = DateTime.Now,
                ParkingId = parkings[1].Id,
                UserId = users[4].Id,
            });
            foreach (var item in userParkingRoles)
                db.UserParkingRoles.Add(item);
            if (dbL0 != null)
            {

                dbL0.UserParkingRoles.Add(userParkingRoles[0]);
                dbL0.UserParkingRoles.Add(userParkingRoles[1]);
                dbL0.UserParkingRoles.Add(userParkingRoles[2]);
                dbL0.UserParkingRoles.Add(userParkingRoles[3]);
                dbL1.UserParkingRoles.Add(userParkingRoles[4]);
                dbL1.UserParkingRoles.Add(userParkingRoles[5]);
            }

            db.SaveChanges();
            if (dbL0 != null)
            {

                dbL0.SaveChanges();
                dbL1.SaveChanges();
            }

            return View();
        }
    }
}