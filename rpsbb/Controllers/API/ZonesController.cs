﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

using ASE.MVC;
using ASE.Data;

using rps.Data;
using rpsbb.Code;
using System.Reflection;

namespace rpsbb.Controllers
{
    [Authorize]
    public class ZonesController : ApiControllerAdv<DB>
    {
        [System.Web.Http.HttpGet]
        public ZoneModelJson Get(Guid id)
        {
            return db.Zones.Find(id).Get();
        }

        [System.Web.Http.HttpGet]
        public IEnumerable<ZoneModelJson> List()
        {
            var q = db.Zones.Where(x => !x._IsDeleted);

            return q.ToList().Select(x => x.Get());
        }

        [System.Web.Http.HttpPost]
        public ZoneModelJson Add(ZoneModelJson model)
        {
            model.Id = Guid.Empty;
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPut]
        public ZoneModelJson Put(ZoneModelJson model)
        {
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPatch]
        public ZoneModelJson Patch(ZoneModelJson model)
        {
            return model.DALSave(db, true).Get();
        }

        [System.Web.Http.HttpDelete]
        public ZoneModelJson Delete([FromUri] ZoneModelJson model)
        {
            return model.DALDelete(db).Get();
        }
    }
}
