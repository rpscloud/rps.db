using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

using ASE.Data;
using ASE.MVC;

namespace rps.Data
{
    [NotMapped]
    public class ZoneModelJson : ZoneModel
    {
        public List<object> Devices { get; set; }
    }

    public static partial class Model2Json
    {
        public static ZoneModelJson Get(this ZoneModel model)
        {
            var ret = (ZoneModelJson)Copy(model, new ZoneModelJson());
            ret.Devices = new List<object>();
            ret.Devices.AddRange(ContextPerRequest.Db.DeviceCMs.Where(x => x.ZoneId == model.Id).Select(x => new { type = "DeviceCMModel", device = x }).ToList());
            ret.Devices.AddRange(ContextPerRequest.Db.DeviceRacks.Where(x => x.ZoneId == model.Id).Select(x => new { type = "DeviceRackModel", device = x }).ToList());
            ret.Devices.AddRange(ContextPerRequest.Db.DeviceDisplays.Where(x => x.ZoneId == model.Id).Select(x => new { type = "DeviceDisplayModel", device = x }).ToList());

            return ret;
        }


        public static ZoneModel DALSave(this ZoneModelJson model, DB db, bool patch = false, Guid? newId = null)
        {
            ZoneModel m = null;
            if (model.Id == Guid.Empty)
            {
                model.Id = newId.HasValue ? newId.Value : Guid.NewGuid();
                m = new ZoneModel();
                db.Zones.Add(m);
            }
            else
            {
                m = db.Zones.Find(model.Id);
            }
            Copy(model, m, patch);
            db.SaveChanges();

            return m;
        }

        public static ZoneModel DALDelete(this ZoneModelJson model, DB db)
        {
            var m = db.Zones.Find(model.Id);
            m._IsDeleted = true;
            db.SaveChanges();

            return m;
        }
    }
}