
CREATE TABLE [dbo].[DeviceRackSettings](
	[ID] [uniqueidentifier] NOT NULL PRIMARY KEY,
	[DeviceID] [uniqueidentifier] NULL,
	[Name] [varchar](max) NULL,
	[Value] [varchar](200) NULL,
	[ValueType] [int] FOREIGN KEY REFERENCES [DeviceRackSettingsTypes](ID),
	[Sync] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TRIGGER [dbo].[SyncRackSettings]
ON [dbo].[DeviceRackSettings]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [DeviceRackSettings] set Sync=@dt	where DeviceID in (select DeviceID from inserted)
	update SyncTables set [DateTime]=@dt where TableName='DeviceRackSettings'
end
