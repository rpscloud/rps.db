﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using System.Threading;

using ASE.Data;
using ASE.MVC;
using System.Data.SqlClient;
using System.Web.Mvc;

namespace rpsbb
{
    public class WebApiApplication : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
            System.Data.Entity.Database.SetInitializer<DB>(new ASE.EF.AutoMigrationInit<DB>());

            //DB db = new DB("ConnectionStringL0");
            //db.Maps.FirstOrDefault();

            //InitDBs("ConnectionString");
            //InitDBs("ConnectionStringL0");
            //InitDBs("ConnectionStringL1");

            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_EndRequest()
        {
            ContextPerRequest.DisposeDbContextPerRequest();
        }
    }
}
