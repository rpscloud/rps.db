jQuery.fn.extend({
    transform: function (name, value) {
        var _this = this;

        var res = this.each(function () {
            if (typeof this.style.transform != 'undefined') {

                var transform = this.style.transform.split(' ');


                var regexp = new RegExp(name + '\\((.*?)\\)', 'ig');


                if (!transform[0]) {
                    transform = '';
                } else {
                    transform = this.style.transform;
                }

                var old_name = regexp.exec(transform);

                if (old_name) {
                    transform = transform.replace(old_name[0], "");

                }

            } else {
                transform = '';
            }

            if (value) {
                transform = transform + ' ' + name + '(' + value + ') ';
                return $(this).css({transform: transform});
            } else {
                _this.res = old_name[1] ? parseFloat(old_name[1]) : 1;
            }

        });

        if (value) {
            return res;
        } else {
            return _this.res;
        }

    },
    caretTo: function (pos) {
        return this.each(function () {

            if (this.setSelectionRange) {
                this.focus();
                if (pos == 'start') {
                    this.setSelectionRange(0, 0);
                } else {
                    this.setSelectionRange(this.value.length, this.value.length);

                }
            } else if (this.createTextRange) {
                var range = this.createTextRange();

                if (pos == 'start') {
                    range.moveStart('character', 0);
                } else {
                    range.moveStart('character', this.value.length);
                }
                range.moveEnd('character', this.value.length);
                range.collapse();
                range.select();
            }
        });
    }

});
