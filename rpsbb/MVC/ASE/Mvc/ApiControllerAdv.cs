﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.IO;
#if OWIN
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.EntityFramework;
#endif

using ASE.EF;
using ASE.ToolsModels;
using rps;
using System.Web.Http;
using ASE.Data;
using System.Web.Security;
using rps.Data;

namespace ASE.MVC
{
    public class ApiControllerAdv<DBType> : ApiController where DBType : DbContext, new()
    {
        //internal DBType db;
        public DB db
        {
            get
            {
                return ContextPerRequest.Db;
            }
        }

        public ApiControllerAdv()
        {
            //db = new DBType();
        }

#region OWIN
#if OWIN

        private ApplicationUserManager _UserManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                if (_UserManager == null)
                {
                    _UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    //_UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
                    _UserManager.UserTokenProvider = new EmailTokenProvider<ApplicationUser, string>(); 
                }

                return _UserManager;
            }
        }

        private RoleManager<ApplicationRole> _RoleManager;
        public RoleManager<ApplicationRole> RoleManager
        {
            get
            {
                if (_RoleManager == null)
                    _RoleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new ApplicationDbContext()));

                return _RoleManager;
            }
        }

        public Guid UserId
        {
            get
            {
                return Guid.Parse(this.User.Identity.GetUserId());
            }
        }

        public string UserIdS
        {
            get
            {
                return UserId.ToString();
            }
        }

#endif
#endregion OWIN

        /*public void SortOrder(ref string sortOrder, string defSort)
        {
            sortOrder = String.IsNullOrEmpty(sortOrder) ? (this.GetSavedSortOrder("Index") ?? defSort) : sortOrder;
            this.SaveSortOrder("Index", sortOrder);
        }

        public void Filter(ref string filter, string defFilter)
        {
            filter = String.IsNullOrEmpty(filter) ? this.GetSavedFilter("Index") ?? defFilter : filter;
            this.SaveFilter("Index", filter);
        }

        public void SortOrderAndFilter(ref string sortOrder, string defSort, ref string filter, string defFilter)
        {
            SortOrder(ref sortOrder, defSort);

            Filter(ref filter, defFilter);
        }*/

        /*public void AttachModel<TEntity>(TEntity entity) where TEntity : class
        {
            //db.AttachModel(entity, HttpContext.Current.Request.Request);
        }*/

        /*public bool IsSelect
        {
            get
            {
                var ret = false;
                if (Request.Params.AllKeys.Contains("isSelect"))
                    bool.TryParse(Request.Params["isSelect"], out ret);

                return ret;
            }
        }*/

        /*public ToolModelIdValue SelectResultField
        {
            get
            {
                return new ToolModelIdValue
                {
                    Id = Request.Params["selectResultFieldId"],
                    Value = Request.Params["selectResultField"],
                };
            }
        }*/

        /*public virtual void SessionPush(string name, object value)
        {
            Session.Add(this.GetType().Name + "." + name, value);
        }

        public virtual DateTime SessionPull(string name, DateTime value)
        {
            var r = Session[this.GetType().Name + "." + name];
            if (r is DateTime)
                return (DateTime) r;

            return value;
        }
        public virtual int SessionPull(string name, int value)
        {
            var r = Session[this.GetType().Name + "." + name];
            if (r is int)
                return (int)r;

            return value;
        }

        public DateTime DefaultDateFrom(DateTime? dt)
        {
            return DefaultDateFrom(dt, "DateFrom");
        }*/

        /*public DateTime DefaultDateFrom(DateTime? dt, string name)
        {
            var r = dt.HasValue ? dt.Value.Date : SessionPull(name, DateTime.Now.Date);
            SessionPush(name, r);
            return r;
        }

        public DateTime DefaultDateTo(DateTime? dt)
        {
            return DefaultDateTo(dt, "DateTo");
        }

        public DateTime DefaultDateTo(DateTime? dt, string name)
        {
            var r = dt.HasValue ? dt.Value.Date.AddDays(1).AddMilliseconds(-1) : SessionPull(name, DateTime.Now.Date.AddDays(1).AddMilliseconds(-1));
            SessionPush(name, r);
            return r;
        }

        public List<KeyValuePair<string,string>> History()
        {
            List<KeyValuePair<string, string>> history = (Session["ASEHistory"] as List<KeyValuePair<string, string>>);
            if (history == null)
                history = new List<KeyValuePair<string, string>>();

            return history;
        }

        public ActionResult HistoryBack(ActionResult url)
        {
            var h = History();
            if (h.Count != 0)
            {
                var r = h[h.Count - 1].Value;
                if ((r != Request.Url.AbsoluteUri.ToLower()) & (r != Request.UrlReferrer.AbsoluteUri.ToLower()))
                    return Redirect(r);

                if (h.Count != 1)
                {
                    r = h[h.Count - 2].Value;
                    if (r != Request.Url.AbsoluteUri.ToLower())
                        return Redirect(r);
                }
            }

            return url;
        }*/

        public Guid UserId
        {
            get
            {
                return (Guid) Membership.GetUser().ProviderUserKey;
            }
        }

        public string UserIdS
        {
            get
            {
                return UserId.ToString();
            }
        }

        UserModel _user = null;
        public UserModel UserModel
        {
            get
            {
                if (_user == null)
                    _user = db.Users.Find(UserId);

                return _user;
            }
        }

        protected override void Dispose(bool disposing)
        {
#if OWIN
            if (disposing && UserManager != null)
            {
                _UserManager.Dispose();
                _UserManager = null;
            }
#endif
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}