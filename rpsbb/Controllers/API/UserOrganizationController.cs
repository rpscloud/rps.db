﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

using ASE.Data;
using ASE.MVC;

using rps.Data;
using rpsbb.Code;
using Elmah;

namespace rpsbb.Controllers
{
    [Authorize]
    public class UserOrganizationController : ApiControllerAdv<DB>
    {
        [System.Web.Http.HttpGet]
        public IEnumerable<UserOrganizationModelJson> List([FromUri] Guid? organizationId = null, [FromUri] Guid? userId = null)
        {
            if (organizationId.HasValue)
            {
                var p = db.OrganizationParkings.Where(x => 
                    x.OrganizationId == organizationId
                    & !x.Deleted
                    & !x.Organization.Deleted
                    & !x.Parking.Deleted
                    ).Select(x => x.ParkingId).ToList();

                return db.UserParkingRoles.Where(x => 
                    p.Contains(x.ParkingId)
                    & !x.Deleted
                    & !x.Parking.Deleted
                    & !x.User.Deleted 
                    ).Select(x => new UserOrganizationModelJson { OrganizationId = organizationId.Value, UserId = x.UserId }).ToList();
            }
            if (userId.HasValue)
            {
                var p = db.UserParkingRoles.Where(x => 
                    x.UserId == userId
                    & !x.Deleted
                    & !x.User.Deleted
                    & !x.Parking.Deleted
                    ).Select(x => x.ParkingId).ToList();

                return db.OrganizationParkings.Where(x => 
                    p.Contains(x.ParkingId)
                    & !x.Deleted
                    & !x.Parking.Deleted
                    & !x.Organization.Deleted
                    ).Select(x => new UserOrganizationModelJson { UserId = userId.Value, OrganizationId = x.OrganizationId }).ToList();
            }

            return new List<UserOrganizationModelJson>();
        }
    }
}
