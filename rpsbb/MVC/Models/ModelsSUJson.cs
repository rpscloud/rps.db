﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rps.Data
{
    public static partial class Model2Json
    {
        public static DeviceMapModelJson Get(this DeviceMapModel model)
        {
            var ret = (DeviceMapModelJson)Copy(model, new DeviceMapModelJson());

            return ret;
        }

        public static MapModelJson Get(this MapModel model)
        {
            var ret = (MapModelJson)Copy(model, new MapModelJson());

            return ret;
        }
    }

    [NotMapped]
    public class AlarmsModelJson : AlarmModel
    {
    }

    [NotMapped]
    public class DeviceMapModelJson : DeviceMapModel
    {
    }

    [NotMapped]
    public class MapModelJson : MapModel
    {
    }
}
