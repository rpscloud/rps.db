﻿CREATE TABLE [dbo].[AuthRole] (
    [Name]        NVARCHAR (128) NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.AuthRole] PRIMARY KEY CLUSTERED ([Name] ASC)
);

