﻿CREATE TABLE [dbo].[AuthItemChild] (
    [parent] VARCHAR (64) NOT NULL,
    [child]  VARCHAR (64) NOT NULL,
    CONSTRAINT [PK_AuthItemChild] PRIMARY KEY CLUSTERED ([parent] ASC, [child] ASC),
    CONSTRAINT [FK_AuthItemChild_AuthItem] FOREIGN KEY ([parent]) REFERENCES [dbo].[AuthItem] ([name]),
    CONSTRAINT [FK_AuthItemChild_AuthItem1] FOREIGN KEY ([child]) REFERENCES [dbo].[AuthItem] ([name])
);

