﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ASE.Data;

using rps.Data;
using ASE.MVC;
using System.Threading;

namespace rpsbb.Code
{
    public static partial class DAL
    {
        static object Copy(object from, object to, bool patch, params string[] skip)
        {
            if (to is ISyncTraking)
                (to as ISyncTraking).ChangedDateTime = DateTime.Now;

            foreach (var prop in to.GetType().GetProperties())
            {
                if (skip.FirstOrDefault(x => x == prop.Name) != null)
                    continue;

                var pi = from.GetType().GetProperty(prop.Name);
                if (pi == null)
                    continue;
                if (pi.PropertyType.GetInterface("ICollection`1") != null)
                    continue;

                var val = pi.GetValue(from);
                if (patch)
                {
                    if (val == null)
                        continue;

                    if ((val is Guid) && ((Guid) val == Guid.Empty))
                        continue;
                }

                to.GetType().GetProperty(prop.Name).SetValue(to, val);
            }

            return to;
        }

        static void StartSync()
        {
            //ThreadPool.QueueUserWorkItem(o => Synchronization.Start());
        }

        #region OrganizationModel

        /// <summary>
        /// OrganizationModel
        /// </summary>
        /// <param name="model"></param>
        /// <param name="db"></param>
        /// <param name="patch"></param>
        /// <returns></returns>
        public static OrganizationModel DALSave(this OrganizationModelJson model, DB db, bool patch = false, Guid? newId = null)
        {
            OrganizationModel m = null;
            if (model.Id == Guid.Empty)
            {
                model.Id = newId.HasValue ? newId.Value : Guid.NewGuid();
                m = new OrganizationModel();
                db.Organizations.Add(m);
            }
            else
            {
                m = db.Organizations.Find(model.Id);
            }
            Copy(model, m, patch);
            db.SaveChanges();

            StartSync();
            return m;
        }

        public static OrganizationModel DALDelete(this OrganizationModelJson model, DB db)
        {
            var m = db.Organizations.Find(model.Id);
            m.Deleted = true;
            //db.Organizations.Remove(m);
            db.SaveChanges();

            StartSync();
            return m;
        }

        #endregion OrganizationModel

        #region ParkingModel

        /// <summary>
        /// ParkingModel
        /// </summary>
        /// <param name="model"></param>
        /// <param name="db"></param>
        /// <param name="patch"></param>
        /// <returns></returns>
        public static ParkingModel DALSave(this ParkingModelJson model, DB db, bool patch = false, Guid? newId = null)
        {
            ParkingModel m = null;
            if (model.Id == Guid.Empty)
            {
                model.Id = newId.HasValue ? newId.Value : Guid.NewGuid();
                m = new ParkingModel();
                db.Parkings.Add(m);

                var role = new RoleModel { Id = Guid.NewGuid(), Name = "Полные права", ParkingId = model.Id, FullAccess = true };
                db.Roles.Add(role);
                foreach (var item in db.Rights.ToList())
                {
                    db.RightsInRoles.Add(new RightsInRoleModel
                    {
                        Id = Guid.NewGuid(),
                        RightId = item.Id,
                        RoleId = role.Id,
                        Active = true,
                    });
                }
            }
            else
            {
                m = db.Parkings.Find(model.Id);
            }
            Copy(model, m, patch);
            db.SaveChanges();

            //Local DB
            /*try
            {
                DB dbL = new DB(String.Format("data source={0};Initial Catalog={1};User ID={2};Password={3}", model.S3Sql, model.S3Db, model.S3User, model.S3Password));
                dbL.Database.Create();
                dbL.Database.Initialize(true);
            }
            catch
            {

            }*/

            StartSync();
            return m;
        }


        public static ParkingModel DALDelete(this ParkingModelJson model, DB db)
        {
            var m = db.Parkings.Find(model.Id);
            m.Deleted = true;
            //db.Parkings.Remove(m);
            db.SaveChanges();

            StartSync();
            return m;
        }

        #endregion ParkingModel

        #region UserModel

        /// <summary>
        /// UserModel
        /// </summary>
        /// <param name="model"></param>
        /// <param name="db"></param>
        /// <param name="patch"></param>
        /// <returns></returns>
        public static UserModel DALSave(this UserModelJson model, DB db, bool patch = false, UserModel user = null, Guid? newId = null)
        {
            UserModel m = null;
            AuthUser au = null;

            if (model.Id == Guid.Empty)
            {
                model.Id = newId.HasValue ? newId.Value : Guid.NewGuid();
                m = new UserModel();
                db.Users.Add(m);

                au = new AuthUser { Id = model.Id, CreateDate = DateTime.Now };
                db.AuthUsers.Add(au);

                if (user != null)
                {
                    var p = db.UserParkingRoles.FirstOrDefault(x => x.UserId == user.Id);
                    if (p != null)
                        db.UserParkingRoles.Add(new UserParkingRoleModel
                        { 
                            Id = Guid.NewGuid(),
                            ParkingId = p.ParkingId,
                            UserId = model.Id,
                            RoleId = db.Roles.FirstOrDefault(x => x.ParkingId == p.ParkingId & x.FullAccess).Id,
                            Deleted = true
                        });
                }
            }
            else
            {
                m = db.Users.Find(model.Id);
                au = db.AuthUsers.Find(model.Id);
            }
            Copy(model, m, patch);

            if (au == null)
            {
                au = new AuthUser { Id = model.Id, CreateDate = DateTime.Now };
                db.AuthUsers.Add(au);
            }
            au.LoginName = model.LoginName;
            au.Password = model.Password;

            db.SaveChanges();

            if (m.Is_Admin)
            {
                foreach(var item in db.Parkings.ToList())
                {
                    if (db.UserParkingRoles.FirstOrDefault(x => x.ParkingId == item.Id & x.UserId == m.Id) == null)
                    {
                        var link = new UserParkingRoleModelJson { ParkingId = item.Id, UserId = m.Id, RoleId = db.Roles.FirstOrDefault(x => x.ParkingId == item.Id & x.FullAccess).Id };
                        DALSave(link, db, false);
                    }
                }
            }

            StartSync();
            return m;
        }


        public static UserModel DALDelete(this UserModelJson model, DB db)
        {
            var m = db.Users.Find(model.Id);
            //db.Users.Remove(m);
            m.Deleted = true;
            db.AuthUsers.Remove(db.AuthUsers.Find(model.Id));
            db.SaveChanges();

            StartSync();
            return m;
        }

        #endregion UserModel

        #region OrganizationParkingModel

        /// <summary>
        /// OrganizationParkingModel
        /// </summary>
        /// <param name="model"></param>
        /// <param name="db"></param>
        /// <param name="patch"></param>
        /// <returns></returns>
        public static OrganizationParkingModel DALSave(this OrganizationParkingModelJson model, DB db, bool patch = false)
        {
            OrganizationParkingModel m = null;
            if (model.Id == Guid.Empty)
            {
                m = db.OrganizationParkings.FirstOrDefault(x => x.OrganizationId == model.OrganizationId & x.ParkingId == model.ParkingId);
                if (m != null)
                {
                    m.Deleted = false;
                    db.SaveChanges();

                    StartSync();
                    return m;
                }
                model.Id = Guid.NewGuid();
                m = new OrganizationParkingModel();
                db.OrganizationParkings.Add(m);
            }
            else
            {
                m = db.OrganizationParkings.Find(model.Id);
            }
            Copy(model, m, patch, "Parking", "Organization");
            m.Deleted = false;
            db.SaveChanges();

            StartSync();
            return m;
        }


        public static OrganizationParkingModel DALDelete(this OrganizationParkingModelJson model, DB db)
        {
            var m = db.OrganizationParkings.Find(model.Id);
            m.Deleted = true;
            //db.OrganizationParkings.Remove(m);
            db.SaveChanges();

            StartSync();
            return m;
        }

        #endregion OrganizationParkingModel

        #region UserParkingRoleModel

        /// <summary>
        /// UserParkingRoleModel
        /// </summary>
        /// <param name="model"></param>
        /// <param name="db"></param>
        /// <param name="patch"></param>
        /// <returns></returns>
        public static UserParkingRoleModel DALSave(this UserParkingRoleModelJson model, DB db, bool patch = false)
        {
            UserParkingRoleModel m = null;
            if (model.Id == Guid.Empty)
            {
                m = db.UserParkingRoles.FirstOrDefault(x => x.UserId == model.UserId & x.ParkingId == model.ParkingId);
                if (m != null)
                {
                    m.Deleted = false;
                    db.SaveChanges();

                    StartSync();
                    return m;
                }

                model.Id = Guid.NewGuid();
                m = new UserParkingRoleModel();
                db.UserParkingRoles.Add(m);
            }
            else
            {
                m = db.UserParkingRoles.Find(model.Id);
            }
            Copy(model, m, patch, "Parking", "Role");
            m.Deleted = false;
            db.SaveChanges();

            StartSync();
            return m;
        }


        public static UserParkingRoleModel DALDelete(this UserParkingRoleModelJson model, DB db)
        {
            var m = db.UserParkingRoles.Find(model.Id);
            m.Deleted = true;
            //db.UserParkingRoles.Remove(m);
            db.SaveChanges();

            return m;
        }

        #endregion UserParkingRoleModel

        #region RightModel

        /// <summary>
        /// RightModel
        /// </summary>
        /// <param name="model"></param>
        /// <param name="db"></param>
        /// <param name="patch"></param>
        /// <returns></returns>
        public static RightModel DALSave(this RightModelJson model, DB db, bool patch = false)
        {
            RightModel m = null;
            if (model.Id == Guid.Empty)
            {
                model.Id = Guid.NewGuid();
                m = new RightModel();
                db.Rights.Add(m);
            }
            else
            {
                m = db.Rights.Find(model.Id);
            }
            Copy(model, m, patch, "Parking", "Role");
            m.Deleted = false;
            db.SaveChanges();

            StartSync();
            return m;
        }


        public static RightModel DALDelete(this RightModelJson model, DB db)
        {
            var m = db.Rights.Find(model.Id);
            m.Deleted = true;
            //db.Rights.Remove(m);
            db.SaveChanges();

            StartSync();
            return m;
        }

        #endregion RightModel

        #region RoleModel

        /// <summary>
        /// RoleModel
        /// </summary>
        /// <param name="model"></param>
        /// <param name="db"></param>
        /// <param name="patch"></param>
        /// <returns></returns>
        public static RoleModel DALSave(this RoleModelJson model, DB db, bool patch = false)
        {
            RoleModel m = null;
            if (model.Id == Guid.Empty)
            {
                model.Id = Guid.NewGuid();
                m = new RoleModel();
                db.Roles.Add(m);
                foreach(var item in db.Rights.ToList())
                {
                    db.RightsInRoles.Add(new RightsInRoleModel { 
                        Id = Guid.NewGuid(),
                        RightId = item.Id,
                        RoleId = model.Id,
                        Active = false
                    });
                }
            }
            else
            {
                m = db.Roles.Find(model.Id);
            }
            Copy(model, m, patch, "Parking", "Role", "RightsInRole");
            m.Deleted = false;
            db.SaveChanges();

            StartSync();
            return m;
        }


        public static RoleModel DALDelete(this RoleModelJson model, DB db)
        {
            var m = db.Roles.Find(model.Id);
            m.Deleted = true;
            //db.Roles.Remove(m);
            db.SaveChanges();

            StartSync();
            return m;
        }

        #endregion RoleModel

        #region RoleModel

        /// <summary>
        /// RoleModel
        /// </summary>
        /// <param name="model"></param>
        /// <param name="db"></param>
        /// <param name="patch"></param>
        /// <returns></returns>
        public static RightsInRoleModel DALSave(this RightsInRoleModelJson model, DB db, bool patch = false)
        {
            RightsInRoleModel m = db.RightsInRoles.FirstOrDefault(x => x.RoleId == model.RoleId & x.Right.Id == model.RightId);
            if (m == null)
            {
                model.Id = Guid.NewGuid();
                m = new RightsInRoleModel();
                db.RightsInRoles.Add(m);
            }
            Copy(model, m, patch, "Parking", "Role", "Id");
            m.Deleted = false;
            db.SaveChanges();

            StartSync();
            return m;
        }


        public static RightsInRoleModel DALDelete(this RightsInRoleModelJson model, DB db)
        {
            var m = db.RightsInRoles.Find(model.Id);
            //db.RightsInRoles.Remove(m);
            m.Deleted = true;
            db.SaveChanges();

            StartSync();
            return m;
        }

        #endregion RoleModel
    
    }
}
