﻿using ASE.Data;
using rps.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace rpsbb.Controllers
{
    [Authorize]
    public class ItemsController : ApiController
    {
        DB db = new DB();

        [System.Web.Http.HttpGet]
        public List<NavMapRect> List()
        {
            return db.NavMapRects.ToList();
        }
    }
}
