﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rps.Data
{
    /// <summary>
    /// Карта/Уровень
    /// </summary>
    public class MapModel : BaseModel
    {
        [Key]
        public Guid Id { get; set; }

        //public Guid UserId { get; set; }

        [Display(Name = "Этаж")]
        public int Level { get; set; }

        [Display(Name = "Наименование")]
        public string Name { get; set; }

        [JsonIgnore]
        public byte[] Image { get; set; }

        public int ImageW { get; set; }

        public int ImageH { get; set; }

        public string ImageName { get; set; }

        public Guid? ParkingId { get; set; }

        public virtual ParkingModel Parking { get; set; }

        [JsonIgnore]
        public DateTime? ChangedDateTime { get; set; }

        [JsonIgnore]
        public bool Deleted { get; set; }
    }

    /// <summary>
    /// Прямоугольник на карте
    /// </summary>
    public class NavMapRect : BaseModel
    {
        [Key]
        public Guid Id { get; set; }

        [Display(Name = "Номер")]
        public int Index { get; set; }

        [Display(Name = "Парковка")]
        public Guid NavMapId { get; set; }

        [Display(Name = "Парковка")]
        public virtual MapModel NavMap { get; set; }

        [Display(Name = "Наименование")]
        public string Name { get; set; }

        [Display(Name = "Отображать")]
        public bool Visible { get; set; }

        [Display(Name = "Трансформация")]
        public string Transform { get; set; }

        public int Type { get; set; }

        public long? CurrentReservId { get; set; }

        public virtual NavMapRectReserv CurrentReserv { get; set; }
    }

    public class NavMapRectReserv : BaseModel
    {
        [Key]
        public Guid Id { get; set; }

        public Guid NavMapRectId { get; set; }

        public virtual NavMapRect NavMapRect { get; set; }

        public DateTime DateTime { get; set; }
    }

    /// <summary>
    /// Настройки
    /// </summary>
    public class Setting : BaseModel
    {
        [Key]
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Value { get; set; }
    }

    /*public class SyncItem
    {
        [Key]
        public Guid Id { get; set; }

        public Guid ObjecId { get; set; }

        public int ObjecType { get; set; }

        public int Operation { get; set; }

        public DateTime Created { get; set; }
    }*/
}
