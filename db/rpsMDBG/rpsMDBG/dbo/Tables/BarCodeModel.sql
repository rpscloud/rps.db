﻿CREATE TABLE [dbo].[BarCodeModel] (
    [Id]               UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [OperId]           UNIQUEIDENTIFIER NULL,
    [CompanyId]        UNIQUEIDENTIFIER NULL,
    [IdTP]             TINYINT          NULL,
    [IdTS]             TINYINT          NULL,
    [DiscountAmount]   INT              NULL,
    [GroupIdFC]        TINYINT          NULL,
    [Description]             VARCHAR (MAX)    DEFAULT (NULL) NULL,
    [AnySum]           BIT              NULL,
    [IdTPForCount] UNIQUEIDENTIFIER NULL, 
    [IdTSForCount] UNIQUEIDENTIFIER NULL, 
    [TPForRent] UNIQUEIDENTIFIER NULL, 
    [TSForRent] UNIQUEIDENTIFIER NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [BarCodeModel_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[CompanyModel] ([Id]),
    CONSTRAINT [BarCodeModel_Group] FOREIGN KEY ([GroupIdFC]) REFERENCES [dbo].[Group] ([idFC]),
    CONSTRAINT [BarCodeModel_Tariffs] FOREIGN KEY ([IdTP]) REFERENCES [dbo].[Tariffs] ([IdFC]),
    CONSTRAINT [BarCodeModel_TariffScheduleModel] FOREIGN KEY ([IdTS]) REFERENCES [dbo].[TariffScheduleModel] ([IdFC]),
    CONSTRAINT [BarCodeModel_UserModel] FOREIGN KEY ([OperId]) REFERENCES [dbo].[UserModel] ([Id])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Любая сумма', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeModel', @level2type = N'COLUMN', @level2name = N'AnySum';


GO


EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Группа для BCF=3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeModel', @level2type = N'COLUMN', @level2name = N'GroupIdFC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма для BCF=2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeModel', @level2type = N'COLUMN', @level2name = N'DiscountAmount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТC для BCF=1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeModel', @level2type = N'COLUMN', @level2name = N'IdTS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТП для BCF=1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeModel', @level2type = N'COLUMN', @level2name = N'IdTP';



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Компания - заказчик штрихкода', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeModel', @level2type = N'COLUMN', @level2name = N'CompanyId';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Оператор - заказчик штрихкода', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeModel', @level2type = N'COLUMN', @level2name = N'OperId';

GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТП для расчета задолженности арендатора перед УК, если есть места', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeModel', @level2type = N'COLUMN', @level2name = N'TPForRent';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТС для расчета задолженности арендатора перед УК, если есть места', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeModel', @level2type = N'COLUMN', @level2name = N'TSForRent';

GO