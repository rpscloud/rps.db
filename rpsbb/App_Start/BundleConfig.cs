﻿using System.Web;
using System.Web.Optimization;

namespace rpsbb
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/JS/jquery").Include(
                        "~/MVCScripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/MVCScripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/MVCScripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/JS/bootstrap").Include(
                      "~/MVCScripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/CSS/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }
    }
}
