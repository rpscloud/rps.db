﻿using ASE.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

using ASE.MVC;

using rps.Data;
using rps.SignalR;
using Newtonsoft.Json;
using rpsbb.MVC.Data;

namespace rps.Controllers
{
    [Authorize]
    public class RootController : ControllerAdv<DB>
    {

        // GET: Root
        public ActionResult Index()
        {
            //return File("index.html", "text/html");
            //if (Request.IsAuthenticated)
                //return Redirect("index.html");
                //return Redirect("admin.html");
            //else
                //return Redirect("admin.html");
            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> Sync(Guid id)
        {
            //Synchronization.Start(id);

            return Content("Sync started: ");
        }

        [AllowAnonymous]
        public ActionResult Test(string p1, string p2)
        {
            db.Alarms.FirstOrDefault(); 
            return new EmptyResult();
        }

        [AllowAnonymous]
        public ActionResult SignalR()
        {
            return View();
        }

        public ActionResult InitDB()
        {
            AutoMigrationInit.Configure(db);

            return new EmptyResult();
        }

        [AllowAnonymous]
        public JsonResult SignalRAlarm(int code, int state, Guid deviceId)
        {
            var z = db.Zones.FirstOrDefault();
            if (z == null)
            {
                z = new ZoneModel { Id = Guid.NewGuid(), Name = "Zone 1" };
                db.Zones.Add(z);
            }

            var d = db.Devices.FirstOrDefault(x => x.Id == deviceId);
            if (d == null)
            {
                d = new DeviceModel { Id = Guid.NewGuid(), /* Name = "Device 1", DisplayZoneId = z.Id*/ };
                db.Devices.Add(d);
            }
            var m = new AlarmModel { Id = Guid.NewGuid(), TypeId = 1, Begin = DateTime.Now };
            db.Alarms.Add(m);

            db.SaveChanges();

            var jsonResult = new JsonResult
            {
                JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet,
                Data = m
            };


            var hub = Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<HubGeneral>();
            hub.Clients.All.onAlarm(new JavaScriptSerializer().Serialize(jsonResult.Data));

            return jsonResult;
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult SqlManager(string t, string a, string i)
        {
            var hub = Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<HubGeneral>();

            if (t.ToLower() == "alarms")
                hub.Clients.Group("alarms").onAlarm(JsonConvert.SerializeObject(db.Alarms.Find(int.Parse(i))));

            return new EmptyResult();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult SetSqlGate(string url)
        {
            db.Database.ExecuteSqlCommand(String.Format("ALTER PROCEDURE SPRemoteNotifierL @url NVARCHAR (MAX) AS BEGIN DECLARE @S varchar(max); SET @S = Concat('{0}', @url); EXEC SPRemoteNotifier @S END;", url));

            return RedirectToAction("SignalR");
        }

        [AllowAnonymous]
        public ActionResult CallCenter(bool? user)
        {
            if (!user.HasValue)
                return View("CallCenterOperator");

            return View();
        }

        public ActionResult Settings()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult RemoteNotifier(string t, string a, string i)
        {
            var hub = Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<HubGeneral>();
            hub.Clients.Group("RN_" + t).onRemoteNotifier(t, a, i);

            return new EmptyResult();
        }
    }
}