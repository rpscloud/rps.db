-- noinspection SqlNoDataSourceInspectionForFile

BEGIN TRANSACTION
GO
INSERT INTO [dbo].[ConfigSync] ([CurrentLevel]) VALUES (1);
GO

INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'd592df7d-191b-4946-aab6-f8a246a73477', NULL, N'Права доступа', N'/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'5c40f38c-4a18-4358-9614-5bada683a36f', NULL, N'Центр управления', N'/1/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'b8b4c6a5-f6f8-4353-98c2-55d3a97c4ccc', N'5c40f38c-4a18-4358-9614-5bada683a36f', N'Смена режимов работы стойки', N'/1/1/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'a1e01e25-205e-46c2-b4f6-1ed7f01fb358', N'5c40f38c-4a18-4358-9614-5bada683a36f', N'Проезд одного автомобиля', N'/1/2/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'2d0038b7-1251-4096-9ad4-e3e73ea11c6d', N'5c40f38c-4a18-4358-9614-5bada683a36f', N'Голосовые вызовы (входящие и исходящие)', N'/1/3/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'5c40f38c-4a18-4358-9614-5bada683a36f', N'Доступ к карте в кассе', N'/1/4/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'c0a0fc88-6aac-4b02-9c77-cc81a545f14a', N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'Изменение поля “Группа доступа”', N'/1/4/1/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'32199cee-4237-41e7-97c5-d2b313ba7a61', N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'Изменение поля “Обнуление ВП”', N'/1/4/10/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'e1390883-4fdd-46d2-827e-627ef9511963', N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'Изменение поля “КВП”', N'/1/4/11/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'a6984758-79c8-440f-8717-75eb1485289d', N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'Изменение поля "Обнуление абонемент"', N'/1/4/12/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'1716bbce-60a0-4b06-8de0-f000652bcf52', N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'Изменение поля "Тип клиента"', N'/1/4/13/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'ef7b4a64-b634-4cf6-bde7-903ef0ee136e', N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'Блокировать карту', N'/1/4/14/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'b2c24c2e-26e3-49f8-a3d3-3afee06e01a2', N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'Привязать клиента/компанию', N'/1/4/15/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'64348104-8f67-467c-8f0a-2c022c670a18', N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'Изменение поля “Текущая зона”', N'/1/4/2/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'c5de3802-9863-468d-81f6-ecd4934920df', N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'Изменение поля “Время въезда”', N'/1/4/3/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'62c09b53-d5bb-43f4-8bde-6f663609f359', N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'Изменение поля “Время пересчета”', N'/1/4/4/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'1684f958-c410-4428-b719-58e6439af514', N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'Изменение поля “Тарифный план”', N'/1/4/5/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'17947ca2-8938-4543-a917-daf76e472067', N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'Изменение поля “Тарифная сетка”', N'/1/4/6/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'538dcfac-a30b-47ea-9f4c-6289f5b16a12', N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'Изменение поля “Сумма на карте”', N'/1/4/7/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'6e500bc6-e686-4261-9f85-3343a51113de', N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'Изменение поля “ТВП”', N'/1/4/8/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'82527f6e-89b8-49c3-b697-d683e19c72dc', N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'Изменение поля “ТКВП”', N'/1/4/9/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'a35db5af-dd8b-484e-991a-add7c0e1bfa6', N'5c40f38c-4a18-4358-9614-5bada683a36f', N'Просмотр транзакций по карте', N'/1/5/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'd3cc4973-2168-4579-a83b-27a8af337498', N'5c40f38c-4a18-4358-9614-5bada683a36f', N'Звуковое сопровождение ошибок(Одиночное)', N'/1/6/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'b28c7274-4cb6-479e-bf0e-9d90c5e0c5f6', N'5c40f38c-4a18-4358-9614-5bada683a36f', N'Звуковое сопровождение ошибок(Продолжительное)', N'/1/7/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'd118c04f-28ba-41a1-a522-d979b3f8e0e6', NULL, N'Клиенты', N'/2/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'cc8ecaad-70a0-40d8-8da3-608200764bf1', N'd118c04f-28ba-41a1-a522-d979b3f8e0e6', N'Клиенты', N'/2/1/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'5da59ef5-0310-46ed-911e-a09f8b3e752d', N'cc8ecaad-70a0-40d8-8da3-608200764bf1', N'Добавление и редактирование физ.лиц', N'/2/1/1/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'e457b2c9-e55e-454c-8be0-47ebe0daad63', N'cc8ecaad-70a0-40d8-8da3-608200764bf1', N'Удаление физ.лиц', N'/2/1/2/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'3d517dcd-5d31-4e97-b9cb-f4b8dda1fd0e', N'2ed5d51b-b5dc-45ae-b2c2-726b155f787c', N'Добавление и редактирование юр.лиц', N'/2/1/3/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'8ae03010-dfe2-47ab-b94e-8da27f57b7b4', N'2ed5d51b-b5dc-45ae-b2c2-726b155f787c', N'Удаление юр.лиц', N'/2/1/4/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'a08690f8-1123-4c49-8014-f6d24495ec2d', N'd118c04f-28ba-41a1-a522-d979b3f8e0e6', N'Группы', N'/2/2/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'70c47e52-1f45-4d86-a232-d04b1a7db3d3', N'a08690f8-1123-4c49-8014-f6d24495ec2d', N'Добавление и редактирование', N'/2/2/1/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'184b6893-6b41-4902-863b-08ddec44c5cf', N'a08690f8-1123-4c49-8014-f6d24495ec2d', N'Удаление', N'/2/2/2/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'd118c04f-28ba-41a1-a522-d979b3f8e0e6', N'Доступ к карте', N'/2/3/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'4a851ee1-cf9d-4049-9b8e-3a097d2ba71c', N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'Изменение поля “Группа доступа”', N'/2/3/1/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'3cdc1e28-9598-4dbb-a801-b09e304c2f89', N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'Изменение поля “Обнуление ВП”', N'/2/3/10/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'a1adfd2e-e5f2-484c-9416-5c671d15e645', N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'Изменение поля “КВП”', N'/2/3/11/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'6b132d01-4e4a-4732-a8f2-aa0c7582d8b6', N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'Изменение поля "Обнуление абонемент"', N'/2/3/12/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'8b08d89b-bbe0-4ded-afe4-5ef042773946', N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'Изменение поля "Тип клиента"', N'/2/3/13/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'a9dc4617-db5a-4ae3-b285-a29cb3aa75b2', N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'Блокировать карту', N'/2/3/14/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'dc59c9f0-e5a2-4c9a-adea-5d55e6371f4c', N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'Привязать клиента/компанию', N'/2/3/15/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'6fad3f37-0c45-40b8-8b49-cfcd8cf088f3', N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'Изменение поля “Текущая зона”', N'/2/3/2/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'1e1db7fb-91b7-48cc-add8-7a7b15765765', N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'Изменение поля “Время въезда”', N'/2/3/3/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'ed0c2cca-af8b-4494-8c1a-8bf0b2ec2f05', N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'Изменение поля “Время пересчета”', N'/2/3/4/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'6a065c35-9fbb-47f2-86a8-5c5c51abbf20', N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'Изменение поля “Тарифный план”', N'/2/3/5/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'3aa80a84-2b0d-4641-acda-eba1e2074fd3', N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'Изменение поля “Тарифная сетка”', N'/2/3/6/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'c43ac4b6-e98b-48a2-b66c-8d005ff2a1a0', N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'Изменение поля “Сумма на карте”', N'/2/3/7/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'61bb4395-72c3-417b-a4b8-6635f9ec56a3', N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'Изменение поля “ТВП”', N'/2/3/8/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'01847e21-f107-4414-9ccc-568703ae912a', N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'Изменение поля “ТКВП”', N'/2/3/9/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'0dec53e9-9ca3-4b74-bbec-9dbb086030f1', N'd118c04f-28ba-41a1-a522-d979b3f8e0e6', N'Просмотр транзакций по карте', N'/2/4/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'eb063c9d-e845-4f91-807c-b3eb7cb535af', NULL, N'Отчеты', N'/4/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'33ae0e8e-1008-43aa-91e7-0c8897980fdd', N'eb063c9d-e845-4f91-807c-b3eb7cb535af', N'Табличные отчеты', N'/4/1/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'8ef22010-03db-48cc-bd5b-4e58438f00f0', N'eb063c9d-e845-4f91-807c-b3eb7cb535af', N'Создание и редактирование отчетов', N'/4/1/1/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'e48cb7b1-3beb-4b37-a7f8-63d9dd7a760b', N'eb063c9d-e845-4f91-807c-b3eb7cb535af', N'Удаление отчетов', N'/4/1/2/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'46b6eeb7-36fa-4559-99cf-75d7413412ee', N'eb063c9d-e845-4f91-807c-b3eb7cb535af', N'Графические отчеты', N'/4/2/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'554f6a1a-356a-4f63-a7a8-1d4836be5a9f', NULL, N'Настройки', N'/6/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'edf44989-1ab5-485e-a89a-cc11f157302f', N'554f6a1a-356a-4f63-a7a8-1d4836be5a9f', N'Устройства', N'/6/1/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'801259bf-8b55-4f1e-b8d1-91fe0c0d10f1', N'edf44989-1ab5-485e-a89a-cc11f157302f', N'Редактирование настроек устройств', N'/6/1/1/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'64cef616-ad5f-48d9-b795-55a052241b76', N'edf44989-1ab5-485e-a89a-cc11f157302f', N'Подключение новых устройтсв', N'/6/1/2/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'f94a2ca6-5670-4ff5-b397-f827b45896b4', N'554f6a1a-356a-4f63-a7a8-1d4836be5a9f', N'Добавление и Редактирование зон', N'/6/1/3/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'306ea4e5-2981-49f6-882f-d85d3ae5153a', N'554f6a1a-356a-4f63-a7a8-1d4836be5a9f', N'Удаление зон', N'/6/1/4/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'4efa28f8-eb7a-4428-8304-85b8e4650c20', N'554f6a1a-356a-4f63-a7a8-1d4836be5a9f', N'Тарифы', N'/6/3/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'd2904707-2d72-4e2f-bcf1-915a265c5679', N'4efa28f8-eb7a-4428-8304-85b8e4650c20', N'Добавление и редактирование Тарифов', N'/6/3/1/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'06817674-af48-475b-b161-fbe506420486', N'4efa28f8-eb7a-4428-8304-85b8e4650c20', N'Удаление тарифов', N'/6/3/2/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'fbf975c0-b3e7-4fc5-8414-93acc36a3d19', N'4efa28f8-eb7a-4428-8304-85b8e4650c20', N'Добавление и редактирование Тарифных планов', N'/6/3/3/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'1a12f560-7956-4dd2-b41f-02e0360f1ff4', N'4efa28f8-eb7a-4428-8304-85b8e4650c20', N'Удаление тарифных планов', N'/6/3/4/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'947baae5-e1d8-417a-aa88-af5df3143264', N'4efa28f8-eb7a-4428-8304-85b8e4650c20', N'Добавление и редактирование Тарифных сеток', N'/6/3/5/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'6484ae97-bd2b-4dca-bba5-5dfc1c1b5ce2', N'4efa28f8-eb7a-4428-8304-85b8e4650c20', N'Удаление тарифных сеток', N'/6/3/6/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'92142318-d3a2-4dbc-890d-e1460eb41013', N'554f6a1a-356a-4f63-a7a8-1d4836be5a9f', N'Пользователи', N'/6/5/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'8db1a1d2-cf3a-4822-a774-47c8dcef2ff8', N'92142318-d3a2-4dbc-890d-e1460eb41013', N'Добавление и редактирование пользователей', N'/6/5/1/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'9d8aa0d4-5db4-4162-a295-da955ff58831', N'92142318-d3a2-4dbc-890d-e1460eb41013', N'Удаление пользователей', N'/6/5/2/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'fe1d153e-af06-4962-a6f2-d5226d81f536', N'92142318-d3a2-4dbc-890d-e1460eb41013', N'Добавление и редактирование ролей', N'/6/5/3/')
INSERT INTO [dbo].[RightModel] ([Id], [ParentId], [Name], [OrgNode]) VALUES (N'060de55f-30bc-4f0b-9810-cc56ca9e480d', N'92142318-d3a2-4dbc-890d-e1460eb41013', N'Удаление Ролей', N'/6/5/4/')
GO

INSERT INTO dbo.ParkingModel (Id, [Name], SULocal, SURemote, SUPort, SUUser, SUPassword, S3Site, S3Sql, S3User, S3Password, S3Db, S2Sql, S2User, S2Password, S2Db, Longitude, Latitude) VALUES ('{9DB9D1CE-05A3-4329-B64A-3DA3B33A44AD}', N'НОВАЯ РОЛЬ', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO dbo.ParkingModel (Id, [Name], SULocal, SURemote, SUPort, SUUser, SUPassword, S3Site, S3Sql, S3User, S3Password, S3Db, S2Sql, S2User, S2Password, S2Db, Longitude, Latitude) VALUES ('{1FAA3592-ADF9-444B-B0C1-73ED34E21C12}', N'Новый обьект', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO dbo.ParkingModel (Id, [Name], SULocal, SURemote, SUPort, SUUser, SUPassword, S3Site, S3Sql, S3User, S3Password, S3Db, S2Sql, S2User, S2Password, S2Db, Longitude, Latitude) VALUES ('{3F9A3509-D263-4BEE-8A6B-A2323CE170D5}', N'НОВАЯ РОЛЬ', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO dbo.ParkingModel (Id, [Name], SULocal, SURemote, SUPort, SUUser, SUPassword, S3Site, S3Sql, S3User, S3Password, S3Db, S2Sql, S2User, S2Password, S2Db, Longitude, Latitude) VALUES ('{5A21CD59-4900-4935-981D-E168D7D5822C}', N'Новый обьект', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO dbo.ParkingModel (Id, [Name], SULocal, SURemote, SUPort, SUUser, SUPassword, S3Site, S3Sql, S3User, S3Password, S3Db, S2Sql, S2User, S2Password, S2Db, Longitude, Latitude) VALUES ('{8C052CD7-861D-4E1E-94BD-F858B661DCD0}', N'Новый обьект', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
GO

INSERT INTO [dbo].[RoleModel] ([Id], [Name], [ParkingId]) VALUES (N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', N'Новая роль admin', N'9db9d1ce-05a3-4329-b64a-3da3b33a44ad')
GO

INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'f44e1331-585a-4294-9197-00f5fc138b69', N'306ea4e5-2981-49f6-882f-d85d3ae5153a', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'c7d75d03-fd74-47a9-a3ec-08b3a07e4c08', N'c5de3802-9863-468d-81f6-ecd4934920df', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'bc50e836-2247-4b90-8b1f-09dd1fa59b67', N'2d0038b7-1251-4096-9ad4-e3e73ea11c6d', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'a71c89bb-b2a2-45bb-ad82-0dc45541cc83', N'6a065c35-9fbb-47f2-86a8-5c5c51abbf20', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'590485c2-795a-43c5-95cb-1233a7cd59cd', N'0dec53e9-9ca3-4b74-bbec-9dbb086030f1', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'5789ca22-eaef-48ae-ae30-14865bc88be7', N'46b6eeb7-36fa-4559-99cf-75d7413412ee', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'6aae32e4-a2ab-42a6-8bb9-1515b7ea2b69', N'e457b2c9-e55e-454c-8be0-47ebe0daad63', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'f5ff70a8-d18a-40c6-92ee-22704b44a25d', N'8ef22010-03db-48cc-bd5b-4e58438f00f0', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'2ec4a50a-5547-4e67-b9d3-2671b1fa9656', N'4efa28f8-eb7a-4428-8304-85b8e4650c20', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'ae79c75a-9be8-48cb-953c-29ce821ac759', N'92142318-d3a2-4dbc-890d-e1460eb41013', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'd472bcbd-9b9b-4fcd-86e9-29fdfb1e63ae', N'3aa80a84-2b0d-4641-acda-eba1e2074fd3', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'd87ec51b-95ed-4c0d-a574-2d58125a91f3', N'6fad3f37-0c45-40b8-8b49-cfcd8cf088f3', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'4b42e84f-37b3-471d-8d89-2de509a462e5', N'82527f6e-89b8-49c3-b697-d683e19c72dc', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'f3b23343-4628-4e29-8cae-2e9271cc3dcb', N'64348104-8f67-467c-8f0a-2c022c670a18', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'b4edd1c0-c66c-4ab5-96da-36ca48983a18', N'33ae0e8e-1008-43aa-91e7-0c8897980fdd', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'dc1c28c8-e18a-453b-9f28-3c871685e995', N'f94a2ca6-5670-4ff5-b397-f827b45896b4', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'e0f43396-8bcf-49e3-8f07-3cdeb228a1f6', N'4a851ee1-cf9d-4049-9b8e-3a097d2ba71c', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'50bd5433-3e66-40e3-922b-3f82a2b6004c', N'1684f958-c410-4428-b719-58e6439af514', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'9d86e47c-cff6-4a82-b478-4175baf4776e', N'a1adfd2e-e5f2-484c-9416-5c671d15e645', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'fc711af0-f543-40ff-b505-46567a066b76', N'8b08d89b-bbe0-4ded-afe4-5ef042773946', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'b6bfebce-9d76-42ba-b003-496fcb77e52a', N'c0a0fc88-6aac-4b02-9c77-cc81a545f14a', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'7cc48ad5-868d-4222-a7f2-4f985edef1d5', N'a08690f8-1123-4c49-8014-f6d24495ec2d', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'95e5dd07-906e-4ad5-9f0f-542a7297a1b2', N'3cdc1e28-9598-4dbb-a801-b09e304c2f89', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'246aad58-a0fc-4f56-8740-54a9ed7815c1', N'dc59c9f0-e5a2-4c9a-adea-5d55e6371f4c', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'acf31ad1-268c-4275-bd2a-556cae7b3728', N'538dcfac-a30b-47ea-9f4c-6289f5b16a12', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'c1f774e4-26b8-4ab2-b729-58044145177b', N'32199cee-4237-41e7-97c5-d2b313ba7a61', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'65ddbcc8-9147-4139-abe3-5c1095e18fde', N'62c09b53-d5bb-43f4-8bde-6f663609f359', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'8d158125-1add-476f-af66-6017fb64110f', N'c06c7bba-3524-41ff-a4c1-50acdd28fbcc', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'5d3b2459-01c8-4412-924d-6179aa955f59', N'17947ca2-8938-4543-a917-daf76e472067', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'71237b2f-7e4a-4ac9-8608-61cc725b3a9a', N'9d8aa0d4-5db4-4162-a295-da955ff58831', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'419591d0-4c37-4ce1-af91-627b7a5be904', N'6b132d01-4e4a-4732-a8f2-aa0c7582d8b6', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'ca4941f1-a4c8-4e62-ad27-6f6e31e21e37', N'6e500bc6-e686-4261-9f85-3343a51113de', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'cd47a936-bcfa-4337-988a-72cee93a3c1d', N'06817674-af48-475b-b161-fbe506420486', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'8c52ab62-727e-404e-bd05-72f440d3bcc3', N'947baae5-e1d8-417a-aa88-af5df3143264', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'679edf3e-13a0-46f9-8174-79137da9f8f1', N'8ae03010-dfe2-47ab-b94e-8da27f57b7b4', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'048b2709-7ac6-496f-a26e-7c75e5fd59e4', N'e48cb7b1-3beb-4b37-a7f8-63d9dd7a760b', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'18988ef2-5eb7-49f5-a8fa-82756ad4268a', N'eb063c9d-e845-4f91-807c-b3eb7cb535af', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'cdecc5b1-dacf-4b1b-870b-87e5d5a17e9e', N'cc8ecaad-70a0-40d8-8da3-608200764bf1', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'ca0007ff-4f78-4661-99ce-87fa8ef94346', N'3d517dcd-5d31-4e97-b9cb-f4b8dda1fd0e', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'2e631a7a-eff6-4266-9d7a-8c88d6f6ebb8', N'5da59ef5-0310-46ed-911e-a09f8b3e752d', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'676b37f8-e219-42e4-a477-92625ce73525', N'ef7b4a64-b634-4cf6-bde7-903ef0ee136e', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'fb19aedc-da8d-4dda-88a0-968c8c4bb26c', N'1716bbce-60a0-4b06-8de0-f000652bcf52', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'5464ccb7-6cf8-4fd9-bea8-9b986424ccb5', N'ed0c2cca-af8b-4494-8c1a-8bf0b2ec2f05', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'f7c935af-d363-46b6-8a60-9ec491546189', N'64cef616-ad5f-48d9-b795-55a052241b76', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'c75a83b8-65b5-4c31-8a03-a029e8595cf7', N'd118c04f-28ba-41a1-a522-d979b3f8e0e6', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'83a15a5d-72b6-48f7-b726-a0f83718fd4c', N'edf44989-1ab5-485e-a89a-cc11f157302f', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'76f73566-34c0-4fa9-9114-a3582b173d50', N'a9dc4617-db5a-4ae3-b285-a29cb3aa75b2', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'd61fc358-48a8-46cf-8f37-a447aad8036d', N'b2c24c2e-26e3-49f8-a3d3-3afee06e01a2', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'b2eb9ae2-540d-4eb8-8c61-aa82bed3b158', N'd592df7d-191b-4946-aab6-f8a246a73477', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'bb81b3b5-55a5-48cb-b940-ad6238924de4', N'fe1d153e-af06-4962-a6f2-d5226d81f536', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'908f9158-587f-4d20-aa93-b02bba34e970', N'e1390883-4fdd-46d2-827e-627ef9511963', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'92d1b5fe-eefa-4823-aa2e-b0ad7fe38ae9', N'b8b4c6a5-f6f8-4353-98c2-55d3a97c4ccc', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'eacc5264-bc5d-4a18-883e-b131ae64be8a', N'1a12f560-7956-4dd2-b41f-02e0360f1ff4', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'bc8d8dd6-e63f-48de-b38a-bcf7fb946b14', N'a35db5af-dd8b-484e-991a-add7c0e1bfa6', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'6b6caeb2-5cf7-48e2-b0db-c25b03b554bc', N'd2904707-2d72-4e2f-bcf1-915a265c5679', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'0da9bcd1-33eb-458a-828f-c3113d79a680', N'fbf975c0-b3e7-4fc5-8414-93acc36a3d19', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'b223aeed-f113-42f1-9978-c83cbcac60b0', N'060de55f-30bc-4f0b-9810-cc56ca9e480d', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'8ab83862-a31a-42a5-b557-dc0ebf3f6aed', N'381dced9-7ce1-42f3-b2b6-7371231a8185', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'e6626df8-382b-4756-ade4-df8a4a47b301', N'801259bf-8b55-4f1e-b8d1-91fe0c0d10f1', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'c38d8156-37cf-4aac-b5a2-e35d7129c807', N'a1e01e25-205e-46c2-b4f6-1ed7f01fb358', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'2e86e495-2296-4225-abb0-ed17f6b3c865', N'554f6a1a-356a-4f63-a7a8-1d4836be5a9f', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'2b39854f-fdff-4677-962b-eef936680cf7', N'8db1a1d2-cf3a-4822-a774-47c8dcef2ff8', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'16eb5ea1-af54-495f-8868-f09f1bdcc7b1', N'01847e21-f107-4414-9ccc-568703ae912a', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'c136c5f9-d917-4dc5-83a9-f28edeb7fc19', N'184b6893-6b41-4902-863b-08ddec44c5cf', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'72b009f1-52e3-4aa8-8130-f44f1cc688db', N'61bb4395-72c3-417b-a4b8-6635f9ec56a3', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'e047844e-85b7-468b-bfca-f45b477d7479', N'5c40f38c-4a18-4358-9614-5bada683a36f', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'7e0bfb39-d3e4-406a-9a44-fc4b14c1c13c', N'a6984758-79c8-440f-8717-75eb1485289d', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'285dac42-3ac4-4602-a52a-fdedc172e3c5', N'70c47e52-1f45-4d86-a232-d04b1a7db3d3', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'9fde46ff-cfde-4bf2-b428-fe2e842658fd', N'6484ae97-bd2b-4dca-bba5-5dfc1c1b5ce2', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'4ff0298f-edfd-4561-9ed9-fe7165f635df', N'c43ac4b6-e98b-48a2-b66c-8d005ff2a1a0', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'72549877-d122-482e-ab88-ff288e041f7b', N'1e1db7fb-91b7-48cc-add8-7a7b15765765', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)

INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'5ac1e9fd-4c00-4faf-a217-29e558ac7cdd', N'b28c7274-4cb6-479e-bf0e-9d90c5e0c5f6', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)
INSERT INTO [dbo].[RightsInRoleModel] ([Id], [RightId], [RoleId], [Active]) VALUES (N'335807e0-2b86-4626-8aaa-f5ac6fc5328e', N'd3cc4973-2168-4579-a83b-27a8af337498', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a', 1)

GO

-- Data from table "dbo.AlarmColor"
INSERT INTO dbo.AlarmColor ([Name], Id) VALUES  (N'Устройство работает', 0);
INSERT INTO dbo.AlarmColor ([Name], Id) VALUES  (N'Критично, устройство не работает', 1);
INSERT INTO dbo.AlarmColor ([Name], Id) VALUES  (N'Устройство работает с ограниченным функционалом', 2);
INSERT INTO dbo.AlarmColor ([Name], Id) VALUES(N'Устройство скоро перейдет на тревогу 1ого или 2ого уровня', 3);
INSERT INTO dbo.AlarmColor ([Name], Id) VALUES(N'Нет связи.', 4);
INSERT INTO dbo.AlarmColor ([Name], Id) VALUES(N'Системное предупреждение.', 5);
INSERT INTO dbo.AlarmColor ([Name], Id) VALUES(N'Системная ошибка.', 6);
GO

-- Data from table "dbo.AuthAssignment"

-- Data from table "dbo.AuthFields"
SET IDENTITY_INSERT dbo.AuthFields ON
GO

INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES (5, N'Cards', NULL, N'ClientGroupidFC', CONVERT(datetime, '2016-02-14 23:01:17', 120), '{C0A0FC88-6AAC-4B02-9C77-CC81A545F14A}', N'rs');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (6, N'Cards', NULL, N'ZoneidFC', CONVERT(datetime, '2016-02-14 23:01:17', 120), '{64348104-8F67-467C-8F0A-2C022C670A18}', N'rs');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (7, N'Cards', NULL, N'ParkingEnterTime', CONVERT(datetime, '2016-02-14 23:01:17', 120), '{C5DE3802-9863-468D-81F6-ECD4934920DF}', N'rs');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (8, N'Cards', NULL, N'LastRecountTime', CONVERT(datetime, '2016-02-14 23:01:17', 120), '{62C09B53-D5BB-43F4-8BDE-6F663609F359}', N'rs');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (9, N'Cards', NULL, N'TPidFC', CONVERT(datetime, '2016-02-14 23:01:17', 120), '{1684F958-C410-4428-B719-58E6439AF514}', N'rs');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (10, N'Cards', NULL, N'TSidFC', CONVERT(datetime, '2016-02-14 23:01:17', 120), '{17947CA2-8938-4543-A917-DAF76E472067}', N'rs');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (11, N'Cards', NULL, N'SumOnCard', CONVERT(datetime, '2016-02-14 23:01:17', 120), '{538DCFAC-A30B-47EA-9F4C-6289F5B16A12}', N'rs');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (12, N'Cards', NULL, N'TVP', CONVERT(datetime, '2016-02-14 23:01:17', 120), '{6E500BC6-E686-4261-9F85-3343A51113DE}', N'rs');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (13, N'Cards', NULL, N'TKVP', CONVERT(datetime, '2016-02-14 23:01:17', 120), '{82527F6E-89B8-49C3-B697-D683E19C72DC}', N'rs');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (14, N'Cards', NULL, N'Nulltime1', CONVERT(datetime, '2016-02-14 23:01:17', 120), '{32199CEE-4237-41E7-97C5-D2B313BA7A61}', N'rs');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (15, N'Cards', NULL, N'Nulltime2', CONVERT(datetime, '2016-02-14 23:01:17', 120), '{E1390883-4FDD-46D2-827E-627EF9511963}', N'rs');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (16, N'Cards', NULL, N'Nulltime3', CONVERT(datetime, '2016-02-14 23:01:17', 120), '{A6984758-79C8-440F-8717-75EB1485289D}', N'rs');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (17, N'Cards', NULL, N'ClientTypidFC', CONVERT(datetime, '2016-02-14 23:01:17', 120), '{1716BBCE-60A0-4B06-8DE0-F000652BCF52}', N'rs');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (18, N'Cards', NULL, N'ClientId,CompanyId', NULL, '{DC59C9F0-E5A2-4C9A-ADEA-5D55E6371F4C}', N'clients');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (19, N'Cards', NULL, N'ClientId,CompanyId', NULL, '{B2C24C2E-26E3-49F8-A3D3-3AFEE06E01A2}', N'rs');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (20, N'Cards', NULL, N'Blocked', NULL, '{EF7B4A64-B634-4CF6-BDE7-903EF0EE136E}', N'rs');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (21, N'Cards', NULL, N'Blocked', NULL, '{A9DC4617-DB5A-4AE3-B285-A29CB3AA75B2}', N'clients');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (22, N'Cards', NULL, N'ClientGroupidFC', CONVERT(datetime, '2016-02-15 05:33:32', 120), '{4A851EE1-CF9D-4049-9B8E-3A097D2BA71C}', N'clients');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (23, N'Cards', NULL, N'ZoneidFC', CONVERT(datetime, '2016-02-15 05:33:32', 120), '{6FAD3F37-0C45-40B8-8B49-CFCD8CF088F3}', N'clients');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (24, N'Cards', NULL, N'ParkingEnterTime', CONVERT(datetime, '2016-02-15 05:33:32', 120), '{1E1DB7FB-91B7-48CC-ADD8-7A7B15765765}', N'clients');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (25, N'Cards', NULL, N'LastRecountTime', CONVERT(datetime, '2016-02-15 05:33:32', 120), '{ED0C2CCA-AF8B-4494-8C1A-8BF0B2EC2F05}', N'clients');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (26, N'Cards', NULL, N'TPidFC', CONVERT(datetime, '2016-02-15 05:33:32', 120), '{6A065C35-9FBB-47F2-86A8-5C5C51ABBF20}', N'clients');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (27, N'Cards', NULL, N'TSidFC', CONVERT(datetime, '2016-02-15 05:33:32', 120), '{3AA80A84-2B0D-4641-ACDA-EBA1E2074FD3}', N'clients');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (28, N'Cards', NULL, N'SumOnCard', CONVERT(datetime, '2016-02-15 05:33:32', 120), '{C43AC4B6-E98B-48A2-B66C-8D005FF2A1A0}', N'clients');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (29, N'Cards', NULL, N'TVP', CONVERT(datetime, '2016-02-15 05:33:32', 120), '{61BB4395-72C3-417B-A4B8-6635F9EC56A3}', N'clients');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (30, N'Cards', NULL, N'TKVP', CONVERT(datetime, '2016-02-15 05:33:32', 120), '{01847E21-F107-4414-9CCC-568703AE912A}', N'clients');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (31, N'Cards', NULL, N'Nulltime1', CONVERT(datetime, '2016-02-15 05:33:32', 120), '{3CDC1E28-9598-4DBB-A801-B09E304C2F89}', N'clients');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES  (32, N'Cards', NULL, N'Nulltime2', CONVERT(datetime, '2016-02-15 05:33:32', 120), '{A1ADFD2E-E5F2-484C-9416-5C671D15E645}', N'clients');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES (33, N'Cards', NULL, N'Nulltime3', CONVERT(datetime, '2016-02-15 05:33:32', 120), '{6B132D01-4E4A-4732-A8F2-AA0C7582D8B6}', N'clients');
INSERT INTO dbo.AuthFields (Id, ObjectClass, CanReadFields, CanWriteFields, Updated, RightId, Module) VALUES (34, N'Cards', NULL, N'ClientTypidFC', CONVERT(datetime, '2016-02-15 05:33:32', 120), '{8B08D89B-BBE0-4DED-AFE4-5EF042773946}', N'clients');

SET IDENTITY_INSERT dbo.AuthFields OFF
GO



-- Data from table "dbo.AuthItem"
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('.rest/default.list', 0, N'.rest/default.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('.rest/default.update', 0, N'.rest/default.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('465', 0, N'sdfh111', N'D592DF7D-191B-4946-AAB6-F8A246A73477', N's:36:"D592DF7D-191B-4946-AAB6-F8A246A73477";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('admin.*', 0, N'admin.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('admin.userModel.update', 0, N'admin.userModel.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('authuser.userModel.update', 0, N'authuser.userModel.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.*', 0, N'clients.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.barCodeModel.create', 0, N'Клиенты-->Клиенты-->Добавление и редактирование юр.лиц', NULL, N's:36:"3D517DCD-5D31-4E97-B9CB-F4B8DDA1FD0E";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.barCodeModel.delete', 0, N'Клиенты-->Клиенты-->Добавление и редактирование юр.лиц', NULL, N's:36:"3D517DCD-5D31-4E97-B9CB-F4B8DDA1FD0E";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.barCodeModel.update', 0, N'Клиенты-->Клиенты-->Добавление и редактирование юр.лиц', NULL, N's:36:"3D517DCD-5D31-4E97-B9CB-F4B8DDA1FD0E";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.cards.*', 0, N'clients.cards.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.cards.list', 0, N'clients.cards.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.cards.update', 0, N'Клиенты-->Клиенты-->Добавление и редактирование физ.лиц', NULL, N's:36:"5DA59EF5-0310-46ED-911E-A09F8B3E752D";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.carmodels.list', 0, N'clients.carmodels.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.clientcars.create', 0, N'Клиенты-->Клиенты-->Добавление и редактирование физ.лиц', NULL, N's:36:"5DA59EF5-0310-46ED-911E-A09F8B3E752D";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.clientCars.delete', 0, N'Клиенты-->Клиенты-->Добавление и редактирование юр.лиц', NULL, N's:36:"3D517DCD-5D31-4E97-B9CB-F4B8DDA1FD0E";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.clientcars.update', 0, N'Клиенты>Редактирование Компаний>Редактирование', NULL, N's:36:"3D517DCD-5D31-4E97-B9CB-F4B8DDA1FD0E";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.clientCompany.create', 0, N'Клиенты-->Клиенты-->Добавление и редактирование юр.лиц', NULL, N's:36:"3D517DCD-5D31-4E97-B9CB-F4B8DDA1FD0E";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.clientCompany.delete', 0, N'Клиенты-->Клиенты-->Добавление и редактирование юр.лиц', NULL, N's:36:"3D517DCD-5D31-4E97-B9CB-F4B8DDA1FD0E";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.clientModel.create', 0, N'Клиенты-->Клиенты-->Добавление и редактирование физ.лиц', NULL, N's:36:"5DA59EF5-0310-46ED-911E-A09F8B3E752D";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.clientModel.delete', 0, N'Клиенты-->Клиенты-->Удаление физ.лиц', NULL, N's:36:"E457B2C9-E55E-454C-8BE0-47EBE0DAAD63";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.clientmodel.list', 0, N'Клиенты>Редактирование Клиентов', NULL, N's:36:"CC8ECAAD-70A0-40D8-8DA3-608200764BF1";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.clientModel.update', 0, N'Клиенты-->Клиенты-->Добавление и редактирование физ.лиц', NULL, N's:36:"5DA59EF5-0310-46ED-911E-A09F8B3E752D";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.clientModel.view', 0, N'clients.clientModel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.companymodel.*', 0, N'Клиенты>Редактирование Компаний', NULL, N's:36:"2ED5D51B-B5DC-45AE-B2C2-726B155F787C";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.companyModel.create', 0, N'Клиенты-->Клиенты-->Добавление и редактирование юр.лиц', NULL, N's:36:"3D517DCD-5D31-4E97-B9CB-F4B8DDA1FD0E";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.companyModel.delete', 0, N'Клиенты-->Клиенты-->Удаление юр.лиц', NULL, N's:36:"8AE03010-DFE2-47AB-B94E-8DA27F57B7B4";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.companymodel.list', 0, N'Клиенты-->Клиенты', NULL, N's:36:"CC8ECAAD-70A0-40D8-8DA3-608200764BF1";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.companyModel.update', 0, N'Клиенты-->Клиенты-->Добавление и редактирование юр.лиц', NULL, N's:36:"3D517DCD-5D31-4E97-B9CB-F4B8DDA1FD0E";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.companyModel.view', 0, N'clients.companyModel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.deviceModel.update', 0, N'clients.deviceModel.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.deviceModel.view', 0, N'clients.deviceModel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.group.*', 0, N'clients.group.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.group.create', 0, N'clients.group.create', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.group.delete', 0, N'Клиенты-->Группы-->Удаление', NULL, N's:36:"184B6893-6B41-4902-863B-08DDEC44C5CF";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.group.list', 0, N'Клиенты>Редактирование Групп.', NULL, N's:36:"A08690F8-1123-4C49-8014-F6D24495EC2D";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.group.update', 0, N'Клиенты>Редактирование Групп.>Редактирование', NULL, N's:36:"70C47E52-1F45-4D86-A232-D04B1A7DB3D3";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.group.view', 0, N'clients.group.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.tariffs.list', 0, N'clients.tariffs.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.tariffs.update', 0, N'clients.tariffs.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.tariffschedulemodel.list', 0, N'clients.tariffschedulemodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.tariffScheduleModel.update', 0, N'clients.tariffScheduleModel.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.transactions.list', 0, N'clients.transactions.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.usermodel.view', 0, N'clients.usermodel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.zonegroup.create', 0, N'Клиенты>Редактирование Групп.>Редактирование', NULL, N's:36:"70C47E52-1F45-4D86-A232-D04B1A7DB3D3";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES    ('clients.zoneGroup.delete', 0, N'Клиенты-->Группы-->Добавление и редактирование', NULL, N's:36:"70C47E52-1F45-4D86-A232-D04B1A7DB3D3";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('clients.zoneGroup.update', 0, N'Клиенты>Редактирование Групп.>Редактирование', NULL, N's:36:"70C47E52-1F45-4D86-A232-D04B1A7DB3D3";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('clients.zonemodel.list', 0, N'Клиенты>Редактирование Групп.', NULL, N's:36:"A08690F8-1123-4C49-8014-F6D24495EC2D";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rest/default.*', 0, N'rest/default.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rest/default.create', 0, N'rest/default.create', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rest/default.delete', 0, N'rest/default.delete', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rest/default.list', 0, N'rest/default.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rest/default.update', 0, N'rest/default.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rest/organizationmodel.*', 0, N'rest/organizationmodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rest/organizationmodel.list', 0, N'rest/organizationmodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rest/organizationparkingmodel.*', 0, N'rest/organizationparkingmodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rest/parkingmodel.*', 0, N'rest/parkingmodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rest/parkingmodel.view', 0, N'rest/parkingmodel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rest/rolemodel.*', 0, N'rest/rolemodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rest/usermodel.*', 0, N'rest/usermodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rest/usermodel.list', 0, N'rest/usermodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rest/usermodel.view', 0, N'rest/usermodel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rest/userparkingrolemodel.*', 0, N'rest/userparkingrolemodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rs.*', 0, N'rs.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rs.actionJournal.create', 0, N'rs.actionJournal.create', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rs.alarmmodel.*', 0, N'rs.alarmmodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rs.alarmmodel.list', 0, N'rs.alarmmodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rs.cards.list', 0, N'rs.cards.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rs.cards.update', 0, N'rs.cards.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rs.devicemodel.view', 0, N'rs.devicemodel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rs.mapmodel.*', 0, N'rs.mapmodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rs.mapmodel.list', 0, N'rs.mapmodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rs.usermodel.*', 0, N'rs.usermodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rs.userModel.update', 0, N'rs.userModel.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rs.usermodel.view', 0, N'rs.usermodel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rs.zonemodel.*', 0, N'rs.zonemodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rs.zonemodel.list', 0, N'rs.zonemodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('rs.zoneModel.update', 0, N'rs.zoneModel.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('schema.*', 0, N'schema.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('schema.collection', 0, N'schema.collection', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('schema.model', 0, N'schema.model', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.*', 0, N'settings.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.attachdevice.index', 0, N'Настройки-->Устройства-->Подключение новых устройтсв', NULL, N's:36:"64CEF616-AD5F-48D9-B795-55A052241B76";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.authuser.update', 0, N'settings.authuser.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.banknotemodel.list', 0, N'settings.banknotemodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.banknoteModel.update', 0, N'settings.banknoteModel.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.coinmodel.list', 0, N'settings.coinmodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.coinModel.update', 0, N'settings.coinModel.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.comportmodel.list', 0, N'settings.comportmodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.comPortModel.update', 0, N'settings.comPortModel.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.devicecmmodel.create', 0, N'Настройки>Устройства>Подключение новых устройтсв', N'64CEF616-AD5F-48D9-B795-55A052241B76', N's:36:"64CEF616-AD5F-48D9-B795-55A052241B76";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.devicecmmodel.update', 0, N'Настройки-->Устройства-->Редактирование настроек устройств', NULL, N's:36:"801259BF-8B55-4F1E-B8D1-91FE0C0D10F1";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.devicecmmodel.view', 0, N'settings.devicecmmodel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.devicemapmodel.list', 0, N'settings.devicemapmodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.devicemodel.*', 0, N'settings.devicemodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.devicemodel.create', 0, N'Настройки>Устройства>Подключение новых устройтсв', N'64CEF616-AD5F-48D9-B795-55A052241B76', N's:36:"64CEF616-AD5F-48D9-B795-55A052241B76";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.devicemodel.update', 0, N'Настройки-->Устройства-->Редактирование настроек устройств', NULL, N's:36:"801259BF-8B55-4F1E-B8D1-91FE0C0D10F1";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.devicemodel.view', 0, N'settings.devicemodel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.devicerackmodel.create', 0, N'Настройки>Устройства>Подключение новых устройтсв', N'64CEF616-AD5F-48D9-B795-55A052241B76', N's:36:"64CEF616-AD5F-48D9-B795-55A052241B76";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.devicerackmodel.update', 0, N'Настройки-->Устройства-->Редактирование настроек устройств', NULL, N's:36:"801259BF-8B55-4F1E-B8D1-91FE0C0D10F1";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.devicerackmodel.view', 0, N'settings.devicerackmodel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.executabledevice.list', 0, N'settings.executabledevice.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.executableDevice.update', 0, N'settings.executableDevice.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.executabledevicetypemodel.list', 0, N'settings.executabledevicetypemodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.group.list', 0, N'settings.group.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.mapmodel.list', 0, N'settings.mapmodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.rackAltimetrModel.create', 0, N'settings.rackAltimetrModel.create', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.rackaltimetrmodel.delete', 0, N'Настройки-->Устройства-->Редактирование настроек устройств', NULL, N's:36:"801259BF-8B55-4F1E-B8D1-91FE0C0D10F1";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.rackaltimetrmodel.list', 0, N'settings.rackaltimetrmodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.rackaltimetrmodel.update', 0, N'Настройки-->Устройства-->Редактирование настроек устройств', NULL, N's:36:"801259BF-8B55-4F1E-B8D1-91FE0C0D10F1";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.rackaltimetrmodemodel.list', 0, N'settings.rackaltimetrmodemodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.rackworkingmode.list', 0, N'settings.rackworkingmode.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.rightsinrolemodel.update', 0, N'Настройки-->Пользователи-->Добавление и редактирование ролей', NULL, N's:36:"FE1D153E-AF06-4962-A6F2-D5226D81F536";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.rolemodel.create', 0, N'Настройки-->Пользователи-->Добавление и редактирование ролей', NULL, N's:36:"FE1D153E-AF06-4962-A6F2-D5226D81F536";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.rolemodel.delete', 0, N'Настройки-->Пользователи-->Удаление Ролей', NULL, N's:36:"060DE55F-30BC-4F0B-9810-CC56CA9E480D";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.rolemodel.list', 0, N'Настройки-->Пользователи', NULL, N's:36:"92142318-D3A2-4DBC-890D-E1460EB41013";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.rolemodel.update', 0, N'Настройки-->Пользователи-->Добавление и редактирование ролей', NULL, N's:36:"FE1D153E-AF06-4962-A6F2-D5226D81F536";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffintervalmodel.create', 0, N'settings.tariffintervalmodel.create', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffintervalmodel.delete', 0, N'Настройки-->Тарифы-->Добавление и редактирование Тарифных планов', N'FBF975C0-B3E7-4FC5-8414-93ACC36A3D19', N's:36:"FBF975C0-B3E7-4FC5-8414-93ACC36A3D19";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffintervalmodel.update', 0, N'Настройки-->Тарифы-->Добавление и редактирование Тарифов', NULL, N's:36:"D2904707-2D72-4E2F-BCF1-915A265C5679";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffmaxamountintervalmodel.create', 0, N'Настройки>Раздел "Калькулятор">Редактирование тарифных планов', N'FBF975C0-B3E7-4FC5-8414-93ACC36A3D19', N's:36:"FBF975C0-B3E7-4FC5-8414-93ACC36A3D19";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffmaxamountintervalmodel.delete', 0, N'Настройки-->Тарифы-->Добавление и редактирование Тарифных планов', NULL, N's:36:"FBF975C0-B3E7-4FC5-8414-93ACC36A3D19";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffmaxamountintervalmodel.list', 0, N'settings.tariffmaxamountintervalmodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffmaxamountintervalmodel.update', 0, N'Настройки-->Тарифы-->Добавление и редактирование Тарифных планов', N'FBF975C0-B3E7-4FC5-8414-93ACC36A3D19', N's:36:"FBF975C0-B3E7-4FC5-8414-93ACC36A3D19";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffmodel.create', 0, N'Настройки-->Тарифы-->Добавление и редактирование Тарифов', NULL, N's:36:"D2904707-2D72-4E2F-BCF1-915A265C5679";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffmodel.delete', 0, N'Настройки-->Тарифы-->Удаление тарифов', NULL, N's:36:"06817674-AF48-475B-B161-FBE506420486";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffmodel.list', 0, N'settings.tariffmodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffmodel.update', 0, N'Настройки-->Тарифы-->Добавление и редактирование Тарифов', NULL, N's:36:"D2904707-2D72-4E2F-BCF1-915A265C5679";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffplanperiodmodel.create', 0, N'Настройки-->Тарифы-->Добавление и редактирование Тарифных планов', NULL, N's:36:"FBF975C0-B3E7-4FC5-8414-93ACC36A3D19";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffplanperiodmodel.delete', 0, N'Настройки-->Тарифы-->Добавление и редактирование Тарифных планов', N'D2904707-2D72-4E2F-BCF1-915A265C5679', N's:36:"FBF975C0-B3E7-4FC5-8414-93ACC36A3D19";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffplanperiodmodel.update', 0, N'Настройки-->Тарифы-->Добавление и редактирование Тарифных планов', N'D2904707-2D72-4E2F-BCF1-915A265C5679', N's:36:"FBF975C0-B3E7-4FC5-8414-93ACC36A3D19";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffplanperiodmodel.view', 0, N'settings.tariffplanperiodmodel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffplantariffschedulemodel.*', 0, N'settings.tariffplantariffschedulemodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffPlanTariffScheduleModel.create', 0, N'settings.tariffPlanTariffScheduleModel.create', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffplantariffschedulemodel.list', 0, N'settings.tariffplantariffschedulemodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffplantariffschedulemodel.sorted', 0, N'settings.tariffplantariffschedulemodel.sorted', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffplantariffschedulemodel.update', 0, N'Настройки-->Тарифы-->Добавление и редактирование Тарифных сеток', NULL, N's:36:"947BAAE5-E1D8-417A-AA88-AF5DF3143264";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffs.create', 0, N'Настройки-->Тарифы-->Добавление и редактирование Тарифных планов', NULL, N's:36:"FBF975C0-B3E7-4FC5-8414-93ACC36A3D19";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffs.delete', 0, N'Настройки-->Тарифы-->Удаление тарифных планов', NULL, N's:36:"1A12F560-7956-4DD2-B41F-02E0360F1FF4";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffs.list', 0, N'Настройки-->Тарифы', NULL, N's:36:"4EFA28F8-EB7A-4428-8304-85B8E4650C20";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffs.update', 0, N'Настройки-->Тарифы-->Добавление и редактирование Тарифных планов', NULL, N's:36:"FBF975C0-B3E7-4FC5-8414-93ACC36A3D19";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffs.view', 0, N'settings.tariffs.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffschedulemodel.*', 0, N'Настройки-->Тарифы', N'4EFA28F8-EB7A-4428-8304-85B8E4650C20', N's:36:"4EFA28F8-EB7A-4428-8304-85B8E4650C20";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffschedulemodel.create', 0, N'Настройки-->Тарифы-->Добавление и редактирование Тарифных сеток', NULL, N's:36:"947BAAE5-E1D8-417A-AA88-AF5DF3143264";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffschedulemodel.delete', 0, N'Настройки-->Тарифы-->Удаление тарифных сеток', NULL, N's:36:"6484AE97-BD2B-4DCA-BBA5-5DFC1C1B5CE2";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffschedulemodel.list', 0, N'Настройки-->Тарифы', NULL, N's:36:"4EFA28F8-EB7A-4428-8304-85B8E4650C20";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffschedulemodel.update', 0, N'Настройки-->Тарифы-->Добавление и редактирование Тарифных сеток', NULL, N's:36:"947BAAE5-E1D8-417A-AA88-AF5DF3143264";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffschedulemodel.view', 0, N'settings.tariffschedulemodel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffscheduletypemodel.list', 0, N'settings.tariffscheduletypemodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tariffScheduleTypeModel.update', 0, N'settings.tariffScheduleTypeModel.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tarifftariffplanmodel.create', 0, N'Настройки-->Тарифы-->Добавление и редактирование Тарифных сеток', NULL, N's:36:"947BAAE5-E1D8-417A-AA88-AF5DF3143264";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.tarifftariffplanmodel.update', 0, N'Настройки-->Тарифы-->Добавление и редактирование Тарифных сеток', N'947BAAE5-E1D8-417A-AA88-AF5DF3143264', N's:36:"947BAAE5-E1D8-417A-AA88-AF5DF3143264";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.timetypemodel.*', 0, N'settings.timetypemodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.timetypemodel.list', 0, N'settings.timetypemodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.timeTypeModel.update', 0, N'settings.timeTypeModel.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.transittypemodel.list', 0, N'settings.transittypemodel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.transitTypeModel.update', 0, N'settings.transitTypeModel.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.usermodel.*', 0, N'Настройки-->Пользователи', NULL, N's:36:"92142318-D3A2-4DBC-890D-E1460EB41013";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.usermodel.create', 0, N'Настройки-->Пользователи-->Добавление и редактирование пользователей', N'8DFC921D-0A49-49A1-AA32-A9955B7C1FB7', N's:36:"8DB1A1D2-CF3A-4822-A774-47C8DCEF2FF8";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.usermodel.delete', 0, N'Настройки-->Пользователи-->Удаление пользователей', NULL, N's:36:"9D8AA0D4-5DB4-4162-A295-DA955FF58831";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.usermodel.list', 0, N'Настройки>Раздел "Пользователи"', NULL, N's:36:"92142318-D3A2-4DBC-890D-E1460EB41013";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.usermodel.update', 0, N'Настройки-->Пользователи-->Добавление и редактирование пользователей', NULL, N's:36:"8DB1A1D2-CF3A-4822-A774-47C8DCEF2FF8";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.userModel.view', 0, N'settings.userModel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.zonemodel.*', 0, N'settings.zonemodel.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.zonemodel.create', 0, N'Настройки-->Устройства-->Добавление и Редактирование зон', NULL, N's:36:"F94A2CA6-5670-4FF5-B397-F827B45896B4";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.zonemodel.delete', 0, N'Настройки-->Устройства-->Удаление зон', NULL, N's:36:"306EA4E5-2981-49F6-882F-D85D3AE5153A";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.zonemodel.list', 0, N'Настройки>Раздел "Устройства"', NULL, N's:36:"EDF44989-1AB5-485E-A89A-CC11F157302F";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('settings.zonemodel.update', 0, N'Настройки-->Устройства-->Добавление и Редактирование зон', NULL, N's:36:"F94A2CA6-5670-4FF5-B397-F827B45896B4";');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('statistics.*', 0, N'statistics.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('statistics.alarmModel.list', 0, N'statistics.alarmModel.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('statistics.group.*', 0, N'statistics.group.*', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('statistics.group.list', 0, N'statistics.group.list', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('statistics.group.update', 0, N'statistics.group.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('statistics.userModel.update', 0, N'statistics.userModel.update', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('statistics.usermodel.view', 0, N'statistics.usermodel.view', NULL, N'N;');
INSERT INTO dbo.AuthItem ([name], [type], [description], bizrule, data) VALUES  ('usermodel.userModel.update', 0, N'usermodel.userModel.update', NULL, N'N;');
GO

-- Data from table "dbo.AuthItemChild"

-- Data from table "dbo.AuthRole"
INSERT INTO dbo.AuthRole ([Name], [Description]) VALUES (N'SARole', N'СуперАдминистратор');
GO



-- Data from table "dbo.AuthUser"

INSERT INTO dbo.AuthUser (Id, LoginName, [Password], CreateDate) VALUES  ('{3D2D2838-E6F6-4820-9EFE-BEB6EC833632}', N'7777777777', N'1111', CONVERT(datetime, '2016-06-02 21:41:26', 120));
GO

-- Data from table "dbo.AuthUserRole"
INSERT INTO dbo.AuthUserRole (Id, [User], [Role]) VALUES  ('{1ACB25D4-3634-4C15-A79A-A1C29D681B6E}', N'7777777777', N'SARole');
GO

INSERT INTO [dbo].[UserModel] ([Id], [LoginName], [Password], [Email], [FullName], [RoleId]) VALUES (N'3d2d2838-e6f6-4820-9efe-beb6ec833632', N'7777777777', N'1111', N'sa@ws.r-p-s.ru', N'Админ Олег Елена', N'ad1c39c7-44f3-4ac6-aba5-7e07b528d19a');
GO

-- Data from table "dbo.BanknoteModel"
-- RUB (Россия)
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (1, N'10 рублей', 10, N'RUB', N'150', N'65', 99, 4);
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (2, N'50 рублей', 50, N'RUB', N'150', N'65', 100, 8);
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (3, N'100 рублей', 100, N'RUB', N'150', N'65', 101, 16);
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (4, N'500 рублей', 500, N'RUB', N'150', N'65', 102, 32);
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (5, N'1000 рублей', 1000, N'RUB', N'157', N'69', 103, 64);
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (6, N'5000 рублей', 5000, N'RUB', N'157', N'69', 104, 128);
-- CNY (Китай)
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES   (7, N'1 RMB', 1, N'CNY', N'130', N'63', 97, 1);
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (8, N'5 RMB', 5, N'CNY', N'135', N'63', 99, 4);
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (9, N'10 RMB', 10, N'CNY', N'140', N'70', 100, 8);
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (10, N'20 RMB', 20, N'CNY', N'145', N'70', 101, 16);
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (11, N'50 RMB', 50, N'CNY', N'150', N'70', 102, 32);
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (12, N'100 RMB', 100, N'CNY', N'155', N'77', 103, 64);
--KZT (Казахстан)
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (13, N'200 KZT', 200, N'KZT', NULL, NULL, 98, 2);
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (14, N'500 KZT', 500, N'KZT', NULL, NULL, 99, 4);
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (15, N'1000 KZT', 1000, N'KZT', NULL, NULL, 100, 8);
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (16, N'2000 KZT', 2000, N'KZT', NULL, NULL, 101, 16);
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (17, N'5000 KZT', 5000, N'KZT', NULL, NULL, 102, 32);
INSERT INTO dbo.BanknoteModel (Id, [Name], [Value], Code, Length, Width, EscrowData, DisableData) VALUES  (18, N'10000 KZT', 10000, N'KZT', NULL, NULL, 103, 64);



GO

-- Data from table "dbo.BarCodeFunction"
SET IDENTITY_INSERT dbo.BarCodeFunction ON
GO

INSERT INTO dbo.BarCodeFunction (Id, [Name]) VALUES  (1, N'Изм. ТП ТС');
INSERT INTO dbo.BarCodeFunction (Id, [Name]) VALUES  (2, N'Скидки');
INSERT INTO dbo.BarCodeFunction (Id, [Name]) VALUES  (3, N'Изм. группы');
GO

SET IDENTITY_INSERT dbo.BarCodeFunction OFF
GO



-- Data from table "dbo.BarCodeInUse"



-- Data from table "dbo.BarCodeRights"



-- Data from table "dbo.BarCodeUsed"



-- Data from table "dbo.BlackList"



-- Data from table "dbo.CameraSetting"
SET IDENTITY_INSERT dbo.CameraSetting ON
GO


SET IDENTITY_INSERT dbo.CameraSetting OFF
GO



-- Data from table "dbo.CoinModel"
-- RUB (Россия)
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES  (1, N'1 рубль', 1, N'RUB', 1);
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES    (2, N'2 рубля', 2, N'RUB', 3);
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES    (3, N'5 рублей', 5, N'RUB', 5);
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES    (4, N'10 рублей', 10, N'RUB', 7);
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES  (16, N'1 рубль', 1, N'RUB', 2);
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES    (17, N'2 рубля', 2, N'RUB', 4);
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES    (18, N'5 рублей', 5, N'RUB', 6);
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES    (19, N'10 рублей', 10, N'RUB', 8);
-- CNY (Китай)
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES    (5, N'1 RMB', N'1', N'CNY', NULL);
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES    (6, N'2 RMB', N'2', N'CNY', NULL);
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES    (7, N'5 RMB', N'5', N'CNY', NULL);
--KZT (Казахстан)
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES  (8, N'5 KZT', 5, N'KZT', 1);
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES    (9, N'5 KZT', 5, N'KZT', 2);
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES    (10, N'10 KZT', 10, N'KZT', 3);
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES    (11, N'10 KZT', 10, N'KZT', 4);
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES  (12, N'20 KZT', 20, N'KZT', 5);
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES    (13, N'20 KZT', 20, N'KZT', 6);
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES    (14, N'50 KZT', 50, N'KZT', 7);
INSERT INTO dbo.CoinModel (Id, [Name], [Value], Code, DisableData) VALUES    (15, N'100 KZT', 100, N'KZT', 8);
GO

-- Data from table "dbo.ComPortModel"
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES  (0, N'Выбрать');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES    (1, N'COM1');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES    (2, N'COM2');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES    (3, N'COM3');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES    (4, N'COM4');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES    (5, N'COM5');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES    (6, N'COM6');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES    (7, N'COM7');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES    (8, N'COM8');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES    (9, N'COM9');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES    (10, N'COM10');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES    (11, N'COM11');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES    (12, N'COM12');
INSERT INTO dbo.ComPortModel (Id, [Name]) VALUES    (13, N'COM13');
GO

-- Data from table "dbo.ConfigSync"
SET IDENTITY_INSERT dbo.ConfigSync ON
GO


SET IDENTITY_INSERT dbo.ConfigSync OFF
GO


-- Data from table "dbo.ExecutableDeviceTypeModel"
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES  (1, N'Диспенсер купюр');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES  (2, N'Банкнотоприемник');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES  (3, N'Хоппер');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES  (4, N'Монетоприемник');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES  (5, N'Банковский модуль');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES  (6, N'Сканер ШК');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES  (7, N'Диспенсер карт');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES  (8, N'Считыватель карт');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES  (9, N'Дисплей');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES  (10, N'Картоприемник');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES  (11, N'ККМ');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES  (12, N'Принтер');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES  (13, N'Slave-контроллер');
INSERT INTO dbo.ExecutableDeviceTypeModel (Id, [Name]) VALUES  (14, N'Рецайклер банкнот');
GO

-- Data from table "dbo.ExecutableDevice"
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (1, 1, N'Multimech');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (2, 2, N'iVision');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (3, 3, N'Azkoen');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (4, 4, N'Munzprufer');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (5, 5, N'Uniteller');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (6, 6, N'ХЗ что...');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (7, 7, N'Mingte MT166');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (8, 8, N'Mingte MT625');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (9, 9, N'A07-OPHB01');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (10, 10, N'Kytronics KYT1020');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (11, 11, N'AtolKKM');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (12, 12, N'Принтер');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (13, 13, N'Slave-контроллер');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (14, 11, N'IskraKKM');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (15, 8, N'HID OmniKey');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (16, 5, N'GPB');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (17, 11, N'PrimFAKKM');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (18, 5, N'Sber');
INSERT INTO dbo.ExecutableDevice (Id, ExecutableDeviceTypeId, [Name]) VALUES  (90, 14, N'UBA RC'); -- большое значение, чтобы ресайклер был всегда после БП или ДБ.
GO

-- Data from table "dbo.MessageTypeModel"
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (0, 0, N'Устройство работает.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (1, 0, N'Внешняя дверь закрыта.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (2, 0, N'Внутреняя дверь закрыта.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (3, 0, N'Смена открыта.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (101, 2, N'Нет связи с внешним ридером. Проверьте подключение, питание и работоспособность устройства.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (102, 1, N'Ошибка чтения/записи во внешнем ридере');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (103, 2, N'Нет связи с внутренним ридером. Проверьте подключение, питание и работоспособность устройства.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (104, 1, N'Ошибка чтения/записи во внутреннем ридере');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (105, 3, N'Карты в диспенсере карт подходят к концу. Загрузите в тубус новые карты.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (106, 2, N'В диспенсере карт закончились карты. Загрузите в тубус новые карты.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (107, 1, N'В диспенсере карт застряла карта. Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (108, 2, N'Нет связи с диспенсером карт. Устройство не работает. Проверьте устройство.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (109, 2, N'Нет связи со слейв контроллером. Проверьте подключение, питание и работоспособность платы.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (110, 2, N'Ошибка Дискрета');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (111, 2, N'Нет питания 220В. Работа от UPS.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (112, 3, N'Верхняя дверь открыта');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (113, 3, N'Нижний люк открыт');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (114, 2, N'Проблемы с датчиками (петли, ИК)');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (115, 2, N'Проблемы со шлагбаумом');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (116, 2, N'Петля А долго занята');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (117, 2, N'Петля В долго занята');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (118, 2, N'ИК долго занят');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (120, 2, N'Нет лицензии');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (121, 3, N'Этот штрих-код уже используется');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (122, 3, N'Закончился срок действия штрих-кода');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (123, 3, N'Штрих-код не действителен');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (124, 3, N'Проезд запрещён по гос. номеру');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (125, 3, N'кража карты');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (126, 3, N'Проезд запрещён Карта заблокирована');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (127, 3, N'Проезд запрещён Нет карты в базе');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (128, 3, N'Проезд запрещён Нет мест для компании');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (129, 3, N'Проезд запрещён Не совпал гос.номер');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (130, 3, N'Проезд запрещён группе клиентов');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (131, 3, N'Проезд запрещён Несоответствие зоны');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (132, 3, N'проезд с отбоем стрелы');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (133, 3, N'отказ от проезда');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (134, 3, N'попытка неоплаченного проезда');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (135, 3, N'Разовая карта на внешнем считывателе');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (136, 3, N'ОШИБКА ЧТЕНИЯ КАРТЫ');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (137, 3, N'ОШИБКА КЛЮЧА КАРТЫ');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (138, 3, N'ОШИБКА ЗАПИСИ КАРТЫ');

INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (199, 1, N'Касса не обслуживает.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (200, 3, N'Банкнотоприемник почти переполнен. Необходимо провести инкассацию');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (201, 2, N'Банкнотоприемник переполнен. Оплата купюрами не принимается. Необходимо провести инкассацию.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (202, 1, N'Замятие купюры в банкнотоприемнике. Устройство не работает! Требуется присутствие оператора.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (203, 2, N'Нет связи с банкнотопримеником. Купюры не принимаются. Проверьте устройство.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (204, 2, N'Ящик банкнотоприемника не установлен. Купюры не принимаются. Установите ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (205, 1, N'Ошибка банкнотоприемника.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (206, 3, N'В нижней кассете диспенсера почти кончились купюры. Пополните ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (207, 2, N'В нижней кассете диспенсера кончились купюры. Пополните ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (208, 2, N'В диспенсере банкнот отсутсвтует нижняя кассаета');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (209, 3, N'В верхней кассете диспенсера почти кончились купюры. Пополните ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (210, 2, N'В верхней кассете диспенсера кончились купюры. Пополните ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (211, 2, N'В диспенсере банкнот отсутсвтует верхняя кассаета');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (213, 2, N'Отказная ёмкость диспенера банкнот переполнена. Требуется провести инкассацию.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (214, 1, N'В диспенсере банкнот застряла купюра. Выдача сдачи не возможна! Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (215, 2, N'Нет связи с диспенсером банкнот. Сдача купюрами не выдается. Проверьте устройство.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (216, 2, N'Прочие ошибки диспенсера купюр. Сдача купюрами не выдается. Проверьте устройство.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (217, 3, N'Отказная ёмкость диспенера банкнот почти заполнена. ');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (220, 3, N'В правом хоппере почти кончились монеты. Пополните ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (221, 2, N'В правом хоппере закончились монеты. Выдача сдачи монетами ограничена. Пополните ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (222, 1, N'В правом хоппере застряла монета. Выдача сдачи не возможна! Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (223, 2, N'Нет связи с правым Хоппером монет. Сдача монетами ограничена. Проверьте устройство.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (225, 3, N'В левом хоппере почти кончились монеты. Пополните ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (226, 2, N'В левом хоппере закончились монеты. Выдача сдачи монетами ограничена. Пополните ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (227, 1, N'В левом хоппере застряла монета. Выдача сдачи не возможна! Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (228, 2, N'Нет связи с левым Хоппером монет. Сдача монетамами ограничена. Проверьте устройство.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (230, 3, N'Монетоприемник почти переполнен. Необходимо провести инкассацию');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (231, 2, N'Монетоприемник переполнен. Оплата монетами не принимается. Необходимо провести инкассацию.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (232, 1, N'В монетоприемнике застряла монета. Устройство не работает! Требуется присутствие оператора.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (233, 2, N'Нет связи с монетоприемником. Монеты не принимаются. Проверьте устройство.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (240, 3, N'Карты в диспенсере карт подходят к концу. Загрузите в тубус новые карты.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (241, 3, N'В диспенсере карт закончились карты. Загрузите в тубус новые карты.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (242, 1, N'В диспенсере карт застряла карта. Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (243, 1, N'Нет связи с диспенсером карт. Устройство не работает. Проверьте устройство.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (244, 2, N'Отказная кассета переполнена. Заберите карты из отказной кассеты диспенсера.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (245, 1, N'Нет связи с ридером. Проверьте подключение, питание и работоспособность устройства.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (246, 1, N'Ошибка чтения/записи в ридере');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (250, 2, N'Банковский модуль не доступен. Карты к оплате не принимаются. Проверьте устройство.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (251, 1, N'В банковском картоприменом устройстве застряла карта. Требуется присутствие оператора.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (255, 1, N'Нет связи со слейв контроллером. Проверьте подключение, питание и раотоспособность платы.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (256, 2, N'Нет модуля дискрета');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (257, 2, N'Ошибка получения версии слейв-контроллера');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (258, 2, N'Ошибка получения серийного номера слейв-контроллера');

INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (280, 3, N'В ККМ заканчивается бумага. Замените чековую ленту.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (281, 2, N'В ККМ закончилась бумага. Замените чековую ленту.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (282, 2, N'Нет связи с ККМ. Касса не работет. Проверьте устройство.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (283, 2, N'Ошибка установки режима KKM.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (284, 2, N'Неверный пароль доступа KKM.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (285, 2, N'Нерабочий режим ККМ. Касса не работает.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (286, 2, N'Открыта крышка принтера. Касса не работает.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (287, 2, N'Транспоритровка бумаги вручную в принтере. Касса не работает.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (288, 2, N'Механическая ошибка ножа принтера. Касса не работает.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (289, 2, N'Критическая ошибка принтера. Касса не работает.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (290, 2, N'Головка принтера слишком горячая. Касса не работает.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (291, 2, N'Механическая ошибка принтера. Касса не работает.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (292, 1, N'Печать остановлена из-за окончания бумаги. Касса не работает.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (293, 3, N'Внешняя дверь открыта');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (294, 3, N'Внутренняя дверь открыта');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (295, 2, N'Нет питания. Касса работает в автономном режиме.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (296, 4, N'Нет связи с устройством. Проверьте сеть, питание и прочие параметры.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (297, 2, N'Смена закрыта');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (298, 2, N'Закончилось место в буфере смен. Необходимо провести закрытие смены с выдачей всех отложенных Z-отчетов.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (299, 1, N'Незавершанная оплата. Ошибка банкнотоприемника. Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (300, 1, N'Незавершанная оплата. Ошибка монетоприемника. Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (301, 1, N'Незавершанная оплата. Ошибка диспенсера банкнот. Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (302, 1, N'Незавершанная оплата. Ошибка хоппера монет. Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (303, 1, N'Незавершанная оплата. Ошибка ККМ. Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (304, 1, N'Незавершанная оплата. Ошибка ридера карт. Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (305, 1, N'Незавершанная оплата. Ошибка диспенсера карт. Требуется присутствие оператора!');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (306, 1, N'Незавершанная оплата. Ошибка банкоского модуля. Требуется присутствие оператора!');

INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (310, 2, N'Смена превысила 24 часа. Требуется закрытие смены!');

INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (311, 1, N'Попытка обмана! Проверьте банкнотоприемник на наличие нитей.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (312, 1, N'Замятие купюры в голове банкнотоприемника. Откройте крушку банкнотоприемника и проверьте на наличие замятых купюр.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (313, 1, N'Замятие купюры в ящике приема банконт. Выньте ящик и проверьте на наличие замятых купюр.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (314, 1, N'Прочие ошибки банкнотоприемника (0х47 Pause). Обратитесь в службку поддрежки.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (315, 1, N'Прочие ошибки банкнотоприемника (0xAB Box Not Ready Failure). Обратитесь в службку поддрежки.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (316, 1, N'Ошибка работы двигателя банкнотоприемника (A2H Stack motor Failure). Перезагрузите банкнотоприемника. Если ошибка сохранится, обратитесь в службу поддержки.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (317, 1, N'Прочие ошибки банкнотоприемника (0хА5 Transport (feed) motor speed failure). Обратитесь в службку поддержки.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (318, 1, N'Отказ подающего двигателя банкнотоприемника (A6H Tnsport (feed) motor failure). Перезагрузите ресайклер. Если ошибка сохранится, обратитесь в службу поддержки.Отказ подающего двигателя ресайклера банктон (A6H). Перезагрузите ресайклер. Если ошибка сохранится, обратитесь в службу поддержки.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (319, 1, N'Прочие ошибки банкнотоприемника (0хА8 Solenoid failure). Обратитесь в службку поддержки.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (320, 1, N'Прочие ошибки банкнотоприемника (0хА9 PB Unit failure). Обратитесь в службку поддержки.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (321, 1, N'Голова банкнотоприемника не установлена.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (322, 1, N'Прочие ошибки банкнотоприемника (0xB0 BOOT ROM Failure). Обратитесь в службку поддрежки');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (323, 1, N'Прочие ошибки банкнотоприемника (0xB1 External ROM Failure). Обратитесь в службку поддрежки');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (324, 1, N'Прочие ошибки банкнотоприемника (0xB2 RAM Failure). Обратитесь в службку поддрежки');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (325, 1, N'Прочие ошибки банкнотоприемника (0xB3 External ROM writing failure). Обратитесь в службку поддрежки');

INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (326, 1, N'Ошибка работы двигателя в ящике ресайклера/диспенсера банкнот. Перезагрузите ресайклер/диспенсер. Если ошибка сохранится, обратитесь в службу поддержки.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (327, 1, N'Ошибка квалификатора банкнот ресайклера/диспенсера банкнот. Перезагрузите ресайклер/диспенсер. Если ошибка сохранится, обратитесь в службу поддержки.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (328, 1, N'Ошибка дивертора банкнот ресайклера/диспенсера банкнот. Перезагрузите ресайклер/диспенсер. Если ошибка сохранится, обратитесь в службу поддержки.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (329, 1, N'Ошибка транспорта банкнот ресайклера/диспенсера банкнот. Перезагрузите ресайклер/диспенсер. Если ошибка сохранится, обратитесь в службу поддержки.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (330, 1, N'Замятие банкноты на выходе ресайклера/диспенсера банкнот. Вытащите застрявшую банкноту. Если ошибка сохранится, обратитесь в службу поддержки.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (332, 1, N'Ящик ресайклера купюр не установлен. Для продолжения работы установите ящик.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (333, 1, N'Прочие ошибки ресайклера купюр (RC 0х43). Обратитесь в службку поддрежки.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (334, 1, N'Прочие ошибки ресайклера купюр (RC 0х4A). Обратитесь в службку поддрежки.');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (335, 1, N'Ошибка соединения с ресайклером купюр. Обратитесь в службку поддрежки.');



GO

-- Data from table "dbo.Operation"
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (1, N'Голосовой вызов на устройство');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (2, N'Прием голосовго вызова с устройства');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (3, N'Изменнение режима работы проезда');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (4, N'Изменение данных на карте');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (5, N'Добавление устройства');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (6, N'Удаление устройства');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (7, N'Изменение параметров устройства');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (8, N'Добавление зоны');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (9, N'Удаление зоны');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (10, N'Изменение параметров зоны');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (11, N'Добаление тарифа');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (12, N'Добавление тарифного плана');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (13, N'Добавление тарифной сетки');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (14, N'Удаление тарифа');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (15, N'Удаление трифного плана');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (16, N'Удаление тарифной сетки');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (17, N'Изменение параметров Тарифа');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (18, N'Изменение параметров Тарифного плана');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (19, N'Изменение параметров Тарифной сетки');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (20, N'Добавление нового пользователя');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (21, N'Удаление пользователя');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (22, N'Изменение параметров пользователя');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (23, N'Добавление новой роли');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (24, N'Удаление роли');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (25, N'Изменение существующей роли');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (26, N'Добавление Юридического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (27, N'Удаление Юридического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (28, N'Изменение общей информации Юрдического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (29, N'Изменение информации о контактном лице Юрдического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (30, N'Привязка карты к Юрдическому лицу');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (31, N'Удаление связи карты и Юридического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (32, N'Добавление ТС к Юрдическом улицу');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (33, N'Удаление ТС у Юрдического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (34, N'Изменение данных ТС у Юридичекого лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (35, N'Привязка Физического лица к Юридическому');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (36, N'Удаление связи физического лица с Юрдическим.');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (37, N'Добавление Физического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (38, N'Удаление Физического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (39, N'Изменение общей информации Физического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (40, N'Привязка карты к Физического лицу');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (41, N'Удаление связи карты и Физического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (42, N'Добавление ТС к Физического улицу');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (43, N'Удаление ТС у Физического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (44, N'Изменение данных ТС у Физического лица');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (45, N'Изменение данных на карте');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (46, N'Создание новой группы');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (47, N'Удаление группы доступпа');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (48, N'Изменение группы доступа');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (49, N'Создание нового отчета');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (50, N'Удаление отчета');
INSERT INTO dbo.Operation (Id, [Name]) VALUES  (51, N'Изменение отчета');
GO

-- Data from table "dbo.PaymentTransactionType"
INSERT INTO dbo.PaymentTransactionType (Id, [Name]) VALUES  (1, N'Успешная оплата наличными');
INSERT INTO dbo.PaymentTransactionType (Id, [Name]) VALUES  (2, N'Успешная оплата банковской картой');
INSERT INTO dbo.PaymentTransactionType (Id, [Name]) VALUES  (3, N'Отмена оплаты до внесения денежных средств');
INSERT INTO dbo.PaymentTransactionType (Id, [Name]) VALUES  (4, N'Отмена оплаты после внесения денежных средств');
INSERT INTO dbo.PaymentTransactionType (Id, [Name]) VALUES  (5, N'Незавершенная оплата');
INSERT INTO dbo.PaymentTransactionType (Id, [Name]) VALUES  (6, N'Оплата абонемента');
INSERT INTO dbo.PaymentTransactionType (Id, [Name]) VALUES  (7, N'Отмена оплаты после внесения денежных средств, возврат средств осуществлен');
GO

-- Data from table "dbo.Port"
INSERT INTO dbo.Port (Id, [Name]) VALUES  (0, N'Выбор');
INSERT INTO dbo.Port (Id, [Name]) VALUES  (1, N'A');
INSERT INTO dbo.Port (Id, [Name]) VALUES  (2, N'B');
INSERT INTO dbo.Port (Id, [Name]) VALUES  (3, N'C');
INSERT INTO dbo.Port (Id, [Name]) VALUES  (4, N'D');
INSERT INTO dbo.Port (Id, [Name]) VALUES  (5, N'E');
INSERT INTO dbo.Port (Id, [Name]) VALUES  (6, N'F');
INSERT INTO dbo.Port (Id, [Name]) VALUES  (7, N'H');
INSERT INTO dbo.Port (Id, [Name]) VALUES  (8, N'J');
INSERT INTO dbo.Port (Id, [Name]) VALUES  (9, N'K');
GO

-- Data from table "dbo.RackWorkingMode"
INSERT INTO dbo.RackWorkingMode (Id, [Name]) VALUES  (1, N'Проезд закрыт');
INSERT INTO dbo.RackWorkingMode (Id, [Name]) VALUES  (2, N'Свободный проезд с поднятой стрелой');
INSERT INTO dbo.RackWorkingMode (Id, [Name]) VALUES  (3, N'Штатный режим');
GO

-- Data from table "dbo.RasposPlate"
SET IDENTITY_INSERT dbo.RasposPlate ON
GO

SET IDENTITY_INSERT dbo.RasposPlate OFF
GO

-- Data from table "dbo.ReportAccess"

-- Data from table "dbo.ReportModel"

INSERT INTO dbo.ReportUserRight (Id, [Name]) VALUES ('{9EA2452E-628E-4AA8-8D5C-0B35BA8EE9EF}', N'Просмотр');
INSERT INTO dbo.ReportUserRight (Id, [Name]) VALUES ('{4C9C2ED9-C1F3-4549-8A53-2E2A8600D73E}', N'Изменение');
GO

-- Data from table "dbo.ServerExchangeProtocolModel"
INSERT INTO dbo.ServerExchangeProtocolModel (Id, [Name]) VALUES  (1, N'Внутренняя БД');
INSERT INTO dbo.ServerExchangeProtocolModel (Id, [Name]) VALUES  (2, N'Касса создает события');
INSERT INTO dbo.ServerExchangeProtocolModel (Id, [Name]) VALUES  (3, N'Сервер опрашивает кассу');
GO


-- Data from table "dbo.TariffChangingTypeModel"
INSERT INTO dbo.TariffChangingTypeModel (Id, [Name]) VALUES  (1, N'Преим. сменяемого');
INSERT INTO dbo.TariffChangingTypeModel (Id, [Name]) VALUES  (2, N'Точный расчет');
INSERT INTO dbo.TariffChangingTypeModel (Id, [Name]) VALUES  (3, N'Преим. сменяющего');
GO

-- Data from table "dbo.TimeTypeModel"
INSERT INTO dbo.TimeTypeModel (Id, [Name]) VALUES  (0, N'ед.');
INSERT INTO dbo.TimeTypeModel (Id, [Name]) VALUES  (1, N'мин');
INSERT INTO dbo.TimeTypeModel (Id, [Name]) VALUES  (2, N'час');
INSERT INTO dbo.TimeTypeModel (Id, [Name]) VALUES  (3, N'сут');
INSERT INTO dbo.TimeTypeModel (Id, [Name]) VALUES  (4, N'нед');
INSERT INTO dbo.TimeTypeModel (Id, [Name]) VALUES  (5, N'мес');
GO

-- Data from table "dbo.TransitTypeModel"
INSERT INTO dbo.TransitTypeModel (Id, [Name], _IsDeleted) VALUES  (1, N'Въезд', '0');
INSERT INTO dbo.TransitTypeModel (Id, [Name], _IsDeleted) VALUES  (2, N'Выезд', '0');
INSERT INTO dbo.TransitTypeModel (Id, [Name], _IsDeleted) VALUES  (3, N'Переезд', '0');
GO

-- Data from table "dbo.DeviceTypeModel"
INSERT INTO dbo.DeviceTypeModel (Id, [Name], _IsDeleted) VALUES  (0, N'Выбрать', '0');
INSERT INTO dbo.DeviceTypeModel (Id, [Name], _IsDeleted) VALUES  (1, N'Стойка', '0');
INSERT INTO dbo.DeviceTypeModel (Id, [Name], _IsDeleted) VALUES  (2, N'Касса', '0');
INSERT INTO dbo.DeviceTypeModel (Id, [Name], _IsDeleted) VALUES  (3, N'Табло', '0');
INSERT INTO dbo.DeviceTypeModel (Id, [Name], _IsDeleted) VALUES  (4, N'Зонный контроллер', '0');
GO

-- Data from table "dbo.PassageTransactionType"
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES  (N'Штатный проезд', 1);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES  (N'Проезд с отбоем стрелы', 2);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES  (N'проезд  в режиме свободный', 3);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES  (N'отказ от проезда', 4);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES  (N'кража карты', 5);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES  (N'Попытка проезда - не та зона', 6);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES  (N'Попытка проезда - стоп-лист (заблокирована)', 7);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES  (N'Попытка проезда - не та группа', 8);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES  (N'Попытка неоплаченного проезда', 9);
INSERT INTO dbo.PassageTransactionType ([Name], Id) VALUES  (N'Попытка проезда на переполненую парковку', 10);
GO

-- Data from table "dbo.TariffPlanTypeModel"
INSERT INTO dbo.TariffPlanTypeModel (Id, [Name]) VALUES  (0, N'Выбрать');
INSERT INTO dbo.TariffPlanTypeModel (Id, [Name]) VALUES  (1, N'Еженедельный');
INSERT INTO dbo.TariffPlanTypeModel (Id, [Name]) VALUES  (2, N'Ежемесячный');
INSERT INTO dbo.TariffPlanTypeModel (Id, [Name]) VALUES  (3, N'Интервалы');
GO

-- Data from table "dbo.TariffScheduleTypeModel"
INSERT INTO dbo.TariffScheduleTypeModel (Id, [Name]) VALUES  (1, N'Время на парковке');
INSERT INTO dbo.TariffScheduleTypeModel (Id, [Name]) VALUES  (2, N'Кол-во проездов');
INSERT INTO dbo.TariffScheduleTypeModel (Id, [Name]) VALUES  (3, N'Абонемент');
INSERT INTO dbo.TariffScheduleTypeModel (Id, [Name]) VALUES  (4, N'Зона');
GO

-- Data from table "dbo.RackAltimetrModeModel"
INSERT INTO dbo.RackAltimetrModeModel (Id, [Name]) VALUES  (0, N'Не используется');
INSERT INTO dbo.RackAltimetrModeModel (Id, [Name]) VALUES  (1, N'Определение тарифа');
INSERT INTO dbo.RackAltimetrModeModel (Id, [Name]) VALUES  (2, N'Пропуск транспортного средства');
GO

-- Data from table "dbo.SyncTables"

INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'TariffModel', CAST(0x0000A513013ACCAC AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'TariffIntervalModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 10, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'TariffTariffPlanModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 20, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'TariffPlanPeriodModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 30, N'TariffPlanPeriodId', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'Tariffs', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'TariffMaxAmountIntervalModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 10, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'TariffPlanTariffScheduleModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 20, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'TariffSchedule', CAST(0x0000A513013ACDD8 AS DateTime), NULL, NULL, NULL, NULL, NULL,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'Client', CAST(0x0000A513013ACDD8 AS DateTime), NULL, NULL, NULL, NULL, NULL,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'Cards', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 10, N'CardId', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'ClientCompany', CAST(0x0000A513013ACDD8 AS DateTime), NULL, 30, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'ClientCard', CAST(0x0000A513013ACDD8 AS DateTime), NULL, NULL, NULL, NULL, NULL,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'ClientCars', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 30, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'DisplayZone', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 30, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'Group', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'ClientGroup', CAST(0x0000A513013ACDD8 AS DateTime), NULL, NULL, NULL, NULL, NULL,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'ZoneModel', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'BlackList', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [IsRemove], [UslovieRemove], [SyncFrom2]) VALUES (N'AlarmModel', CAST(0x0000A55A00D7A0AA AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1, 1, N'[End] is not null',1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'DeviceModel', CAST(0x0000A55A00C9D3FA AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 10, N'Id', N'Sync', 1,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'DeviceCMModel', CAST(0x0000A55A00C9D3FA AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 20, N'DeviceId', N'Sync', 1,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'TariffScheduleModel', CAST(0x0000A56F0146C33F AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'ZoneGroup', CAST(0x0000A56F014769E7 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'ClientModel', CAST(0x0000A56F01492029 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 20, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [IsRemove], [SyncFrom2]) VALUES (N'Transactions', CAST(0x0000A56F014AF715 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1, 1,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'DeviceRackModel', CAST(0x0000A56F014B2AAA AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 20, N'DeviceId', N'Sync', 1,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'RackCustomerGroup', CAST(0x0000A56F014B657D AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'RackAltimetrModel', CAST(0x0000A56F014B995F AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'CashAcceptorAllowModel', CAST(0x0000A56F014C05C5 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'CoinAcceptorAllowModel', CAST(0x0000A56F014C3DE5 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 1,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [IsRemove], [SyncFrom2]) VALUES (N'CardsTransaction', CAST(0x0000A56F014C65EB AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 30, N'Id', N'Sync', 1, 1,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'CompanyModel', CAST(0x0000A56F0147DEDF AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'License', CAST(0x0000A56F0147DEDF AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'SlaveCode', N'Sync', 2,2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'Parking', CAST(0x0000A56F0147DEDF AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 0, N'SlaveCode', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'Setting', NULL, NULL, 0, N'Name', N'Sync', 2,1);
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'SystemVersion', NULL, NULL, 0, N'SysVersion', N'Sync', 2,2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'UpdateProgramm', NULL, NULL, 0, N'Id', N'Sync', 1,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom]) VALUES (N'CommandDevice', NULL, NULL, 0, N'Id', N'Sync', 2)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [SyncFrom2]) VALUES (N'BarCodeInUse', CAST(0x0000A513013ACDD8 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 10, N'Id', N'Sync', 2,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [IsRemove], [SyncFrom2]) VALUES (N'BCTransaction', CAST(0x0000A56F014C65EB AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 30, N'Id', N'Sync', 1, 1,1)
INSERT [dbo].[SyncTables] ([TableName], [DateTime], [LastUpdate], [OrderSync], [KeyField], [SyncField], [SyncFrom], [IsRemove], [SyncFrom2]) VALUES (N'Log', CAST(0x0000A56F014AF715 AS DateTime), CAST(0x0000A56F014F3AB1 AS DateTime), 30, N'Id', N'Sync', 1, 1,1)

GO
-- Data from table "dbo.MenuItemModel"

INSERT INTO dbo.ZoneModel (Id, [Name],Capacity,Reserved, _IsDeleted, FreeSpace, OccupId, IdFC) VALUES ('{17D66717-64A1-419A-85E0-0BE834A99000}', N'Вне парковки', 20000, 0, '0', 12000, 0,0);
INSERT INTO dbo.ZoneModel (Id, [Name],Capacity,Reserved, _IsDeleted, FreeSpace, OccupId, IdFC) VALUES ('{17D66717-64A1-419A-85E0-0BE834A99001}', N'Зона А', 120, 0, '0', 120, 0,1);
GO

INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (1, N'Критичная тревога', 0, 10, 0)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (2, N'Ограниченная работоспособность', 0, 10, 0)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (3, N'Желтая тревога', 0, 10, 0)

INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (100, N'Транзакция проезд', 1, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (101, N'Штатный проезд', 0, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (102, N'Проезд с отбоем стрелы', 0, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (103, N'Проезд  в режиме свободный', 0, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (104, N'Отказ от проезда', 0, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (105, N'Кража карты', 0, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (106, N'Попытка проезда - не та зона', 0, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (107, N'Попытка проезда - стоп-лист (заблокирована)', 0, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (108, N'Попытка проезда - не та группа', 0, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (109, N'Попытка неоплаченного проезда', 0, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (110, N'Попытка проезда на переполненую парковку', 0, 10, 1)

INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (199, N'Завершение проезда без транзакции', 0, 10, 1)

INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (200, N'Транзакция оплата', 1, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (201, N'Успешная оплата наличными', 0, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (202, N'Успешная оплата банковской картой', 0, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (203, N'Отмена оплаты до внесения денежных средств', 0, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (204, N'Отмена оплаты после внесения денежных средств', 0, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (205, N'Незавершенная оплата', 0, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (206, N'Оплата абонемента', 0, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (207, N'Отмена оплаты после внесения денежных средств, возврат средств осуществлен', 0, 10, 1)
INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (208, N'Таймаут оплаты штрафной карты', 0, 10, 1)

INSERT [dbo].[DeviceAction] ([Id], [Description], [IsWrite], [WriteTime], [IsContinuous]) VALUES (299, N'Завершение оплаты без транзакции', 0, 10, 1)


INSERT [dbo].[DbVersion] ([CurrentPacket], [CurrentItem]) Values (0, 0)
INSERT [dbo].[SystemVersion] (SysVersion, STVer, WebVer, RackVer, CMVer, DbVer,[Status]) VALUES (1,1,1,1,1,80,4)


INSERT [dbo].[UpdateStatus] (Id, Name) VALUES (1, 'Загрузка обновлений на устройства');
INSERT [dbo].[UpdateStatus] (Id, Name) VALUES (2, 'Обновление скачано. Ожидание команды на рестарт');
INSERT [dbo].[UpdateStatus] (Id, Name) VALUES (3, 'Ошибка обновления устройства ');
INSERT [dbo].[UpdateStatus] (Id, Name) VALUES (4, 'Устройство успешно обновлено');
GO


-- Data from table "dbo.Setting"

INSERT INTO [dbo].[Setting] (Name, Category, Value) VALUES ('CardKey', 'Install', 'E7A894330E7615209EDB559E83157ADA');
INSERT INTO [dbo].[Setting] (Name, Category, Value) VALUES ('DB3Host', 'Install', 'mc.r-p-s.ru');
INSERT INTO [dbo].[Setting] (Name, Category, Value) VALUES ('DB3Login', 'Install', 'sa');
INSERT INTO [dbo].[Setting] (Name, Category, Value) VALUES ('DB3Password', 'Install', '02093b34b178c99aa6860b6f943d1800');
INSERT INTO [dbo].[Setting] (Name, Category, Value) VALUES ('DefaultLang', 'Localization', 'ru');


-- noinspection SqlNoDataSourceInspection
INSERT INTO [dbo].[Setting] (Name, Category, Value) VALUES ('FTPHost', 'Install', 'mc.r-p-s.ru');
INSERT INTO [dbo].[Setting] (Name, Category, Value) VALUES ('FTPPassword', 'Install', '02093b34b178c99aa6860b6f943d1800');
INSERT INTO [dbo].[Setting] (Name, Category, Value) VALUES ('FTPPort', 'Install', '21');
INSERT INTO [dbo].[Setting] (Name, Category, Value) VALUES ('FTPUser', 'Install', 'updater');
INSERT INTO [dbo].[Setting] (Name, Category, Value) VALUES ('RtmpPort', 'VideoServer', '1935');
INSERT INTO [dbo].[Setting] (Name, Category, Value) VALUES ('ServerDomain', 'Server', 'test.r-p-s.ru');
INSERT INTO [dbo].[Setting] (Name, Category, Value) VALUES ('VideoServerIp', 'VideoServer', '10.0.0.253');
INSERT INTO [dbo].[Setting] (Name, Category, Value) VALUES ('WebCamChannel', 'VideoServer', '1');



COMMIT
GO