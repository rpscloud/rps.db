﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

using ASE.MVC;
using ASE.Data;

using rps.Data;
using rpsbb.Code;

namespace rpsbb.Controllers
{
    [Authorize]
    public class AlarmsController : ApiControllerAdv<DB>
    {
        [System.Web.Http.HttpGet]
        public AlarmModelJson Get(Guid id)
        {
            return db.Alarms.Find(id).Get();
        }

        [System.Web.Http.HttpGet]
        public IEnumerable<AlarmModelJson> List()
        {
            var q = db.Alarms;

            return q.ToList().Select(x => x.Get());
        }

        /*[System.Web.Http.HttpPost]
        public MapModelJson Add(MapModelJson model)
        {
            model.Id = Guid.Empty;
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPut]
        public MapModelJson Put(MapModelJson model)
        {
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPatch]
        public MapModelJson Patch(MapModelJson model)
        {
            return model.DALSave(db, true).Get();
        }

        [System.Web.Http.HttpDelete]
        public MapModelJson Delete([FromUri] MapModelJson model)
        {
            return model.DALDelete(db).Get();
        }*/
    }
}
