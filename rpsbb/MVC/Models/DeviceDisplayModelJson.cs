using ASE.MVC;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace rps.Data
{
    [NotMapped]
    public class DeviceDisplayModelJson : DeviceDisplayModel
    {

    }

    public static partial class Model2Json
    {
        public static DeviceDisplayModelJson Get(this DeviceDisplayModel model)
        {
            var ret = (DeviceDisplayModelJson)Copy(model, new DeviceDisplayModelJson());
            ret.Device = ret.Device.Get();

            return ret;
        }

        /*public static DeviceCMModel DALSave(this DeviceCMModelJson model, bool patch = false, Guid? newId = null)
        {
            DeviceCMModel m = null;
            if (model.DeviceId == Guid.Empty)
            {
                model.DeviceId = newId.HasValue ? newId.Value : Guid.NewGuid();
                m = new DeviceCMModel();
                ContextPerRequest.Db.DeviceCMs.Add(m);
            }
            else
            {
                m = ContextPerRequest.Db.DeviceCMs.Find(model.DeviceId);
            }
            Copy(model, m, patch);
            ContextPerRequest.Db.SaveChanges();

            //StartSync();
            return m;
        }*/

        public static DeviceDisplayModel DALDelete(this DeviceDisplayModelJson model)
        {
            var m = ContextPerRequest.Db.DeviceDisplays.Find(model.DeviceId);
            m._IsDeleted = true;
            ContextPerRequest.Db.SaveChanges();

            return m;
        }
    }
}