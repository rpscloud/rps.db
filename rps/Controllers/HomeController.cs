﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Drawing;

using ASE.MVC;
using ASE.Data;

using rps.Data;
using rps.ParkingCode;

using RMLib;
using RMLib.Services;
using Parking.Security;
using RMLib.Services.Wcf;
using Parking.Client;

namespace rps.Controllers
{
    //[Authorize]
    public class HomeController : ControllerAdv<DB>
    {
        public ActionResult Index()
        {
            return View();
        }

        /*public ActionResult Index(Guid? id, Guid? parkingId)
        {
            if (!parkingId.HasValue)
                return RedirectToAction("SelectParking");

            Map model;
            if (!id.HasValue)
                model = db.Maps.Where(x => x.ParkingId == parkingId.Value).OrderByDescending(x => x.Level).FirstOrDefault();
            else
                model = db.Maps.FirstOrDefault(x => x.ParkingId == parkingId.Value & x.Id == id);

            if (model == null)
            {
                id = Guid.NewGuid();
                int level = (db.Maps.Where(x => x.UserId == UserId & x.ParkingId == parkingId.Value).Max(x => (int?)x.Level) ?? 0) + 1;
                db.Maps.Add(new Map
                {
                    Id = id.Value,
                    UserId = UserId,
                    Level = level,
                    ParkingId = parkingId,
                    Name = "Уровень " + level
                });
                db.SaveChanges();
                return RedirectToAction("Index", new { id = id.Value, parkingId = parkingId });
            }

            return View(model);
        }

        public ActionResult SelectParking()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Map model, string deleteimage)
        {
            if (deleteimage != null)
            {
                model = db.Maps.Find(model.Id);
                model.Image = null;
                model.ImageName = "";
            }
            else if ((Request.Files.AllKeys.Contains("file")) && (Request.Files["file"].ContentLength != 0))
            {
                AttachModel(model);
                using (var ms = new MemoryStream())
                {
                    Request.Files["file"].InputStream.CopyTo(ms);
                    model.Image = ms.GetBuffer();
                    model.ImageName = Request.Files["file"].FileName;
                    using (var image = new Bitmap(Request.Files["file"].InputStream))
                    {
                        model.ImageW = image.Width;
                        model.ImageH = image.Height;
                    }
                }
            }
            else
                AttachModel(model);

            db.SaveChanges();

            return RedirectToAction("Index", new { id = model.Id, parkingId = model.ParkingId });
        }

        [HttpPost]
        public JsonResult LevelSettingsDialog(Guid id)
        {
            return new JsonResult
            {
                Data = new
                {
                    html = this.RenderPartialView("LevelSettings", db.Maps.Find(id))
                }
            };
        }

        public ActionResult Image(Guid id)
        {
            var a = db.Maps.Find(id);
            if ((a != null) && (a.Image != null))
                return File(a.Image, "image");

            return new EmptyResult();
        }

        [HttpPost]
        public JsonResult GlobalSettingsDialog()
        {
            return new JsonResult
            {
                Data = new
                {
                    html = this.RenderPartialView("GlobalSettings", ViewModelSettings.Load(db))
                }
            };
        }

        [HttpPost]
        public JsonResult GlobalSettingsDialogSave(ViewModelSettings model)
        {
            bool error = false;
            model.Save(db);

            return new JsonResult
            {
                Data = new
                {
                    error = error,
                    html = this.RenderPartialView("GlobalSettings", model)
                }
            };
        }

        [HttpPost]
        public JsonResult DevicesDialog()
        {
            return new JsonResult
            {
                Data = new
                {
                    html = this.RenderPartialView("DevicesDialog", db.Devices.Where(x => x.Level.UserId == UserId).OrderBy(x => x.Level.Level).ThenBy(x => x.Name).ToList())
                }
            };
        }

        [HttpPost]
        public JsonResult AddDeviceDialog(Guid levelId)
        {
            return new JsonResult
            {
                Data = new
                {
                    html = this.RenderPartialView("AddDeviceDialog", new Device { Id = Guid.Empty, LevelId = levelId })
                }
            };
        }        

        [HttpPost]
        public ActionResult AddDevice(Device device)
        {
            device.Id = Guid.NewGuid();
            db.Devices.Add(device);
            db.SaveChanges();

            return RedirectToAction("Index", new { id = device.LevelId });
        }*/
    }
}