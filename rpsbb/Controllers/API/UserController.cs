﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ASE.Data;
using ASE.MVC;

using rps.Data;
using rpsbb.Code;

namespace rpsbb.Controllers
{
    [Authorize]
    public class UserController : ApiControllerAdv<DB>
    {
        [System.Web.Http.HttpGet]
        public UserModelJson Get(Guid id)
        {
            var user = db.Users.Find(id);
            if (user != null)
                return db.Users.Find(id).Get();
            else
            {
                var aUser = db.AuthUsers.Find(id);
                if (aUser != null)
                    return new UserModelJson { Id = id, Name = aUser.LoginName };
            }

            return null;
        }

        [System.Web.Http.HttpGet]
        public IEnumerable<UserModelJson> List()
        {
            var q = db.Users.Where(x => !x.Deleted);
            if ((UserModel != null) && (!UserModel.Is_Admin))
            {
                var p = db.UserParkingRoles.Where(x => !x.Parking.Deleted & x.UserId == UserId).Select(x => x.ParkingId);
                q = db.UserParkingRoles.Where(x => !x.User.Deleted & p.Contains(x.ParkingId)).Select(x => x.User).Distinct();
                if (!UserModel.Is_RPS)
                    q = q.Where(x => !x.Is_RPS);
            }

            return q.ToList().Select(x => x.Get());
        }

        [System.Web.Http.HttpPost]
        public UserModelJson Add(UserModelJson model)
        {
            model.Id = Guid.Empty;
            return model.DALSave(db, false, UserModel).Get();
        }

        [System.Web.Http.HttpPut]
        public UserModelJson Put(UserModelJson model)
        {
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPatch]
        public UserModelJson Patch(UserModelJson model)
        {
            return model.DALSave(db, true).Get();
        }

        [System.Web.Http.HttpDelete]
        public UserModel Delete([FromUri] UserModelJson model)
        {
            return model.DALDelete(db).Get();
        }
    }
}
