require.config({

    baseUrl: "/scripts",


    /* starting point for application */
    deps: ['backbone.marionette', 'bootstrap', 'helpers', 'blockui'],


    shim: {
        helpers: {
            deps: ['jquery']
        },
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery',
                'mousewheel',
                'browserdetect',


            ],
            exports: 'Backbone'
        },
        bootstrap: {
            deps: ['jquery'],
            exports: 'jquery'
        },
        "jqueryui": ["jquery"],
        "iCheck": ["jquery"],
        blockui: ["jquery"],
        common: ["jquery"],
        "backbone.syphon": ["jquery", "backbone"],


        tmpl: ["underscore"]


    },

    paths: {
        helpers: 'helpers',
        jquery: '/bower_components/jquery/dist/jquery',
        iCheck: '/bower_components/iCheck/icheck',
        'jqueryui': '/bower_components/jquery-ui/jquery-ui',
        'blockui': '/bower_components/blockui/jquery.blockUI',
        mousewheel: "/bower_components/jquery-mousewheel/jquery.mousewheel",
        browserdetect: "/bower_components/bowser/bowser",
        backbone: '/bower_components/backbone/backbone',
        localstorage: "/bower_components/backbone.localStorage/backbone.localStorage",
        //slick: '/style/js/slick/slick/slick',
        //cookies: '/style/js/cookie',

        underscore: '/bower_components/underscore/underscore',
        'backbone.syphon': '/bower_components/backbone.syphon/lib/backbone.syphon',
        /* alias all marionette libs */
        'backbone.marionette': '/bower_components/backbone.marionette/lib/core/backbone.marionette',
        'backbone.wreqr': '/bower_components/backbone.wreqr/lib/backbone.wreqr',
        'backbone.babysitter': '/bower_components/backbone.babysitter/lib/backbone.babysitter',

        /* alias the bootstrap js lib */
        bootstrap: 'vendor/bootstrap',

        /* Alias text.js for template loading and shortcut the templates dir to tmpl */
        text: '/bower_components/requirejs-text/text',

        tmpl: 'common/templates',

        /* handlebars from the require handlerbars plugin below */
        handlebars: '/bower_components/require-handlebars-plugin/hbs/handlebars',

        /* require handlebars plugin - Alex Sexton */
        //  i18nprecompile: '/bower_components/require-handlebars-plugin/hbs/i18nprecompile',
        json2: '/bower_components/require-handlebars-plugin/hbs/json2',
        hbs: '/bower_components/require-handlebars-plugin/hbs'
    },

    hbs: {
        disableI18n: true
    }
});
