﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE
{
    public class Log
    {
        public static string Root = "";
        public static bool Enabled = System.Configuration.ConfigurationSettings.AppSettings["ASELogEnabled"] != "false";

        public static void L(Exception exc)
        {
            L("!!! EXCEPTION", exc.GetType());
            L(exc.Message);
            if (exc.InnerException != null)
            {
                L("IE", exc.InnerException.Message);
                if (exc.InnerException.InnerException != null)
                    L("IE2", exc.InnerException.InnerException.Message);
            }
            L(exc.StackTrace);
        }

        public static void L(params object[] args)
        {
            if (!Enabled)
                return;

            try
            {
                System.IO.File.AppendAllText(Root, String.Format("[{0}] ", DateTime.Now.ToString("yyy-MM-dd HH:mm:ss")));
                foreach (var arg in args)
                    if (arg != null)
                        System.IO.File.AppendAllText(Root, String.Format("{0} ", arg));
                
                System.IO.File.AppendAllText(Root, "\r\n");
            }
            catch
            {

            }
        }
    }
}
