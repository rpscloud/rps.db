﻿CREATE TABLE [dbo].[DeliveryChangedModel] (
    [Id]          		UNIQUEIDENTIFIER      	NOT NULL,
    [DeviceId]       	UNIQUEIDENTIFIER    	NULL,
    [ChangedTime]       DATETIME        		NOT NULL,
    [Sync]           	DATETIME     	NULL,
    [LowerCassetQTTY]   INT  			NULL,
    [LowerCassetSum]   	INT  			NULL,
    [UpperCassetQTTY] 	INT         	NULL,
    [UpperCassetSum]  	INT         	NULL,
    [HopperQTTY1]  		INT         	NULL,
    [HopperSum1]       	INT          	NULL,
    [HopperQTTY2]      	INT          	NULL,
    [HopperSum2]        INT          	NULL,
    [TottalSumDisp]  	INT          	NULL,
    [TotalSumHoppers]  	INT             NULL,
    [TotalSum]        	INT         	NULL,

    CONSTRAINT [PK_dbo.DeliveryChangedModel] PRIMARY KEY CLUSTERED ([Id]),
    CONSTRAINT [FK_dbo.DeliveryChanged_dbo.DeviceModel_DeviceId] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[DeviceModel] ([Id])

);


GO


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeliveryChangedModel', @level2type = N'COLUMN', @level2name = N'Sync';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД кассы', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeliveryChangedModel', @level2type = N'COLUMN', @level2name = N'DeviceId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время внесения денег на сдачу', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeliveryChangedModel', @level2type = N'COLUMN', @level2name = N'ChangedTime';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Количество внесено в нижний бокс', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeliveryChangedModel', @level2type = N'COLUMN', @level2name = N'LowerCassetQTTY';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма в нижнем боксе', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeliveryChangedModel', @level2type = N'COLUMN', @level2name = N'LowerCassetSum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Количество внесено в верхний бокс', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeliveryChangedModel', @level2type = N'COLUMN', @level2name = N'UpperCassetQTTY';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма в верхнем боксе', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeliveryChangedModel', @level2type = N'COLUMN', @level2name = N'UpperCassetSum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Количество внесено в хоппер 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeliveryChangedModel', @level2type = N'COLUMN', @level2name = N'HopperQTTY1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма в хоппер 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeliveryChangedModel', @level2type = N'COLUMN', @level2name = N'HopperSum1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Количество внесено в хоппер 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeliveryChangedModel', @level2type = N'COLUMN', @level2name = N'HopperQTTY2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма в хоппер 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeliveryChangedModel', @level2type = N'COLUMN', @level2name = N'HopperSum2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма в диспенсере', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeliveryChangedModel', @level2type = N'COLUMN', @level2name = N'TottalSumDisp';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма в хопперах', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeliveryChangedModel', @level2type = N'COLUMN', @level2name = N'TotalSumHoppers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма на сдачу всего', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeliveryChangedModel', @level2type = N'COLUMN', @level2name = N'TotalSum';


GO
CREATE TRIGGER [dbo].[SyncDeliveryChanged]
ON [dbo].[DeliveryChangedModel]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [DeliveryChangedModel] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='DeliveryChangedModel'
end

GO
