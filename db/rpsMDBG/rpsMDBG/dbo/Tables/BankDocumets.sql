﻿CREATE TABLE [dbo].[BankDocumets]
(
	[Id]  UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL , 
    [ShiftNumber] INT NULL, 
    [TransactionNumber] INT NULL, 
    [OperationCode] TINYINT NULL,
    [Result] TEXT NULL, 
    [Receipt] TEXT NULL, 
	CONSTRAINT [PK_dbo.BankDocumets] PRIMARY KEY CLUSTERED ([Id] ASC)
)

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Идентификатор',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'BankDocumets',
    @level2type = N'COLUMN',
    @level2name = N'Id'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Номер смены',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'BankDocumets',
    @level2type = N'COLUMN',
    @level2name = N'ShiftNumber'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Номер транзакции',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'BankDocumets',
    @level2type = N'COLUMN',
    @level2name = N'TransactionNumber'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Результат',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'BankDocumets',
    @level2type = N'COLUMN',
    @level2name = N'Result'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Текст для печати',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'BankDocumets',
    @level2type = N'COLUMN',
    @level2name = N'Receipt'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Код операции',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'BankDocumets',
    @level2type = N'COLUMN',
    @level2name = N'OperationCode'