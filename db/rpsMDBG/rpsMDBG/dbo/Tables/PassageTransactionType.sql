﻿CREATE TABLE [dbo].[PassageTransactionType] (
    [Name] VARCHAR (MAX) NOT NULL,
    [Id]   INT           NOT NULL,
    CONSTRAINT [PassageTransactionType_pk] PRIMARY KEY CLUSTERED ([Id] ASC)
);

