﻿CREATE TABLE [dbo].[ParkingModel] (
    [Id]         UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Name]       VARCHAR (MAX)    NOT NULL,
    [SULocal]    VARCHAR (MAX)    NULL,
    [SURemote]   VARCHAR (MAX)    NULL,
    [SUPort]     INT              NOT NULL,
    [SUUser]     VARCHAR (MAX)    NULL,
    [SUPassword] VARCHAR (MAX)    NULL,
    [S3Site]     VARCHAR (MAX)    NULL,
    [S3Sql]      VARCHAR (MAX)    NULL,
    [S3User]     VARCHAR (MAX)    NULL,
    [S3Password] VARCHAR (MAX)    NULL,
    [S3Db]       VARCHAR (MAX)    NULL,
    [S2Sql]      VARCHAR (MAX)    NULL,
    [S2User]     VARCHAR (MAX)    NULL,
    [S2Password] VARCHAR (MAX)    NULL,
    [S2Db]       VARCHAR (MAX)    NULL,
    [Longitude]  VARCHAR (MAX)    NULL,
    [Latitude]   VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_dbo.ParkingModel] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наименование', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'СУ для С2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'SULocal';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'СУ для С3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'SURemote';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Порт', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'SUPort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Пользователь', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'SUUser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Пароль', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'SUPassword';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сайт3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'S3Site';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'БД3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'S3Sql';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'БД3 пользователь', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'S3User';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'БД3 пароль', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'S3Password';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'БД3 база данных', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'S3Db';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'БД2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'S2Sql';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'БД2 пользователь', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'S2User';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'БД2 пароль', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'S2Password';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'БД2 база данных', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'S2Db';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Долгота', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'Longitude';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Широта', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingModel', @level2type = N'COLUMN', @level2name = N'Latitude';

