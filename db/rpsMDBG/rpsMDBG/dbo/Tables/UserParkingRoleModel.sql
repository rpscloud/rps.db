﻿CREATE TABLE [dbo].[UserParkingRoleModel] (
    [Id]        UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [UserId]    UNIQUEIDENTIFIER NOT NULL,
    [ParkingId] UNIQUEIDENTIFIER NOT NULL,
    [RoleId]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_dbo.UserParkingRoleModel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.UserParkingRoleModel_dbo.ParkingModel_ParkingId] FOREIGN KEY ([ParkingId]) REFERENCES [dbo].[ParkingModel] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.UserParkingRoleModel_dbo.RoleModel_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[RoleModel] ([Id]),
    CONSTRAINT [FK_dbo.UserParkingRoleModel_dbo.UserModel_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserModel] ([Id]) ON DELETE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_UserId_ParkingId]
    ON [dbo].[UserParkingRoleModel]([UserId] ASC, [ParkingId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RoleId]
    ON [dbo].[UserParkingRoleModel]([RoleId] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'UserParkingRoleModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Пользователь', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'UserParkingRoleModel', @level2type = N'COLUMN', @level2name = N'UserId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Парковка', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'UserParkingRoleModel', @level2type = N'COLUMN', @level2name = N'ParkingId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Роль', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'UserParkingRoleModel', @level2type = N'COLUMN', @level2name = N'RoleId';

