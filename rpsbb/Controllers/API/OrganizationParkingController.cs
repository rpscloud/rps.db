﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

using ASE.Data;
using ASE.MVC;

using rps.Data;
using rpsbb.Code;
using Elmah;

namespace rpsbb.Controllers
{
    [Authorize]
    public class OrganizationParkingController : ApiControllerAdv<DB>
    {
        [System.Web.Http.HttpGet]
        public OrganizationParkingModelJson List(Guid id)
        {
            return db.OrganizationParkings.Find(id).Get();
        }

        [System.Web.Http.HttpGet]
        public IEnumerable<OrganizationParkingModelJson> List([FromUri] Guid? organizationId = null, [FromUri] Guid? parkingId = null)
        {
            var q = db.OrganizationParkings.Where(x => 
                !x.Deleted
                & !x.Deleted
                & !x.Organization.Deleted
                & !x.Parking.Deleted
                );
            if (organizationId.HasValue)
                q = q.Where(x => x.OrganizationId == organizationId.Value);
            if (parkingId.HasValue)
                q = q.Where(x => x.ParkingId == parkingId.Value);

            return q.ToList().Select(x => x.Get());
        }

        [System.Web.Http.HttpPost]
        public OrganizationParkingModelJson Add(OrganizationParkingModelJson model)
        {
            model.Id = Guid.Empty;
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPut]
        public OrganizationParkingModelJson Put(OrganizationParkingModelJson model)
        {
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPatch]
        public OrganizationParkingModelJson Patch(OrganizationParkingModelJson model)
        {
            return model.DALSave(db, true).Get();
        }

        [System.Web.Http.HttpDelete]
        public OrganizationParkingModelJson Delete([FromUri] OrganizationParkingModelJson model)
        {
            return model.DALDelete(db).Get();
        }
    }
}
