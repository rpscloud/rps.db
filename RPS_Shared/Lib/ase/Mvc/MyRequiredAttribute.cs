﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Resources;
using System.Web;

namespace ASE.MVC
{
    public class MyRequiredAttribute : RequiredAttribute
    {

        public MyRequiredAttribute()
        {

        }

        public override bool IsValid(object value)
        {
            if ((value is Guid) && ((Guid)value == Guid.Empty))
                return false;

            return base.IsValid(value);
        }
    }
}