﻿CREATE TABLE [dbo].[SystemVersion](
	[SysVersion] [int] NOT NULL,
	[STVer] [int] NOT NULL,
	[WebVer] [int] NOT NULL,
	[RackVer] [int] NOT NULL,
	[CMVer] [int] NOT NULL,
	[DbVer] [int] NOT NULL,
	[Marker] INT NULL, 
    [Date] DATETIME NULL, 
    [Comment] NVARCHAR(MAX) NULL, 
    [UpDateTime] DATETIME NULL, 
    [Sync] DATETIME NULL, 
    [Status] INT NULL, 
    CONSTRAINT [PK_SystemVersion] PRIMARY KEY CLUSTERED 
(
	[SysVersion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--Изминение тригеров для AlrmModel
CREATE TRIGGER [dbo].[SyncSystemVersion]
ON [dbo].[SystemVersion]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [SystemVersion] set Sync=@dt	where SysVersion in (select SysVersion from inserted)
	update SyncTables set [DateTime]=@dt where TableName='SystemVersion'
end