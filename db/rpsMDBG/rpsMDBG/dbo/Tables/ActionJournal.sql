﻿CREATE TABLE [dbo].[ActionJournal] (
    [Id]           UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [DataAfter]    VARCHAR (MAX)    NULL,
    [DataBefore]   VARCHAR (MAX)    NULL,
    [OperationId]  BIGINT           NOT NULL,
    [UserId]       UNIQUEIDENTIFIER NOT NULL,
    [_IsDeleted]   BIT              NOT NULL,
    [ActionNumber] INT              NULL,
    [ChangeTime]   DATETIME         NULL,
    [TableName]    VARCHAR (MAX)    NULL,
    [ColumnName]   VARCHAR (MAX)    NULL,
    [ObjectGUID]   UNIQUEIDENTIFIER NULL,
    [Type]         TINYINT          NULL,
    CONSTRAINT [PK_dbo.ActionJournal] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [Foreign_key7674] FOREIGN KEY ([OperationId]) REFERENCES [dbo].[Operation] ([Id])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ActionJournal', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Данные после изменения', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ActionJournal', @level2type = N'COLUMN', @level2name = N'DataAfter';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Данные до изменения', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ActionJournal', @level2type = N'COLUMN', @level2name = N'DataBefore';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Предмет изменения', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ActionJournal', @level2type = N'COLUMN', @level2name = N'OperationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Пользователь', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ActionJournal', @level2type = N'COLUMN', @level2name = N'UserId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Порядковый номер действия', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ActionJournal', @level2type = N'COLUMN', @level2name = N'ActionNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время изменения', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ActionJournal', @level2type = N'COLUMN', @level2name = N'ChangeTime';

