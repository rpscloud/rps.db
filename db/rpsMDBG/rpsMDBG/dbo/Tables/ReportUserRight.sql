﻿CREATE TABLE [dbo].[ReportUserRight] (
    [Id]   UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Name] VARCHAR (MAX)    NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

