﻿CREATE TABLE [dbo].[AlarmColor] (
    [Name] VARCHAR (MAX) NOT NULL,
    [Id]   INT           NOT NULL,
    CONSTRAINT [AlarmColor_pk] PRIMARY KEY CLUSTERED ([Id] ASC)
);

