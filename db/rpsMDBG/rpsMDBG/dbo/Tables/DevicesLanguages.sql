﻿CREATE TABLE [dbo].[DevicesLanguages]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [DeviceId] UNIQUEIDENTIFIER NOT NULL, 
    [Language] NCHAR(2) NOT NULL, 
    [Checked] BIT NOT NULL, 
    [Sync] DATETIME NULL, 
    CONSTRAINT [FK_DevicesLanguages_DeviceModel] FOREIGN KEY ([DeviceId]) REFERENCES [DeviceModel]([Id]) 
)

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Флаг синхронизации со 2ым ур.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DevicesLanguages', @level2type = N'COLUMN', @level2name = 'Sync';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Язык интерфейса', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DevicesLanguages', @level2type = N'COLUMN', @level2name = 'Language';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id устройства', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DevicesLanguages', @level2type = N'COLUMN', @level2name = 'DeviceId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Признак доступности языка в интерфейсе', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DevicesLanguages', @level2type = N'COLUMN', @level2name = 'Checked';

GO
CREATE TRIGGER [dbo].[SyncDevicesLanguages]
ON [dbo].[DevicesLanguages]
AFTER INSERT,UPDATE
AS
BEGIN
SET NOCOUNT ON;
declare @level int;
if(Exists(select top 1 CurrentLevel from ConfigSync))
begin
set @level=(select top 1 CurrentLevel from ConfigSync);
if(@level=1)
begin
declare @dt datetime;
set @dt=GetDate();
update [DevicesLanguages] set [Sync]=@dt	where Id in (select Id from inserted)
if(Exists(select top 1 Id from SyncTables where TableName='DevicesLanguages'))
begin
update SyncTables set [DateTime]=@dt where TableName='DevicesLanguages'
end
else
begin
insert into SyncTables (TableName,[DateTime])values('DevicesLanguages',@dt)
end
end
end
end

GO

CREATE UNIQUE INDEX [IX_DevicesLanguages_DeviceId_Language] ON [dbo].[DevicesLanguages] ([DeviceId],[Language])
