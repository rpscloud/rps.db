﻿CREATE TABLE [dbo].[BanknoteModel] (
    [Id]     INT           NOT NULL,
    [Name]   VARCHAR (MAX) NULL,
    [Value]  int NULL,
    [Code]   VARCHAR (MAX) NULL,
    [Length] VARCHAR (MAX) NULL,
    [Width]  VARCHAR (MAX) NULL,
    [EscrowData] INT NULL, 
    [DisableData] INT NULL, 
    CONSTRAINT [PK_dbo.BanknoteModel] PRIMARY KEY CLUSTERED ([Id] ASC)
);

