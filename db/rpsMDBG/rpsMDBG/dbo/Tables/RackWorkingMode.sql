﻿CREATE TABLE [dbo].[RackWorkingMode] (
    [Id]   INT           NOT NULL,
    [Name] VARCHAR (MAX) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'asdfg', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RackWorkingMode', @level2type = N'COLUMN', @level2name = N'Name';

