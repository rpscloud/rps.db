﻿CREATE TABLE [dbo].[Operation] (
    [Id]   BIGINT        NOT NULL,
    [Name] VARCHAR (MAX) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

