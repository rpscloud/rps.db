﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using ASE.Data;
using rps.Data;
using System.Data.Entity;
using System.Net;
using Elmah;

/*
using RMLib;
using Parking.Security;

using rps.Data;
using RMLib.Services;
using RMLib.Services.Wcf;
using Parking.Client;
using Parking.Workstation;
*/

namespace rps.Integration
{
    public class Synchronization
    {
        public static void Startup()
        {
            try
            {
                /*ClientWrapper.Invoke((c) =>
                {
                    ICardClient cc = c.GetConnected<ICardClient>();
                    cc.LoadItems();
                    var x = cc.Count;
                    var z = cc.Count;
                    IZoneClient zc = c.GetService<IZoneClient>();
                    var t = zc.Count;
                    //SetMessage(0, cc.Count.ToString());

                    /*IZoneClient zc = c.GetService<IZoneClient>();
                    zc.LoadItems(); //загрузка зон в кэш клиента

                    //пример использования - добавление устройства
                    Zone z = zc.Items.FirstOrDefault(); //новому устройству нужна зона
                    if (z == null)
                        return;

                    IDeviceClient dc = c.GetService<IDeviceClient>();

                    //устанавливаем свойства
                    Device d = Device.Create(DeviceType.EntryStand);
                    d.NetworkAddress = dc.Items.Max(i => i.NetworkAddress) + 1; //уникальный адрес для внутренней сети
                    d.Name = String.Format("Въезд #{0}", MathHelper.Rnd.Next());
                    d.DisplayZoneID = z.ID;

                    Stand s = (Stand)d; //у въезда обязательно должна быть зона после, у выезда - зона до
                    //if (d.Type == DeviceType.EntryStand)
                    //{
                    s.ZoneBeforeID = Zone.Outer.ID;
                    s.ZoneAfterID = z.ID;
                    //}
                    //else if (d.Type.IsExitStand())
                    //{
                    //  s.ZoneBeforeID = z.ID;
                    //  s.ZoneAfterID = Zone.Outer.ID;
                    //}

                    Device newDevice = dc.Add(d);
                    SetMessage(newDevice.ID, newDevice.Name);
                });*/
            }
            catch
            {

            }
        }

        private static readonly Object forLock = new Object();

        public static void Start()
        {
            try
            {
                ErrorLog.GetDefault(null).Log(new Error(new Exception("Synchronization.Start()")));

                if (System.Configuration.ConfigurationManager.AppSettings["MC"] != null)
                {
                    List<Guid> ids = new List<Guid>();
                    var dt = DateTime.Now.AddSeconds(-15);
                    DB db = new DB();
                    foreach(var parking in db.Parkings.Where(x => x.ChangedDateTime > dt).ToList())
                        if (ids.IndexOf(parking.Id) == -1)
                            ids.Add(parking.Id);

                    foreach (var parking in db.OrganizationParkings.Where(x => x.ChangedDateTime > dt).Select(x => x.ParkingId).ToList())
                        if (ids.IndexOf(parking) == -1)
                            ids.Add(parking);

                    foreach (var parking in db.OrganizationParkings.Where(x => x.Organization.ChangedDateTime > dt).Select(x => x.ParkingId).ToList())
                        if (ids.IndexOf(parking) == -1)
                            ids.Add(parking);

                    foreach (Guid id in ids)
                        Start(id);

                    if (ids.Count == 0)
                        ErrorLog.GetDefault(null).Log(new Error(new Exception("Synchronization.Start() No ids")));
                }
                else if (System.Configuration.ConfigurationManager.AppSettings["Site"] != null)
                {
                    using (var wc = new WebClient())
                    {
                        var url = System.Configuration.ConfigurationManager.AppSettings["Site"].ToString() + "/Root/Sync/" + System.Configuration.ConfigurationManager.AppSettings["Id"].ToString();
                        wc.DownloadString(url);
                    }
                }
            }
            catch(Exception exc)
            {
                ErrorLog.GetDefault(null).Log(new Error(exc));
            }
        }

        public static void Start(Guid id)
        {
            ErrorLog.GetDefault(null).Log(new Error(new Exception("Sync start: " + id)));

            bool lockWasTaken = false;
            try
            {
                Monitor.Enter(forLock, ref lockWasTaken);

                if (lockWasTaken)
                {
                    InternalStart(id);
                }
            }
            finally
            {
                if (lockWasTaken)
                {
                    Monitor.Exit(forLock);
                }
            }

            return;
        }

        private static void UpdateState(DB db1, DB db2, ISyncTraking model1, ISyncTraking model2)
        {
            if (model2 == null)
                db2.Entry(model1).State = EntityState.Added;
            else if ((!model1.ChangedDateTime.HasValue) & (!model2.ChangedDateTime.HasValue))
                return;
            else if ((model1.ChangedDateTime > model2.ChangedDateTime) || (model2.ChangedDateTime == null))
                db2.Entry(model1).State = System.Data.Entity.EntityState.Modified;
            //else if ((model1.ChangedDateTime < model2.ChangedDateTime) || (model1.ChangedDateTime == null))
                //db1.Entry(model2).State = System.Data.Entity.EntityState.Modified;
        }

        private static void Sync<T>(DB db1, DB db2, List<T> list1, List<T> list2) where T : ISyncTraking
        {
            foreach (var item in list1)
            {
                var item2 = list2.FirstOrDefault(x => x.Id == item.Id);
                UpdateState(db1, db2, item, item2);
            }
            foreach (var item in list2)
            {
                var item2 = list1.FirstOrDefault(x => x.Id == item.Id);
                UpdateState(db2, db1, item, item2);
            }
        }

        private static void InternalStart(Guid id)
        {
            DB dbL = new DB();
            DB dbR = null;
            var parkingL = dbL.Parkings.Find(id);
            if (parkingL == null)
                return;

            if (String.IsNullOrEmpty(parkingL.S2Sql) | String.IsNullOrEmpty(parkingL.S2Db) | String.IsNullOrEmpty(parkingL.S2User) | String.IsNullOrEmpty(parkingL.S2Password))
                return;

            try
            {
                dbR = new DB(String.Format("data source={0};Initial Catalog={1};User ID={2};Password={3}", parkingL.S2Sql, parkingL.S2Db, parkingL.S2User, parkingL.S2Password));
            }
            catch(Exception exc)
            {
                ErrorLog.GetDefault(null).Log(new Error(exc));
                return;
            }

            dbR.Entry(parkingL).State = System.Data.Entity.EntityState.Detached;

            var tranL = dbL.Database.BeginTransaction();
            var tranR = dbR.Database.BeginTransaction();
            try
            {
                //Parking
                var parkingR = dbR.Parkings.Find(id);
                dbL.Entry(parkingL).State = System.Data.Entity.EntityState.Detached;
                if (parkingR != null)
                    dbR.Entry(parkingR).State = System.Data.Entity.EntityState.Detached;

                UpdateState(dbL, dbR, parkingL, parkingR);
                UpdateState(dbR, dbL, parkingR, parkingL);
                dbR.SaveChanges();
                dbL.SaveChanges();

                //Organization
                Sync(dbL, dbR,
                     dbL.OrganizationParkings.Where(x => x.ParkingId == parkingL.Id).Select(x => x.Organization).AsNoTracking().ToList(), 
                     dbR.OrganizationParkings.Where(x => x.ParkingId == parkingL.Id).Select(x => x.Organization).AsNoTracking().ToList());
                dbR.SaveChanges();
                dbL.SaveChanges();

                //OrganizationParking
                Sync(dbL, dbR, 
                    dbL.OrganizationParkings.Where(x => x.ParkingId == parkingL.Id).AsNoTracking().ToList(),
                    dbR.OrganizationParkings.Where(x => x.ParkingId == parkingL.Id).AsNoTracking().ToList()
                    );
                dbR.SaveChanges();
                dbL.SaveChanges();

                //Users
                Sync(dbL, dbR,
                     dbL.UserParkingRoles.Where(x => x.ParkingId == parkingL.Id).Select(x => x.User).AsNoTracking().ToList(),
                     dbR.UserParkingRoles.Where(x => x.ParkingId == parkingL.Id).Select(x => x.User).AsNoTracking().ToList());
                dbR.SaveChanges();
                dbL.SaveChanges();

                //Right
                Sync(dbL, dbR,
                     dbL.Rights.AsNoTracking().ToList(),
                     dbR.Rights.AsNoTracking().ToList());
                dbR.SaveChanges();
                dbL.SaveChanges();

                //Role
                Sync(dbL, dbR,
                     dbL.Roles.Where(x => x.ParkingId == parkingL.Id).AsNoTracking().ToList(),
                     dbR.Roles.Where(x => x.ParkingId == parkingL.Id).AsNoTracking().ToList());
                dbR.SaveChanges();
                dbL.SaveChanges();

                //UserParkingRole
                Sync(dbL, dbR,
                    dbL.UserParkingRoles.Where(x => x.ParkingId == parkingL.Id).AsNoTracking().ToList(),
                    dbR.UserParkingRoles.Where(x => x.ParkingId == parkingL.Id).AsNoTracking().ToList()
                    );
                dbR.SaveChanges();
                dbL.SaveChanges();

                tranL.Commit();
                tranR.Commit();
            }
            catch(Exception exc)
            {
                tranL.Rollback();
                tranR.Rollback();

                ErrorLog.GetDefault(null).Log(new Error(exc));
            }
        }
    }
}
