﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

using ASE.Data;
using ASE.MVC;

using rps.Data;
using rpsbb.Code;

namespace rpsbb.Controllers
{
    [Authorize]
    public class OrganizationController : ApiControllerAdv<DB>
    {
        [System.Web.Http.HttpGet]
        public OrganizationModelJson Get(Guid id)
        {
            return db.Organizations.Find(id).Get();
        }

        [System.Web.Http.HttpGet]
        public IEnumerable<OrganizationModelJson> List()
        {
            var q = db.Organizations.Where(x => !x.Deleted);
            if ((UserModel != null) && (!UserModel.Is_Admin))
            {
                var p =  db.UserParkingRoles.Where(x => !x.Deleted & !x.Parking.Deleted & x.UserId == UserId).Select(x => x.ParkingId);
                q = db.OrganizationParkings.Where(x => !x.Deleted & !x.Parking.Deleted & p.Contains(x.ParkingId)).Select(x => x.Organization);
            }

            return q.ToList().Select(x => x.Get());
        }

        [System.Web.Http.HttpPost]
        public OrganizationModelJson Add(OrganizationModelJson model)
        {
            model.Id = Guid.Empty;
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPut]
        public OrganizationModelJson Put(OrganizationModelJson model)
        {
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPatch]
        public OrganizationModelJson Patch(OrganizationModelJson model)
        {
            return model.DALSave(db, true).Get();
        }

        [System.Web.Http.HttpDelete]
        public OrganizationModelJson Delete([FromUri] OrganizationModelJson model)
        {
            return model.DALDelete(db).Get();
        }
    }
}
