﻿CREATE TABLE [dbo].[CommandDevice]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [DeviceId] UNIQUEIDENTIFIER NOT NULL, 
    [Command] NVARCHAR(MAX) NULL, 
    [Sync] DATETIME NULL, 
    [Status] INT NOT NULL
)
go
CREATE TRIGGER [dbo].[SyncCommandDevice]
ON [dbo].[CommandDevice]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [CommandDevice] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='CommandDevice'
end
