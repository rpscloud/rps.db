﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ASE.MVC
{
    public static class BasicHelpers
    {

        public static MvcHtmlString IF<TProperty>(this HtmlHelper helper, Expression<Func<TProperty>> condition, object onNull, object onTrue, object onFalse)
        {
            object r;
            try
            {
                r = condition.Compile().Invoke();
            }
            catch
            {
                r = null;
            }

            if ((r == null) | (!(r is bool)))
                return new MvcHtmlString(onNull.ToString());

            if ((bool)r)
                return new MvcHtmlString(onTrue.ToString());

            if (!(bool)r)
                return new MvcHtmlString(onFalse.ToString());

            return new MvcHtmlString("");
        }

        public static MvcHtmlString IF(this HtmlHelper helper, bool? condition, object onNull, object onTrue, object onFalse)
        {
            if (!condition.HasValue)
                return new MvcHtmlString(onNull.ToString());

            if ((bool)condition)
                return new MvcHtmlString(onTrue.ToString());

            if (!(bool)condition)
                return new MvcHtmlString(onFalse.ToString());

            return new MvcHtmlString("");
        }

        public static MvcHtmlString NULL<TProperty>(this HtmlHelper helper, Expression<Func<TProperty>> expression, object onNull)
        {
            object r;
            try
            {
                r = expression.Compile().Invoke();
            }
            catch
            {
                r = null;
            }

            if (r == null)
                return new MvcHtmlString(onNull.ToString());

            return new MvcHtmlString(r.ToString());
        }

        public static MvcHtmlString Div(this HtmlHelper helper, object attrs)
        {
            return Tag(helper, "div", attrs, null);
        }

        public static MvcHtmlString Div(this HtmlHelper helper, object attrs, params dynamic[] inner)
        {
            return Tag(helper, "div", attrs, inner);
        }

        public static MvcHtmlString Tag(this HtmlHelper helper, string tag, object attrs, params dynamic[] inner)
        {
            TagBuilder tagAll = new TagBuilder(tag);
            RouteValueDictionary rvdAttrs = new RouteValueDictionary(attrs);
            foreach (var key in rvdAttrs.Keys)
                tagAll.Attributes.Add(key, rvdAttrs[key].ToString());

            if (inner != null)
                foreach (var item in inner)
                    tagAll.InnerHtml += item.ToString();

            return new MvcHtmlString(tagAll.ToString());
        }

        public static IDisposable TagBegin(this HtmlHelper helper, string tag, object attrs, params dynamic[] inner)
        {
            TagBuilder tagAll = new TagBuilder(tag);
            RouteValueDictionary rvdAttrs = new RouteValueDictionary(attrs);
            foreach (var key in rvdAttrs.Keys)
                tagAll.Attributes.Add(key, rvdAttrs[key].ToString());

            helper.ViewContext.Writer.Write(tagAll.ToString(TagRenderMode.StartTag));

            if (inner != null)
                foreach (var item in inner)
                    tagAll.InnerHtml += item.ToString();

            return new TagTerminator(helper, tag);
        }
    }

    public class TagTerminator : IDisposable
    {
        readonly HtmlHelper helper;
        readonly string terminator;

        public TagTerminator(HtmlHelper helper, string terminator)
        {
            this.helper = helper;
            this.terminator = terminator;
        }

        public void Dispose()
        {
            helper.ViewContext.Writer.Write(terminator);
        }
    }
}