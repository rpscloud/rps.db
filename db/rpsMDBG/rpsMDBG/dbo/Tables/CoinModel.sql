﻿CREATE TABLE [dbo].[CoinModel] (
    [Id]    INT           NOT NULL,
    [Name]  VARCHAR (MAX) NULL,
    [Value] int NULL,
    [Code]  VARCHAR (MAX) NULL,
	[DisableData] int NULL,
    CONSTRAINT [PK_dbo.CoinModel] PRIMARY KEY CLUSTERED ([Id] ASC)
);

