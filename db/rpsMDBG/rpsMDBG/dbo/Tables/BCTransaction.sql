CREATE TABLE [dbo].[BCTransaction] (
    [Id]               UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [BarCodeInUseId]   UNIQUEIDENTIFIER NULL,
    [OperId]           UNIQUEIDENTIFIER NULL,
    [CompanyId]        UNIQUEIDENTIFIER NULL,
    [IdTP]             TINYINT          NULL,
    [IdTS]             TINYINT          NULL,
    [DiscountAmount]   INT              NULL,
    [GroupIdFC]        TINYINT          NULL,
    [UsageTime]        DATETIME         DEFAULT (getdate()) NULL,
    [Sync]             DATETIME         NULL,
    [GuestPlateNumber] NVARCHAR (MAX)   NULL,
	[GuestCarModel]    NVARCHAR (MAX)   NULL,
    [GuestName]        NVARCHAR (MAX)   NULL,
    [GuestEmail]       NVARCHAR (MAX)   NULL,
    [ParkingTime]      INT              NULL,
    [CreatedAt]        DATETIME         NULL,
    [EnterId]          UNIQUEIDENTIFIER NULL,
    [ExitId]           UNIQUEIDENTIFIER NULL,
    [IdTPForCount]     UNIQUEIDENTIFIER NULL,
    [IdTSForCount]     UNIQUEIDENTIFIER NULL,
    [CardId]           BIGINT           NULL,
    [Ammount]          INT              NULL,
    [Used]				BIT				NULL, 
    [ActivatedAt]		DATETIME		NULL,
    [IdBCM]            UNIQUEIDENTIFIER NULL,
    [Comment]          NVARCHAR (MAX)   NULL,
    [ParkingEnterTime] DATETIME         NULL,
	[TPForRent] UNIQUEIDENTIFIER NULL, 
    [TSForRent] UNIQUEIDENTIFIER NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [BCTransaction_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[CompanyModel] ([Id]),
    CONSTRAINT [BCTransaction_Group] FOREIGN KEY ([GroupIdFC]) REFERENCES [dbo].[Group] ([idFC]),
    CONSTRAINT [BCTransaction_Tariffs] FOREIGN KEY ([IdTP]) REFERENCES [dbo].[Tariffs] ([IdFC]),
    CONSTRAINT [BCTransaction_TariffScheduleModel] FOREIGN KEY ([IdTS]) REFERENCES [dbo].[TariffScheduleModel] ([IdFC])
);


GO
ALTER TABLE [dbo].[BCTransaction] NOCHECK CONSTRAINT [BCTransaction_Tariffs];


GO
ALTER TABLE [dbo].[BCTransaction] NOCHECK CONSTRAINT [BCTransaction_TariffScheduleModel];




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = 'Sync';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время активации штрихкода', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'UsageTime';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Группа для BCF=3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'GroupIdFC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма для BCF=2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'DiscountAmount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТC для BCF=1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'IdTS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТП для BCF=1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'IdTP';


GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Компания - заказчик штрихкода', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'CompanyId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Оператор - заказчик штрихкода', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'OperId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Штрихкод Id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'BarCodeInUseId';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Гос номер', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'GuestPlateNumber';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id транзакции въезда', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'EnterId';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id транзакции выезда', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'ExitId';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТП для расчета задолженности арендатора перед УК', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'IdTPForCount';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТС для расчета задолженности арендатора перед УК', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'IdTSForCount';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Номер выданной по штрих коду карты', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'CardId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма задолженности арендатора перед УК', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'Ammount';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время активации', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'ActivatedAt';

GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТП для расчета задолженности арендатора перед УК, если есть места', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'TPForRent';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТС для расчета задолженности арендатора перед УК, если есть места', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BCTransaction', @level2type = N'COLUMN', @level2name = N'TSForRent';

GO
CREATE TRIGGER [dbo].[SyncBCTransaction]
ON [dbo].[BCTransaction]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [BCTransaction] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='BCTransaction'
-- 	declare @level int;
-- 
-- 	if(Exists(select top 1 CurrentLevel from ConfigSync))
-- 	begin
-- 		set @level=(select top 1 CurrentLevel from ConfigSync);
-- 		if(@level=2)
-- 		begin
-- 			update BarCodeInUse set 
--         OperId=i.OperId,
--         CompanyId=i.CompanyId,
--         IdTP=i.IdTP,
--         IdTS=i.IdTS,
--         DiscountAmount=i.DiscountAmount,
--         GroupIdFC=i.GroupIdFC,
--         UsageTime=i.UsageTime,
--         Sync=i.Sync,
--         GuestPlateNumber=i.GuestPlateNumber,
-- 		GuestCarModel=i.GuestCarModel,
--         GuestName=i.GuestName,
--         GuestEmail=i.GuestEmail,
--         ParkingTime=i.ParkingTime,
--         CreatedAt=i.CreatedAt,
--         EnterId=i.EnterId,
--         ExitId=i.ExitId,
--         IdTPForCount=i.IdTPForCount,
--         IdTSForCount=i.IdTSForCount,
--         CardId=i.CardId,
--         Ammount=i.Ammount,
--         Used=i.Used,
--         ActivatedAt=i.ActivatedAt,
-- 		TPForRent=i.TPForRent,
--         TSForRent=i.TSForRent
--         
-- 			from BarCodeInUse c,inserted i
-- 			where c.Id=i.BarCodeInUseId;
-- 
-- 			
-- 		end
-- 	end
end
GO

CREATE TRIGGER [dbo].[BCTransaction_insert]
  ON [dbo].[BCTransaction]
FOR INSERT
AS
  BEGIN
    SET NOCOUNT ON;
    declare @level int;
    if(Exists(select top 1 CurrentLevel from ConfigSync))
      begin
        set @level=(select top 1 CurrentLevel from ConfigSync);
        if(@level=2)
          begin
            insert into Command(Status,TypePacket,DataPacket,DateCreate)
              select 1,'InsertBCTransactions',Id,GETDATE() from inserted

          end
      end
  END
GO


