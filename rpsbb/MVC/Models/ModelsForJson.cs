﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using ASE.MVC;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ASE.Data;

namespace rps.Data
{
    public static partial class Model2Json
    {
        public static object CopySV(object from, object to, params string[] skip)
        {
            return CopyInt(from, to, true, skip);
        }

        public static object Copy(object from, object to, params string[] skip)
        {
            return CopyInt(from, to, false, skip);
        }

        public static object CopyInt(object from, object to, bool skipVirtual, params string[] skip)
        {
            foreach (var prop in from.GetType().GetProperties())
                if (skip.FirstOrDefault(x => x == prop.Name) == null)
                {
                    var pi = to.GetType().GetProperty(prop.Name);
                    if (pi == null)
                        continue;

                    if (skipVirtual) 
                        if (pi.GetGetMethod().IsVirtual)
                            continue;

                    pi.SetValue(to, from.GetType().GetProperty(prop.Name).GetValue(from));
                }

            return to;
        }

        static object Copy(object from, object to, bool patch, params string[] skip)
        {
            if (to is ISyncTraking)
                (to as ISyncTraking).ChangedDateTime = DateTime.Now;

            foreach (var prop in to.GetType().GetProperties())
            {
                if (skip.FirstOrDefault(x => x == prop.Name) != null)
                    continue;

                var pi = from.GetType().GetProperty(prop.Name);
                if (pi == null)
                    continue;
                if (pi.PropertyType.GetInterface("ICollection`1") != null)
                    continue;

                var val = pi.GetValue(from);
                if (patch)
                {
                    if (val == null)
                        continue;

                    if ((val is Guid) && ((Guid)val == Guid.Empty))
                        continue;
                }

                to.GetType().GetProperty(prop.Name).SetValue(to, val);
            }

            return to;
        }


        /// <summary>
        /// OrganizationModelJson
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static OrganizationModelJson Get(this OrganizationModel model)
        {
            var ret = (OrganizationModelJson) Copy(model, new OrganizationModelJson());
            ret.Parkings = ContextPerRequest.Db.OrganizationParkings.Where(x => 
                x.OrganizationId == ret.Id 
                & !x.Deleted 
                & !x.Organization.Deleted
                & !x.Parking.Deleted
                ).Select(x => new OrganizationParkingModelJson { Id = x.Id, OrganizationId = x.OrganizationId, ParkingId = x.ParkingId }).ToList();

            return ret;
        }

        /// <summary>
        /// OrganizationModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static OrganizationModel Get(this OrganizationModelJson model)
        {
            var ret = (OrganizationModel)Copy(model, new OrganizationModel());

            return ret;
        }

        /// <summary>
        /// ParkingModelJson
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ParkingModelJson Get(this ParkingModel model)
        {
            var ret = (ParkingModelJson)Copy(model, new ParkingModelJson());
            ret.Users = ContextPerRequest.Db.UserParkingRoles.Where(x => 
                x.ParkingId == ret.Id 
                & !x.Deleted
                & !x.Parking.Deleted
                & !x.User.Deleted
                ).Select(x => new UserParkingRoleModelJson { Id = x.Id, ParkingId = x.ParkingId, UserId = x.UserId }).ToList();
            ret.Organizations = ContextPerRequest.Db.OrganizationParkings.Where(x => x.ParkingId == ret.Id & !x.Deleted).Select(x => new OrganizationParkingModelJson { Id = x.Id, OrganizationId = x.OrganizationId, ParkingId = x.ParkingId }).ToList();

            return ret;
        }

        /// <summary>
        /// UserModelJson
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static UserModelJson Get(this UserModel model)
        {
            var ret = (UserModelJson)Copy(model, new UserModelJson());
            ret.Parkings = ContextPerRequest.Db.UserParkingRoles.Where(x => 
                x.UserId == ret.Id 
                & !x.Deleted
                ).Select(x => new UserParkingRoleModelJson { Id = x.Id, ParkingId = x.ParkingId, UserId = x.UserId }).ToList();

            return ret;
        }

        /// <summary>
        /// OrganizationParkingModelJson
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static OrganizationParkingModelJson Get(this OrganizationParkingModel model)
        {
            var ret = (OrganizationParkingModelJson)Copy(model, new OrganizationParkingModelJson(), "Parking", "Organization");

            return ret;
        }

        /// <summary>
        /// UserParkingRoleModelJson
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static UserParkingRoleModelJson Get(this UserParkingRoleModel model)
        {
            var ret = (UserParkingRoleModelJson)Copy(model, new UserParkingRoleModelJson(), "Parking", "Role", "User");

            return ret;
        }

        /// <summary>
        /// RightModelJson
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static RightModelJson Get(this RightModel model)
        {
            var ret = (RightModelJson)Copy(model, new RightModelJson(), "Parent");

            return ret;
        }

        private static void RoleModelActive(List<RightModel> list, RoleModel model)
        {
            if (list != null)
                foreach(var item in list)
                {
                    item.Active = false;
                    var m = model.RightsInRole.FirstOrDefault(z => z.RightId == item.Id);
                    if (m != null)
                    {
                        item.Active = m.Active;
                        RoleModelActive(item.Children, model);
                    }
                }
        }


        /// <summary>
        /// RoleModelJson
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static RoleModelJson Get(this RoleModel model)
        {
            var ret = (RoleModelJson)Copy(model, new RoleModelJson(), "Rights");
            ret.Rights = ContextPerRequest.Db.Rights.Where(x => x.ParentId == null).OrderBy(x => x.Name).ToList();
            if (model.RightsInRole != null)
            {
                foreach (var item in ret.Rights)
                {
                    item.Active = false;
                    var m = model.RightsInRole.FirstOrDefault(z => z.RightId == item.Id);
                    if (m != null)
                        item.Active = m.Active;
                }
                ret.Rights.ForEach(x => RoleModelActive(x.Children, model));
            }

            return ret;
        }

        /// <summary>
        /// RightsInRoleModelJson
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static RightsInRoleModelJson Get(this RightsInRoleModel model)
        {
            var ret = (RightsInRoleModelJson)Copy(model, new RightsInRoleModelJson(), "Parent", "Role");

            return ret;
        }

        public static MenuItemModelJson Get(this MenuItemModel model)
        {
            var ret = (MenuItemModelJson)Copy(model, new MenuItemModelJson());

            return ret;
        }
    }

    [NotMapped]
    public class OrganizationModelJson : OrganizationModel
    {
        public List<OrganizationParkingModelJson> Parkings { get; set; }

    }

    [NotMapped]
    public class OrganizationParkingModelJson : OrganizationParkingModel
    {
    }

    [NotMapped]
    public class ParkingModelJson : ParkingModel
    {
        public List<OrganizationParkingModelJson> Organizations { get; set; }

        public List<UserParkingRoleModelJson> Users { get; set; }
    }

    [NotMapped]
    public class UserParkingRoleModelJson : UserParkingRoleModel
    {
    }

    [NotMapped]
    public class UserModelJson : UserModel
    {
        public List<UserParkingRoleModelJson> Parkings { get; set; }
    }

    [NotMapped]
    public class RightModelJson : RightModel
    {
    }

    [NotMapped]
    public class RoleModelJson : RoleModel
    {
        public List<RightModel> Rights { get; set; }
    }

    [NotMapped]
    public class RightsInRoleModelJson : RightsInRoleModel
    {
    }

    [NotMapped]
    public class MenuItemModelJson : MenuItemModel
    {
    }

    [NotMapped]
    public class UserOrganizationModelJson
    {
        public Guid OrganizationId { get; set; }

        public Guid UserId { get; set; }        
    }
}
