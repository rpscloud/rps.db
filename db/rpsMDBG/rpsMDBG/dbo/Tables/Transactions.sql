﻿CREATE TABLE [dbo].[Transactions] (
    [Id]                       UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [DeviceId]                 UNIQUEIDENTIFIER NOT NULL,
    [Time]                     DATETIME         NOT NULL,
    [TimeEntry]                DATETIME         NOT NULL,
    [TimeExit]                 DATETIME         NULL,
    [ZoneAfterId]              BIGINT           NOT NULL,
    [ZoneBeforeId]             BIGINT           NOT NULL,
    [_IsDeleted]               BIT              NOT NULL,
    [Sync]                     DATETIME         NULL,
    [DeviceTypeID]             INT              NULL,
    [Cardnumber]               VARCHAR (MAX)    NULL,
    [ClientID]                 UNIQUEIDENTIFIER NULL,
    [TarifPlanId]              UNIQUEIDENTIFIER NULL,
    [TariffScheduleId]         UNIQUEIDENTIFIER NULL,
    [Nulltime1]                DATETIME         NULL,
    [Nulltime2]                DATETIME         NULL,
    [Nulltime3]                DATETIME         NULL,
    [TVP]                      DATETIME         NULL,
    [TKVP]                     INT              NULL,
    [SumOnCardBefore]          MONEY            NULL,
    [Amount]                   MONEY            NULL,
    [Balance]                  MONEY            NULL,
    [Paid]                     MONEY            NULL,
    [SumOnCardAfter]           MONEY            NULL,
    [SumAccepted]              MONEY            NULL,
    [ClientGroupId]            UNIQUEIDENTIFIER NULL,
    [ClientTypidFC]            INT              NULL,
    [ZoneId]                   UNIQUEIDENTIFIER NULL,
    [LastRecountTime]          DATETIME         NULL,
    [TimeOplat]                DATETIME         NULL,
    [PassageTransactionType]   INT              NULL,
    [PlateNumberEntrance]      VARCHAR (MAX)    NULL,
    [PlateNumberExit]          VARCHAR (MAX)    NULL,
    [PaymentTransactionTypeId] INT              NULL,
    [ChangeIssued]             MONEY            NULL,
    [CachDispenerUp]           INT              NULL,
    [CachDispenerDown]         INT              NULL,
    [Hopper1]                  INT              NULL,
    [Hopper2]                  INT              NULL,
    [BanknotesAccepted]        INT              NULL,
    [CoinsAccepted]            INT              NULL,
    [BankCardAccepted]         MONEY            NULL,
    [Tariff]                   UNIQUEIDENTIFIER NULL,
    [SheduleSwitchTariff]      UNIQUEIDENTIFIER NULL,
    [SheduleReturnTariff]      UNIQUEIDENTIFIER NULL,
    [PoradNumber]              INT              NULL,
    CONSTRAINT [PK_dbo.Transactions] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Transactions_dbo.DeviceModel_DeviceId] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[DeviceModel] ([Id]),
    CONSTRAINT [Transactions_fk] FOREIGN KEY ([ZoneId]) REFERENCES [dbo].[ZoneModel] ([Id]),
    CONSTRAINT [Transactions_fk2] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[ClientModel] ([Id]),
    CONSTRAINT [Transactions_fk3] FOREIGN KEY ([ClientGroupId]) REFERENCES [dbo].[Group] ([Id]),
    CONSTRAINT [Transactions_fk4] FOREIGN KEY ([PaymentTransactionTypeId]) REFERENCES [dbo].[PaymentTransactionType] ([Id]),
    CONSTRAINT [Transactions_fk5] FOREIGN KEY ([PassageTransactionType]) REFERENCES [dbo].[PassageTransactionType] ([Id])
);






GO

CREATE NONCLUSTERED INDEX [IX_DeviceId]
    ON [dbo].[Transactions]([DeviceId] ASC);
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID устройства', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'DeviceId';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время трансакции', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'Time';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время въезда на парковку', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'TimeEntry';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время выезда с парковки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'TimeExit';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Зона после', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'ZoneAfterId';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Зона до', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'ZoneBeforeId';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Флаг синхронизации со вторым уровнем.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'Sync';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип устройства', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'DeviceTypeID';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Номер карты', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'Cardnumber';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наименование клиента', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'ClientID';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Номер текущего тарифного плана.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'TarifPlanId';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID ТС (тарифной сетки)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'TariffScheduleId';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время обнуления ВП', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'Nulltime1';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Вермя обнуления КВП', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'Nulltime2';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Верям обнуления абонемента (окончания)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'Nulltime3';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Текущее время за период', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'TVP';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Текущее количество въездов за период', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'TKVP';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма на карте - задолженность', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = 'Balance';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Оплачено', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = 'Paid';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Группа клиента ( для СКУД)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'ClientGroupId';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип клиента: 0 - разовый, 1 - постоянный (1 бит на карте)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'ClientTypidFC';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Зона на карте', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'ZoneId';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время въезда в зону (пересчета)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'LastRecountTime';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время оплаты', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'TimeOplat';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип трансакции проезда', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'PassageTransactionType';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Гос номер на въезде', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'PlateNumberEntrance';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Гос номер на вызде', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'PlateNumberExit';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип трансакции оплаты', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'PaymentTransactionTypeId';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма выданной сдачи', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'ChangeIssued';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Кол-во купюр выданных из верхней кассеты дисп', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'CachDispenerUp';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Кол-во купюр выданных из нижней кассеты дисп', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'CachDispenerDown';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Кол-во монет выданных из 1ого хоппера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'Hopper1';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Кол-во монет выданных из 2ого хоппера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'Hopper2';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Принято купюр', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'BanknotesAccepted';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Принято монет', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'CoinsAccepted';
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Принято черзе БК', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'BankCardAccepted';
GO

CREATE TRIGGER [dbo].[SyncTransactions]
ON [dbo].[Transactions]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [Transactions] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='Transactions'
end

GO
GO

--Изминение триггера Transactions
CREATE TRIGGER [dbo].[PoradNumberTransactions]
ON [dbo].[Transactions]
AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;
	declare @level int;
	if(Exists(select top 1 CurrentLevel from ConfigSync))
	begin
		set @level=(select top 1 CurrentLevel from ConfigSync);
		if(@level=2)
		begin
			declare @id uniqueidentifier
			declare @curmax int
			set @curmax=(select Max(PoradNumber) from Transactions where PoradNumber is not null)
			--set @curmax=ISNULL(@curmax,1)
			if(@curmax is null)
			begin
				set @curmax=1
			end
			else
			begin
				set @curmax=@curmax+1
			end
			update Transactions set PoradNumber=@curmax where Id in (select Id from inserted)

			insert into Command(Status,TypePacket,DataPacket,DateCreate)
			select 1,'InsertTransactions',Id,GETDATE() from inserted

		end
	end
END

go

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма на карте до оплаты', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Transactions', @level2type = N'COLUMN', @level2name = N'SumOnCardBefore';


GO
CREATE TRIGGER [dbo].[AddToHourParking]
	ON [dbo].[Transactions]
	AFTER INSERT
AS BEGIN
	declare @dt datetime;
	declare @date_in datetime;
	declare @date_exit datetime;
	declare @new_id int;
	declare @last_id int;
	declare @cardnumber varchar(max);
	declare @client_id uniqueidentifier;
	declare @id uniqueidentifier;
	declare @minutes int;
	declare @last_date datetime;

	set @dt=GetDate();
	select @date_in    = i.TimeEntry from inserted i;
	select @date_exit  = i.TimeExit from inserted i;
	select @cardnumber = i.Cardnumber from inserted i;
	select @client_id  = i.ClientID from inserted i;
	select @id  = i.Id from inserted i;


	if (DATEDIFF(MINUTE,DATEFROMPARTS(2000,1,1),@date_in)>0 and (@date_exit IS NULL OR DATEDIFF(MINUTE,DATEFROMPARTS(2000,1,1),@date_exit)=0) AND @cardnumber IS NOT NULL) begin
		insert into [dbo].[TransactionHoursParking](Minutes, DateCreated, ClientId, CardNumber, EnterId) 
		VALUES (0, @date_in, @client_id, @cardnumber, @id);
	end

	if (DATEDIFF(MINUTE,DATEFROMPARTS(2000,1,1),@date_exit)>0 AND @cardnumber IS NOT NULL) begin
		SELECT TOP 1 @last_id = [Id], @last_date = [DateCreated]
		from [dbo].[TransactionHoursParking]
		where [Cardnumber] = @cardnumber and [ExitId] IS NULL
		order by [DateCreated] desc;

		SELECT @minutes = DATEDIFF(minute, @last_date, @date_exit);

		UPDATE [dbo].[TransactionHoursParking] SET DateClosed = @date_exit, Minutes = @minutes, ExitId = @id
		where [Id] = @last_id;
	end
END