﻿CREATE TABLE [dbo].[TariffPlanTariffScheduleModel] (
    [Id]                                  UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [TariffScheduleId]                    UNIQUEIDENTIFIER NOT NULL,
    [TariffPlanId]                        UNIQUEIDENTIFIER NOT NULL,
    [TariffPlanNextId]                    UNIQUEIDENTIFIER NULL,
    [TypeId]                              INT              NOT NULL,
    [_IsDeleted]                          BIT              NOT NULL,
    [Sync]                                DATETIME         NULL,
    [ConditionZoneAfter]                  UNIQUEIDENTIFIER NULL,
    [ConditionParkingTime]                INT              NULL,
    [ConditionParkingTimeTypeId]          INT              NULL,
    [ConditionEC]                         INT              NULL,
    [ConditionECProtectInterval]          INT              NULL,
    [ConditionECProtectIntervalTimeTypId] INT              NULL,
    [ConditionAbonementPrice]             INT              NULL,
    [ConditionBackTime]                   INT              NULL,
    [ConditionBackTimeTypeId]             INT              NULL,
    CONSTRAINT [PK_dbo.TariffPlanTariffScheduleModel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.TariffPlanTariffScheduleModel_dbo.Tariffs_TariffPlanId] FOREIGN KEY ([TariffPlanId]) REFERENCES [dbo].[Tariffs] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.TariffPlanTariffScheduleModel_dbo.Tariffs_TariffPlanNextId] FOREIGN KEY ([TariffPlanNextId]) REFERENCES [dbo].[Tariffs] ([Id]),
    CONSTRAINT [FK_dbo.TariffPlanTariffScheduleModel_dbo.TariffScheduleModel_TariffScheduleId] FOREIGN KEY ([TariffScheduleId]) REFERENCES [dbo].[TariffScheduleModel] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.TariffPlanTariffScheduleModel_dbo.TariffScheduleTypeModel_TypeId] FOREIGN KEY ([TypeId]) REFERENCES [dbo].[TariffScheduleTypeModel] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [TariffPlanTariffScheduleModel_fk] FOREIGN KEY ([ConditionParkingTimeTypeId]) REFERENCES [dbo].[TimeTypeModel] ([Id]) ON DELETE SET DEFAULT,
    CONSTRAINT [TariffPlanTariffScheduleModel_fk2] FOREIGN KEY ([ConditionECProtectIntervalTimeTypId]) REFERENCES [dbo].[TimeTypeModel] ([Id]),
    CONSTRAINT [TariffPlanTariffScheduleModel_fk3] FOREIGN KEY ([ConditionBackTimeTypeId]) REFERENCES [dbo].[TimeTypeModel] ([Id]),
    CONSTRAINT [TariffPlanTariffScheduleModel_fk4] FOREIGN KEY ([ConditionZoneAfter]) REFERENCES [dbo].[ZoneModel] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_TariffScheduleId]
    ON [dbo].[TariffPlanTariffScheduleModel]([TariffScheduleId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TariffPlanId]
    ON [dbo].[TariffPlanTariffScheduleModel]([TariffPlanId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TariffPlanNextId]
    ON [dbo].[TariffPlanTariffScheduleModel]([TariffPlanNextId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TypeId]
    ON [dbo].[TariffPlanTariffScheduleModel]([TypeId] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanTariffScheduleModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сетка', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanTariffScheduleModel', @level2type = N'COLUMN', @level2name = N'TariffScheduleId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'План', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanTariffScheduleModel', @level2type = N'COLUMN', @level2name = N'TariffPlanId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Следующий план', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanTariffScheduleModel', @level2type = N'COLUMN', @level2name = N'TariffPlanNextId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип условия', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanTariffScheduleModel', @level2type = N'COLUMN', @level2name = N'TypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanTariffScheduleModel', @level2type = N'COLUMN', @level2name = N'Sync';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Зона после', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanTariffScheduleModel', @level2type = N'COLUMN', @level2name = N'ConditionZoneAfter';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время на парковке', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanTariffScheduleModel', @level2type = N'COLUMN', @level2name = N'ConditionParkingTime';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип времени времени на парковке', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanTariffScheduleModel', @level2type = N'COLUMN', @level2name = N'ConditionParkingTimeTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Кол-во въездов', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanTariffScheduleModel', @level2type = N'COLUMN', @level2name = N'ConditionEC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Величина защитного интервала для кол-ва въездов', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanTariffScheduleModel', @level2type = N'COLUMN', @level2name = N'ConditionECProtectInterval';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип времени защитного интервала', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanTariffScheduleModel', @level2type = N'COLUMN', @level2name = N'ConditionECProtectIntervalTimeTypId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Стоимость абонемента', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanTariffScheduleModel', @level2type = N'COLUMN', @level2name = N'ConditionAbonementPrice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Период действия', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanTariffScheduleModel', @level2type = N'COLUMN', @level2name = N'ConditionBackTime';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип времени период действия', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanTariffScheduleModel', @level2type = N'COLUMN', @level2name = N'ConditionBackTimeTypeId';

GO
CREATE TRIGGER [dbo].[SyncTariffPlanTariffScheduleModel]
ON [dbo].[TariffPlanTariffScheduleModel]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [TariffPlanTariffScheduleModel] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='TariffPlanTariffScheduleModel'
end
