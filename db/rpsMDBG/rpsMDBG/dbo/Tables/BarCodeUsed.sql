﻿CREATE TABLE [dbo].[BarCodeUsed] (
    [BarCodeInUseId] UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Used]           BIT              NULL,
    PRIMARY KEY CLUSTERED ([BarCodeInUseId] ASC),
    CONSTRAINT [BarCodeModel_BarCodeRights] FOREIGN KEY ([BarCodeInUseId]) REFERENCES [dbo].[BarCodeInUse] ([Id])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Признак наличия активации', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeUsed', @level2type = N'COLUMN', @level2name = N'Used';

