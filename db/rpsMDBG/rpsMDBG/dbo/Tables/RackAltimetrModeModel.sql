﻿CREATE TABLE [dbo].[RackAltimetrModeModel] (
    [Id]   INT           NOT NULL,
    [Name] VARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.RackAltimetrModeModel] PRIMARY KEY CLUSTERED ([Id] ASC)
);

