﻿CREATE TABLE [dbo].[RackCustomerGroup] (
    [Id]            UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Sync]          DATETIME         NULL,
    [DeviceId]      UNIQUEIDENTIFIER NOT NULL,
    [ClientGroupId] TINYINT          NOT NULL,
    CONSTRAINT [PK_RackCustomerGroup] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [RackCustomerGroup_fk] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[DeviceModel] ([Id]),
    CONSTRAINT [RackCustomerGroup_fk2] FOREIGN KEY ([ClientGroupId]) REFERENCES [dbo].[Group] ([idFC])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RackCustomerGroup', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Флаг синхронизации со 2ым ур.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RackCustomerGroup', @level2type = N'COLUMN', @level2name = N'Sync';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Устройство', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RackCustomerGroup', @level2type = N'COLUMN', @level2name = N'DeviceId';

GO
CREATE TRIGGER [dbo].[SyncRackCustomerGroup]
ON [dbo].[RackCustomerGroup]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [RackCustomerGroup] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='RackCustomerGroup'
end
