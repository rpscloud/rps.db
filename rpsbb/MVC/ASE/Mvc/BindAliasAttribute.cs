﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
//using System.Web.Http.Controllers;
//using System.Web.Http.ModelBinding;

namespace ASE.MVC
{
    public class DefaultModelBinderEx : System.Web.Http.ModelBinding.IModelBinder
    {
        public DefaultModelBinderEx()
        {

        }

        public bool BindModel(System.Web.Http.Controllers.HttpActionContext actionContext, System.Web.Http.ModelBinding.ModelBindingContext bindingContext)
        {
            string ct = actionContext.Request.Content.ReadAsStringAsync().Result;
            var input = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            var body = actionContext.Request.Content.ReadAsStringAsync().Result;
            var settings = new JsonSerializerSettings();
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            var pollRequest = JsonConvert.DeserializeObject<rps.Data.OrganizationModel>(body, settings);
            JObject json = (JObject)JsonConvert.DeserializeObject(body, settings);
            bindingContext.Model = pollRequest;

            return true;
        }
    }

    class GeoPointConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            return base.ConvertFrom(context, culture, value);
        }
    }
    public class BindingNameAttribute : Attribute
    {
        public string Name { get; set; }

        public BindingNameAttribute()
        {

        }

        public BindingNameAttribute(string name)
        {
            Name = name;
        }
    }
}
