﻿CREATE TABLE [dbo].[RightsInRoleModel] (
    [Id]      UNIQUEIDENTIFIER NOT NULL,
    [RightId] UNIQUEIDENTIFIER NOT NULL,
    [RoleId]  UNIQUEIDENTIFIER NOT NULL,
    [Active]  BIT              NULL,
    CONSTRAINT [PK_dbo.RightsInRoleModel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.RightsInRoleModel_dbo.RightModel_RightId] FOREIGN KEY ([RightId]) REFERENCES [dbo].[RightModel] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.RightsInRoleModel_dbo.RoleModel_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[RoleModel] ([Id]) ON DELETE CASCADE
);




GO
CREATE NONCLUSTERED INDEX [IX_RightId]
    ON [dbo].[RightsInRoleModel]([RightId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RoleId]
    ON [dbo].[RightsInRoleModel]([RoleId] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RightsInRoleModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Право', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RightsInRoleModel', @level2type = N'COLUMN', @level2name = N'RightId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Роль', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RightsInRoleModel', @level2type = N'COLUMN', @level2name = N'RoleId';


GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Активен',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'RightsInRoleModel',
    @level2type = N'COLUMN',
    @level2name = N'Active'
GO
CREATE NONCLUSTERED INDEX [RightsInRoleModel_Active_index]
    ON [dbo].[RightsInRoleModel]([Active] ASC);

