﻿CREATE TABLE [dbo].[CoinAcceptorAllowModel] (
    [DeviceId] UNIQUEIDENTIFIER NOT NULL,
    [CoinId]   INT              NOT NULL,
    [Sync]    DATETIME         NULL,
    [Allow] BIT NOT NULL DEFAULT 0, 
    [Id] UNIQUEIDENTIFIER NOT NULL, 
    CONSTRAINT [PK_dbo.CoinAcceptorAllowModel] PRIMARY KEY CLUSTERED ([Id]),
    CONSTRAINT [FK_dbo.CoinAcceptorAllowModel_dbo.CoinModel_CoinId] FOREIGN KEY ([CoinId]) REFERENCES [dbo].[CoinModel] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.CoinAcceptorAllowModel_dbo.DeviceModel_DeviceId] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[DeviceModel] ([Id]) ON DELETE CASCADE
);

GO
CREATE UNIQUE INDEX [IX_CoinAcceptorAllowModel_DeviceId_CoinId] ON [dbo].[CoinAcceptorAllowModel] ([DeviceId], [CoinId] ASC)


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Флаг синхронизации со 2ым ур.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CoinAcceptorAllowModel', @level2type = N'COLUMN', @level2name = 'Sync';


GO
CREATE TRIGGER [dbo].[SyncCoinAcceptorAllowModel]
ON [dbo].[CoinAcceptorAllowModel]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [CoinAcceptorAllowModel] set [Sync]=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='CoinAcceptorAllowModel'
end
