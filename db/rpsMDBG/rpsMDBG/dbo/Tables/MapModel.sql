﻿CREATE TABLE [dbo].[MapModel] (
    [Id]              UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Level]           INT              NOT NULL,
    [Name]            NVARCHAR (MAX)   NULL,
    [Image]           VARBINARY (MAX)  NULL,
    [ImageW]          INT              NOT NULL,
    [ImageH]          INT              NOT NULL,
    [ImageName]       NVARCHAR (MAX)   NULL,
    [ParkingId]       UNIQUEIDENTIFIER NULL,
    [ChangedDateTime] DATETIME         NULL,
    [Deleted]         BIT              NOT NULL,
    [_IsDeleted]      BIT              NOT NULL,
    CONSTRAINT [PK_dbo.MapModel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.MapModel_dbo.ParkingModel_ParkingId] FOREIGN KEY ([ParkingId]) REFERENCES [dbo].[ParkingModel] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_ParkingId]
    ON [dbo].[MapModel]([ParkingId] ASC);

