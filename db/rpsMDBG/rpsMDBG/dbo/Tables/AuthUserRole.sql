﻿CREATE TABLE [dbo].[AuthUserRole] (
    [Id]   UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [User] NVARCHAR (MAX)   NULL,
    [Role] NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_dbo.AuthUserRole] PRIMARY KEY CLUSTERED ([Id] ASC)
);

