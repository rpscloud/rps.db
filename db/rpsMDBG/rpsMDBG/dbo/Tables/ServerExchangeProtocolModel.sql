﻿CREATE TABLE [dbo].[ServerExchangeProtocolModel] (
    [Id]   INT           NOT NULL,
    [Name] VARCHAR (MAX) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

