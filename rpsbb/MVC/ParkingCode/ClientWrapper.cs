﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Linq;

using Parking.Client;
using Parking.Workstation;
using Parking.Security;

using RMLib.Services.Wcf;
using RMLib.Services;
using RMLib;

using ASE.MVC;

namespace rps.ParkingCode
{
    internal class LogOnStub : ILogOnWindow
    {
        public string UserName;
        public string Password;

        #region [ ILogOnWindow ]

        public event Func<string, char[], bool> CheckPassword;

        public IIdentity GetIdentity()
        {
            return new SecurityIdentity(UserName, Password);
        }

        public bool LogOn()
        {
            return true;
        }

        public void SetIdentity(IIdentity identity)
        {
            //
        }

        public void SetLockMode(bool state)
        {
            //
        }

        public void SetUsers(IEnumerable<string> users)
        {
            //
        }

        #endregion
    }

    internal class CustomSecurityProvider : SecurityProvider
    {
        private LogOnStub _logon;

        public CustomSecurityProvider()
        {
            _logon = new LogOnStub();
        }

        public void SetCredentials(string userName, string password)
        {
            _logon.UserName = userName;
            _logon.Password = password;
        }

        protected override ILogOnWindow CreateLogOnWindow()
        {
            return _logon;
        }
    }

    public static class ClientWrapper
    {
        private static CustomSecurityProvider _provider;
        private static ServiceContainer _container;

        static ClientWrapper()
        {
            AssemblyInitializer.UseCurrentDomain();

            _provider = new CustomSecurityProvider();
            SecurityContext.InitializeSecurity(_provider, _provider);

            ClientExtensions.LoadOnConnection = false;
            Setup("localhost", "Admin", "123");
        }

        public static void Setup(string serverAddress, string userName, string password)
        {
            var c = new ServiceContainer();
            try
            {
                c.RegisterInstance<IWcfClientConfigurator>(ClientConfigurator.Create(serverAddress));

                c.RegisterInstance<ISystemClient>(new SystemClient());
                c.RegisterInstance<IZoneClient>(new ZoneClient());
                c.RegisterInstance<IDeviceClient>(new DeviceClient());
                c.RegisterInstance<ICardClient>(new CardClient());

                c.Rebuild();

                if (_container != null)
                    _container.Dispose();

                _container = c;
            }
            catch
            {
                c.Dispose();
            }

            _provider.SetCredentials(userName, password);
            Authentication.ChangeUser();
        }

        public static void Invoke(Action<ServiceContainer> action)
        {
            try
            {
                action(_container);
            }
            finally
            {
                foreach (var svc in _container.GetAnyServices<IWcfClient>())
                    svc.Disconnect();
            }
        }

        public static T GetConnected<T>(this IServiceContainer c) where T : IWcfClient
        {
            T svc = c.GetService<T>();
            if (svc == null)
                throw new InvalidOperationException(String.Format("Service {0} not found", typeof(T)));

            if (!svc.Connect())
                throw new ApplicationException(String.Format("Connection with service {0} failed", typeof(T)));

            return svc;
        }
    }
}
