USE [rpsMDBG]

GO

ALTER TABLE BarCodeInUse
ADD TPForRent uniqueidentifier
ALTER TABLE BarCodeInUse
ADD TSForRent uniqueidentifier
ALTER TABLE BCTransaction
ADD TPForRent uniqueidentifier
ALTER TABLE BCTransaction
ADD TSForRent uniqueidentifier
ALTER TABLE BarCodeModel
ADD TPForRent uniqueidentifier
ALTER TABLE BarCodeModel
ADD TSForRent uniqueidentifier

GO

DROP TRIGGER [dbo].[SyncBCTransaction]
GO

CREATE TRIGGER [dbo].[SyncBCTransaction]
ON [dbo].[BCTransaction]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [BCTransaction] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='BCTransaction'
	declare @level int;

	if(Exists(select top 1 CurrentLevel from ConfigSync))
	begin
		set @level=(select top 1 CurrentLevel from ConfigSync);
		if(@level=2)
		begin
			update BarCodeInUse set 
        OperId=i.OperId,
        CompanyId=i.CompanyId,
        IdTP=i.IdTP,
        IdTS=i.IdTS,
        DiscountAmount=i.DiscountAmount,
        GroupIdFC=i.GroupIdFC,
        UsageTime=i.UsageTime,
        Sync=i.Sync,
        GuestPlateNumber=i.GuestPlateNumber,
		GuestCarModel=i.GuestCarModel,
        GuestName=i.GuestName,
        GuestEmail=i.GuestEmail,
        ParkingTime=i.ParkingTime,
        CreatedAt=i.CreatedAt,
        EnterId=i.EnterId,
        ExitId=i.ExitId,
        IdTPForCount=i.IdTPForCount,
        IdTSForCount=i.IdTSForCount,
        CardId=i.CardId,
        Ammount=i.Ammount,
        Used=i.Used,
        ActivatedAt=i.ActivatedAt,
		TPForRent=i.TPForRent,
        TSForRent=i.TSForRent
        
			from BarCodeInUse c,inserted i
			where c.Id=i.BarCodeInUseId;

			
		end
	end
end