﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

using ASE.Data;
using ASE.MVC;

using rps.Data;
using rpsbb.Code;

namespace rpsbb.Controllers
{
    [Authorize]
    public class RightController : ApiControllerAdv<DB>
    {
        [System.Web.Http.HttpGet]
        public RightModelJson Get(Guid id)
        {
            return db.Rights.Find(id).Get();
        }

        [System.Web.Http.HttpGet]
        public IEnumerable<RightModelJson> List()
        {
            return db.Rights.ToList().Select(x => x.Get());
        }

        [System.Web.Http.HttpPost]
        public RightModelJson Add(RightModelJson model)
        {
            model.Id = Guid.Empty;
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPut]
        public RightModelJson Put(RightModelJson model)
        {
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPatch]
        public RightModelJson Patch(RightModelJson model)
        {
            return model.DALSave(db, true).Get();
        }

        [System.Web.Http.HttpDelete]
        public RightModelJson Delete([FromUri] RightModelJson model)
        {
            return model.DALDelete(db).Get();
        }
    }
}
