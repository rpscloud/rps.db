﻿CREATE TABLE [dbo].[AlarmModel] (
    [Id]           UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [DeviceId]     UNIQUEIDENTIFIER NOT NULL,
    [Begin]        DATETIME         NOT NULL,
    [End]          DATETIME         NULL,
    [Value]        VARCHAR (MAX)    NULL,
    [ValueEnd]     VARCHAR (MAX)    NULL,
    [_IsDeleted]   BIT              NOT NULL,
    [Sync]         DATETIME         NULL,
    [AlarmColorId] INT              NULL,
    [TypeId]       INT              NULL,
    CONSTRAINT [PK_dbo.AlarmModel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [AlarmModel_fk] FOREIGN KEY ([AlarmColorId]) REFERENCES [dbo].[AlarmColor] ([Id]) ON DELETE SET NULL,
    CONSTRAINT [FK_dbo.AlarmModel_dbo.DeviceModel_DeviceId] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[DeviceModel] ([Id]) ON DELETE CASCADE
);






GO
CREATE NONCLUSTERED INDEX [IX_DeviceId] ON [dbo].[AlarmModel]([DeviceId] ASC);
    
GO
CREATE INDEX [AlarmModel_End_index] ON [dbo].[AlarmModel] ([End]);

GO
CREATE INDEX [AlarmModel_Begin_index] ON [dbo].[AlarmModel] ([Begin] DESC);

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AlarmModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Устройство', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AlarmModel', @level2type = N'COLUMN', @level2name = N'DeviceId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Начало', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AlarmModel', @level2type = N'COLUMN', @level2name = N'Begin';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Завершение', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AlarmModel', @level2type = N'COLUMN', @level2name = N'End';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Флаг синхр. со 2ым уровнем', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AlarmModel', @level2type = N'COLUMN', @level2name = 'Sync';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ТипТревоги', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AlarmModel', @level2type = N'COLUMN', @level2name = N'AlarmColorId';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IdТревоги', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AlarmModel', @level2type = N'COLUMN', @level2name = N'TypeId';

GO

--Изминение тригеров для AlrmModel
CREATE TRIGGER [dbo].[SyncAlarmModel]
ON [dbo].[AlarmModel]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [AlarmModel] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='AlarmModel'

	declare @level int;
	if(Exists(select top 1 CurrentLevel from ConfigSync))
	begin
		set @level=(select top 1 CurrentLevel from ConfigSync);
		if(@level=2)
		begin
			insert into Command(Status,TypePacket,DataPacket,DateCreate)
			select 1,'Alarm',Id,GETDATE() from inserted
		end
	end
end
GO

                GO

                GO

                
