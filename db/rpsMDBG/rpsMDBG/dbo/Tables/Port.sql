﻿CREATE TABLE [dbo].[Port] (
    [Id]   INT           NOT NULL,
    [Name] VARCHAR (MAX) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Port', @level2type = N'COLUMN', @level2name = N'Id';

