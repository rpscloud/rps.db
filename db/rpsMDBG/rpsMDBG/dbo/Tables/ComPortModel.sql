﻿CREATE TABLE [dbo].[ComPortModel] (
    [Id]   INT           NOT NULL,
    [Name] VARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.ComPortModel] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ComPortModel', @level2type = N'COLUMN', @level2name = N'Id';

