﻿CREATE TABLE [dbo].[DeviceTypeModel] (
    [Id]         INT           NOT NULL,
    [Name]       NVARCHAR(MAX) NULL,
    [_IsDeleted] BIT           NOT NULL,
    CONSTRAINT [PK_dbo.DeviceTypeModel] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceTypeModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наименование', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceTypeModel', @level2type = N'COLUMN', @level2name = N'Name';

