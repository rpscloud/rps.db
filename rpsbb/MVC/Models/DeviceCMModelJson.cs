using ASE.MVC;
using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace rps.Data
{
    public class CashAcceptorAllowModelJson
    {
        public bool Allow { get; set; }
        
        public virtual BanknoteModel Banknote { get; set; }
    }

    public class CoinAcceptorAllowModelJson
    {
        public bool Allow { get; set; }
        public virtual CoinModel Coin { get; set; }
    }

    [NotMapped]
    public class DeviceCMModelJson : DeviceCMModel
    {
        public List<CashAcceptorAllowModelJson> CashAcceptorAllow { get; set; }

        public List<CoinAcceptorAllowModelJson> CoinAcceptorAllow { get; set; }
    }

    public static partial class Model2Json
    {
        public static DeviceCMModelJson Get(this DeviceCMModel model)
        {
            var ret = (DeviceCMModelJson)Copy(model, new DeviceCMModelJson());
            ret.Device = ret.Device.Get();
            var qc = from c in ContextPerRequest.Db.Coins.Where(x => x.Id > 0)
                     join a in ContextPerRequest.Db.CoinAcceptorAllows.Where(x => x.DeviceId == ret.DeviceId) on c.Id equals a.CoinId into allow
                     from x in allow.DefaultIfEmpty()
                     select new CoinAcceptorAllowModelJson { Coin = c, Allow = x == null ? false : x.Allow};
            ret.CoinAcceptorAllow = qc.ToList();

            var qb = from c in ContextPerRequest.Db.Banknotes.Where(x => x.Id > 0)
                    join a in ContextPerRequest.Db.CashAcceptorAllows.Where(x => x.DeviceId == ret.DeviceId) on c.Id equals a.BanknoteId into allow
                    from x in allow.DefaultIfEmpty()
                    select new CashAcceptorAllowModelJson { Banknote = c, Allow = x == null ? false : x.Allow };
            ret.CashAcceptorAllow = qb.ToList();

            return ret;
        }

        public static DeviceCMModel DALSave(this DeviceCMModelJson model, bool patch = false, Guid? newId = null)
        {
            DeviceCMModel m = null;
            if (model.DeviceId == Guid.Empty)
            {
                model.DeviceId = newId.HasValue ? newId.Value : Guid.NewGuid();
                m = new DeviceCMModel();
                ContextPerRequest.Db.DeviceCMs.Add(m);
            }
            else
            {
                m = ContextPerRequest.Db.DeviceCMs.Find(model.DeviceId);
            }
            Copy(model, m, patch);
            ContextPerRequest.Db.SaveChanges();

            //StartSync();
            return m;
        }

        public static DeviceCMModel DALDelete(this DeviceCMModelJson model)
        {
            var m = ContextPerRequest.Db.DeviceCMs.Find(model.DeviceId);
            //m.Deleted = true;
            //db.Organizations.Remove(m);
            ContextPerRequest.Db.SaveChanges();

            //StartSync();
            return m;
        }
    }
}