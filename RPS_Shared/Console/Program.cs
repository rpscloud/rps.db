﻿using System;
using System.Linq;
using RPS_Shared;
using System.Net;
using rps.Sync;
using RPSConsole1;
using rps.Data;

namespace ASE.MVC
{
    class Program
    {
        private static WebServer webServer = null;

        static void Main(string[] args)
        {
            ASE.Log.Root = System.Reflection.Assembly.GetExecutingAssembly().Location + ".txt";

            //Автоматическая миграция
            //При изменении модели изменится БД
//            System.Data.Entity.Database.SetInitializer<Entities>(new AutoMigrationInit());
            //System.Data.Entity.Database.SetInitializer<DB>(new CreateDatabaseIfNotExists<DB>());

            //Доступ к бд через db
            String Name = "Касса";
            Int16 Type = 4;
            System.Data.SqlClient.SqlConnectionStringBuilder cb = new System.Data.SqlClient.SqlConnectionStringBuilder(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            cb.ApplicationName = "RPSConsole";
            if ((args != null) && (args.Length > 0) && (args[0] == "1"))
            {
                cb.InitialCatalog = "rpsMDBG_RACK";
                Name = "Cтойка";
                Type = 1;
            }
            if ((args != null) && (args.Length > 0) && (args[0] == "2"))
            {
                cb.InitialCatalog = "rpsMDBG_CM";
                Name = "Касса";
                Type = 4;
            }
            if ((args != null) && (args.Length > 0) && (args[0] == "3"))
            {
                cb.InitialCatalog = "rpsMDBG_DISPLAY";
                Name = "Табло";
                Type = 5;
            }
            if ((args != null) && (args.Length > 0) && (args[0] == "4"))
            {
                cb.InitialCatalog = "rpsMDBG_ZCU";
                Name = "Зонный контроллер";
                Type = 6;
            }
            if ((args != null) && (args.Length > 0) && (args[0] == "-1"))
            {
                cb.InitialCatalog = "rpsMDBG_S2";
                Name = "Сайт 2";
                Type = -1;
            }
            if ((args != null) && (args.Length > 0) && (args[0] == "-2"))
            {
                cb.InitialCatalog = "rpsMDBG_S3";
                Name = "Сайт 3";
                Type = -2;
            }

            Entities.ConnectionString = cb.ConnectionString;
            Entities db = new Entities(cb.ConnectionString);

            #region ИНИЦИАЛИЗАЦИЯ (КАССА В ПРИМЕРЕ)

            //Возьмем любое устройство из БД
            var device = db.DeviceModel.FirstOrDefault();
            var deviceRack = db.DeviceRackModel.FirstOrDefault();
            var deviceCM = db.DeviceCMModel.FirstOrDefault();
            var deviceDisplay = db.DeviceDisplayModel.FirstOrDefault();
            var deviceZCU = db.DeviceZCUModel.FirstOrDefault();

            if (device == null) //Нет еще устройств
            {
                device = new DeviceModel { Id = Guid.NewGuid(), Type = Type, Name = Name, Host = cb.DataSource, DataBase = cb.InitialCatalog, User = cb.UserID, Password = cb.Password };
                db.DeviceModel.Add(device);
                
                var zone = new ZoneModel { Id = Guid.NewGuid(), Name = "Зона 1" };
                db.ZoneModel.Add(zone);

                //Добавим кассу в устройства с указанием параметров БД
                if (Type == 1)
                    db.DeviceRackModel.Add(new DeviceRackModel { DeviceId = device.Id });

                if (Type == 4)
                    db.DeviceCMModel.Add(new DeviceCMModel { DeviceId = device.Id });

                if (Type == 5)
                    db.DeviceDisplayModel.Add(new DeviceDisplayModel { DeviceId = device.Id });

                if (Type == 6)
                    db.DeviceZCUModel.Add(new DeviceZCUModel { DeviceId = device.Id });

                db.SaveChanges();
            }

            #endregion ИНИЦИАЛИЗАЦИЯ (КАССА В ПРИМЕРЕ)

            System.Data.Entity.Database.SetInitializer<Entities>(null);
            //Веб сервер для интеграции
            if (Type == 1)
                webServer = new WebServer(SendResponse, "http://localhost:8181/external/");
            if (Type == 2)
                webServer = new WebServer(SendResponse, "http://localhost:8182/external/");
            if (Type == 3)
                webServer = new WebServer(SendResponse, "http://localhost:8183/external/");
            if (Type == 4)
                webServer = new WebServer(SendResponse, "http://localhost:8184/external/");
            if (Type == 5)
                webServer = new WebServer(SendResponse, "http://localhost:8185/external/");
            if (Type == 6)
                webServer = new WebServer(SendResponse, "http://localhost:8186/external/");

            if (webServer != null)
                webServer.Run();

            //Установка получателя уведомлений
            Configurator.SetRemoteNotifier(db, System.Configuration.ConfigurationManager.AppSettings["RNType" + Type].ToString());

            #region КАЛЬКУЛЯТОР

            //Добавить новый план
            //db.TariffTariffPlans.Add(new TariffTariffPlanModel { Id = 1, TariffId = 1, TariffPlanId = 1 });

            Tariffs tpm;
            //Получение по Id (вариант 1)
            tpm = db.Tariffs.FirstOrDefault(x => x.TypeId == 1); //Еженедельный
            tpm = db.Tariffs.FirstOrDefault(x => x.TypeId == 2); //Ежемесячный
            tpm = db.Tariffs.FirstOrDefault(x => x.TypeId == 3); //Интервалы
            //Получение по Id (вариант 2)
            //tariffTariffPlanModel = db.TariffTariffPlans.FirstOrDefault(x => x.Id == 1);

            //Запишем изменения
            db.SaveChanges();

            //-----------------
            //взяли первый план (пока он у нас 1)
            var tariffPlan = db.Tariffs.FirstOrDefault(x => x.TypeId == 1);
            var t50 = tariffPlan.ToTariff();
            var calc = new Calculator();
            DateTime dt1 = new DateTime(2015, 7, 20, 19, 30, 0);
            DateTime dt2 = new DateTime(2015, 7, 23, 21, 50, 0);
            var z = calc.Calculate(t50, dt1, dt2);

            //Тарифы в тарифном плане
            //var tariffTariffPlans = db.TariffTariffPlans.Where(x => x.TariffPlanId == tariffPlan.Id).ToList();
            //Для каждого тарифа есть периоды действия
            //foreach (var tariffTariffPlan in tariffTariffPlans)
            foreach (var tp in tariffPlan.TariffTariffPlanModel)
            {
                //Интервалы
                foreach (var tariffInterval in tp.TariffModel.TariffIntervalModel)
                {
                  //
                }

                //Периоды 
                //var tariffPlanPeriods = db.TariffPlanPeriods.Where(x => x.TariffTariffPlanId == tariffTariffPlan.Id).ToList();
                foreach (var p in tp.TariffPlanPeriodModel)
                {
                    if (tariffPlan.TypeId == 1) //Еженедельный
                    {
                        var DayOfWeekStart = p.Start.DayOfWeek;
                        var DayOfWeekEnd = p.End.DayOfWeek;
                        var TimeStart = p.Start.TimeOfDay;
                        var TimeEnd = p.End.TimeOfDay;
                    }
                    if (tariffPlan.TypeId == 2) //Ежемесячный
                    {
                        var DayStart = p.Start.Day;
                        var DayEnd = p.End.Day;
                        var TimeStart = p.Start.TimeOfDay;
                        var TimeEnd = p.End.TimeOfDay;
                    }
                    if (tariffPlan.TypeId == 3) //Интервалы
                    {
                        var TimeStart = p.Start.TimeOfDay;
                        var TimeEnd = p.End.TimeOfDay;
                    }
                }
            }

            #endregion КУЛЬКУЛЯТОР


            if (Type == 4)
            {
                Synchronize.StartPing(-1, Synchronize.L1_To_L23);
                Synchronize.StartPing(-2, Synchronize.L1_To_L23);
            }

            if (Type == 1)
            {
                Synchronize.StartPing(-1, Synchronize.L1_To_L23);
                Synchronize.StartPing(-2, Synchronize.L1_To_L23);
            }

            Console.WriteLine("Ready Type = " + Type);
            Console.ReadLine();
        }


        /// <summary>
        /// Этот метод вызывается на внешние события
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string SendResponse(HttpListenerRequest request)
        {
            try
            {
                string s = "REQ: ";
                foreach (var item in request.QueryString.AllKeys)
                    s += "&" + item + "=" + request.QueryString.Get(item);

                Console.WriteLine(s);

                //Запуск синхронизации 
                //Если T = "devicecmmodel"
                if ((request.QueryString.AllKeys.Contains("T")) && (request.QueryString["T"].ToLower() == "devicecmmodel"))
                    Synchronize.DeviceCMModel();

                if ((request.QueryString.AllKeys.Contains("T")) && (request.QueryString["T"].ToLower() == "devicerackmodel"))
                    Synchronize.DeviceRackModel();
            }
            catch (Exception exc)
            {
                return "Error! " + exc.Message;
            }

            return "ok";
        }
    }
}

namespace rps.Data
{
    public static partial class Model2Json
    {
        static object Copy(object child, object parent, params string[] skip)
        {
            foreach (var prop in child.GetType().GetProperties())
                if (skip.FirstOrDefault(x => x == prop.Name) == null)
                    parent.GetType().GetProperty(prop.Name).SetValue(parent, child.GetType().GetProperty(prop.Name).GetValue(child));

            return parent;
        }

        static object Copy(object from, object to, bool patch, params string[] skip)
        {
            if (to is Entities.ISyncTraking)
                (to as Entities.ISyncTraking).ChangedDateTime = DateTime.Now;

            foreach (var prop in to.GetType().GetProperties())
            {
                if (skip.FirstOrDefault(x => x == prop.Name) != null)
                    continue;

                var pi = from.GetType().GetProperty(prop.Name);
                if (pi == null)
                    continue;
                if (pi.PropertyType.GetInterface("ICollection`1") != null)
                    continue;

                var val = pi.GetValue(from);
                if (patch)
                {
                    if (val == null)
                        continue;

                    if ((val is Guid) && ((Guid)val == Guid.Empty))
                        continue;
                }

                to.GetType().GetProperty(prop.Name).SetValue(to, val);
            }

            return to;
        }
    }
}
