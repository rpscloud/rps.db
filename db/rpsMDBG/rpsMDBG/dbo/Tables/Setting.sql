﻿CREATE TABLE [dbo].[Setting] (
    [Name]     VARCHAR (50)   NOT NULL,
    [Category] NVARCHAR (MAX) NULL,
    [Value]    NVARCHAR (MAX) NULL,
    [Sync]     DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([Name] ASC)
);


GO
CREATE TRIGGER [dbo].[SyncSetting]
ON [dbo].[Setting]
AFTER INSERT,UPDATE
AS
  BEGIN
    SET NOCOUNT ON;
    declare @dt datetime;
    set @dt=GetDate();
    update [Setting] set Sync=@dt	where Name in (select Name from inserted)
    update SyncTables set [DateTime]=@dt where TableName='Setting'
  END