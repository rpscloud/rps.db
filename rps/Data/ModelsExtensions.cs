﻿using ASE.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rps.Data
{
    public static class SyncItemExtensions
    {
        public static int ObjectType(this DB db, object obj)
        {
            if ((obj.GetType() == typeof(OrganizationModel)) || (obj.GetType().BaseType == typeof(OrganizationModel)))
                return 1;

            if ((obj.GetType() == typeof(ParkingModel)) || (obj.GetType().BaseType == typeof(ParkingModel)))
                return 2;

            if ((obj.GetType() == typeof(UserModel)) || (obj.GetType().BaseType == typeof(UserModel)))
                return 3;

            if ((obj.GetType() == typeof(UserParkingRoleModel)) || (obj.GetType().BaseType == typeof(UserParkingRoleModel)))
                return 4;

            return 0;
        }

        public static void SyncAddRemove(this DB db, Object obj)
        {
            db.SyncItems.Add(new SyncItem
            {
                Id = Guid.NewGuid(),
                ObjecId = (Guid)obj.GetType().GetProperty("Id").GetValue(obj, null),
                ObjecType = ObjectType(db, obj),
                Operation = 1,
                Created = DateTime.Now
            });
            db.SaveChanges();
            rps.ParkingCode.Synchronization.Start();
        }
        public static void SyncAddUpdate(this DB db, Object obj)
        {
            db.SyncItems.Add(new SyncItem
            {
                Id = Guid.NewGuid(),
                ObjecId = (Guid)obj.GetType().GetProperty("Id").GetValue(obj, null),
                ObjecType = ObjectType(db, obj),
                Operation = 2,
                Created = DateTime.Now
            });
            db.SaveChanges();
            rps.ParkingCode.Synchronization.Start();
        }

        /*public static void AddChanged(this DB db, params IChanged[] objects)
        {
            foreach (var item in objects)
                item.ChangedDateTime = DateTime.Now;
        }*/
    }
}
