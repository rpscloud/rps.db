﻿CREATE TABLE [dbo].[Notifications] (
    [Id]            UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [CompanyId]     UNIQUEIDENTIFIER NULL,
    [CreateDate]    DATETIME         NULL,
    [OperatorId]    UNIQUEIDENTIFIER NULL,
    [Notifications] NVARCHAR (MAX)   NULL,
    [ColorId]       INT              NULL,
    [IsRead]        BIT              NULL,
    [ReadDate]      DATETIME         NULL
);




GO

  

GO

