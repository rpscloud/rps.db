﻿CREATE TABLE [dbo].[MenuItemModel] (
    [Id]              UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [ParentId]        UNIQUEIDENTIFIER NULL,
    [Category]        NVARCHAR (MAX)   NULL,
    [Name]            NVARCHAR (MAX)   NULL,
    [Url]             NVARCHAR (MAX)   NULL,
    [SortOrder]       INT              NOT NULL DEFAULT 0,
    [Active]          BIT              NOT NULL DEFAULT 0,
    [ChangedDateTime] DATETIME         NULL,
    [_IsDeleted]      BIT              NOT NULL DEFAULT 0,
    CONSTRAINT [PK_dbo.MenuItemModel] PRIMARY KEY CLUSTERED ([Id] ASC)
);



