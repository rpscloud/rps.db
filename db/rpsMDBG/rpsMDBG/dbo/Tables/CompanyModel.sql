﻿CREATE TABLE [dbo].[CompanyModel] (
    [Id]                  UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [CompanyName]         VARCHAR (MAX)    NULL,
    [CompanyPhone]        VARCHAR (MAX)    NULL,
    [CompanyEmail]        VARCHAR (MAX)    NULL,
    [CompnayContactName]  VARCHAR (MAX)    NULL,
    [CompnayContactPhone] VARCHAR (MAX)    NULL,
    [CompnayContactEmail] VARCHAR (MAX)    NULL,
	[ParkingSpacesCount]  INT              NULL,
    [_IsDeleted]          BIT              NULL,
    [Sync]                DATETIME         NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Количество парковочных мест', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyModel', @level2type = N'COLUMN', @level2name = N'ParkingSpacesCount';



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Название компании', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyModel', @level2type = N'COLUMN', @level2name = N'CompanyName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Телефон компании', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyModel', @level2type = N'COLUMN', @level2name = N'CompanyPhone';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Почта компании', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyModel', @level2type = N'COLUMN', @level2name = N'CompanyEmail';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ФИО контактного лица', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyModel', @level2type = N'COLUMN', @level2name = N'CompnayContactName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Телефон контактного лица', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyModel', @level2type = N'COLUMN', @level2name = N'CompnayContactPhone';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Почта контактного лица', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyModel', @level2type = N'COLUMN', @level2name = N'CompnayContactEmail';

GO
CREATE TRIGGER [dbo].[SyncCompanyModel]
ON [dbo].[CompanyModel]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [CompanyModel] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='CompanyModel'
end


GO
CREATE TRIGGER [dbo].[AddToUserAuth]
	ON [dbo].[CompanyModel]
	AFTER INSERT,UPDATE
AS BEGIN
	declare @dt datetime;
	declare @company_id uniqueidentifier;
	declare @login varchar(20);

	--select @company_id = i.Id from inserted i;
	--select @login = i.CompanyPhone from inserted i;
	--set @dt=GetDate();
	--IF NOT EXISTS (SELECT * FROM [dbo].[AuthUser] where Id=@company_id)
	--BEGIN
	--	IF @login IS NOT NULL 
	--	BEGIN
	--		insert into [dbo].[AuthUser]([Id]
	--		  ,[LoginName]
	--		  ,[Password]
	--		  ,[CreateDate]) 
	--		VALUES (@company_id, @login, @login, @dt);
	--	END
	--	ELSE
	--	IF @login IS NOT NULL 
	--	BEGIN
	--		update [dbo].[AuthUser] set
			  
	--		   [LoginName]=@login
			  
	--		where Id=@company_id
	--	END
	--END
END