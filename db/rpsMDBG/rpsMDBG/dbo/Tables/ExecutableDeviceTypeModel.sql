﻿CREATE TABLE [dbo].[ExecutableDeviceTypeModel] (
    [Id]   INT           NOT NULL,
    [Name] VARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.ExecutableDeviceTypeModel] PRIMARY KEY CLUSTERED ([Id] ASC)
);

