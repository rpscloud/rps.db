﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(rps.Startup))]
namespace rps
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
