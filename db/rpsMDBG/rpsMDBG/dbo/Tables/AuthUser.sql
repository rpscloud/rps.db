﻿CREATE TABLE [dbo].[AuthUser] (
    [Id]         UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [LoginName]  NVARCHAR (MAX)   NULL,
    [Password]   NVARCHAR (MAX)   NULL,
    [CreateDate] DATETIME         NOT NULL,
    CONSTRAINT [PK_dbo.AuthUser] PRIMARY KEY CLUSTERED ([Id] ASC)
);

