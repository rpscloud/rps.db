﻿CREATE TABLE [dbo].[BlackList] (
    [Id]          UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Description] VARCHAR (MAX)    NULL,
    [PlateNumber] VARCHAR (MAX)    NULL,
    [CreatedAt]   DATETIME         CONSTRAINT [DF_BlackList_CreatedAt] DEFAULT (getdate()) NULL,
    [WayAdded]    INT              CONSTRAINT [DF_BlackList_WayAdded] DEFAULT ((1)) NOT NULL,
    [Sync]        DATETIME         NULL,
    [_IsDeleted]  BIT              CONSTRAINT [DF_BlackList__IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_dbo.BlackList] PRIMARY KEY CLUSTERED ([Id] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BlackList', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Комментарий', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BlackList', @level2type = N'COLUMN', @level2name = N'Description';


GO


GO
CREATE TRIGGER [dbo].[SyncBlackList]
ON [dbo].[BlackList]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [BlackList] set [Sync]=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='BlackList'
end

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Госномер', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BlackList', @level2type = N'COLUMN', @level2name = N'PlateNumber';

