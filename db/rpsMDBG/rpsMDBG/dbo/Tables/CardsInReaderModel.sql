﻿CREATE TABLE [dbo].[CardsInReaderModel] (
    [Id]               UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [CardId]           BIGINT NOT NULL,
    [ParkingEnterTime] DATETIME         NOT NULL,
    [LastRecountTime]  DATETIME         NOT NULL,
    [LastPaymentTime]  DATETIME         NOT NULL,
    [TSidFC]           INT              NOT NULL,
    [TPidFC]           INT              NOT NULL,
    [ZoneidFC]         TINYINT          NOT NULL,
    [ClientGroupidFC]  TINYINT          NOT NULL,
    [SumOnCard]        INT              NOT NULL,
    [Nulltime1]        DATETIME         NOT NULL,
    [Nulltime2]        DATETIME         NOT NULL,
    [Nulltime3]        DATETIME         NOT NULL,
    [TVP]              DATETIME         NOT NULL,
    [TKVP]             INT              NOT NULL,
    [ClientTypidFC]    INT              NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [CardsInReaderModel_fk] FOREIGN KEY ([CardId]) REFERENCES [dbo].[Cards] ([CardId]),
    CONSTRAINT [CardsInReaderModel_fk2] FOREIGN KEY ([ClientGroupidFC]) REFERENCES [dbo].[Group] ([idFC]),
    CONSTRAINT [CardsInReaderModel_fk3] FOREIGN KEY ([ZoneidFC]) REFERENCES [dbo].[ZoneModel] ([IdFC])
);



