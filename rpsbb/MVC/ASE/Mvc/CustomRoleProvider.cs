﻿using System;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Security;

using ASE.Data;

namespace ASE.MVC
{
    public class CustomRoleProvider : RoleProvider
    {
        int cacheTimeoutInMinutes = 30;

        public override void Initialize(string name, NameValueCollection config)
        {
            int val;

            if (!string.IsNullOrEmpty(config["cacheTimeoutInMinutes"]) && Int32.TryParse(config["cacheTimeoutInMinutes"], out val))
                cacheTimeoutInMinutes = val;

            base.Initialize(name, config);
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            var userRoles = GetRolesForUser(username);
            return userRoles.Contains(roleName);
        }

        public override string[] GetRolesForUser(string nickname)
        {
            //if (!HttpContext.Current.User.Identity.IsAuthenticated)
                //return null;

            var cacheKey = string.Format("UserRoles_{0}", nickname);

            if (HttpRuntime.Cache[cacheKey] != null)
                return (string[])HttpRuntime.Cache[cacheKey];

            var userRoles = new string[] { };

            using (var context = new DB())
            {
                userRoles = context.AuthUserRoles.Where(x => x.User.ToLower() == nickname.ToLower()).Select(x => x.Role).ToArray();
            }

            HttpRuntime.Cache.Insert(cacheKey, userRoles, null, DateTime.Now.AddMinutes(cacheTimeoutInMinutes), Cache.NoSlidingExpiration);

            return userRoles.ToArray();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            using (var db = new DB())
            {
                foreach(var username in usernames)
                {
                    foreach(var roleName in roleNames)
                    {
                        db.AuthUserRoles.Add(new AuthUserRole { Id = Guid.NewGuid(), Role = roleName, User = username });
                    }
                }
                db.SaveChanges();
            }
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            using (var db = new DB())
            {
                db.AuthRoles.Add(new AuthRole { Name = roleName, Description = roleName });
                db.SaveChanges();
            }
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            var ret = false;
            using (var db = new DB())
            {
                ret = db.AuthRoles.FirstOrDefault(x => x.Name.ToLower() == roleName.ToLower()) != null;
            }
            return ret;
        }
    }
}