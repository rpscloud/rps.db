﻿CREATE TABLE [dbo].[DisplayZone]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY DEFAULT (newid()), 
    [Sync] DATETIME NULL, 
    [_IsDeleted] BIT NULL, 
    [DeviceId] UNIQUEIDENTIFIER NULL, 
    [ZoneId] UNIQUEIDENTIFIER NULL
);



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DisplayZone', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DisplayZone', @level2type = N'COLUMN', @level2name = N'Sync';



GO
CREATE TRIGGER [dbo].[SyncDisplayZone]
ON [dbo].[DisplayZone]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [DisplayZone] set [Sync]=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='DisplayZone'
end

