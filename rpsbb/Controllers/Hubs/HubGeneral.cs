﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using ASE.MVC;
using Newtonsoft.Json;

namespace rps.SignalR
{
    public class HubGeneral : Hub
    {
        public string JoinInGroup(string group)
        {
            string ret = "ok";
            try
            {
                Groups.Add(Context.ConnectionId, group);

                Clients.Group(group).onJoinInGroup(Context.ConnectionId, group);

                if (group.ToLower() == "alarms")
                {
                    foreach(var item in ContextPerRequest.Db.Alarms.Where(x => x.End == null).ToList())
                        Clients.Client(Context.ConnectionId).onAlarm(JsonConvert.SerializeObject(item));
                }
            }
            catch(Exception exc)
            {
                ret = exc.Message;
            }

            return ret;
        }

        public void LeaveGroup(string group)
        {
            Groups.Remove(Context.ConnectionId, group);

            Clients.Group(group).onLeaveGroup(Context.ConnectionId, group);
        }

        public void MessageGroup(string group, string message)
        {
            Clients.Group(group).onMessageGroup(Context.ConnectionId, group, message);
        }

        public void MessageAll(string message)
        {
            Clients.All.onMessageAll(Context.ConnectionId, message);
        }

        public void Message(string userId, string message)
        {
            Clients.Client(userId).onMessage(Context.ConnectionId, message);
        }

        #region OnConnected + OnDisconnected + OnReconnected

        public override Task OnConnected()
        {
            Clients.All.onConnected(Context.ConnectionId);

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            Clients.All.onDisconnected(Context.ConnectionId);

            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            Clients.All.onReconnected(Context.ConnectionId);

            return base.OnReconnected();
        }

        #endregion OnConnected + OnDisconnected + OnReconnected
    }
}