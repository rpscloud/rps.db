﻿CREATE TABLE [dbo].[UserModel] (
    [Id]        UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [LoginName] VARCHAR (MAX)    NOT NULL,
    [Password]  VARCHAR (MAX)    NOT NULL,
    [Email]     VARCHAR (MAX)    NULL,
    [FullName]  VARCHAR (MAX)    NULL,
    [RoleId] UNIQUEIDENTIFIER NULL, 
    CONSTRAINT [PK_dbo.UserModel] PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_UserModel_RoleModel] FOREIGN KEY ([RoleId]) REFERENCES [RoleModel]([Id])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'UserModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИмяПользвателя', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'UserModel', @level2type = N'COLUMN', @level2name = N'LoginName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Пароль', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'UserModel', @level2type = N'COLUMN', @level2name = N'Password';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Почта', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'UserModel', @level2type = N'COLUMN', @level2name = N'Email';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наименование', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'UserModel', @level2type = N'COLUMN', @level2name = N'FullName';


GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Роль',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'UserModel',
    @level2type = N'COLUMN',
    @level2name = N'RoleId'