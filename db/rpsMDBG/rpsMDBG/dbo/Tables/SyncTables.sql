﻿CREATE TABLE [dbo].[SyncTables] (
    [Id]        INT           NOT NULL IDENTITY(1, 1),
    [TableName] VARCHAR (128) NOT NULL,
    [DateTime]  DATETIME       NULL, 
    [LastUpdate] DATETIME NULL, 
    [OrderSync] INT NULL, 
    [KeyField] NVARCHAR(MAX) NULL, 
    [SyncField] NVARCHAR(MAX) NULL, 
    [SyncFrom] INT NULL, 
    [IsRemove] BIT NULL, 
    [UslovieRemove] NVARCHAR(MAX) NULL, 
    [OrderSync2] INT NULL, 
    [SyncFrom2] INT NULL, 
    CONSTRAINT [PK_SyncTables] PRIMARY KEY ([Id])
);


GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'2-from server(2 уровень) 1-from device (1 уровень)',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'SyncTables',
    @level2type = N'COLUMN',
    @level2name = N'SyncFrom'
GO

CREATE UNIQUE INDEX [TableNameUK] ON [dbo].[SyncTables] ([TableName])
