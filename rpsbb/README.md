# README #



### Коллекции ###

"maps","items","lmenu","adm_menu","company","objects","users","roles"

### Примеры запросов ###

## Запрос ##
http://parking.dev.datasfera.com.ua:9292/test/company

## Ответ##

[
  {
    "_id": {"$oid": "551a2f502567a5843f000013"},
    "name": "Новая",
    "model": "company"
  },
  {
    "_id": {"$oid": "551ade462567a5843f00001a"},
    "name": "Новая компания",
    "model": "company"
  },
  {
    "_id": {"$oid": "551713b483ba888be38b4568"},
    "name": "ООО «Новые технологии»",
    "users": [
      {"user_id": "5517143383ba888be38b4569"},
      {"user_id": "5517143383ba888be38b456b"},
      {"user_id": "5517143383ba888be38b456a"},
      {"user_id": "5517143383ba888be38b456c"}
    ],
    "model": "company",
    "active": 0
  },
  {
    "_id": {"$oid": "551715eb83ba888be38b456d"},
    "name": "ООО «Сухие листья»",
    "users": [
      {"user_id": "5517143383ba888be38b456c"},
      {"user_id": "5517143383ba888be38b456b"},
      {"user_id": "5517143383ba888be38b456a"},
      {"user_id": "551ade7a2567a5843f00001b"}
    ],
    "model": "company",
    "active": 0
  },
  {
    "_id": {"$oid": "551b2ee22567a5843f00002a"},
    "name": "Новая компания",
    "model": "company",
    "active": 0
  }
]


## Запрос ##
http://parking.dev.datasfera.com.ua:9292/test/objects

## Ответ##
[
  {
    "_id": {"$oid": "551713cf83ba88f0e08b456b"},
    "name": "Парковка 4",
    "roles": [
      {
        "id": "5517133483ba88f0e08b4568",
        "rights": "rw"
      },
      {
        "id": "5517133483ba88f0e08b4567",
        "rights": "r"
      },
      {
        "id": "5517133483ba88f0e08b4569",
        "rights": "a"
      }
    ],
    "company": [
      "551715eb83ba888be38b456d",
      "551713b483ba888be38b4568",
      "551713cf83ba88f0e08b456b",
      "551ade462567a5843f00001a"
    ],
    "model": "objects",
    "active": 1,
    "objects": [
      "551713cf83ba88f0e08b456e",
      "551713cf83ba88f0e08b456d",
      "5517143383ba888be38b456c"
    ],
    "lat": "",
    "long": "",
    "db": "",
    "db_port": "",
    "db_user": "",
    "su": "",
    "su_port": "",
    "su_user": "",
    "su_pass": "",
    "db_pass": ""
  },
  {
    "_id": {"$oid": "551713cf83ba88f0e08b456c"},
    "name": "Парковка 3",
    "roles": [
      {
        "id": "5517133483ba88f0e08b4568",
        "rights": "rw"
      },
      {
        "id": "5517133483ba88f0e08b4567",
        "rights": "r"
      },
      {
        "id": "5517133483ba88f0e08b4569",
        "rights": "a"
      }
    ],
    "company": [
      "551715eb83ba888be38b456d",
      "551713b483ba888be38b4568",
      "5517138c83ba8866e38b4567",
      "551ade462567a5843f00001a"
    ],
    "model": "objects",
    "active": 0,
    "lat": "",
    "long": "",
    "db": "",
    "db_port": "",
    "db_user": "",
    "su": "",
    "su_port": "",
    "su_user": "",
    "su_pass": "",
    "db_pass": ""
  },
  {
    "_id": {"$oid": "551713cf83ba88f0e08b456d"},
    "name": "Парковка 2",
    "roles": [
      {
        "id": "5517133483ba88f0e08b4568",
        "rights": "rw"
      },
      {
        "id": "5517133483ba88f0e08b4567",
        "rights": "r"
      },
      {
        "id": "5517133483ba88f0e08b4569",
        "rights": "a"
      }
    ],
    "company": [
      "551715eb83ba888be38b456d",
      "551713b483ba888be38b4568",
      "5517138c83ba8866e38b4567",
      "551ade462567a5843f00001a",
      "551a2f502567a5843f000013"
    ],
    "model": "objects",
    "active": 1,
    "lat": "",
    "long": "",
    "db": "",
    "db_port": "",
    "db_user": "",
    "su": "",
    "su_port": "",
    "su_user": "",
    "su_pass": "",
    "db_pass": ""
  },
  {
    "_id": {"$oid": "551713cf83ba88f0e08b456a"},
    "name": "Парковка 5",
    "roles": [
      {
        "id": "5517133483ba88f0e08b4568",
        "rights": "rw"
      },
      {
        "id": "5517133483ba88f0e08b4567",
        "rights": "r"
      },
      {
        "id": "5517133483ba88f0e08b4569",
        "rights": "a"
      }
    ],
    "company": [
      "551713b483ba888be38b4568",
      "5517138c83ba8866e38b4567",
      "551ade462567a5843f00001a",
      "551715eb83ba888be38b456d"
    ],
    "model": "objects",
    "active": 1,
    "objects": [
      "551713cf83ba88f0e08b456e",
      "551713cf83ba88f0e08b456d",
      "5517143383ba888be38b456c"
    ],
    "lat": "",
    "long": "",
    "db": "",
    "db_port": "",
    "db_user": "",
    "su": "",
    "su_port": "",
    "su_user": "",
    "su_pass": "",
    "db_pass": ""
  }
 
]

## Запрос ##
http://parking.dev.datasfera.com.ua:9292/test/users

## Ответ##
[
  {
    "_id": {"$oid": "5517143383ba888be38b4569"},
    "name": "Администратор",
    "role_id": "5517133483ba88f0e08b4568",
    "objects": [
      "551713cf83ba88f0e08b456e",
      "551713cf83ba88f0e08b456d",
      "551713cf83ba88f0e08b456a"
    ],
    "model": "users",
    "active": 1
  },
  {
    "_id": {"$oid": "5517143383ba888be38b456a"},
    "name": "Овчинников В.В.",
    "role_id": "5517133483ba88f0e08b4568",
    "objects": [
      "551713cf83ba88f0e08b456e",
      "551713cf83ba88f0e08b456d",
      "551713cf83ba88f0e08b456a"
    ],
    "model": "users",
    "active": 1
  },
  {
    "_id": {"$oid": "5517143383ba888be38b456c"},
    "name": "Пупкин5 В.Н.",
    "role_id": "5517133483ba88f0e08b4568",
    "objects": [
      "551713cf83ba88f0e08b456e",
      "551713cf83ba88f0e08b456d",
      "551713cf83ba88f0e08b456a"
    ],
    "model": "users",
    "active": 1
  },
  {
    "_id": {"$oid": "551ade7a2567a5843f00001b"},
    "name": "Новый пользователь",
    "model": "users",
    "active": 0,
    "role_id": "5517133483ba88f0e08b4568",
    "objects": [
      "551713cf83ba88f0e08b456e",
      "551713cf83ba88f0e08b456d"
    ]
  },
  {
    "_id": {"$oid": "551af0d82567a5843f00001d"},
    "name": "Новый пользователь",
    "model": "users",
    "active": 0,
    "role_id": "5517133483ba88f0e08b4568",
    "objects": [
      "551713cf83ba88f0e08b456e",
      "551713cf83ba88f0e08b456d"
    ]
  },
  {
    "_id": {"$oid": "551af44b2567a5843f000021"},
    "name": "Новый пользователь",
    "model": "users",
    "active": 0,
    "role_id": "5517133483ba88f0e08b4568",
    "objects": [
      "551713cf83ba88f0e08b456e",
      "551713cf83ba88f0e08b456d"
    ]
  },
  {
    "_id": {"$oid": "5517143383ba888be38b456b"},
    "name": "Кузнецов Е.В.",
    "role_id": "5517133483ba88f0e08b4568",
    "objects": [
      "551713cf83ba88f0e08b456e",
      "551713cf83ba88f0e08b456d",
      "551713cf83ba88f0e08b456b",
      "551713cf83ba88f0e08b456a",
      "551b03e52567a5843f000027"
    ],
    "model": "users",
    "active": 1
  },
  {
    "_id": {"$oid": "551b2d032567a5843f000029"},
    "name": "Новый пользователь",
    "model": "users",
    "active": 0,
    "objects": []
  },
  {
    "_id": {"$oid": "551b2cee2567a5843f000028"},
    "name": "Новый пользователь",
    "model": "users",
    "active": 1,
    "objects": ["551713cf83ba88f0e08b456a"]
  }
]

