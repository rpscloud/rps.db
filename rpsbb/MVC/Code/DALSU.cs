﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ASE.Data;

using rps.Data;
using ASE.MVC;
using System.Threading;

namespace rpsbb.Code
{
    public static partial class DAL
    {

        #region DeviceMapModelJson

        public static DeviceMapModel DALSave(this DeviceMapModelJson model, DB db, bool patch = false)
        {
            DeviceMapModel m = db.DeviceMaps.FirstOrDefault(x => x.DeviceId == model.DeviceId);
            if (m == null)
            {
                m = new DeviceMapModel { DeviceId = model.DeviceId };
                db.DeviceMaps.Add(m);
            }
            Copy(model, m, patch, "Parking", "Role", "DeviceId");
            m.Deleted = false;
            db.SaveChanges();

            StartSync();
            return m;
        }


        public static DeviceMapModel DALDelete(this DeviceMapModelJson model, DB db)
        {
            var m = db.DeviceMaps.Find(model.DeviceId);
            m.Deleted = true;
            db.SaveChanges();

            StartSync();
            return m;
        }

        #endregion DeviceMapModelJson

        #region MapModelJson

        public static MapModel DALSave(this MapModelJson model, DB db, bool patch = false)
        {
            MapModel m = db.Maps.FirstOrDefault(x => x.Id == model.Id);
            if (m == null)
            {
                m = new MapModel { Id = Guid.NewGuid() };
                db.Maps.Add(m);
            }
            Copy(model, m, patch, "Parking", "Role", "Id");
            m.Deleted = false;
            db.SaveChanges();

            StartSync();
            return m;
        }


        public static MapModel DALDelete(this MapModelJson model, DB db)
        {
            var m = db.Maps.Find(model.Id);
            m.Deleted = true;
            db.SaveChanges();

            StartSync();
            return m;
        }

        #endregion MapModelJson
    }
}
