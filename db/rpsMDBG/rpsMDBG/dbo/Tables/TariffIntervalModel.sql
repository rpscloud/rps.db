﻿CREATE TABLE [dbo].[TariffIntervalModel] (
    [Id]         UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [TariffId]   UNIQUEIDENTIFIER NOT NULL,
    [Start]      INT              NOT NULL,
    [Duration]   INT              NOT NULL,
    [Amount]     DECIMAL (18, 2)  NOT NULL,
    [_IsDeleted] BIT              NOT NULL,
    [Sync]       DATETIME         NULL,
    CONSTRAINT [PK_dbo.TariffIntervalModel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.TariffIntervalModel_dbo.TariffModel_TariffId] FOREIGN KEY ([TariffId]) REFERENCES [dbo].[TariffModel] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_TariffId]
    ON [dbo].[TariffIntervalModel]([TariffId] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffIntervalModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тариф', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffIntervalModel', @level2type = N'COLUMN', @level2name = N'TariffId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Начало', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffIntervalModel', @level2type = N'COLUMN', @level2name = N'Start';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Длительность', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffIntervalModel', @level2type = N'COLUMN', @level2name = N'Duration';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffIntervalModel', @level2type = N'COLUMN', @level2name = N'Amount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffIntervalModel', @level2type = N'COLUMN', @level2name = N'Sync';

GO
CREATE TRIGGER [dbo].[SyncTariffIntervalModel]
ON [dbo].[TariffIntervalModel]
AFTER INSERT,UPDATE
AS
BEGIN
SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [TariffIntervalModel] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='TariffIntervalModel'
end
