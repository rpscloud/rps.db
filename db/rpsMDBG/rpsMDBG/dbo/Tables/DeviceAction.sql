﻿CREATE TABLE [dbo].[DeviceAction]
(
	[Id] INT NOT NULL PRIMARY KEY NONCLUSTERED, 
    [Description] VARCHAR(MAX) NOT NULL, 
    [IsWrite] BIT NOT NULL DEFAULT 0, 
    [WriteTime] INT NOT NULL DEFAULT 10, 
    [IsContinuous] BIT NOT NULL DEFAULT 0
)
