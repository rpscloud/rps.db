﻿CREATE TABLE [dbo].[BarCodeRights] (
    [Id]        UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [IdBCM]     UNIQUEIDENTIFIER NULL,
    [OperId]    UNIQUEIDENTIFIER NULL,
    [CompanyId] UNIQUEIDENTIFIER NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    --CONSTRAINT [BarCodeRights_BarCodeModel] FOREIGN KEY ([IdBCM]) REFERENCES [dbo].[BarCodeModel] ([Id]),
    CONSTRAINT [BarCodeRights_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[CompanyModel] ([Id]),
    CONSTRAINT [BarCodeRights_UserModel] FOREIGN KEY ([OperId]) REFERENCES [dbo].[UserModel] ([Id])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Компания - заказчик штрихкода', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeRights', @level2type = N'COLUMN', @level2name = N'CompanyId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Оператор - заказчик штрихкода', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeRights', @level2type = N'COLUMN', @level2name = N'OperId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Штрихкод', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BarCodeRights', @level2type = N'COLUMN', @level2name = N'IdBCM';

