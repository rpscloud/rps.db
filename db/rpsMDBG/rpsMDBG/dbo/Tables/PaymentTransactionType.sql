﻿CREATE TABLE [dbo].[PaymentTransactionType] (
    [Id]   INT  NOT NULL,
    [Name] VARCHAR (MAX)    NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

