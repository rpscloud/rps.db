using System;

namespace ASE.MVC
{
    public class AuthUserRole
    {
        public Guid Id { get; set; }

        public string User { get; set; }

        public string Role { get; set; }
    }
}