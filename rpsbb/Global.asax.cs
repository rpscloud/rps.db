﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using System.Threading;
using System.Data.SqlClient;
using System.Web.Mvc;
using System.Data.Sql;
using System.Web.Optimization;

using ASE.Data;
using ASE.MVC;
using rps.Data;
using System.Data.Entity;


namespace rpsbb
{
    public class WebApiApplication : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
            System.Data.Entity.Database.SetInitializer<DB>(new rpsbb.MVC.Data.AutoMigrationInit());
            //System.Data.Entity.Database.SetInitializer<DB>(new CreateDatabaseIfNotExists<DB>());
            ASE.Log.Root = Server.MapPath("~/App_Data/log.txt");

            //DB db = new DB("ConnectionStringL0");
            DB db = new DB();
            db.Maps.FirstOrDefault();
            //ALTER FUNCTION [dbo].[fnSiteUrl]() RETURNS varchar(255) AS BEGIN RETURN 'https://localhost'; END
            

            //InitDBs("ConnectionString");
            //InitDBs("ConnectionStringL0");
            //InitDBs("ConnectionStringL1");

            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //SqlDependency.Start(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            //SqlDependency dependency = new SqlDependency(new Sql"SELECT * FROM _CardType");

            Configurator.SetRemoteNotifier(db, System.Configuration.ConfigurationManager.AppSettings["RemoteNotifier"]);
        }

        protected void Application_EndRequest()
        {
            //SqlDependency.Stop(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            ContextPerRequest.DisposeDbContextPerRequest();
        }
    }
}
