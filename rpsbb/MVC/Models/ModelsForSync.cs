﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using ASE.MVC;

namespace rps.Data
{
    public interface ISyncTraking
    {
        Guid Id { get; set; }

        DateTime? ChangedDateTime { get; set; }

        bool Deleted { get; set; }
    }
    
    public class OrganizationModel : ISyncTraking
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [DisplayName("Наименование")]
        public string Name { get; set; }

        [JsonIgnore]
        public DateTime? ChangedDateTime { get; set; }

        [JsonIgnore]
        public bool Deleted { get; set; }
    }

    public class ParkingModel : ISyncTraking
    {
        [Key]
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [Required]
        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("СУ локальный")]
        public string SULocal { get; set; }

        [DisplayName("СУ VPN")]
        public string SURemote { get; set; }

        [DisplayName("Порт")]
        public int SUPort { get; set; }

        [DisplayName("Пользователь")]
        public string SUUser { get; set; }

        [DisplayName("Пароль")]
        public string SUPassword { get; set; }

        [DisplayName("Сайт3")]
        public string S3Site { get; set; }

        [DisplayName("Сервер2")]
        public string S3Sql { get; set; }

        [DisplayName("Пользователь")]
        public string S3User { get; set; }

        [DisplayName("Пароль")]
        public string S3Password { get; set; }

        [DisplayName("БД")]
        public string S3Db { get; set; }

        [DisplayName("Сервер2")]
        public string S2Sql { get; set; }

        [DisplayName("Пользователь")]
        public string S2User { get; set; }

        [DisplayName("Пароль")]
        public string S2Password { get; set; }

        [DisplayName("БД")]
        public string S2Db { get; set; }


        [DisplayName("Долгота")]
        public string Longitude { get; set; }

        [DisplayName("Широта")]
        public string Latitude { get; set; }

        [JsonIgnore]
        public DateTime? ChangedDateTime { get; set; }

        [JsonIgnore]
        public virtual List<UserParkingRoleModel> UserParkings { get; set; }

        public virtual List<RoleModel> Roles { get; set; }

        [JsonIgnore]
        public bool Deleted { get; set; }
    }

    public class OrganizationParkingModel : ISyncTraking
    {
        [Key]
        public Guid Id { get; set; }

        [Index("UIX_OrganizationId_ParkingId", 1, IsUnique = true)]
        [MyRequired]
        public Guid OrganizationId { get; set; }
        
        public virtual OrganizationModel Organization { get; set; }

        [Index("UIX_OrganizationId_ParkingId", 2, IsUnique = true)]
        [MyRequired]
        public Guid ParkingId { get; set; }

        public virtual ParkingModel Parking { get; set; }

        [JsonIgnore]
        public DateTime? ChangedDateTime { get; set; }

        public bool Active { get; set; }

        [JsonIgnore]
        public bool Deleted { get; set; }
    }

    public class UserModel : ISyncTraking
    {
        public UserModel()
        {
            CreateDate = DateTime.Now;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public string LoginName { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string Name { get; set; }

        public string Email { get; set; }

        [JsonIgnore]
        public DateTime CreateDate { get; set; }

        [JsonIgnore]
        public DateTime? ChangedDateTime { get; set; }

        /// <summary>
        /// Add to all parkings
        /// </summary>
        public bool Is_Admin { get; set; }

        /// <summary>
        /// Allow to MC
        /// </summary>
        public bool Is_MC { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Is_Root { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Is_RPS { get; set; }

        [JsonIgnore]
        public bool Deleted { get; set; }

        public Guid? DefaultParking { get; set; }
    }

    public class RightModel : ISyncTraking
    {
        [Key]
        [JsonProperty(Order = 1)]
        public Guid Id { get; set; }

        [JsonProperty(Order = 2)]
        public Guid? ParentId { get; set; }

        [JsonProperty(Order = 3)]
        [JsonIgnore]
        public virtual RightModel Parent { get; set; }

        [JsonProperty(Order = 4)]
        [Required]
        public string Name { get; set; }

        [JsonProperty(Order = 5)]
        public bool Active { get; set; }

        [JsonIgnore]
        public DateTime? ChangedDateTime { get; set; }

        [JsonProperty(Order = 7)]
        public virtual List<RightModel> Children { get; set; }

        [JsonIgnore]
        public bool Deleted { get; set; }
    }

    public class RoleModel : ISyncTraking
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        [MyRequired]
        public Guid ParkingId { get; set; }

        [JsonIgnore]
        public virtual ParkingModel Parking { get; set; }

        public bool Active { get; set; }

        [JsonIgnore]
        public DateTime? ChangedDateTime { get; set; }

        [JsonIgnore]
        public virtual List<RightsInRoleModel> RightsInRole { get; set; }

        public bool FullAccess { get; set; }

        [JsonIgnore]
        public bool Deleted { get; set; }
    }

    public class RightsInRoleModel : ISyncTraking
    {
        [Key]
        public Guid Id { get; set; }

        [MyRequired]
        public Guid RightId { get; set; }

        [JsonIgnore]
        public virtual RightModel Right { get; set; }

        [MyRequired]
        public Guid RoleId { get; set; }

        [JsonIgnore]
        public virtual RoleModel Role { get; set; }

        public bool Active { get; set; }

        [JsonIgnore]
        public DateTime? ChangedDateTime { get; set; }

        [JsonIgnore]
        public bool Deleted { get; set; }
    }

    public class UserParkingRoleModel : ISyncTraking
    {
        [Key]
        public Guid Id { get; set; }

        public bool Active { get; set; }

        [Index("UIX_UserId_ParkingId", 1, IsUnique = true)]
        [MyRequired]
        public Guid UserId { get; set; }

        public virtual UserModel User { get; set; }

        [Index("UIX_UserId_ParkingId", 2, IsUnique = true)]
        [MyRequired]
        public Guid ParkingId { get; set; }

        public virtual ParkingModel Parking { get; set; }

        public Guid? RoleId { get; set; }

        public virtual RoleModel Role { get; set; }

        [JsonIgnore]
        public DateTime? ChangedDateTime { get; set; }

        [JsonIgnore]
        public bool Deleted { get; set; }
    }


    public class MenuItemModel : ISyncTraking
    {
        [Key]
        public Guid Id { get; set; }

        public Guid? ParentId { get; set; }

        public string Category { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public int SortOrder { get; set; }

        public bool Active { get; set; }

        [JsonIgnore]
        public DateTime? ChangedDateTime { get; set; }

        [JsonIgnore]
        public bool Deleted { get; set; }
    }
}
