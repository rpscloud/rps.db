﻿CREATE TABLE [dbo].[Translations] (
    [translation] NVARCHAR (MAX) COLLATE Cyrillic_General_CS_AS NOT NULL,
    [lang]        VARCHAR (2)    NOT NULL,
    [source]      VARCHAR (255)  COLLATE Cyrillic_General_CS_AS NOT NULL,
    [smodule]     VARCHAR (255)  NOT NULL,
    CONSTRAINT [PK_Translations] PRIMARY KEY CLUSTERED ([smodule] ASC, [source] ASC, [lang] ASC)
);

