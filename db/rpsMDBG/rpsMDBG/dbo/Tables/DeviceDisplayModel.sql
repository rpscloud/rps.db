﻿CREATE TABLE [dbo].[DeviceDisplayModel] (
    [DeviceId]   UNIQUEIDENTIFIER NOT NULL,
    [ZoneId]     UNIQUEIDENTIFIER NULL,
    [_IsDeleted] BIT              NOT NULL,
    CONSTRAINT [PK_dbo.DeviceDisplayModel] PRIMARY KEY CLUSTERED ([DeviceId] ASC),
    CONSTRAINT [FK_dbo.DeviceDisplayModel_dbo.DeviceModel_DeviceId] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[DeviceModel] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_DeviceId]
    ON [dbo].[DeviceDisplayModel]([DeviceId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ZoneId]
    ON [dbo].[DeviceDisplayModel]([ZoneId] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Устройство', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceDisplayModel', @level2type = N'COLUMN', @level2name = N'DeviceId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Зона', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceDisplayModel', @level2type = N'COLUMN', @level2name = N'ZoneId';

