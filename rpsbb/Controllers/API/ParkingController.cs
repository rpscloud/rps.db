﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

using ASE.MVC;
using ASE.Data;

using rps.Data;
using rpsbb.Code;

namespace rpsbb.Controllers
{
    [Authorize]
    public class ParkingController : ApiControllerAdv<DB>
    {
        [System.Web.Http.HttpGet]
        public ParkingModelJson Get(Guid id)
        {
            return db.Parkings.Find(id).Get();
        }

        [System.Web.Http.HttpGet]
        public IEnumerable<ParkingModelJson> List()
        {
            var q = db.Parkings.Where(x => !x.Deleted);
            if ((UserModel != null) && (!UserModel.Is_Admin))
                q = db.UserParkingRoles.Where(x => !x.Deleted & !x.Parking.Deleted & x.UserId == UserId).Select(x => x.Parking);

            return q.ToList().Select(x => x.Get());
        }

        [System.Web.Http.HttpPost]
        public ParkingModelJson Add(ParkingModelJson model)
        {
            model.Id = Guid.Empty;
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPut]
        public ParkingModelJson Put(ParkingModelJson model)
        {
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPatch]
        public ParkingModelJson Patch(ParkingModelJson model)
        {
            return model.DALSave(db, true).Get();
        }

        [System.Web.Http.HttpDelete]
        public ParkingModelJson Delete([FromUri] ParkingModelJson model)
        {
            return model.DALDelete(db).Get();
        }
    }
}
