﻿CREATE TABLE [dbo].[Update] (
    [SystemVerId] INT              NOT NULL,
    [DeviceId]    UNIQUEIDENTIFIER NULL,
    [DeviceName]  VARCHAR (MAX)    NOT NULL,
    [Status]      INT              DEFAULT (NULL) NULL,
    [Message]     TEXT             DEFAULT (NULL) NULL,
    [Sync]        DATETIME         DEFAULT (NULL) NULL,
    CONSTRAINT [Update_DeviceModel_Id_fk] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[DeviceModel] ([Id])
);

