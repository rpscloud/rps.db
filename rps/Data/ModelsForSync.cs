﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rps.Data
{
    public interface IChanged
    {
        Guid Id { get; set; }

        DateTime? ChangedDateTime { get; set; }
    }
    
    public class OrganizationModel : IChanged
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [DisplayName("Наименование")]
        public string Name { get; set; }

        public DateTime? ChangedDateTime { get; set; }
    }

    [Table("Parking")]
    public class ParkingModel : IChanged
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("СУ локальный")]
        public string SULocal { get; set; }

        [DisplayName("СУ VPN")]
        public string SURemote { get; set; }

        [DisplayName("Порт")]
        public int SUPort { get; set; }

        [DisplayName("Пользователь")]
        public string SUUser { get; set; }

        [DisplayName("Пароль")]
        public string SUPassword { get; set; }

        [DisplayName("Сайт3")]
        public string S3Site { get; set; }

        [DisplayName("Сервер2")]
        public string S3Sql { get; set; }

        [DisplayName("Пользователь")]
        public string S3User { get; set; }

        [DisplayName("Пароль")]
        public string S3Password { get; set; }

        [DisplayName("БД")]
        public string S3Db { get; set; }

        [DisplayName("Сервер2")]
        public string S2Sql { get; set; }

        [DisplayName("Пользователь")]
        public string S2User { get; set; }

        [DisplayName("Пароль")]
        public string S2Password { get; set; }

        [DisplayName("БД")]
        public string S2Db { get; set; }


        [DisplayName("Долгота")]
        public string Longitude { get; set; }

        [DisplayName("Широта")]
        public string Latitude { get; set; }

        public DateTime? ChangedDateTime { get; set; }

        public virtual List<UserParkingRoleModel> UserParkings { get; set; }

        public virtual List<RoleModel> Roles { get; set; }
    }

    public class OrganizationParkingModel : IChanged
    {
        [Key]
        public Guid Id { get; set; }
        
        public bool Active { get; set; }

        public Guid OrganizationId { get; set; }
        
        public virtual OrganizationModel Organization { get; set; }

        public Guid ParkingId { get; set; }

        public virtual ParkingModel Parking { get; set; }

        public DateTime? ChangedDateTime { get; set; }
    }

    public class UserModel : IChanged
    {
        [Key]
        public Guid Id { get; set; }

        public string FullName { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public DateTime? ChangedDateTime { get; set; }
    }

    public class RightModel : IChanged
    {
        [Key]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }

        public DateTime? ChangedDateTime { get; set; }
    }

    public class RoleModel : IChanged
    {
        [Key]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid ParkingId { get; set; }

        public virtual ParkingModel Parking { get; set; }

        public bool Active { get; set; }

        public DateTime? ChangedDateTime { get; set; }
    }

    public class RightsInRoleModel : IChanged
    {
        [Key]
        public Guid Id { get; set; }

        public Guid RightId { get; set; }

        public virtual RightModel Right { get; set; }

        public Guid RoleId { get; set; }

        public virtual RoleModel Role { get; set; }

        public bool Active { get; set; }

        public DateTime? ChangedDateTime { get; set; }
    }

    public class UserParkingRoleModel : IChanged
    {
        [Key]
        public Guid Id { get; set; }

        public bool Active { get; set; }

        public Guid UserId { get; set; }

        public virtual UserModel User { get; set; }

        public Guid ParkingId { get; set; }

        public virtual ParkingModel Parking { get; set; }

        public Guid? RoleId { get; set; }

        public virtual RoleModel Role { get; set; }

        public DateTime? ChangedDateTime { get; set; }
    }

    public
}
