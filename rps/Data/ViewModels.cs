﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ASE.Data;

namespace rps.Data
{
    public class ViewModelSettings
    {
        [Display(Name="Сервер")]
        public string Server { get; set; }

        public static ViewModelSettings Load(DB db)
        {
            ViewModelSettings ret = new ViewModelSettings();
            var s = db.Settings.FirstOrDefault(x => x.Name == "Server");
            if (s != null)
                ret.Server = s.Value;

            return ret;
        }

        public void Save(DB db)
        {
            var s = db.Settings.FirstOrDefault(x => x.Name == "Server");
            if (s == null)
                db.Settings.Add(new Setting { Name = "Server", Type = "String", Value = Server });
            else
                s.Value = Server;

            db.SaveChanges();
        }
    }

    public class ViewSortFilterModel
    {
        public string Sort { get; set; }

        public string Filter { get; set; }
    }
}
