﻿using System;
using System.Linq;
using System.Linq.Expressions;
using EntityFramework.Extensions;
using EntityFramework.Batch;
using EntityFramework.Future;
using RPS_Shared;

namespace rps.Sync
{
    public static partial class Synchronize
    {
        /// <summary>
        /// USAGE ONLY LEVEL 1
        /// </summary>
        /// <param name="model"></param>
        private static int DeviceRackModel<TEntity>(DeviceModel remoteDb, Expression<Func<DeviceRackModel, bool>> predicate, Expression<Func<DeviceRackModel, DeviceRackModel>> update) where TEntity : class
        {
            if (remoteDb == null)
                return 1;

            try
            {
                using (var db = new Entities()) //LOCAL
                {
                    var cs = String.Format("data source={0};Initial Catalog={1};User ID={2};Password={3};{4}", remoteDb.Host, remoteDb.DataBase, remoteDb.User, remoteDb.Password, remoteDb.Advanced);
                    if (cs.ToLower().IndexOf(";connection timeout=") == -1)
                        cs += ";Connection Timeout=10";

                    using (var dbR = new Entities(cs)) //SITE2-3
                    {
                        var list = db.DeviceRackModel.Where(predicate).ToList();

                        foreach (var item in list)
                        {
                            var device = item.DeviceModel;
                            db.Entry(device).State = System.Data.Entity.EntityState.Detached;
                            db.Entry(item).State = System.Data.Entity.EntityState.Detached;

                            var deviceR = dbR.DeviceModel.Find(device.Id);
                            if (deviceR == null)
                                dbR.DeviceModel.Add(device);

                            var itemR = dbR.DeviceRackModel.Find(item.DeviceId);

                            if (itemR == null)
                                dbR.Entry(item).State = System.Data.Entity.EntityState.Added;
                            else
                            {
                                dbR.Entry(itemR).State = System.Data.Entity.EntityState.Detached;
                                dbR.Entry(item).State = System.Data.Entity.EntityState.Modified;
                            }

                            dbR.SaveChanges();

                            db.DeviceRackModel.Where(x => x.C_Modified == item.C_Modified & x.DeviceId == item.DeviceId).Update<DeviceRackModel>(update);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ASE.Log.L(exc);

                return 2;
            }

            return 0;
        }
    }
}
