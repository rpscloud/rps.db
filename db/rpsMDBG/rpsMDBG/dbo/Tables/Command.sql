﻿CREATE TABLE [dbo].[Command] (
    [Id]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]     INT            NOT NULL,
    [TypePacket] NVARCHAR (50)  NOT NULL,
    [DataPacket] NVARCHAR (MAX) NULL,
    [Message]    NVARCHAR (MAX) NULL,
    [DateCreate] DATETIME       NOT NULL,
    CONSTRAINT [PK_Command] PRIMARY KEY CLUSTERED ([Id] ASC)
);

