﻿CREATE TABLE [dbo].[Log]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY NONCLUSTERED, 
	[ObjectId] UNIQUEIDENTIFIER NULL, 
    [DeviceId] UNIQUEIDENTIFIER NULL, 
    [AlarmId] UNIQUEIDENTIFIER NULL, 
    [TransactionId] UNIQUEIDENTIFIER NULL, 
	[DeviceActionId] INT NOT NULL,
	[TimeBegin] DateTime,
	[Message] TEXT,
    [Sync] DATETIME NULL, 
    CONSTRAINT [FK_Log_DeviceModel] FOREIGN KEY ([DeviceId]) REFERENCES [DeviceModel]([Id]),
    CONSTRAINT [FK_Log_DeviceAction] FOREIGN KEY ([DeviceActionId]) REFERENCES [DeviceAction]([Id])
);

GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Флаг синхронизации со вторым уровнем.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Log', @level2type = N'COLUMN', @level2name = N'Sync';
GO


CREATE TRIGGER [dbo].[SyncLog]
ON [dbo].[Log]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [Log] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='Log'
end

