using ASE.MVC;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace rps.Data
{
    [NotMapped]
    public class DeviceModelJson : DeviceModel
    {
    }

    public static partial class Model2Json
    {
        public static DeviceModelJson Get(this DeviceModel model)
        {
            var ret = (DeviceModelJson)Copy(model, new DeviceModelJson());

            return ret;
        }

        public static DeviceModel DALSave(this DeviceModelJson model, bool patch = false, Guid? newId = null)
        {
            DeviceModel m = null;
            if (model.Id == Guid.Empty)
            {
                model.Id = newId.HasValue ? newId.Value : Guid.NewGuid();
                m = new DeviceModel();
                ContextPerRequest.Db.Devices.Add(m);
            }
            else
            {
                m = ContextPerRequest.Db.Devices.Find(model.Id);
            }
            Copy(model, m, patch);
            ContextPerRequest.Db.SaveChanges();

            //StartSync();
            return m;
        }

        public static DeviceModel DALDelete(this DeviceModelJson model)
        {
            var m = ContextPerRequest.Db.Devices.Find(model.Id);
            //m.Deleted = true;
            //db.Organizations.Remove(m);
            ContextPerRequest.Db.SaveChanges();

            //StartSync();
            return m;
        }
    }
}