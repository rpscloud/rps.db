﻿CREATE TABLE [dbo].[CarParkCapacity] (
    [Id]           UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [TimeDate]     DATETIME         NOT NULL,
    [ZoneId]       UNIQUEIDENTIFIER NOT NULL,
    [NumberOfCars] INT              NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [CarParkCapacity_fk] FOREIGN KEY ([ZoneId]) REFERENCES [dbo].[ZoneModel] ([Id])
);




GO
CREATE NONCLUSTERED INDEX [CarParkCapacity_TimeDate_index]
    ON [dbo].[CarParkCapacity]([TimeDate] ASC);


GO
CREATE NONCLUSTERED INDEX [CarParkCapacity_NumberOfCars_index]
    ON [dbo].[CarParkCapacity]([NumberOfCars] ASC);

