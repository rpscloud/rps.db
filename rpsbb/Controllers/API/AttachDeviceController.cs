﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using rps.Data;
using ASE.Data;

namespace rpsbb.Controllers
{
    public class AttachDeviceController : ApiController
    {
        [System.Web.Http.HttpPost]
        public object Add(ConnectionModel model)
        {
            string cs = String.Format("data source={0};Initial Catalog={1};User ID={2};Password={3};{4}", model.Host, model.Database, model.User, model.Password, model.Advanced);
            try
            {
                using (var dbR = new DB(cs))
                {
                    if (dbR.Devices.Count() == 0)
                        throw new Exception("no device"); ;

                    if (dbR.DeviceRacks.Count() == 1)
                    {
                        var device = dbR.DeviceRacks.FirstOrDefault().Get();
                        var ld = dbR.DeviceRacks.FirstOrDefault(x => x.DeviceId == device.DeviceId);
                        device.ZoneId = model.ZoneId;
                        return new { type = "DeviceRackModel", device = device, CurrentZoneId = ld == null ? null : ld.ZoneId };
                    }

                    if (dbR.DeviceCMs.Count() == 1)
                    {
                        var device = dbR.DeviceCMs.FirstOrDefault().Get();
                        var ld = dbR.DeviceCMs.FirstOrDefault(x => x.DeviceId == device.DeviceId);
                        device.ZoneId = model.ZoneId;
                        return new { type = "DeviceCMModel", device = device, CurrentZoneId = ld == null ? null : ld.ZoneId };
                    }

                    if (dbR.DeviceDisplays.Count() == 1)
                    {
                        var device = dbR.DeviceDisplays.FirstOrDefault().Get();
                        var ld = dbR.DeviceDisplays.FirstOrDefault(x => x.DeviceId == device.DeviceId);
                        device.ZoneId = model.ZoneId;
                        return new { type = "DeviceDisplayModel", device = device, CurrentZoneId = ld == null ? null : ld.ZoneId };
                    }

                    if (dbR.DeviceZCUs.Count() == 1)
                    {
                        var device = dbR.DeviceZCUs.FirstOrDefault().Get();
                        return new { type = "DeviceZCUModel", device = device };
                    }
                }
            }
            catch(Exception exc)
            {
                throw new Exception(exc.Message);
            }

            throw new Exception("no result");
        }
    }
}
