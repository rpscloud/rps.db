﻿CREATE TABLE [dbo].[DeviceModel] (
    [Id]         UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Type]       INT              NOT NULL,
    [Name]       VARCHAR (MAX)    NULL,
    [Host]       VARCHAR (MAX)    NULL,
    [User]       VARCHAR (MAX)    NULL,
    [Password]   VARCHAR (MAX)    NULL,
    [Advanced]   VARCHAR (MAX)    NULL,
    [_IsDeleted] BIT              NULL,
    [DataBase]   VARCHAR (MAX)    NULL,
    [Sync] DATETIME NULL, 
    [SlaveCode] NVARCHAR(50) NULL, 
    CONSTRAINT [PK_dbo.DeviceModel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.DeviceModel_dbo.DeviceTypeModel_Type] FOREIGN KEY ([Type]) REFERENCES [dbo].[DeviceTypeModel] ([Id]) ON DELETE CASCADE
);




GO
CREATE NONCLUSTERED INDEX [IX_Type]
    ON [dbo].[DeviceModel]([Type] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceModel', @level2type = N'COLUMN', @level2name = N'Type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наименование', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceModel', @level2type = N'COLUMN', @level2name = N'Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сетевой адрес', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceModel', @level2type = N'COLUMN', @level2name = N'DataBase';
Go

CREATE TRIGGER [dbo].[SyncDeviceModel]
ON [dbo].[DeviceModel]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [DeviceModel] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='DeviceModel'
end

GO
