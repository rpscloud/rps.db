﻿CREATE TABLE [dbo].[RackAltimetrModel] (
    [Id]                 INT              IDENTITY (1, 1) NOT NULL,
    [Sync]               DATETIME         NULL,
    [DeviceId]           UNIQUEIDENTIFIER NOT NULL,
    [Input]              INT              NOT NULL,
    [RackAltimetrModeId] INT              NOT NULL,
    [TariffId]           TINYINT              NULL,
    [Mode_Id]            INT              NULL,
    [TariffScheduleId] TINYINT NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RackAltimetrModel_DeviceModel] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[DeviceModel] ([Id]),
    CONSTRAINT [RackAltimetrModel_fk] FOREIGN KEY ([RackAltimetrModeId]) REFERENCES [dbo].[RackAltimetrModeModel] ([Id]),
    CONSTRAINT [RackAltimetrModel_fk2] FOREIGN KEY (TariffId) REFERENCES [dbo].[Tariffs] ([IdFC]),
	CONSTRAINT [RackAltimetrModel_fk3] FOREIGN KEY (TariffScheduleId) REFERENCES [dbo].[TariffScheduleModel] ([IdFC])

);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RackAltimetrModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Флаг синхронизации со 2ым ур.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RackAltimetrModel', @level2type = N'COLUMN', @level2name = N'Sync';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Устройство', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RackAltimetrModel', @level2type = N'COLUMN', @level2name = N'DeviceId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Номер дискретного входа PortDiscret3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RackAltimetrModel', @level2type = N'COLUMN', @level2name = N'Input';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RackAltimetrModel', @level2type = N'COLUMN', @level2name = N'RackAltimetrModeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тариф (ТП) для разовых на въезде', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RackAltimetrModel', @level2type = N'COLUMN', @level2name = N'TariffId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'из RackAltimetrMode', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RackAltimetrModel', @level2type = N'COLUMN', @level2name = N'Mode_Id';

GO
CREATE TRIGGER [dbo].[SyncRackAltimetrModel]
ON [dbo].[RackAltimetrModel]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [RackAltimetrModel] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='RackAltimetrModel'
end
