﻿CREATE PROCEDURE [dbo].[get_last_updated]
	@table nvarchar(max)=N'UserModel',
	@updated datetime OUTPUT
AS
	
    SET NOCOUNT ON;

	if(exists((SELECT last_user_update
	FROM   sys.dm_db_index_usage_stats

		WHERE object_id = object_id(@table)
	)))
	begin
		set @updated = (SELECT TOP 1 last_user_update
		FROM   sys.dm_db_index_usage_stats

			WHERE object_id = object_id(@table)
		)
	end
    ELSE 
	begin
		set @updated=NULL;
	end

	return