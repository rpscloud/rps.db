﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

using ASE.Data;
using ASE.MVC;

using rps.Data;
using rpsbb.Code;
using System.Web.Security;

namespace rpsbb.Controllers
{
    [Authorize]
    public class MenuItemModelController : ApiControllerAdv<DB>
    {
        [System.Web.Http.HttpGet]
        public MenuItemModelJson Get(Guid id)
        {
            return db.MenuItems.Find(id).Get();
        }

        [System.Web.Http.HttpGet]
        public IEnumerable<MenuItemModelJson> List([FromUri] string category = null)
        {
            if ((category != null) && (category.ToLower() == "auth"))
            {
                List<MenuItemModelJson> list = new List<MenuItemModelJson>();
                var user = db.Users.Find(UserId);
                if ((user == null) || (user.Is_MC))
                    list.Add(new MenuItemModelJson { SortOrder = 0, Name = "Мониторинговый центр", Id = Guid.Parse("A76093E4-0CED-4A7A-BC7B-824399B789FA"), ParentId = Guid.Empty, Url = "#monitor" });

                var parkings = db.UserParkingRoles.Where(x => x.UserId == UserId).Select(x => new MenuItemModelJson { Id = x.ParkingId, Name = x.Parking.Name, Url = "#monitor" }).ToList();
                for (int i = 0; i < parkings.Count; i++)
                {
                    parkings[i].SortOrder = i + 1;
                    list.Add(parkings[i]);
                }

                /*if ((user != null) && (user.DefaultParking != null))
                    list.Add(new MenuItemModelJson { SortOrder = 100, Name = "Запомнить выбор", Id = Guid.Parse("2AF7A208-13F3-4F3B-8002-3ADE6E825206"), ParentId = user.DefaultParking, Url = "#monitor" });
                else
                    list.Add(new MenuItemModelJson { SortOrder = 100, Name = "Запомнить выбор", Id = Guid.Parse("2AF7A208-13F3-4F3B-8002-3ADE6E825206"), ParentId = Guid.Empty, Url = "#monitor" });*/

                return list;
            }

            return db.MenuItems.Where(x => !x.Deleted & x.Category == category).ToList().Select(x => x.Get());
        }

        /*[System.Web.Http.HttpPost]
        public MenuItemModelJson Add(MenuItemModelJson model)
        {
            model.Id = Guid.Empty;
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPut]
        public MenuItemModelJson Put(MenuItemModelJson model)
        {
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPatch]
        public MenuItemModelJson Patch(MenuItemModelJson model)
        {
            return model.DALSave(db, true).Get();
        }

        [System.Web.Http.HttpDelete]
        public MenuItemModelJson Delete([FromUri] MenuItemModelJson model)
        {
            return model.DALDelete(db).Get();
        }*/
    }
}
