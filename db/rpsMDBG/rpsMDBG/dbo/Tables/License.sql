﻿CREATE TABLE [dbo].[License]
(
	[SlaveCode] NVARCHAR(50) NOT NULL PRIMARY KEY, 
    [Data] IMAGE NULL, 
    [Sync] DATETIME NULL
)
go

CREATE TRIGGER [dbo].[SyncLicense]
ON [dbo].[License]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [License] set Sync=@dt	where SlaveCode in (select SlaveCode from inserted)
	update SyncTables set [DateTime]=@dt where TableName='License'
end
go

