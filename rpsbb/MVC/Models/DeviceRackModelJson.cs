using ASE.MVC;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace rps.Data
{
    [NotMapped]
    public class DeviceRackModelJson : DeviceRackModel
    {

    }

    public static partial class Model2Json
    {
        public static DeviceRackModelJson Get(this DeviceRackModel model)
        {
            var ret = (DeviceRackModelJson)Copy(model, new DeviceRackModelJson());
            ret.Device = ret.Device.Get();
            //ret.Device.DisplayZone = ret.Device.DisplayZone.Get();

            return ret;
        }

        /*public static DeviceCMModel DALSave(this DeviceCMModelJson model, bool patch = false, Guid? newId = null)
        {
            DeviceCMModel m = null;
            if (model.DeviceId == Guid.Empty)
            {
                model.DeviceId = newId.HasValue ? newId.Value : Guid.NewGuid();
                m = new DeviceCMModel();
                ContextPerRequest.Db.DeviceCMs.Add(m);
            }
            else
            {
                m = ContextPerRequest.Db.DeviceCMs.Find(model.DeviceId);
            }
            Copy(model, m, patch);
            ContextPerRequest.Db.SaveChanges();

            //StartSync();
            return m;
        }

        public static DeviceCMModel DALDelete(this DeviceCMModelJson model)
        {
            var m = ContextPerRequest.Db.DeviceCMs.Find(model.DeviceId);
            //m.Deleted = true;
            //db.Organizations.Remove(m);
            ContextPerRequest.Db.SaveChanges();

            //StartSync();
            return m;
        }*/
    }
}