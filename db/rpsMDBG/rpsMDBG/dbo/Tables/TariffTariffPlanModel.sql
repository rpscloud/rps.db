﻿CREATE TABLE [dbo].[TariffTariffPlanModel] (
    [Id]           UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [TariffId]     UNIQUEIDENTIFIER NOT NULL,
    [TariffPlanId] UNIQUEIDENTIFIER NOT NULL,
    [_IsDeleted]   BIT              NOT NULL,
    [Sync]         DATETIME         NULL,
    CONSTRAINT [PK_dbo.TariffTariffPlanModel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.TariffTariffPlanModel_dbo.TariffModel_TariffId] FOREIGN KEY ([TariffId]) REFERENCES [dbo].[TariffModel] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.TariffTariffPlanModel_dbo.Tariffs_TariffPlanId] FOREIGN KEY ([TariffPlanId]) REFERENCES [dbo].[Tariffs] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_TariffId]
    ON [dbo].[TariffTariffPlanModel]([TariffId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TariffPlanId]
    ON [dbo].[TariffTariffPlanModel]([TariffPlanId] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffTariffPlanModel', @level2type = N'COLUMN', @level2name = N'Sync';

GO
CREATE TRIGGER [dbo].[SyncTariffTariffPlanModel]
ON [dbo].[TariffTariffPlanModel]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [TariffTariffPlanModel] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='TariffTariffPlanModel'
end
