﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

using ASE.MVC;
using ASE.Data;

using rps.Data;
using rpsbb.Code;
using System.Web;

namespace rpsbb.Controllers
{
    [Authorize]
    public class DeviceCMController : ApiControllerAdv<DB>
    {
        [System.Web.Http.HttpGet]
        public DeviceCMModelJson Get(Guid id)
        {
            return db.DeviceCMs.Find(id).Get();
        }

        [System.Web.Http.HttpGet]
        public IEnumerable<DeviceCMModelJson> List()
        {
            var q = db.DeviceCMs;

            return q.ToList().Select(x => x.Get());
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.HttpPost]
        public DeviceCMModel Add(DeviceCMModelJson model)
        {
            if (model.Device == null)
                return model;

            string cs = String.Format("data source={0};Initial Catalog={1};User ID={2};Password={3};{4}", model.Device.Host, model.Device.Database, model.Device.User, model.Device.Password, model.Device.Advanced);
            try
            {
                using (var dbR = new DB(cs))
                {
                    var localCS = new System.Data.SqlClient.SqlConnectionStringBuilder(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                    var site2 = dbR.Devices.FirstOrDefault(x => x.Type == -1);
                    if ((site2 == null) & (!Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["Site3"].ToString())))
                    {
                        var device = new DeviceModel { Id = Guid.NewGuid(), Type = -1, Host = localCS.DataSource, Database = localCS.InitialCatalog, User = localCS.UserID, Password = localCS.Password };
                        dbR.Devices.Add(device);
                        dbR.SaveChanges();
                    }

                    var site3 = dbR.Devices.FirstOrDefault(x => x.Type == -2);
                    if ((site3 == null) & (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["Site3"].ToString())))
                    {
                        var device = new DeviceModel { Id = Guid.NewGuid(), Type = -2, Host = localCS.DataSource, Database = localCS.InitialCatalog, User = localCS.UserID, Password = localCS.Password };
                        dbR.Devices.Add(device);
                        dbR.SaveChanges();
                    }

                    model._IsSync2 = false;
                    model._IsSync3 = false;
                    var item = dbR.DeviceCMs.Find(model.DeviceId);
                    Model2Json.CopySV(model, item);

                    var zone = dbR.Zones.Find(item.ZoneId);
                    if (zone == null)
                        dbR.Zones.Add(new ZoneModel { Id = item.ZoneId.Value });
                    dbR.Entry(item).State = System.Data.Entity.EntityState.Modified;

                    if (model.CashAcceptorAllow != null)
                        foreach (var ca in model.CashAcceptorAllow)
                        {
                            try
                            {
                                var caDb = dbR.CashAcceptorAllows.FirstOrDefault(x => x.DeviceId == model.DeviceId & x.BanknoteId == ca.Banknote.Id);
                                if (caDb == null)
                                    dbR.CashAcceptorAllows.Add(new CashAcceptorAllowModel { DeviceId = model.DeviceId, BanknoteId = ca.Banknote.Id, Allow = ca.Allow });
                                else
                                    caDb.Allow = ca.Allow;
                            }
                            catch
                            {

                            }
                        }

                    if (model.CoinAcceptorAllow != null)
                        foreach (var ca in model.CoinAcceptorAllow)
                        {
                            try
                            {
                                var caDb = dbR.CoinAcceptorAllows.FirstOrDefault(x => x.DeviceId == model.DeviceId & x.CoinId == ca.Coin.Id);
                                if (caDb == null)
                                    dbR.CoinAcceptorAllows.Add(new CoinAcceptorAllowModel { DeviceId = model.DeviceId, CoinId = ca.Coin.Id, Allow = ca.Allow });
                                else
                                    caDb.Allow = ca.Allow;
                            }
                            catch
                            {

                            }
                        }

                    dbR.SaveChanges();

                    return model;
                }
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message);
                //return new { error = exc.Message, cs = cs };
            }

            throw new Exception("no result");
        }

        /*[System.Web.Http.HttpPut]
        public DeviceCMModelJson Put(DeviceCMModelJson model)
        {
            return model.DALSave().Get();
        }*/

        [System.Web.Http.HttpPatch]
        public DeviceCMModelJson Patch(DeviceCMModelJson model)
        {
            return model.DALSave(true).Get();
        }

        [System.Web.Http.HttpDelete]
        public DeviceCMModelJson Delete([FromUri] DeviceCMModelJson model)
        {
            return model.DALDelete().Get();
        }
    }
}
