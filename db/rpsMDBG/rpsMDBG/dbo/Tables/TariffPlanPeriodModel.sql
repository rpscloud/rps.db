﻿CREATE TABLE [dbo].[TariffPlanPeriodModel] (
    [TariffPlanPeriodId] UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Start]              DATETIME         NOT NULL,
    [End]                DATETIME         NOT NULL,
    [_IsDeleted]         BIT              NOT NULL,
    [Sync]               DATETIME         NULL,
    [TariffTariffPlanId] UNIQUEIDENTIFIER NOT NULL, 
    CONSTRAINT [FK_dbo.TariffPlanPeriodModel_dbo.TariffTariffPlanModel_TariffTariffPlanId] FOREIGN KEY ([TariffTariffPlanId]) REFERENCES [dbo].[TariffTariffPlanModel] ([Id]) ON DELETE CASCADE, 
    CONSTRAINT [PK_TariffPlanPeriodModel] PRIMARY KEY ([TariffPlanPeriodId])
);


GO
CREATE NONCLUSTERED INDEX [IX_TariffTariffPlanId]
    ON [dbo].[TariffPlanPeriodModel]([TariffTariffPlanId] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanPeriodModel', @level2type = N'COLUMN', @level2name = N'TariffTariffPlanId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffPlanPeriodModel', @level2type = N'COLUMN', @level2name = N'Sync';

GO
CREATE TRIGGER [dbo].[SyncTariffPlanPeriodModel]
ON [dbo].[TariffPlanPeriodModel]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [TariffPlanPeriodModel] set Sync=@dt	where TariffPlanPeriodId in (select TariffPlanPeriodId from inserted)
	update SyncTables set [DateTime]=@dt where TableName='TariffPlanPeriodModel'
end
