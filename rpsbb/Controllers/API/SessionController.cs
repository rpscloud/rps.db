﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using ASE.MVC;
using ASE.Data;
using rps.Data;

namespace rpsbb.Controllers.API
{
    [Authorize]
    public class SessionController : ControllerAdv<DB>
    {
        [HttpPost]
        [AllowAnonymous]
        [Response302to401]
        public ActionResult Login(string userName, string password)
        {
            if (db.AuthUsers.Count() == 0)
            {
                var user = new AuthUser { Id = Guid.NewGuid(), LoginName = "sa@ws.r-p-s.ru", Password = "123456", CreateDate = DateTime.Now };
                db.AuthUsers.Add(user);
                
                var role = new AuthRole { Name = "SARole", Description = "СуперАдминистратор" };
                db.AuthRoles.Add(role);
                
                db.AuthUserRoles.Add(new AuthUserRole { Id = Guid.NewGuid(), User = user.LoginName, Role = role.Name });

                db.SaveChanges();
            }

            if (Membership.ValidateUser(userName, password))
            {
                FormsAuthentication.SetAuthCookie(userName, true);
                var user = db.AuthUsers.FirstOrDefault(x => x.LoginName == userName);
                Response.SetCookie(new HttpCookie("UserId", user.Id.ToString()));
                //return Redirect(Url.Content("~/admin/index.html"));
                //Response.StatusCode = 200;
                //return new JsonNetResult { Data = new { message = "ok" } };
                return Redirect("/admin.html#menu");
            }

            //return Redirect(Url.Content("~/admin.html"));
            Response.StatusCode = 400;
            return new JsonNetResult { Data = new { message = "Имя пользователя или пароль не верный!" } };
        }

        [AllowAnonymous]
        public ActionResult Admin()
        {
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return Redirect(Url.Content("~/admin.html"));
        }

        public class Response302to401 : AuthorizeAttribute
        {
            protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
            {
                if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
                {
                    if (filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        filterContext.Result = new JsonResult
                        {
                            Data = new { Message = "Your session has died a terrible and gruesome death" },
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        };
                        filterContext.HttpContext.Response.StatusCode = 401;
                        filterContext.HttpContext.Response.StatusDescription = "Humans and robots must authenticate";
                        filterContext.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;
                    }
                }
                //base.HandleUnauthorizedRequest(filterContext);
            }
        }
    }
}