﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

using ASE.MVC;
using ASE.Data;

using rps.Data;
using rpsbb.Code;

namespace rpsbb.Controllers
{
    [Authorize]
    public class UserParkingRoleController : ApiControllerAdv<DB>
    {
        [System.Web.Http.HttpGet]
        public UserParkingRoleModelJson Get(Guid id)
        {
            return db.UserParkingRoles.Find(id).Get();
        }

        [System.Web.Http.HttpGet]
        public IEnumerable<UserParkingRoleModelJson> List([FromUri] Guid? userId = null, [FromUri] Guid? parkingId = null)
        {
            var q = db.UserParkingRoles.Where(x => 
                !x.Deleted
                & !x.User.Deleted
                & !x.Parking.Deleted
                );
            if (userId.HasValue)
                q = q.Where(x => x.UserId == userId.Value);
            if (parkingId.HasValue)
                q = q.Where(x => x.ParkingId == parkingId.Value);

            return q.ToList().Select(x => x.Get());
        }

        [System.Web.Http.HttpPost]
        public UserParkingRoleModelJson Add(UserParkingRoleModelJson model)
        {
            model.Id = Guid.Empty;
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPut]
        public UserParkingRoleModelJson Put(UserParkingRoleModelJson model)
        {
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPatch]
        public UserParkingRoleModelJson Patch(UserParkingRoleModelJson model)
        {
            return model.DALSave(db, true).Get();
        }

        [System.Web.Http.HttpDelete]
        public UserParkingRoleModelJson Delete([FromUri] UserParkingRoleModelJson model)
        {
            return model.DALDelete(db).Get();
        }
    }
}
