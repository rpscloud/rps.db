﻿CREATE TABLE [dbo].[Parking]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(MAX) NULL, 
    [EmailLogin] NVARCHAR(MAX) NULL, 
    [EmailPassword] NVARCHAR(MAX) NULL, 
    [EmailSMTP] NVARCHAR(MAX) NULL, 
    [EmailSMTPPort] INT NULL, 
    [EmailSSL] NVARCHAR(MAX) NULL, 
    [ContactEmail] NVARCHAR(MAX) NULL, 
    [Sync] DATETIME NULL, 
    [CompanyId] UNIQUEIDENTIFIER NULL
)
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Название объекта',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Parking',
    @level2type = N'COLUMN',
    @level2name = N'Name'
	go

CREATE TRIGGER [dbo].[SyncParking]
ON [dbo].[Parking]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [Parking] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='Parking'
end
go