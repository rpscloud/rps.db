﻿CREATE TABLE [dbo].[DeviceStateModel] (
    [DeviceId] UNIQUEIDENTIFIER NOT NULL,
    [State]    INT              NOT NULL,
    [Updated]  DATETIME         NOT NULL,
    PRIMARY KEY CLUSTERED ([DeviceId] ASC),
    CONSTRAINT [DeviceStateModel_fk] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[DeviceModel] ([Id])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Устройство', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceStateModel', @level2type = N'COLUMN', @level2name = N'DeviceId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Состояние', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceStateModel', @level2type = N'COLUMN', @level2name = N'State';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Обновлено', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceStateModel', @level2type = N'COLUMN', @level2name = N'Updated';

