﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

using ASE.Data;
using ASE.MVC;

using rps.Data;
using rpsbb.Code;

namespace rpsbb.Controllers
{
    [Authorize]
    public class RightsInRoleController : ApiControllerAdv<DB>
    {
        [System.Web.Http.HttpGet]
        public RightsInRoleModelJson Get(Guid id)
        {
            return db.RightsInRoles.Find(id).Get();
        }

        [System.Web.Http.HttpGet]
        public IEnumerable<RightsInRoleModelJson> List()
        {
            return db.RightsInRoles.ToList().Select(x => x.Get());
        }

        [System.Web.Http.HttpPost]
        public RightsInRoleModelJson Add(RightsInRoleModelJson model)
        {
            model.Id = Guid.Empty;
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPut]
        public RightsInRoleModelJson Put(RightsInRoleModelJson model)
        {
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPatch]
        public RightsInRoleModelJson Patch(RightsInRoleModelJson model)
        {
            return model.DALSave(db, true).Get();
        }

        [System.Web.Http.HttpDelete]
        public RightsInRoleModelJson Delete([FromUri] RightsInRoleModelJson model)
        {
            return model.DALDelete(db).Get();
        }
    }
}
