﻿CREATE TABLE [dbo].[UpdateProgramm](
	[Id] [uniqueidentifier] NOT NULL,
	[SysVersion] [int] NOT NULL,
	[VersionProgramm] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[DeviceId] [uniqueidentifier] NULL,
	[TypeProgramm] [nvarchar](50) NULL,
	[Error] [nvarchar](max) NULL,
 [Sync] DATETIME NULL, 
    CONSTRAINT [PK_UpdateProgramm] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[UpdateProgramm]  WITH CHECK ADD  CONSTRAINT [FK_UpdateProgramm_SystemVersion] FOREIGN KEY([SysVersion])
REFERENCES [dbo].[SystemVersion] ([SysVersion])
GO

ALTER TABLE [dbo].[UpdateProgramm] CHECK CONSTRAINT [FK_UpdateProgramm_SystemVersion]
GO

CREATE TRIGGER [dbo].[SyncUpdateProgramm]
ON [dbo].[UpdateProgramm]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [UpdateProgramm] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='UpdateProgramm'
end
