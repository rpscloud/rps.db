﻿CREATE TABLE [dbo].[Cards] (
    [Blocked]          TINYINT          NULL,
    [_IsDeleted]       BIT              NULL,
    [Sync]             DATETIME         NULL,
    [CardId]           BIGINT    NOT NULL,
    [ClientId]         UNIQUEIDENTIFIER DEFAULT (NULL) NULL,
    [CompanyId]        UNIQUEIDENTIFIER DEFAULT (NULL) NULL,
    [ParkingEnterTime] DATETIME         NULL,
    [LastRecountTime]  DATETIME         NULL,
    [LastPaymentTime]  DATETIME         NULL,
    [TSidFC]           TINYINT          NULL,
    [TPidFC]           TINYINT          NULL,
    [ZoneidFC]         TINYINT          NULL,
    [ClientGroupidFC]  TINYINT          NULL,
    [SumOnCard]        INT              NULL,
    [Nulltime1]        DATETIME         NULL,
    [Nulltime2]        DATETIME         NULL,
    [Nulltime3]        DATETIME         NULL,
    [TVP]              DATETIME         NULL,
    [TKVP]             INT              NULL,
    [ClientTypidFC]    INT              NULL,
    [DateSaveCard]     DATETIME         NULL,
    [LastPlate]		NVARCHAR(MAX) NULL, 
    CONSTRAINT [PK_dbo.Cards] PRIMARY KEY CLUSTERED ([CardId]),
    CONSTRAINT [Foreign_key01aadfh] FOREIGN KEY ([ClientGroupidFC]) REFERENCES [dbo].[Group] ([idFC]),
    CONSTRAINT [Foreign_key01asdg] FOREIGN KEY ([ZoneidFC]) REFERENCES [dbo].[ZoneModel] ([IdFC]),
    CONSTRAINT [Foreign_key01client] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[ClientModel] ([Id]) ON DELETE SET NULL,
    CONSTRAINT [Foreign_key01company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[CompanyModel] ([Id]) ON DELETE SET NULL
);




GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Блокировка карты.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'Blocked';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'Sync';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД на карте', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'CardId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время въезда на парковку', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'ParkingEnterTime';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего пересчета', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'LastRecountTime';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время оплаты', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'LastPaymentTime';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД ТС для карты', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'TSidFC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД ТП для карты', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'TPidFC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД текущей зоны для карты', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'ZoneidFC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Группа клиента', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'ClientGroupidFC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма на карте', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'SumOnCard';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время обнуления ВП', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'Nulltime1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Вермя обнуления КВП', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'Nulltime2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Верям обнуления абонемента (окончания)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'Nulltime3';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Текущее время за период', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'TVP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Текущее количество въездов за период', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'TKVP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип клиента: 0 - разовый, 1 - постоянный (1 бит на карте)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'ClientTypidFC';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Гос номер последний распознанный', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Cards', @level2type = N'COLUMN', @level2name = N'LastPlate';

GO
CREATE TRIGGER [dbo].[SyncCards]
ON [dbo].[Cards]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [Cards] set Sync=@dt	where CardId in (select CardId from inserted)
	update SyncTables set [DateTime]=@dt where TableName='Cards'
end
