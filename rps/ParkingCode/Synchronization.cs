﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ASE.Data;

using RMLib;
using Parking.Security;

using rps.Data;
using RMLib.Services;
using RMLib.Services.Wcf;
using Parking.Client;
using Parking.Workstation;
using System.Threading;

namespace rps.ParkingCode
{
    public class Synchronization
    {
        public static void Startup()
        {
            try
            {
                ClientWrapper.Invoke((c) =>
                {
                    ICardClient cc = c.GetConnected<ICardClient>();
                    cc.LoadItems();
                    var x = cc.Count;
                    var z = cc.Count;
                    IZoneClient zc = c.GetService<IZoneClient>();
                    var t = zc.Count;
                    //SetMessage(0, cc.Count.ToString());

                    /*IZoneClient zc = c.GetService<IZoneClient>();
                    zc.LoadItems(); //загрузка зон в кэш клиента

                    //пример использования - добавление устройства
                    Zone z = zc.Items.FirstOrDefault(); //новому устройству нужна зона
                    if (z == null)
                        return;

                    IDeviceClient dc = c.GetService<IDeviceClient>();

                    //устанавливаем свойства
                    Device d = Device.Create(DeviceType.EntryStand);
                    d.NetworkAddress = dc.Items.Max(i => i.NetworkAddress) + 1; //уникальный адрес для внутренней сети
                    d.Name = String.Format("Въезд #{0}", MathHelper.Rnd.Next());
                    d.DisplayZoneID = z.ID;

                    Stand s = (Stand)d; //у въезда обязательно должна быть зона после, у выезда - зона до
                    //if (d.Type == DeviceType.EntryStand)
                    //{
                    s.ZoneBeforeID = Zone.Outer.ID;
                    s.ZoneAfterID = z.ID;
                    //}
                    //else if (d.Type.IsExitStand())
                    //{
                    //  s.ZoneBeforeID = z.ID;
                    //  s.ZoneAfterID = Zone.Outer.ID;
                    //}

                    Device newDevice = dc.Add(d);
                    SetMessage(newDevice.ID, newDevice.Name);*/
                });
            }
            catch
            {

            }
        }

        private static readonly Object forLock = new Object();

        public async static Task<bool> Start()
        {
            bool lockWasTaken = false;
            try
            {
                Monitor.Enter(forLock, ref lockWasTaken);

                if (lockWasTaken)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["IsSite2"] == "true")
                        Start2To3();
                    else
                        Start3To2();
                }
            }
            finally
            {
                if (lockWasTaken)
                {
                    Monitor.Exit(forLock); 
                }
            }

            return true;
        }

        private static void Start3To2()
        {
            DB db = new DB();
            foreach (var item in db.SyncItems.OrderBy(x => x.Created).ToList())
            {
                try
                {
                    /*if (item.ObjecType == 1)
                    {
                        var parking = db.Parkings.FirstOrDefault(x => x.OrganizationId == item.ObjecId);
                        if ((parking != null) && (!String.IsNullOrEmpty(parking.S2Server)))
                        {
                            DB dbR = new DB(parking.S2Server);

                            var obj = db.Organizations.Find(item.ObjecId);
                            dbR.Entry(obj).State = System.Data.Entity.EntityState.Detached;
                            var objR = dbR.Organizations.Find(item.ObjecId);
                            if (item.Operation == 1)
                            {
                                if (objR != null)
                                    dbR.Organizations.Remove(objR);
                            }
                            if (item.Operation == 2)
                            {
                                if (objR == null)
                                    dbR.Organizations.Add(obj);
                                else
                                    CopyProperties(obj, objR);
                            }
                            dbR.SaveChanges();
                        }
                    }
                    if (item.ObjecType == 2)
                    {
                        var parking = db.Parkings.FirstOrDefault(x => x.Id == item.ObjecId);
                        if ((parking != null) && (!String.IsNullOrEmpty(parking.S2Server)))
                        {
                            DB dbR = new DB(parking.S2Server);

                            var obj = db.Parkings.Find(item.ObjecId);
                            dbR.Entry(obj).State = System.Data.Entity.EntityState.Detached;
                            var objR = dbR.Parkings.Find(item.ObjecId);
                            if (item.Operation == 1)
                            {
                                if (objR != null)
                                    dbR.Parkings.Remove(objR);
                            }
                            if (item.Operation == 2)
                            {
                                if (objR == null)
                                    dbR.Parkings.Add(obj);
                                else
                                    CopyProperties(obj, objR, null, "Organization");
                            }

                            foreach (var old in dbR.Organizations.ToList())
                                dbR.Organizations.Remove(old);

                            var org = db.Organizations.Find(obj.OrganizationId);
                            db.Entry(org).State = System.Data.Entity.EntityState.Detached;
                            var orgR = dbR.Organizations.Find(parking.OrganizationId.Value);
                            if (orgR == null)
                                dbR.Organizations.Add(org);
                            else
                                CopyProperties(org, dbR);

                            dbR.SaveChanges();
                        }
                    }
                    if (item.ObjecType == 3)
                    {
                        var parking = db.Parkings.FirstOrDefault(x => x.Id == item.ObjecId);

                    }

                    db.SyncItems.Remove(item);
                    db.SaveChanges();*/
                }
                catch(Exception exc)
                {

                }
            }

            /*foreach (var parking in db.Parkings.ToList())
            {
                if (String.IsNullOrEmpty(parking.S2Server))
                    continue;

                db.Entry(parking).State = System.Data.Entity.EntityState.Detached;

                DB dbR = new DB(parking.S2Server);
                //Parkings
                foreach(var item in db.SyncItems.Where(x => x.ObjecId == parking.Id).OrderBy(x => x.Created).ToList())
                {
                    var rItem = dbR.Parkings.FirstOrDefault(x => x.Id == item.ObjecId);

                    if ((item.Operation == 1) & (rItem == null))
                    {
                        dbR.Parkings.Remove(rItem);
                        if (parking.OrganizationId.HasValue)
                        {
                            var org = dbR.Organizations.Find(parking.OrganizationId.Value);
                            if (org != null)
                                dbR.Organizations.Remove(org);
                        }

                        dbR.SaveChanges();

                        db.SyncItems.Remove(item);
                        db.SaveChanges();
                    }

                    if (item.Operation == 2)
                    {
                        if (rItem == null)
                            dbR.Parkings.Add(parking);
                        else
                            CopyProperties(parking, rItem);
                        foreach (var old in dbR.Organizations.ToList())
                            dbR.Organizations.Remove(old);
                        if (parking.OrganizationId.HasValue)
                        {
                            var org = db.Organizations.Find(parking.OrganizationId);
                            db.Entry(org).State = System.Data.Entity.EntityState.Detached;
                            var orgR = dbR.Organizations.Find(parking.OrganizationId.Value);
                            if (orgR == null)
                                dbR.Organizations.Add(org);
                            else
                                CopyProperties(org, dbR);

                        }
                        dbR.SaveChanges();

                        db.SyncItems.Remove(item);
                        db.SaveChanges();
                    }
                }
            }*/
        }

        private static void Start2To3()
        {
            /*DB db = new DB();
            var parking = db.Parkings.FirstOrDefault();
            if (parking == null)
                return;
            if (String.IsNullOrEmpty(parking.S3Server))
                return;

            DB dbR = new DB(parking.S3Server);
            foreach(var item in db.SyncItems.OrderBy(x => x.Created).ToList())
            {
                try
                {
                    if (item.ObjecType == 1)
                    {
                        var org = dbR.Organizations.Find(parking.OrganizationId);
                        CopyProperties(parking.Organization, org);

                        dbR.SaveChanges();

                        db.SyncItems.Remove(item);
                        db.SaveChanges();
                    }
                    if (item.ObjecType == 2)
                    {
                        var rItem = dbR.Parkings.FirstOrDefault(x => x.Id == item.ObjecId);
                        if (rItem == null)
                            continue;

                        if (item.Operation == 2)
                        {
                            CopyProperties(parking, rItem, null, "Organization");
                            if (parking.Organization != null)
                            {
                                var org = dbR.Organizations.Find(parking.OrganizationId);
                                CopyProperties(parking.Organization, org);
                            }
                            dbR.SaveChanges();

                            db.SyncItems.Remove(item);
                            db.SaveChanges();
                        }
                    }

                }
                catch(Exception exc)
                {

                }
            }*/
        }

        private static void CopyProperties(object from, object to)
        {
            CopyProperties(from, to, null, null);

        }
        private static void CopyProperties(object from, object to, string includeFields, string excludeFields)
        {
            string[] includeFieldsL = new string[0];
            if (includeFields != null)
                includeFieldsL = includeFields.Split(',');
            string[] excludeFieldsL = new string[0];
            if (excludeFields != null)
                excludeFieldsL = excludeFields.Split(',');

            if (includeFieldsL.Length > 0)
            {
                foreach (var prop in includeFieldsL)
                    to.GetType().GetProperty(prop).SetValue(to, from.GetType().GetProperty(prop).GetValue(from));
            }
            else
            {
                var props = from.GetType().GetProperties();
                foreach (var prop in props)
                    if (excludeFieldsL.FirstOrDefault(x => x == prop.Name) == null)
                        to.GetType().GetProperty(prop.Name).SetValue(to, from.GetType().GetProperty(prop.Name).GetValue(from));
            }
        }
    }
}
