﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using ASE.Data;

namespace ASE.MVC
{
    public static class ContextPerRequest
    {
        private const string myDbPerRequestContext = "MDPRC";

        public static DB Db
        {
            get
            {
                if (!HttpContext.Current.Items.Contains(myDbPerRequestContext))
                {
                    HttpContext.Current.Items.Add(myDbPerRequestContext, new DB());
                }

                return HttpContext.Current.Items[myDbPerRequestContext] as DB;
            }
        }

        /// <summary>
        /// Called automatically on Application_EndRequest()
        /// </summary>
        public static void DisposeDbContextPerRequest()
        {
            // Getting dbContext directly to avoid creating it in case it was not already created.
            var entityContext = HttpContext.Current.Items[myDbPerRequestContext] as DB;
            if (entityContext != null)
            {
                entityContext.Dispose();
                HttpContext.Current.Items.Remove(myDbPerRequestContext);
            }
        }
    }
}
