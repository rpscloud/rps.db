﻿CREATE TABLE [dbo].[Tariffs] (
    [Id]                          UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Name]                        NVARCHAR(MAX)    NULL,
    [ChangingTypeId]              INT              NULL,
    [TypeId]                      INT              NOT NULL,
    [Initial]                     INT              NOT NULL,
    [InitialTimeTypeId]           INT              NULL,
    [InitialAmount]               INT              NOT NULL,
    [ProtectedInterval]           INT              NOT NULL,
    [ProtectedIntervalTimeTypeId] INT              NULL,
    [FreeTime]                    INT              NOT NULL,
    [FreeTimeTypeId]          INT              NULL,
    [_IsDeleted]                  BIT              NOT NULL,
    [Sync]                        DATETIME         NULL,
    [IdFC]                        TINYINT          CONSTRAINT [Tariffs_Default_Constraint01] DEFAULT 1 NULL,
    CONSTRAINT [PK_dbo.Tariffs] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Tariffs_dbo.TariffChangingTypeModel_ChangingTypeId] FOREIGN KEY ([ChangingTypeId]) REFERENCES [dbo].[TariffChangingTypeModel] ([Id]),
    CONSTRAINT [FK_dbo.Tariffs_dbo.TariffPlanTypeModel_TypeId] FOREIGN KEY ([TypeId]) REFERENCES [dbo].[TariffPlanTypeModel] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.Tariffs_dbo.TimeTypeModel_FreeTimeTimeTypeId] FOREIGN KEY ([FreeTimeTypeId]) REFERENCES [dbo].[TimeTypeModel] ([Id]),
    CONSTRAINT [FK_dbo.Tariffs_dbo.TimeTypeModel_InitialTimeTypeId] FOREIGN KEY ([InitialTimeTypeId]) REFERENCES [dbo].[TimeTypeModel] ([Id]),
    CONSTRAINT [FK_dbo.Tariffs_dbo.TimeTypeModel_ProtectedIntervalTimeTypeId] FOREIGN KEY ([ProtectedIntervalTimeTypeId]) REFERENCES [dbo].[TimeTypeModel] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_ChangingTypeId]
    ON [dbo].[Tariffs]([ChangingTypeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TypeId]
    ON [dbo].[Tariffs]([TypeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_InitialTimeTypeId]
    ON [dbo].[Tariffs]([InitialTimeTypeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FreeTimeTimeTypeId]
    ON [dbo].[Tariffs]([FreeTimeTypeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ProtectedIntervalTimeTypeId]
    ON [dbo].[Tariffs]([ProtectedIntervalTimeTypeId] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Tariffs_Column]
    ON [dbo].[Tariffs]([IdFC] ASC) WITH (IGNORE_DUP_KEY = ON);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Tariffs', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наименование', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Tariffs', @level2type = N'COLUMN', @level2name = N'Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Правило перехода', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Tariffs', @level2type = N'COLUMN', @level2name = N'ChangingTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время действия', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Tariffs', @level2type = N'COLUMN', @level2name = N'TypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время в начале', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Tariffs', @level2type = N'COLUMN', @level2name = N'Initial';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип времени', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Tariffs', @level2type = N'COLUMN', @level2name = N'InitialTimeTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Стоимость время вначале', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Tariffs', @level2type = N'COLUMN', @level2name = N'InitialAmount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Защитный интервал', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Tariffs', @level2type = N'COLUMN', @level2name = N'ProtectedInterval';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип времени', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Tariffs', @level2type = N'COLUMN', @level2name = N'ProtectedIntervalTimeTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Беспл. время после оплаты', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Tariffs', @level2type = N'COLUMN', @level2name = N'FreeTime';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип времени', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Tariffs', @level2type = N'COLUMN', @level2name = 'FreeTimeTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Tariffs', @level2type = N'COLUMN', @level2name = N'Sync';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД для карты', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Tariffs', @level2type = N'COLUMN', @level2name = N'IdFC';

GO
CREATE TRIGGER [dbo].[SyncTariffs]
ON [dbo].[Tariffs]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [Tariffs] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='Tariffs'
end
