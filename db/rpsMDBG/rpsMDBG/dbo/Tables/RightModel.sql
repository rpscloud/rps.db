﻿CREATE TABLE [dbo].[RightModel] (
    [Id]       UNIQUEIDENTIFIER    CONSTRAINT [DF__RightModel__Id__51300E55] DEFAULT (newid()) NOT NULL,
    [ParentId] UNIQUEIDENTIFIER    NULL,
    [Name]     VARCHAR (MAX)       NOT NULL,
    [OrgNode]  [sys].[hierarchyid] NULL,
    [Level]    AS                  ([OrgNode].[GetLevel]()) PERSISTED,
    CONSTRAINT [PK_dbo.RightModel] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RightModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RightModel', @level2type = N'COLUMN', @level2name = N'ParentId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наименование', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RightModel', @level2type = N'COLUMN', @level2name = N'Name';

