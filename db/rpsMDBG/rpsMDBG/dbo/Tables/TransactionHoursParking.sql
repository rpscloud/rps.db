﻿CREATE TABLE [dbo].[TransactionHoursParking] (
    [Id]          INT        IDENTITY (1, 1) NOT NULL,   
    [Minutes] INT NULL,
	[CardNumber] NVARCHAR(MAX) NULL,
	[ClientId] UNIQUEIDENTIFIER NULL,
	[EnterId] UNIQUEIDENTIFIER NULL,
	[ExitId] UNIQUEIDENTIFIER NULL,
	[DateCreated] DATETIME NULL,
	[DateClosed] DATETIME NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

