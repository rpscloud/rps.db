﻿CREATE TABLE [dbo].[ReportAccess] (
    [ReportModelId] UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [UserId]        UNIQUEIDENTIFIER NOT NULL,
    [RightId]       UNIQUEIDENTIFIER NOT NULL,
    [Id]            INT              IDENTITY (1, 1) NOT NULL,
    [Active]        BIT              NULL,
    CONSTRAINT [PK_ReportAccess] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [ReportAccess_fk] FOREIGN KEY ([ReportModelId]) REFERENCES [dbo].[ReportModel] ([Id]),
    CONSTRAINT [ReportAccess_fk2] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserModel] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE
);