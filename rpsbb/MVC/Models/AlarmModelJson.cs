using System.ComponentModel.DataAnnotations.Schema;

namespace rps.Data
{
    [NotMapped]
    public class AlarmModelJson : AlarmModel
    {

    }

    public static partial class Model2Json
    {
        public static AlarmModelJson Get(this AlarmModel model)
        {
            var ret = (AlarmModelJson)Copy(model, new AlarmModelJson());

            return ret;
        }
    }
}