﻿CREATE TABLE [dbo].[MessageTypeModel] (
    [Id]      INT           NOT NULL,
    [Color]   INT           NULL,
    [Message] VARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

