﻿using ASE.Data;
using ASE.MVC;
using rps.Data;
using rps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace rps.Controllers
{
    [Authorize(Roles="Admin")]
    public class AdminQController : ControllerAdv<DB>
    {
        ApplicationDbContext udb = new ApplicationDbContext();

        public ActionResult Index()
        {
            return View();
        }

        // GET: Admin
        /*public ActionResult Index()
        {
            //rps.ParkingCode.Synchronization.Startup();
            rps.ParkingCode.Synchronization.Start();

            var users = UserManager.Users.ToList();
            foreach(var user in users)
            {
                var duser = db.Users.Find(Guid.Parse(user.Id));
                if (duser == null)
                    db.Users.Add(new User { Id = Guid.Parse(user.Id), FullName = user.UserName, Email = user.Email, Password = ""});
            }
            db.SaveChanges();

            return View();
        }


        public ActionResult OrganizationList(ViewSortFilterModel model)
        {
            return PartialView("OrganizationList", model);
        }

        [HttpPost]
        public ActionResult OrganizationDelete(Guid id)
        {
            var model = db.Organizations.Find(id);
            db.Organizations.Remove(model);
            db.SaveChanges();
            db.SyncAddRemove(model);

            return PartialView("OrganizationList", new ViewSortFilterModel { Sort = "Name_asc", Filter = "" });
        }

        public JsonResult OrganizationEdit(Guid id)
        {
            Organization model;
            if (id != Guid.Empty)
                model = db.Organizations.FirstOrDefault(x => x.Id == id);
            else
                model = new Organization { Id = Guid.Empty };

            return new JsonResult
            {
                JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet,
                Data = new
                {
                    html = this.RenderPartialView("OrganizationEdit", model),
                    //linked = db.Parkings.Where(x => x.OrganizationId == id).Select(x => x.Id).FirstOrDefault()
                }
            };
        }

        [HttpPost]
        public ActionResult OrganizationEdit(Guid id, string name)
        {
            Organization model;
            if (id != Guid.Empty)
                model = db.Organizations.Find(id);
            else
            {
                model = new Organization { Id = Guid.NewGuid() };
                db.Organizations.Add(model);
            }
            model.Name = name;
            db.SaveChanges();
            db.SyncAddUpdate(model);

            return PartialView("OrganizationList", new ViewSortFilterModel { Sort = "Name_asc", Filter = "" });
        }

        public ActionResult ParkingList(ViewSortFilterModel model)
        {
            return PartialView("ParkingList", model);
        }

        public JsonResult ParkingEdit(Guid id)
        {
            ParkingModel model;
            if (id != Guid.Empty)
                model = db.Parkings.FirstOrDefault(x => x.Id == id);
            else
                model = new ParkingModel { Id = Guid.Empty };

            return new JsonResult
            {
                JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet,
                Data = new
                {
                    html = this.RenderPartialView("ParkingEdit", model),
                    //linked = model.OrganizationId,
                    linkedUsers = db.UserParkings.Where(x => x.ParkingId == id & x.Active).Select(x => x.UserId).ToList()
                }
            };
        }

        [HttpPost]
        public ActionResult ParkingDelete(Guid id)
        {
            var model = db.Parkings.Find(id);
            db.Parkings.Remove(model);
            db.SaveChanges();
            db.SyncAddRemove(model);

            return PartialView("ParkingList", new ViewSortFilterModel { Sort = "Name_asc", Filter = "" });
        }

        [HttpPost]
        public ActionResult ParkingEdit(ParkingModel model)
        {
            if (model.Id != Guid.Empty)
                AttachModel(model);
            else
            {
                model.Id = Guid.NewGuid();
                db.Parkings.Add(model);
            }

            db.SaveChanges();
            db.SyncAddUpdate(model);

            return PartialView("ParkingList", new ViewSortFilterModel { Sort = "Name_asc", Filter = "" });
        }

        [HttpPost]
        public JsonResult LinkOrganization2Parking(Guid? organizationId, Guid? parkingId, bool remove)
        {
            var result = true;
            if (System.Configuration.ConfigurationManager.AppSettings["IsSite2"] != "true")
                try
                {
                    var organization = db.Organizations.Find(organizationId);
                    var parking = db.Parkings.Find(parkingId);
                    if ((organization != null) & (parking != null))
                    {
                        if (remove)
                            parking.OrganizationId = null;
                        else
                            parking.OrganizationId = organization.Id;

                        db.SaveChanges();
                        db.SyncAddUpdate(parking);
                    }
                }
                catch
                {
                    result = false;
                }

            return new JsonResult
            {
                Data = new
                {
                    result = result
                }
            };
        }

        [HttpPost]
        public JsonResult LinkOrganization2ParkingGet(Guid? organizationId, Guid? parkingId)
        {
            Guid? ret = null;
            if (organizationId.HasValue)
                ret = db.Parkings.Where(x => x.OrganizationId == organizationId.Value).Select(x => x.Id).FirstOrDefault();
            if (parkingId.HasValue)
                ret = db.Parkings.Where(x => x.Id == parkingId.Value).Select(x => x.Id).FirstOrDefault();

            return new JsonResult
            {
                Data = new
                {
                    result = ret
                }
            };
        }

        public ActionResult UserList(ViewSortFilterModel model)
        {
            return PartialView("UserList", model);
        }

        public JsonResult UserEdit(Guid id)
        {
            User model;
            if (id != Guid.Empty)
                model = db.Users.FirstOrDefault(x => x.Id == id);
            else
                model = new User { Id = Guid.Empty };

            return new JsonResult
            {
                JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet,
                Data = new
                {
                    html = this.RenderPartialView("UserEdit", model),
                    linked = db.UserParkings.Where(x => x.UserId == id & x.Active).Select(x => x.ParkingId).ToList()
                }
            };
        }

        [HttpPost]
        public ActionResult UserEdit(User model)
        {
            if (model.Id != Guid.Empty)
                AttachModel(model);
            else
            {
                model.Id = Guid.NewGuid();
                db.Users.Add(model);
            }
            db.SaveChanges();
            db.SyncAddUpdate(model);

            return PartialView("UserList", new ViewSortFilterModel { Sort = "FullName_asc", Filter = "" });
        }

        [HttpPost]
        public ActionResult UserDelete(Guid id)
        {
            var model = db.Users.Find(id);
            db.Users.Remove(model);
            db.SaveChanges();
            db.SyncAddRemove(model);

            return PartialView("UserList", new ViewSortFilterModel { Sort = "FullName_asc", Filter = "" });
        }

        [HttpPost]
        public JsonResult LinkUser2Parking(Guid? userId, Guid? parkingId, bool remove)
        {
            var result = true;
            if (System.Configuration.ConfigurationManager.AppSettings["IsSite2"] != "true")
                try
                {
                    var user = db.Users.Find(userId);
                    var parking = db.Parkings.Find(parkingId);
                    if ((user != null) & (parking != null))
                    {
                        var item = db.UserParkings.FirstOrDefault(x => x.UserId == userId & x.ParkingId == parkingId);
                        if ((remove) & (item != null))
                            item.Active = false;
                        else if ((!remove) & (item == null)) {
                            item = new UserParking { Id = Guid.NewGuid(), UserId = userId.Value, ParkingId = parkingId.Value, Active = true};
                            db.UserParkings.Add(item);
                        }
                        else if ((!remove) & (item != null))
                        {
                            item.Active = true;
                        }

                        db.AddChanged(item);
                        db.SaveChanges();
                    }
                }
                catch
                {
                    result = false;
                }

            return new JsonResult
            {
                Data = new
                {
                    result = result
                }
            };
        }*/
    }
}