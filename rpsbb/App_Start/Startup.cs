﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(rpsbb.Startup))]
namespace rpsbb
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}
