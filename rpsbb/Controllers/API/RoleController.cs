﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

using ASE.Data;
using ASE.MVC;

using rps.Data;
using rpsbb.Code;

namespace rpsbb.Controllers
{
    [Authorize]
    public class RoleController : ApiControllerAdv<DB>
    {
        [System.Web.Http.HttpGet]
        public RoleModelJson Get(Guid id)
        {
            return db.Roles.Find(id).Get();
        }

        [System.Web.Http.HttpGet]
        public IEnumerable<RoleModelJson> List([FromUri] Guid? parkingId = null)
        {
            var q = db.Roles.Where(x => !x.Deleted);
            if (parkingId.HasValue)
                q = q.Where(x => x.ParkingId == parkingId.Value);

            return q.ToList().Select(x => x.Get());
        }

        [System.Web.Http.HttpPost]
        public RoleModelJson Add(RoleModelJson model)
        {
            model.Id = Guid.Empty;
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPut]
        public RoleModelJson Put(RoleModelJson model)
        {
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPatch]
        public RoleModelJson Patch(RoleModelJson model)
        {
            return model.DALSave(db, true).Get();
        }

        [System.Web.Http.HttpDelete]
        public RoleModelJson Delete([FromUri] RoleModelJson model)
        {
            return model.DALDelete(db).Get();
        }
    }
}
