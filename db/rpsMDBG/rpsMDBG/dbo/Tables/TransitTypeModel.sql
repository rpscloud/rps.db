﻿CREATE TABLE [dbo].[TransitTypeModel] (
    [Id]         INT           NOT NULL,
    [Name]       VARCHAR (MAX) NULL,
    [_IsDeleted] BIT           NOT NULL,
    CONSTRAINT [PK_dbo.TransitTypeModel] PRIMARY KEY CLUSTERED ([Id] ASC)
);

