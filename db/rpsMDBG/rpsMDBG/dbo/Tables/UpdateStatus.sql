﻿CREATE TABLE [dbo].[UpdateStatus]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(MAX) NULL
)
