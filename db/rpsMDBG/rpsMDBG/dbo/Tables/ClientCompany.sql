﻿CREATE TABLE [dbo].[ClientCompany] (
    [Id]        UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Sync]      DATETIME         NULL,
    [CompanyId] UNIQUEIDENTIFIER NULL,
    [ClientId]  UNIQUEIDENTIFIER NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [Foreign_key02] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[CompanyModel] ([Id]) ON DELETE SET NULL,
    CONSTRAINT [Foreign_key03] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[ClientModel] ([Id]) ON DELETE SET NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ClientCompany', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ClientCompany', @level2type = N'COLUMN', @level2name = N'Sync';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД компании', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ClientCompany', @level2type = N'COLUMN', @level2name = N'CompanyId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД клиента', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ClientCompany', @level2type = N'COLUMN', @level2name = N'ClientId';

GO
CREATE TRIGGER [dbo].[SyncClientCompany]
ON [dbo].[ClientCompany]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [ClientCompany] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='ClientCompany'
end
