﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

using ASE.MVC;
using ASE.Data;

using rps.Data;
using rpsbb.Code;

namespace rpsbb.Controllers
{
    [Authorize]
    public class DeviceMapModelController : ApiControllerAdv<DB>
    {
        [System.Web.Http.HttpGet]
        public DeviceMapModelJson Get(Guid id)
        {
            return db.DeviceMaps.Find(id).Get();
        }

        [System.Web.Http.HttpGet]
        public IEnumerable<DeviceMapModelJson> List()
        {
            var q = db.DeviceMaps;

            return q.ToList().Select(x => x.Get());
        }

        [System.Web.Http.HttpPost]
        public DeviceMapModelJson Add(DeviceMapModelJson model)
        {
            //model.DeviceId = 0;
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPut]
        public DeviceMapModelJson Put(DeviceMapModelJson model)
        {
            return model.DALSave(db).Get();
        }

        [System.Web.Http.HttpPatch]
        public DeviceMapModelJson Patch(DeviceMapModelJson model)
        {
            return model.DALSave(db, true).Get();
        }

        [System.Web.Http.HttpDelete]
        public DeviceMapModelJson Delete([FromUri] DeviceMapModelJson model)
        {
            return model.DALDelete(db).Get();
        }
    }
}
