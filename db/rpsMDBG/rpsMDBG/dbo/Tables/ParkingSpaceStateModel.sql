﻿CREATE TABLE [dbo].[ParkingSpaceStateModel] (
    [Id]              UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [ParkingSpaceId]  INT              NOT NULL,
    [Hour0]           INT              NOT NULL,
    [Hour1]           INT              NOT NULL,
    [Hour2]           INT              NOT NULL,
    [Hour3]           INT              NOT NULL,
    [Hour4]           INT              NOT NULL,
    [Hour5]           INT              NOT NULL,
    [Hour6]           INT              NOT NULL,
    [Hour7]           INT              NOT NULL,
    [Hour8]           INT              NOT NULL,
    [Hour9]           INT              NOT NULL,
    [Hour10]          INT              NOT NULL,
    [Hour11]          INT              NOT NULL,
    [Hour12]          INT              NOT NULL,
    [Hour13]          INT              NOT NULL,
    [Hour14]          INT              NOT NULL,
    [Hour15]          INT              NOT NULL,
    [Hour16]          INT              NOT NULL,
    [Hour17]          INT              NOT NULL,
    [Hour18]          INT              NOT NULL,
    [Hour19]          INT              NOT NULL,
    [Hour20]          INT              NOT NULL,
    [Hour21]          INT              NOT NULL,
    [Hour22]          INT              NOT NULL,
    [Hour23]          INT              NOT NULL,
    [_IsDeleted]      BIT              NOT NULL,
    [ParkingSpace_Id] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_dbo.ParkingSpaceStateModel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.ParkingSpaceStateModel_dbo.ParkingSpaceModel_ParkingSpace_Id] FOREIGN KEY ([ParkingSpace_Id]) REFERENCES [dbo].[ParkingSpaceModel] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_ParkingSpace_Id]
    ON [dbo].[ParkingSpaceStateModel]([ParkingSpace_Id] ASC);

