﻿CREATE TABLE [dbo].[DeviceCMModel] (
    [DeviceId]                 UNIQUEIDENTIFIER NOT NULL,
    [ZoneId]                   UNIQUEIDENTIFIER NULL,
    [CashDispenserTypeId]      INT              NULL,
    [MaxCountUp]               INT              NULL,
    [MaxCountDown]             INT              NULL,
    [CountUp]                  INT              NULL,
    [CountDown]                INT              NULL,
    [CashDispenserComPortId]   INT              NULL,
    [CashAcceptorLimit]        INT              NOT NULL DEFAULT 0,
    [CashAcceptorAlarm]        INT              NOT NULL DEFAULT 0,
    [CashAcceptorCurrent]      INT              NOT NULL DEFAULT 0,
    [CashAcceptorComPortId]    INT              NULL,
    [HopperTypeId]             INT              NULL,
    [MaxCount2]            INT              NULL,
    [MaxCount1]             INT              NULL,
    [Count2]               INT              NULL,
    [Count1]                INT              NULL,
    [Hopper1ComPortId]         INT              NULL,
    [Hopper2ComPortId]         INT              NULL,
    [CoinAcceptorLimit]        INT              NOT NULL DEFAULT 0,
    [CoinAcceptorAlarm]        INT              NOT NULL DEFAULT 0,
    [CoinAcceptorCurrent]      INT              NOT NULL DEFAULT 0,
    [BankModuleTypeId]         INT              NULL,
    [Reader]                   BIT              NULL,
    [PayPass]                  BIT              NULL,
    [BarcodeTypeId]            INT              NULL,
    [BarcodeComPortId]         INT              NULL,
    [VAT]                      INT              NULL,
    [MaximalChange]            INT              NULL,
    [PrinterTypeId]            INT              NULL,
    [PrinterComPortId]         INT              NULL,
    [CardReaderComPortId]      INT              NULL,
    [CardTimeOutToBasket]      INT              NULL,
    [PenalCardTimeOut]         INT              NULL,
    [CancelRefund]             BIT              NULL,
    [ShiftOpened]              BIT              NULL,
    [_IsDeleted]               BIT              NULL,
    [CardKey]                  VARCHAR (MAX)    NULL,
    [CashierNumber]            INT              NULL,
    [CashDispenserExist]       BIT              NULL,
    [NominalUpId]              INT              NULL,
    [NominalDownId]            INT              NULL,
    [RejectCount]              INT              NULL,
    [CashAcceptorExist]        BIT              NULL,
    [HopperExist]              BIT              NULL,
    [NominalRightId]           INT              NULL,
    [NominalLeftId]            INT              NULL,
    [CoinAcceptorExist]        BIT              NULL,
    [CoinAcceptorTypeId]       INT              NULL,
    [BankModuleExist]          BIT              NULL,
    [CardDispenserExists]      BIT              NULL,
    [CardDispenserTypeId]      INT              NULL,
    [CardDispenserComPortId]   INT              NULL,
    [BarcodeExist]             BIT              NULL,
    [KKMExists]                BIT              NULL,
    [KKMTypeId]                INT              NULL,
    [ShiftAutoClose]           BIT              NULL,
    [ShiftNewShiftTime]        DATETIME         NULL,
    [CommandPassword]          VARCHAR (MAX)    NULL,
    [RegistartionPass]         VARCHAR (MAX)    NULL,
    [WithOutCleaningPass]      VARCHAR (MAX)    NULL,
    [WithCleaningPass]         VARCHAR (MAX)    NULL,
    [KKMComPortId]             INT              NULL,
    [PrinterExists]            BIT              NULL,
    [CardReaderExist]          BIT              NULL,
    [CardReaderTypeId]         INT              NULL,
    [SlaveExist]               BIT              NULL,
    [SlaveTypeId]              INT              NULL,
    [SlaveComPortId]           INT              NULL,
    [PenaltyCardTPId]          INT              NULL,
    [PenaltyCardTSId]          INT              NULL,
    [SyncNominals]             BIT              NULL,
    [ServerExchangeProtocolId] INT              NULL,
    [ServerURL]                VARCHAR (MAX)    NULL,
    [CurrentTransactionNumber] INT              NULL,
    [LanguageTimeOut]          INT              NULL,
    [DefaultLanguage]        NCHAR(2)              NULL,
    [WebCameraIP]              VARCHAR (MAX)    NULL,
    [WebCameraLogin]           VARCHAR (MAX)    NULL,
    [WebCameraPassword]        VARCHAR (MAX)    NULL,
    [LastShiftOpenTime]        DATETIME         NULL,
    [CashAcceptorTypeId]       INT              NULL,
    [CoinAcceptorComPortId]    INT              NULL,
    [Sync]                     DATETIME         NULL,
    [WebCameraUrl]             VARCHAR (MAX)    NULL,
	[GroupId]                  UNIQUEIDENTIFIER NULL,
    [CoinAcceptorPinCode] VARCHAR(MAX) NULL, 
    [CoinAcceptorAddress] TINYINT NULL, 
    [SingleClient0] BIT NULL, 
    [CashierType] TINYINT NULL , 
    [LogLevel] TINYINT NOT NULL DEFAULT 2, 
    [SectorNumber] TINYINT NULL, 
    [ChequeTimeOut] TINYINT NULL, 
    [ShiftNumber] INT NULL, 
    [ChequeWork] TINYINT NULL , 
    CONSTRAINT [PK_dbo.DeviceCMModel] PRIMARY KEY CLUSTERED ([DeviceId] ASC),
    CONSTRAINT [DeviceCMModel_fk] FOREIGN KEY ([CoinAcceptorComPortId]) REFERENCES [dbo].[ComPortModel] ([Id]),
	CONSTRAINT [DeviceCMModel_Zone_FK] FOREIGN KEY ([ZoneId]) REFERENCES [dbo].[ZoneModel] ([Id]),
    CONSTRAINT [FK_dbo.DeviceCMModel_dbo.ComPortModel_BarcodeComPortId] FOREIGN KEY ([BarcodeComPortId]) REFERENCES [dbo].[ComPortModel] ([Id]),
    CONSTRAINT [FK_dbo.DeviceCMModel_dbo.ComPortModel_CardReaderComPortId] FOREIGN KEY ([CardReaderComPortId]) REFERENCES [dbo].[ComPortModel] ([Id]),
    CONSTRAINT [FK_dbo.DeviceCMModel_dbo.ComPortModel_CashAcceptorComPortId] FOREIGN KEY ([CashAcceptorComPortId]) REFERENCES [dbo].[ComPortModel] ([Id]),
    CONSTRAINT [FK_dbo.DeviceCMModel_dbo.ComPortModel_CashDispenserComPortId] FOREIGN KEY ([CashDispenserComPortId]) REFERENCES [dbo].[ComPortModel] ([Id]),
    CONSTRAINT [FK_dbo.DeviceCMModel_dbo.ComPortModel_Hopper1ComPortId] FOREIGN KEY ([Hopper1ComPortId]) REFERENCES [dbo].[ComPortModel] ([Id]),
    CONSTRAINT [FK_dbo.DeviceCMModel_dbo.ComPortModel_Hopper2ComPortId] FOREIGN KEY ([Hopper2ComPortId]) REFERENCES [dbo].[ComPortModel] ([Id]),
    CONSTRAINT [FK_dbo.DeviceCMModel_dbo.ComPortModel_PrinterComPortId] FOREIGN KEY ([PrinterComPortId]) REFERENCES [dbo].[ComPortModel] ([Id]),
    CONSTRAINT [FK_dbo.DeviceCMModel_dbo.DeviceModel_DeviceId] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[DeviceModel] ([Id]),
    CONSTRAINT [FK_dbo.DeviceCMModel_dbo.ExecutableDevice_BankModuleTypeId] FOREIGN KEY ([BankModuleTypeId]) REFERENCES [dbo].[ExecutableDevice] ([Id]),
    CONSTRAINT [FK_dbo.DeviceCMModel_dbo.ExecutableDevice_BarcodeTypeId] FOREIGN KEY ([BarcodeTypeId]) REFERENCES [dbo].[ExecutableDevice] ([Id]),
    CONSTRAINT [FK_dbo.DeviceCMModel_dbo.ExecutableDevice_CashDispenserTypeId] FOREIGN KEY ([CashDispenserTypeId]) REFERENCES [dbo].[ExecutableDevice] ([Id]),
    CONSTRAINT [FK_dbo.DeviceCMModel_dbo.ExecutableDevice_HopperTypeId] FOREIGN KEY ([HopperTypeId]) REFERENCES [dbo].[ExecutableDevice] ([Id]),
    CONSTRAINT [FK_dbo.DeviceCMModel_dbo.ExecutableDevice_PrinterTypeId] FOREIGN KEY ([PrinterTypeId]) REFERENCES [dbo].[ExecutableDevice] ([Id]),
    CONSTRAINT [Foreign_key0111] FOREIGN KEY ([ServerExchangeProtocolId]) REFERENCES [dbo].[ServerExchangeProtocolModel] ([Id]),
    CONSTRAINT [Foreign_key0112] FOREIGN KEY ([CardDispenserComPortId]) REFERENCES [dbo].[ComPortModel] ([Id]),
    CONSTRAINT [Foreign_key011212] FOREIGN KEY ([CashAcceptorTypeId]) REFERENCES [dbo].[ExecutableDevice] ([Id]),
    CONSTRAINT [Foreign_key01121233] FOREIGN KEY ([CardReaderTypeId]) REFERENCES [dbo].[ExecutableDevice] ([Id]),
    CONSTRAINT [Foreign_key0154] FOREIGN KEY ([CoinAcceptorComPortId]) REFERENCES [dbo].[ComPortModel] ([Id]),
    CONSTRAINT [Foreign_key01asdgf] FOREIGN KEY ([SlaveComPortId]) REFERENCES [dbo].[ComPortModel] ([Id]),
    CONSTRAINT [Foreign_key01sadg] FOREIGN KEY ([SlaveTypeId]) REFERENCES [dbo].[ExecutableDevice] ([Id]),
	CONSTRAINT [DeviceCMModel_Group_FK] FOREIGN KEY ([GroupId]) REFERENCES [dbo].[Group] ([Id])
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID устройства', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'DeviceId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID зоны', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'ZoneId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип диспенсера купюр', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CashDispenserTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Кол-во купюр верх', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'MaxCountUp';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Кол-во купюр низ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'MaxCountDown';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Текущее кол-во верхн.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CountUp';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Текущее кол-во нижн.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CountDown';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Com Port диспенсера купюр', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CashDispenserComPortId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Порог заполнения банкнотоприемника', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CashAcceptorLimit';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Порог оповещения банкнотоприемника', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CashAcceptorAlarm';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Текущее заполнение банкнотоприемника', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CashAcceptorCurrent';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Com Port  банкнотоприемника', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CashAcceptorComPortId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип хоппера  монет', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'HopperTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ёмкость правого ящика', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = 'MaxCount2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ёмкость левого ящика', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = 'MaxCount1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Текущее кол-во в правом', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = 'Count2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Текущее кол-во в левом', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = 'Count1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ком порт правого хоппера монет', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'Hopper1ComPortId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ком порт левого хоппера монет', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'Hopper2ComPortId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Порог заполнения монетоприемника', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CoinAcceptorLimit';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Порог оповещения монетоприемника', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CoinAcceptorAlarm';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Текущее кол-во монет в  монетоприемнике', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CoinAcceptorCurrent';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип банковского модуля', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'BankModuleTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие моторизированного считывателя', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'Reader';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие PayPass', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'PayPass';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип Сканера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'BarcodeTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Порт сканера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'BarcodeComPortId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Величина НДС', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'VAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Максимальная сумма на сдачу', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'MaximalChange';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип принтера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'PrinterTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ком порт принтера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'PrinterComPortId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ком порт картридера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CardReaderComPortId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Таймаут карты в губах (в секундах)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CardTimeOutToBasket';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Таймаут штрафной карты (в секундах)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'PenalCardTimeOut';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Возврат при отмене', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CancelRefund';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Смена открыта?', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'ShiftOpened';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ключ MiFare', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CardKey';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Номер кассы', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CashierNumber';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие диспенсера купюр', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CashDispenserExist';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Номинал верхнего', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'NominalUpId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Номинал нижнего', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'NominalDownId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Текущее кол-во отбракованных купюр', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'RejectCount';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие банкнотоприемника', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CashAcceptorExist';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие хоппера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'HopperExist';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Номинал правого', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'NominalRightId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Номинал левого', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'NominalLeftId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие монетоприемника', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CoinAcceptorExist';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип монетоприемника', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = 'CoinAcceptorTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие банковсокго модуля', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'BankModuleExist';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие диспенсера карт', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CardDispenserExists';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип диспенсера карт', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CardDispenserTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Порт диспенсера карт', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CardDispenserComPortId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие сканера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'BarcodeExist';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие ККМ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'KKMExists';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип ККМ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'KKMTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Авто смена', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'ShiftAutoClose';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время пересменка', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'ShiftNewShiftTime';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Пароль для команд', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CommandPassword';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Пароль для входа в режим регистрации', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'RegistartionPass';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Пароль для входа в режим Отчетов без гашения', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'WithOutCleaningPass';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Пароль для входа в режим Отчетов с гашением', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'WithCleaningPass';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ком порт ККМ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'KKMComPortId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие принтера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'PrinterExists';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие картридера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CardReaderExist';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип картридера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CardReaderTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие Слейв контроллера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'SlaveExist';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип Слейв контроллера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'SlaveTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ком порт Слейв контроллера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'SlaveComPortId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД тарифного плана для штрафного тарифа', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'PenaltyCardTPId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД тарифной сетки для штрафного тарифа', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'PenaltyCardTSId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сихронизация номналов', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'SyncNominals';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Способ обмена с сервером', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'ServerExchangeProtocolId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL сервера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'ServerURL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Текущая транзакция в смене', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CurrentTransactionNumber';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Таймаут языка по умолчанию', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'LanguageTimeOut';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Язык по умолчанию', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = 'DefaultLanguage';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IP камеры', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'WebCameraIP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Логин для подключения к камере', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'WebCameraLogin';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Пароль для подключения к камере', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'WebCameraPassword';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время открытия текущей смены', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'LastShiftOpenTime';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Банкнотоприемник', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CashAcceptorTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ком порт монетоприемника', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'CoinAcceptorComPortId';
GO
CREATE TRIGGER [dbo].[SyncDeviceCMModel]
ON [dbo].[DeviceCMModel]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [DeviceCMModel] set Sync=@dt	where DeviceId in (select DeviceId from inserted)
	update SyncTables set [DateTime]=@dt where TableName='DeviceCMModel'
end;
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Строка для подключения к камере', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceCMModel', @level2type = N'COLUMN', @level2name = N'WebCameraUrl';
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Адрес (параметр для взаимодействия с монетоприемником.',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'DeviceCMModel',
    @level2type = N'COLUMN',
    @level2name = N'CoinAcceptorAddress';
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Пинкод монетоприемника 12 байт (шифрованное поле)',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'DeviceCMModel',
    @level2type = N'COLUMN',
    @level2name = N'CoinAcceptorPinCode';
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Время последней синхронизации',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'DeviceCMModel',
    @level2type = N'COLUMN',
    @level2name = N'Sync';
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'ID группы',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'DeviceCMModel',
    @level2type = N'COLUMN',
    @level2name = N'GroupId';
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Работа с разовым клиентом при нулевом балансе ',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'DeviceCMModel',
    @level2type = N'COLUMN',
    @level2name = N'SingleClient0';
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Уровень логирования в файл',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'DeviceCMModel',
    @level2type = N'COLUMN',
    @level2name = N'LogLevel';
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Номер сектора на карте',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'DeviceCMModel',
    @level2type = N'COLUMN',
    @level2name = N'SectorNumber';
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Время забора чека',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'DeviceCMModel',
    @level2type = N'COLUMN',
    @level2name = N'ChequeTimeOut';
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Тип кассы (0 - полная, 1 - легкая, 2 - ручная)',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'DeviceCMModel',
    @level2type = N'COLUMN',
    @level2name = N'CashierType';
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Номер смены',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'DeviceCMModel',
    @level2type = N'COLUMN',
    @level2name = 'ShiftNumber';
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Что делать с чеком (0 - забирать, 1 - ничего, 2 - выбрасывать)',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'DeviceCMModel',
    @level2type = N'COLUMN',
    @level2name = N'ChequeWork';
GO
