﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using ASE.MVC;
using Newtonsoft.Json;

namespace rps.SignalR
{
    public class HubCallCentral : Hub
    {
        private static Dictionary<string, string> calls = new Dictionary<string, string>();

        public void JoinAsOperator()
        {
            Groups.Add(Context.ConnectionId, "CallCenter");
        }

        public void CallStart(string message)
        {
            lock (calls)
            {
                if (!calls.ContainsKey(Context.ConnectionId))
                    calls.Add(Context.ConnectionId, message);
            }
            Clients.Group("CallCenter").onClientCallStart(Context.ConnectionId, message);
        }

        public void CallStop(string message)
        {
            lock (calls)
            {
                if (calls.ContainsKey(Context.ConnectionId))
                {
                    calls.Remove(Context.ConnectionId);
                    Clients.Group("CallCenter").onClientCallStop(Context.ConnectionId, message);
                }
            }
        }

        public string Answer(string clienId)
        {
            lock (calls)
            {
                if (!calls.ContainsKey(clienId))
                    return "No call - answered or cancelled";

                calls.Remove(clienId);
                Clients.Group("CallCenter").onClientCallStop(clienId, "Operator answered");
                Clients.Client(clienId).onAnswer(Context.ConnectionId);

                return "ok";
            }
        }

        public void OfferClient2Operator(string operatorId, string offer)
        {
            Clients.Client(operatorId).onClientOffer(Context.ConnectionId, offer);
        }

        #region OnConnected + OnDisconnected + OnReconnected

        public override Task OnConnected()
        {
            Clients.All.onConnected(Context.ConnectionId);

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            lock (calls)
            {
                lock (calls)
                {
                    if (calls.ContainsKey(Context.ConnectionId))
                    {
                        calls.Remove(Context.ConnectionId);
                        Clients.Group("CallCenter").onClientCallStop(Context.ConnectionId, "Disconnected");
                    }
                }
            }
            Clients.All.onDisconnected(Context.ConnectionId);

            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            Clients.All.onReconnected(Context.ConnectionId);

            return base.OnReconnected();
        }

        #endregion OnConnected + OnDisconnected + OnReconnected
    }
}