﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rps.Data
{
    /// <summary>
    /// Карта/Уровень
    /// </summary>
    public class Map
    {
        [Key]
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        [Display(Name = "Этаж")]
        public int Level { get; set; }

        [Display(Name = "Наименование")]
        public string Name { get; set; }

        public byte[] Image { get; set; }

        public int ImageW { get; set; }

        public int ImageH { get; set; }

        public string ImageName { get; set; }

        public Guid? ParkingId { get; set; }

        public virtual ParkingModel Parking { get; set; }
    }

    /// <summary>
    /// Прямоугольник на карте
    /// </summary>
    public class NavMapRect
    {
        [Key]
        public Guid Id { get; set; }

        [Display(Name = "Номер")]
        public int Index { get; set; }

        [Display(Name = "Парковка")]
        public Guid NavMapId { get; set; }

        [Display(Name = "Парковка")]
        public virtual Map NavMap { get; set; }

        [Display(Name = "Наименование")]
        public string Name { get; set; }

        [Display(Name = "Отображать")]
        public bool Visible { get; set; }

        [Display(Name = "Трансформация")]
        public string Transform { get; set; }

        public int Type { get; set; }

        public long? CurrentReservId { get; set; }

        public virtual NavMapRectReserv CurrentReserv { get; set; }
    }

    public class NavMapRectReserv
    {
        [Key]
        public long Id { get; set; }

        public Guid NavMapRectId { get; set; }

        public virtual NavMapRect NavMapRect { get; set; }

        public DateTime DateTime { get; set; }
    }

    /// <summary>
    /// Настройки
    /// </summary>
    public class Setting
    {
        [Key]
        public long Id { get; set; }

        public Guid UserId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Value { get; set; }
    }

    public enum DeviceType
    {
        EntryStand = 1,
        ExitStand = 2,
        CrossStand = 3,
        CashBox = 4,
        Board = 5,
        StandCash = 6,
        ZoneController = 7,
        ManualCashBox = 8,
    }

    public class Device//: Parking.Data.Devices.Device
    {
        public static Dictionary<int, string> DeviceTypeList = new Dictionary<int, string>
        {
            { 1, "Въездная стойка" },
            { 2, "Выездная стойка" },
            { 3, "Переездная стойка" },
            { 4, "Касса" },
            { 5, "Табло" },
            { 6, "Касса на выезде" },
            { 7, "Контроллер машиномест" },
            { 8, "Ручная касса" }
        };

        [Key]
        public Guid Id { get; set; }

        [DisplayName("Внешний код")]
        public long ExternalId { get; set; }

        [DisplayName("Уровень")]
        public Guid LevelId { get; set; }

        [DisplayName("Уровень")]
        public virtual Map Level { get; set; }

        [DisplayName("Включен")]
        public bool Enabled { get; set; }

        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Тип")]
        public DeviceType Type { get; set; }

        //public int NetworkAddress { get; set; }
        //public OnlineStatusParameter OnlineStatus { get; }
        //public Dictionary<int, DeviceParameter> Parameters { get; }
        //public DeviceType Type { get; set; }
        //public HardwareVersion Version { get; set; }
    }

    public class SyncItem
    {
        [Key]
        public Guid Id { get; set; }

        public Guid ObjecId { get; set; }

        public int ObjecType { get; set; }

        public int Operation { get; set; }

        public DateTime Created { get; set; }
    }

    public class GuiItems
    {
        [Key]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int ObjecType { get; set; }

        public int Operation { get; set; }

        public DateTime Created { get; set; }
    }
}
