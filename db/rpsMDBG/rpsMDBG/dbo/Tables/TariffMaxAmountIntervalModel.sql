﻿CREATE TABLE [dbo].[TariffMaxAmountIntervalModel] (
    [Id]                 UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Amount]             DECIMAL (18, 2)  NOT NULL,
    [_IsDeleted]         BIT              NOT NULL,
    [Sync]               DATETIME         NULL,
    [TariffId]           UNIQUEIDENTIFIER NULL,
    [Start]              INT              NULL,
    [StartTimeTypeId]    INT              NULL,
    [Duration]           INT              NULL,
    [DurationTimeTypeId] INT              NULL,
    [Repeat]             INT              NULL,
    CONSTRAINT [PK_dbo.TariffMaxAmountIntervalModel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TariffMaxAmountIntervalModel_Tariffs] FOREIGN KEY ([TariffId]) REFERENCES [dbo].[Tariffs] ([Id]),
    CONSTRAINT [TariffMaxAmountIntervalModel_fk] FOREIGN KEY ([StartTimeTypeId]) REFERENCES [dbo].[TimeTypeModel] ([Id]),
    CONSTRAINT [TariffMaxAmountIntervalModel_fk2] FOREIGN KEY ([DurationTimeTypeId]) REFERENCES [dbo].[TimeTypeModel] ([Id])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffMaxAmountIntervalModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Сумма', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffMaxAmountIntervalModel', @level2type = N'COLUMN', @level2name = N'Amount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffMaxAmountIntervalModel', @level2type = N'COLUMN', @level2name = N'Sync';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Начало', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffMaxAmountIntervalModel', @level2type = N'COLUMN', @level2name = N'Start';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип времени', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffMaxAmountIntervalModel', @level2type = N'COLUMN', @level2name = N'StartTimeTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Длительность', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffMaxAmountIntervalModel', @level2type = N'COLUMN', @level2name = N'Duration';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип времени', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffMaxAmountIntervalModel', @level2type = N'COLUMN', @level2name = N'DurationTimeTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Кол-во повторов', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffMaxAmountIntervalModel', @level2type = N'COLUMN', @level2name = N'Repeat';

GO
CREATE TRIGGER [dbo].[SyncTariffMaxAmountIntervalModel]
ON [dbo].[TariffMaxAmountIntervalModel]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [TariffMaxAmountIntervalModel] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='TariffMaxAmountIntervalModel'
end
