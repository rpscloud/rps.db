﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

using ASE.Data;
using ASE.MVC;

using rps.Data;

namespace rps.Controllers
{
    public class IntegrationController : ControllerAdv<DB>
    {
        //[HttpPost]
        public JsonResult StartSynchronization(Guid id)
        {
            var ret = Start(id).Result;

            return new JsonResult
            {
                JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet,
                Data = new 
                {
                    started = DateTime.Now,
                    messages = ret
                }
            };
        }

        private static readonly Object forLock = new Object();

        public static void AddInLog(List<KeyValuePair<int, string>> log, int i, string s)
        {
            log.Add(new KeyValuePair<int, string>(i, s));
        }

        public async static Task<List<KeyValuePair<int, string>>> Start(Guid id)
        {
            List<KeyValuePair<int, string>> log = new List<KeyValuePair<int, string>>();

            bool lockWasTaken = false;
            try
            {
                Monitor.Enter(forLock, ref lockWasTaken);

                if (lockWasTaken)
                {
                    AddInLog(log, 0, "Synchronization start at: " + DateTime.Now);

                    DB db3 = new DB();
                    var parking3 = db3.Parkings.Find(id);
                    if (parking3 == null) 
                    {
                        AddInLog(log, 1, "Parking not found, id: " + id);
                        return log;
                    }

                    DB dbL = new DB(String.Format("data source={0};Initial Catalog={1};User ID={2};Password={3}", parking3.S3Sql, parking3.S3Db, parking3.S3User, parking3.S3Password));
                    DB dbR = new DB(String.Format("data source={0};Initial Catalog={1};User ID={2};Password={3}", parking3.S2Sql, parking3.S2Db, parking3.S2User, parking3.S2Password));

                    var tranL = dbL.Database.BeginTransaction();
                    var tranR = dbR.Database.BeginTransaction();
                    try
                    {
                        //Organization
                        AddInLog(log, 0, "Organization start at: " + DateTime.Now);
                        Organization(db3, dbL, dbR, parking3, log);
                        AddInLog(log, 0, "Organization end at: " + DateTime.Now);

                        //Parkings
                        AddInLog(log, 0, "Parkings start at: " + DateTime.Now);
                        //Parkings(db3, db2l, db2r, parking3, log);
                        AddInLog(log, 0, "Parkings end at: " + DateTime.Now);

                        //Users
                        AddInLog(log, 0, "OrganizationParkings start at: " + DateTime.Now);
                        //OrganizationParkings(db3, db2l, db2r, parking3, log);
                        AddInLog(log, 0, "OrganizationParkings end at: " + DateTime.Now);

                        //Users
                        AddInLog(log, 0, "Users start at: " + DateTime.Now);
                        //Users(db3, db2l, db2r, parking3, log);
                        AddInLog(log, 0, "Users end at: " + DateTime.Now);

                        //UserParking
                        AddInLog(log, 0, "UserParkings start at: " + DateTime.Now);
                        //UserParkings(db3, db2l, db2r, parking3, log);
                        AddInLog(log, 0, "UserParkings end at: " + DateTime.Now);

                        //Rights
                        AddInLog(log, 0, "Rights start at: " + DateTime.Now);
                        //Rights(db3, db2l, db2r, parking3, log);
                        AddInLog(log, 0, "Rights end at: " + DateTime.Now);

                        //Roles
                        //RightInRole

                        tranL.Commit();
                        tranR.Commit();
                    }
                    catch (Exception exc)
                    {
                        tranL.Rollback();
                        tranR.Rollback();

                        AddInLog(log, 100, "Rollback: " + DateTime.Now);
                        AddInLog(log, 100, " " + exc.Message);
                        if (exc.InnerException != null)
                        {
                            AddInLog(log, 100, " " + exc.InnerException.Message);
                            if (exc.InnerException.InnerException != null)
                                AddInLog(log, 100, " " + exc.InnerException.InnerException.Message);
                        }
                    }
                }
            }
            finally
            {
                if (lockWasTaken)
                {
                    Monitor.Exit(forLock);
                }
            }

            return log;
        }

        private static bool HaveChanges(params IChanged[] models)
        {
            foreach (var model in models)
                if (model != null)
                    if (model.ChangedDateTime != null)
                        return true;

            return false;
        }

        private static void Detach(DB db, object model)
        {
            if (model != null)
                db.Entry(model).State = System.Data.Entity.EntityState.Detached;
        }

        private static T Newest<T>(params T[] models) where T : IChanged
        {
            T ret = default(T);
            var prev = DateTime.MinValue;

            foreach (var model in models)
                if (model != null)
                    if (model.ChangedDateTime != null)
                        if (model.ChangedDateTime > prev)
                            ret = model;

            return ret;
        }

        private static void AddOrUpdate(DB db, IChanged model, IChanged modelDb)
        {
            if (modelDb == null)
            {
                model.ChangedDateTime = null;
                db.Entry(model).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();
            }
            else if (modelDb != null)
            {
                if (model.ChangedDateTime > modelDb.ChangedDateTime)
                {
                    model.ChangedDateTime = null;
                    db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
            db.Entry(model).State = System.Data.Entity.EntityState.Detached;
        }

        public static int Organization(DB dbM, DB dbL, DB dbR, ParkingModel parking3, List<KeyValuePair<int, string>> log)
        {
            var listL = dbL.Organizations.Where(x => x.ChangedDateTime != null).AsNoTracking().ToList();

            foreach (var item in listL)
            {
                var modelR = dbR.Organizations.Find(item.Id);
                AddOrUpdate(dbR, item, modelR);

                var modelL = dbL.Organizations.Find(item.Id);
                modelL.ChangedDateTime = null;
                dbL.SaveChanges();
            }

            var listR = dbR.Organizations.Where(x => x.ChangedDateTime != null).AsNoTracking().ToList();
            foreach (var item in listR)
            {
                var modelM = dbM.Organizations.Find(item.Id);
                var modelL = dbR.Organizations.Find(item.Id);

                AddOrUpdate(dbM, item, modelM);
                AddOrUpdate(dbL, item, modelL);

                var modelR = dbR.Organizations.Find(item.Id);
                modelR.ChangedDateTime = null;
                dbR.SaveChanges();
            }
            return 0;
        }

        public static int Parkings(DB db3, DB db2l, DB db2r, ParkingModel parking3, List<KeyValuePair<int, string>> log)
        {
            /*var model3 = db3.Parkings.Find(parking3.Id);
            var model2l = db2l.Parkings.Find(parking3.Id);
            var model2r = db2r.Parkings.Find(parking3.Id);

            //No objects
            //if ((model3 == null) & (model2l == null) & (model2r == null))
                //return 0;

            //No changes
            if (!HaveChanges(model3, model2l, model2r))
                return 0;

            //Detach
            Detach(db3, model3);
            Detach(db2l, model2l);
            Detach(db2r, model2r);

            //Where newset version
            var model = Newest(model3, model2l, model2r);
            model.ChangedDateTime = null;

            AddOrUpdate(model, model3, db3);
            AddOrUpdate(model, model2l, db2l);
            AddOrUpdate(model, model2r, db2r);*/

            return 0;
        }

        public static int OrganizationParkings(DB db3, DB db2l, DB db2r, ParkingModel parking3, List<KeyValuePair<int, string>> log)
        {
            /*var list = db3.OrganizationParkings.Where(x => x.ParkingId == parking3.Id).ToList();
            foreach (var item in list)
            {
                var model3 = db3.OrganizationParkings.Find(item.Id);
                var model2l = db2l.OrganizationParkings.Find(item.Id);
                var model2r = db2r.OrganizationParkings.Find(item.Id);

                //No objects
                //if ((model3 == null) & (model2l == null) & (model2r == null))
                    //continue;

                //No changes
                if (!HaveChanges(model3, model2l, model2r))
                    continue;

                //Detach
                Detach(db3, model3);
                Detach(db2l, model2l);
                Detach(db2r, model2r);

                //Where newset version
                var model = Newest(model3, model2l, model2r);
                model.ChangedDateTime = null;

                AddOrUpdate(model, model3, db3);
                AddOrUpdate(model, model2l, db2l);
                AddOrUpdate(model, model2r, db2r);
            }*/
            return 0;
        }

        public static void AddUpdate(bool add, IChanged item, IChanged item2, DB db1, DB db2)
        {
            if (add)
            {
                db1.Entry(item).State = EntityState.Added;
                db1.SaveChanges();
                db1.Entry(item).State = EntityState.Detached;
            }
            else if (item.ChangedDateTime > item2.ChangedDateTime)
            { 
                db1.Entry(item).State = EntityState.Modified;
                db1.SaveChanges();
                db1.Entry(item).State = EntityState.Detached;
            }
            

            if (db2 != null)
            {
                if (add)
                {
                    db2.Entry(item).State = EntityState.Added;
                    db2.SaveChanges();
                    db2.Entry(item).State = EntityState.Detached;
                }
                else if (item.ChangedDateTime > item2.ChangedDateTime)
                { 
                    db2.Entry(item).State = EntityState.Modified;
                    db2.SaveChanges();
                    db2.Entry(item).State = EntityState.Detached;
                }
            }
        }

        public static void SyncItems<T>(List<T> list2l, List<T> list2r, DB db2l, DB db2r, DB db3) where T : IChanged
        {
            //db2l -> db2r
            foreach (var item in list2l)
            {
                var item2 = list2r.FirstOrDefault(x => x.Id == item.Id);
                AddUpdate((item2 == null), item, item2, db2r, null);
            }
            //db2r -> db2l
            foreach (var item in list2r)
            {
                var item2 = list2l.FirstOrDefault(x => x.Id == item.Id);
                AddUpdate((item2 == null), item, item2, db2l, db3);
            }
        }

        public static int Users(DB db3, DB db2l, DB db2r, ParkingModel parking3, List<KeyValuePair<int, string>> log)
        {
            var list2l = db2l.Users.AsNoTracking().ToList();
            var list2r = db2r.Users.AsNoTracking().ToList();

            SyncItems(list2l, list2r, db2l, db2r, db3);

            return 0;
        }

        public static int UserParkings(DB db3, DB db2l, DB db2r, ParkingModel parking3, List<KeyValuePair<int, string>> log)
        {
            /*var list = db3.UserParkings.Where(x => x.ParkingId == parking3.Id).ToList();
            foreach (var item in list)
            {
                var model3 = db3.UserParkings.Find(item.Id);
                var model2l = db2l.UserParkings.Find(item.Id);
                var model2r = db2r.UserParkings.Find(item.Id);

                //No objects
                //if ((model3 == null) & (model2l == null) & (model2r == null))
                //continue;

                //No changes
                if (!HaveChanges(model3, model2l, model2r))
                    continue;

                //Detach
                Detach(db3, model3);
                Detach(db2l, model2l);
                Detach(db2r, model2r);

                //Where newset version
                var model = Newest(model3, model2l, model2r);
                model.ChangedDateTime = null;

                AddOrUpdate(model, model3, db3);
                AddOrUpdate(model, model2l, db2l);
                AddOrUpdate(model, model2r, db2r);
            }*/
            return 0;
        }

        /*public static int UserParkings(DB db3, DB db2l, DB db2r, ParkingModel parking3, List<KeyValuePair<int, string>> log)
        {
            var list = db3.UserParkings.Where(x => x.ParkingId == parking3.Id).ToList();
            foreach (var item in list)
            {
                var model3 = db3.UserParkings.Find(item.Id);
                var model2l = db2l.UserParkings.Find(item.Id);
                var model2r = db2r.UserParkings.Find(item.Id);

                //No objects
                //if ((model3 == null) & (model2l == null) & (model2r == null))
                //continue;

                //No changes
                if (!HaveChanges(model3, model2l, model2r))
                    continue;

                //Detach
                Detach(db3, model3);
                Detach(db2l, model2l);
                Detach(db2r, model2r);

                //Where newset version
                var model = Newest(model3, model2l, model2r);
                model.ChangedDateTime = null;

                AddOrUpdate(model, model3, db3);
                AddOrUpdate(model, model2l, db2l);
                AddOrUpdate(model, model2r, db2r);
            }
            return 0;
        }*/
    }
}