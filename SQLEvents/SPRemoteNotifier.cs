using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Threading;
using System.Net;

public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void SPRemoteNotifier(string url)
    {
        SqlContext.Pipe.Send("SPRemoteNotifier: " + url);

        ThreadPool.QueueUserWorkItem(HttpPost, url);
    }

    public static void HttpPost(object param)
    {
        using(var wc = new WebClient())
        {
            try
            {
                wc.UploadDataAsync(new Uri(param.ToString()), new byte[0]);
            }
            catch
            {

            }
        }
    }
}
