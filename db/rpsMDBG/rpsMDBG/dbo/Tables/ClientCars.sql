﻿CREATE TABLE [dbo].[ClientCars] (
    [Id]        UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Sync]      DATETIME         NULL,
    [ClientId]  UNIQUEIDENTIFIER NULL,
    [CompanyId] UNIQUEIDENTIFIER NULL,
    [CarModel]  VARCHAR (MAX)    NULL,
    [CarColor]  VARCHAR (MAX)    NULL,
    [CarPlate]  VARCHAR (MAX)    NULL,
    [_IsDeleted] BIT NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [ClientCars_fk] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[CompanyModel] ([Id]),
    CONSTRAINT [Foreign_key01] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[ClientModel] ([Id]) ON DELETE SET NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ClientCars', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ClientCars', @level2type = N'COLUMN', @level2name = N'Sync';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД клиента', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ClientCars', @level2type = N'COLUMN', @level2name = N'ClientId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Модель ТС', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ClientCars', @level2type = N'COLUMN', @level2name = N'CarModel';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Цвет ТС', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ClientCars', @level2type = N'COLUMN', @level2name = N'CarColor';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Гос номер', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ClientCars', @level2type = N'COLUMN', @level2name = N'CarPlate';

GO
CREATE TRIGGER [dbo].[SyncClientCars]
ON [dbo].[ClientCars]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [ClientCars] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='ClientCars'
end
