﻿CREATE TABLE [dbo].[ParkingSpaceModel] (
    [Id]             UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [DeviceId]       UNIQUEIDENTIFIER NOT NULL,
    [Line]           INT              NOT NULL,
    [Name]           VARCHAR (MAX)    NULL,
    [NetworkAddress] INT              NOT NULL,
    [MMHeight]       INT              NOT NULL,
    [State]          INT              NOT NULL,
    [Refreshed]      DATETIME         NULL,
    [X]              DECIMAL (18, 2)  NOT NULL,
    [Y]              DECIMAL (18, 2)  NOT NULL,
    [W]              INT              NOT NULL,
    [H]              INT              NOT NULL,
    [R]              DECIMAL (18, 2)  NOT NULL,
    [_IsDeleted]     BIT              NOT NULL,
    CONSTRAINT [PK_dbo.ParkingSpaceModel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.ParkingSpaceModel_dbo.DeviceModel_DeviceId] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[DeviceModel] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_DeviceId]
    ON [dbo].[ParkingSpaceModel]([DeviceId] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'устройство', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingSpaceModel', @level2type = N'COLUMN', @level2name = N'DeviceId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'линия', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingSpaceModel', @level2type = N'COLUMN', @level2name = N'Line';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'наименование', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingSpaceModel', @level2type = N'COLUMN', @level2name = N'Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'сетевой адрес', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingSpaceModel', @level2type = N'COLUMN', @level2name = N'NetworkAddress';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'высота', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingSpaceModel', @level2type = N'COLUMN', @level2name = N'MMHeight';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'состояние', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingSpaceModel', @level2type = N'COLUMN', @level2name = N'State';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'обновление состояния', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingSpaceModel', @level2type = N'COLUMN', @level2name = N'Refreshed';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'координата Х', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingSpaceModel', @level2type = N'COLUMN', @level2name = N'X';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'координата Y', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingSpaceModel', @level2type = N'COLUMN', @level2name = N'Y';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ширина', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingSpaceModel', @level2type = N'COLUMN', @level2name = N'W';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'высота', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingSpaceModel', @level2type = N'COLUMN', @level2name = N'H';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'поворот', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ParkingSpaceModel', @level2type = N'COLUMN', @level2name = N'R';

