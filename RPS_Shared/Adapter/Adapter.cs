﻿using System;
using System.Collections.Generic;
using System.Linq;
using rps.Data;

namespace RPS_Shared
{
    internal enum TimeType : int
    {
        Minutes = 1,
        Hours = 2,
        Days = 3,
        Weeeks = 4,
        Months = 5
    }

    internal enum PlanType : int
    {
        Weekly = 1,
        Monthly = 2,
        Intervals = 3
    }

    internal enum ChangeType : int
    {
        Previous = 1,
        Exact = 2,
        Next = 3
    }

    public static class Adapter
    {
        public static Tariff ToTariff(this Tariffs model)
        {
            Tariff tariff = new Tariff();
            tariff.Name = String.IsNullOrWhiteSpace(model.Name) ? model.Id.ToString() : model.Name;
            tariff.Protection = ((TimeType)model.ProtectedIntervalTimeTypeId).GetTime(model.ProtectedInterval);
            tariff.SwitchMode = ((ChangeType)model.ChangingTypeId).GetMode();
            tariff.Initial = ((TimeType)model.InitialTimeTypeId).GetTime(model.Initial);
            tariff.InitialAmount = model.InitialAmount;

            List<Rule> rules = new List<Rule>();
            foreach (TariffTariffPlanModel tpm in model.TariffTariffPlanModel.ToList())
            {
                if (
                    (!tpm.C_IsDeleted)  // Не удален
                    && (model.Id == tpm.TariffPlanId) // Соответствует заказанному тарифу
                    ) 
                {
                    TariffModel tm = tpm.TariffModel;
                    Rule r = new Rule(tm.Id);
                    r.Name = tm.Name;

                    TimeType tt = (TimeType)tm.TimeTypeId;
                    Interval prev = null;
                    foreach (TariffIntervalModel tim in tm.TariffIntervalModel.OrderBy(x => x.Start))
                    {
                        if (!tim.C_IsDeleted)
                        {
                            Interval i = new Interval(tim.Id);
                            i.Start = tt.GetTime(tim.Start);
                            i.Amount = tim.Amount;
                            i.Period = tt.GetTime(tim.Duration);

                            r.Intervals.Add(i);

                            if (prev != null)
                                prev.Duration = i.Start - prev.Start;

                            prev = i;
                        }
                    }

                    prev.Duration = Interval.Infinite;

                    PlanType pt = (PlanType)model.TypeId;
                    List<ICondition> cs = new List<ICondition>();
                    ICondition c = null;
                    foreach (TariffPlanPeriodModel p in tpm.TariffPlanPeriodModel)
                    {
                        if (!p.C_IsDeleted)
                        {
                            switch (pt)
                            {
                                case PlanType.Weekly:
                                    c = new WeeklyIntervalCondition(p.Start.DayOfWeek, p.Start.TimeOfDay, p.End.DayOfWeek, p.End.TimeOfDay);
                                    break;
                                case PlanType.Monthly:
                                    c = new MonthlyIntervalCondition(p.Start.Day, p.Start.TimeOfDay, p.End.Day, p.End.TimeOfDay);
                                    break;
                                case PlanType.Intervals:
                                    c = new DateIntervalCondition(p.Start, p.End);
                                    break;
                                default:
                                    throw new ApplicationException(String.Format("Период тарифного плана {0} не поддерживается", pt));
                            }

                            cs.Add(c);
                        }
                    }

                    if (cs.Count > 1)
                        c = cs.Concat().Any();
                    else
                    {
                        c = cs.FirstOrDefault();
                        if (c == null)
                            throw new ApplicationException("Не заданы условия действия тарифа");
                    }

                    r.Match = c;
                    rules.Add(r);
                }
            }

            List<Limitation> lims = new List<Limitation>();
            foreach (TariffMaxAmountIntervalModel tmi in model.TariffMaxAmountIntervalModel)
            {
                if (!tmi.C_IsDeleted)
                {
                    Limitation l = new Limitation(tmi.Id);
                    TimeType tts = (TimeType)tmi.StartTimeTypeId;
                    TimeType ttd = (TimeType)tmi.DurationTimeTypeId;
                    l.Match = new RepeatIntervalCondition(tts.GetTime((int)tmi.Start), ttd.GetTime((int)tmi.Duration));
                    l.Amount = tmi.Amount;
                    l.Repeat = ((tmi.Repeat == null) ? Interval.Unlimited : (((int)tmi.Repeat) < 0) ? Interval.Unlimited : (int)tmi.Repeat);
                    //if (tmi.TariffId != null)
                    //{
                    //  var rule = rules.FirstOrDefault(r => r.ID == tmi.TariffId);
                    //  if (rule != null)
                    //    l.Bind(rule);
                    //}

                    lims.Add(l);
                }
            }

            tariff.Rules.AddRange(rules);
            tariff.Limitations.AddRange(lims);

            return tariff;
        }

        internal static SwitchMode GetMode(this ChangeType ct)
        {
            SwitchMode m = SwitchMode.Inherit;
            switch (ct)
            {
                case ChangeType.Previous:
                    m = SwitchMode.PreviousPriority;
                    break;
                case ChangeType.Exact:
                    m = SwitchMode.Match;
                    break;
                case ChangeType.Next:
                    m = SwitchMode.NextPriority;
                    break;
                default:
                    throw new ApplicationException(String.Format("Правило перехода {0} не поддерживается", ct));
            }

            return m;
        }

        internal static TimeSpan GetTime(this TimeType tt, int value)
        {
            if (value < 0)
                return Interval.Infinite;

            TimeSpan ts = TimeSpan.Zero;
            switch (tt)
            {
                case TimeType.Minutes:
                    ts = TimeSpan.FromMinutes(1);
                    break;
                case TimeType.Hours:
                    ts = TimeSpan.FromHours(1);
                    break;
                case TimeType.Days:
                    ts = TimeSpan.FromDays(1);
                    break;
                case TimeType.Weeeks:
                    ts = TimeSpan.FromDays(7);
                    break;
                case TimeType.Months:
                    {
                        DateTime NewDate = DateTime.Now.AddMonths(1);
                        ts = NewDate - DateTime.Now;
                    }
                    break;
                default:
                    throw new ApplicationException(String.Format("Тип временного интервала {0} не поддерживается", tt));
            }

            return TimeSpan.FromMinutes(ts.TotalMinutes * value);
        }

        internal static TimeSpan GetTime(this TimeType tt, DateTime value)
        {
            DateTime Zero = new DateTime(value.Year, value.Month, value.Day);

            TimeSpan ts = value - Zero;

            return TimeSpan.FromMinutes(ts.TotalMinutes);
        }
    }
}