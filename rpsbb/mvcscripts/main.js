require([
        'app',


        'backbone',

    ],
    function (App, Backbone) {
        'use strict';


        // any extras?
        App.on('initialize:after', function () {
            // if (Backbone.history){
            // Backbone.history.start();
            // }
        });

        App.on("start", function (options) {
            if (Backbone.history) {
                Backbone.history.start();
            }
            console.log(App);


        });

        // Start Marionette Application in desktop mode (default)
        App.start();


    });
