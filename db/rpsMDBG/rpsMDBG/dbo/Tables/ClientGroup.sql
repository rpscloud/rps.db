﻿CREATE TABLE [dbo].[ClientGroup] (
    [Id]     UNIQUEIDENTIFIER NOT NULL,
    [Sync]   DATETIME         NOT NULL,
    [ZoneID] UNIQUEIDENTIFIER NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [ClientGroup_fk] FOREIGN KEY ([ZoneID]) REFERENCES [dbo].[ZoneModel] ([Id])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД Зоны', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ClientGroup', @level2type = N'COLUMN', @level2name = N'ZoneID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ClientGroup', @level2type = N'COLUMN', @level2name = 'Sync';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ClientGroup', @level2type = N'COLUMN', @level2name = N'Id';

