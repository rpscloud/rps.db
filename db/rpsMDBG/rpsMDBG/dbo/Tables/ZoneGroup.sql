CREATE TABLE [dbo].[ZoneGroup] (
    [Id]      UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Sync]    DATETIME         NULL,
    [_IsDeleted] BIT              NULL,
    [GroupId] UNIQUEIDENTIFIER NULL,
    [ZoneId]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_dbo.ZoneGroup] PRIMARY KEY CLUSTERED ([Id] ASC),

);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ZoneGroup', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ZoneGroup', @level2type = N'COLUMN', @level2name = N'Sync';



GO
CREATE TRIGGER [dbo].[SyncZoneGroup]
ON [dbo].[ZoneGroup]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [ZoneGroup] set [Sync]=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='ZoneGroup'
end
