﻿CREATE TABLE [dbo].[AuthItem] (
    [name]        VARCHAR (64) NOT NULL,
    [type]        INT          NOT NULL,
    [description] TEXT         NULL,
    [bizrule]     TEXT         NULL,
    [data]        TEXT         NULL,
    PRIMARY KEY CLUSTERED ([name] ASC)
);

