using System;

namespace ASE.MVC
{
    public class AuthUser
    {
        public Guid Id { get; set; }

        public string LoginName { get; set; }

        public string Password { get; set; }

        public DateTime CreateDate { get; set; }

    }
}