﻿CREATE TABLE [dbo].[CashAcceptorAllowModel] (
    [DeviceId]   UNIQUEIDENTIFIER NOT NULL,
    [BanknoteId] INT              NOT NULL,
    [Sync]      DATETIME         NULL,
    [Allow] BIT NOT NULL DEFAULT 0, 
    [Id] UNIQUEIDENTIFIER NOT NULL, 
    CONSTRAINT [FK_dbo.CashAcceptorAllowModel_dbo.BanknoteModel_BanknoteId] FOREIGN KEY ([BanknoteId]) REFERENCES [dbo].[BanknoteModel] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.CashAcceptorAllowModel_dbo.DeviceModel_DeviceId] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[DeviceModel] ([Id]) ON DELETE CASCADE, 
    CONSTRAINT [PK_CashAcceptorAllowModel] PRIMARY KEY ([Id])
);


GO
CREATE UNIQUE INDEX [IX_CashAcceptorAllowModel_DeviceId_BancnoteId] ON [dbo].[CashAcceptorAllowModel] ([DeviceId], [BanknoteId] ASC)

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Устройство', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CashAcceptorAllowModel', @level2type = N'COLUMN', @level2name = N'DeviceId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Флаг синхронизации со 2ым ур.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CashAcceptorAllowModel', @level2type = N'COLUMN', @level2name = 'Sync';

GO
CREATE TRIGGER [dbo].[SyncCashAcceptorAllowModel]
ON [dbo].[CashAcceptorAllowModel]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [CashAcceptorAllowModel] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='CashAcceptorAllowModel'
end
