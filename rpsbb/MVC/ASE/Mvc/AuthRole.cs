using System.ComponentModel.DataAnnotations;

namespace ASE.MVC
{
    public class AuthRole
    {
        [Key]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}