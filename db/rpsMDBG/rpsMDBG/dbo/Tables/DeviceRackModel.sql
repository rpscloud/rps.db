﻿CREATE TABLE [dbo].[DeviceRackModel] (
    [DeviceId]            UNIQUEIDENTIFIER NOT NULL,
    [ZoneBeforeId]        TINYINT          NULL,
    [ZoneAfterId]         TINYINT          NULL,
    [DispenserTypeId]     INT              NULL,
    [BarcodeTypeId]       INT              NULL,
    [BankModuleTypeId]    INT              NULL,
    [MaxBusyTimeAntennaA] INT              NOT NULL,
    [MaxBusyTimeAntennaB] INT              NOT NULL,
    [MaxBusyTimeIR]       INT              NOT NULL,
    [_IsDeleted]          BIT              NOT NULL,
    [Sync]               DATETIME         NULL,
    [IdStoyky]            INT              NULL,
    [SlaveExist]          BIT              NULL,
    [ReaderOutExists]     BIT              NULL,
    [OutReaderTypeId]     INT              NULL,
    [ReaderInExists]      BIT              NULL,
    [InReaderTypeId]      INT              NULL,
    [DispensExists]       BIT              NULL,
    [FeederExists]        BIT              NULL,
    [FeederTypeId]        INT              NULL,
    [BarcodeExist]        BIT              NULL,
    [BankModuleExist]     BIT              NULL,
    [DisplayExists]       BIT              NULL,
    [DisplayTypeId]       INT              NULL,
    [ComDisplay]           INT              NULL,
    [ComSlave]            INT              NULL,
    [ComReadOut]          INT              NULL,
    [ComReadIn]           INT              NULL,
    [ComDispens]          INT              NULL,
    [ComFeeder]           INT              NULL,
    [ComBarcode]          INT              NULL,
    [PortDiscret1]        INT              NULL,
    [PortDiscret2]        INT              NULL,
    [PortDiscret3]        INT              NULL,
	[PlateNumberUse]        INT              NULL,
    [TimeCardV]           INT              NULL,
    [TimeBarrier]         INT              NULL,
    [NumerSector]         INT              NULL,
    [TarifIdScheduleId]   TINYINT          NULL,
    [TarifPlanId]         TINYINT          NULL,
    [ClientGroupidToC]    TINYINT          NULL,
    [TimeNotLoopA]        INT              NULL,
    [TimeNotIr]           INT              NULL,
    [LoopA]               BIT              NULL,
    [LoopB]               BIT              NULL,
    [IR]                  BIT              NULL,
    [PUSH]                BIT              NULL,
    [Debug]               BIT              NULL,
    [SlaveDebug]          BIT              NULL,
    [ReaderOutDebug]      BIT              NULL,
    [ReaderInDebug]       BIT              NULL,
    [DispensDebug]        BIT              NULL,
    [FeederDebug]         BIT              NULL,
    [checkBoxItog]        BIT              NULL,
    [logNastroy]          INT              NULL,
    [CardKey]             VARCHAR (MAX)    NULL,
    [WebCameraIP]         VARCHAR (MAX)    NULL,
    [WebCameraLogin]      VARCHAR (MAX)    NULL,
    [WebCameraPassword]   VARCHAR (MAX)    NULL,
    [TransitTypeId]       INT              NOT NULL,
    [RackWorkingModeId]   INT              NOT NULL,
    [ZoneId] UNIQUEIDENTIFIER NULL, 
    [ServerURL] NVARCHAR(MAX) NULL, 
    CONSTRAINT [PK_dbo.DeviceRackModel] PRIMARY KEY CLUSTERED ([DeviceId] ASC),
    CONSTRAINT [DeviceRackModel_fk] FOREIGN KEY ([RackWorkingModeId]) REFERENCES [dbo].[RackWorkingMode] ([Id]),
    CONSTRAINT [DeviceRackModel_fk2] FOREIGN KEY ([TransitTypeId]) REFERENCES [dbo].[TransitTypeModel] ([Id]),
    CONSTRAINT [DeviceRackModel_fk3] FOREIGN KEY ([ZoneBeforeId]) REFERENCES [dbo].[ZoneModel] ([IdFC]),
    CONSTRAINT [DeviceRackModel_fk4] FOREIGN KEY ([ZoneAfterId]) REFERENCES [dbo].[ZoneModel] ([IdFC]),
    CONSTRAINT [DeviceRackModel_fk5] FOREIGN KEY ([BarcodeTypeId]) REFERENCES [dbo].[ExecutableDevice] ([Id]),
    CONSTRAINT [DeviceRackModel_fk6] FOREIGN KEY ([OutReaderTypeId]) REFERENCES [dbo].[ExecutableDevice] ([Id]),
    CONSTRAINT [DeviceRackModel_fk8] FOREIGN KEY ([TarifIdScheduleId]) REFERENCES [dbo].[TariffScheduleModel] ([IdFC]),
    CONSTRAINT [FK_dbo.DeviceRackModel_dbo.DeviceModel_DeviceId] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[DeviceModel] ([Id]),
    CONSTRAINT [FK_dbo.DeviceRackModel_dbo.ExecutableDevice_BankModuleTypeId] FOREIGN KEY ([BankModuleTypeId]) REFERENCES [dbo].[ExecutableDevice] ([Id]),
    CONSTRAINT [FK_dbo.DeviceRackModel_dbo.ExecutableDevice_BarcodeTypeId] FOREIGN KEY ([BarcodeTypeId]) REFERENCES [dbo].[ExecutableDevice] ([Id]),
    CONSTRAINT [FK_dbo.DeviceRackModel_dbo.ExecutableDevice_DispenserTypeId] FOREIGN KEY ([DispenserTypeId]) REFERENCES [dbo].[ExecutableDevice] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [Foreign_key01fhnb] FOREIGN KEY ([ClientGroupidToC]) REFERENCES [dbo].[Group] ([idFC]),
    CONSTRAINT [Foreign_key01rtyuo] FOREIGN KEY ([TarifPlanId]) REFERENCES [dbo].[Tariffs] ([IdFC]),
    CONSTRAINT [FK_DeviceRackModel_ZoneModel] FOREIGN KEY ([ZoneId]) REFERENCES [dbo].[ZoneModel]([Id])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Устройство', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'DeviceId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Зона до', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'ZoneBeforeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Зона после', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'ZoneAfterId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип диспенсера карт', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'DispenserTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип сканера штрихкода', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'BarcodeTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип банковского модуля', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'BankModuleTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Макс. время занятой антенны А', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'MaxBusyTimeAntennaA';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Макс. время занятой антенны B', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'MaxBusyTimeAntennaB';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Макс. время занятого ИК', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'MaxBusyTimeIR';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Флаг синхронизации со 2ым ур.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = 'Sync';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие слейв', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'SlaveExist';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие внешнего считывателя', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'ReaderOutExists';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип внешнего считывателя', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'OutReaderTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие считывателя на дисп.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'ReaderInExists';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип внутреннего считывателя', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'InReaderTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие диспенсера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'DispensExists';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие моториз. картоприемника.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'FeederExists';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип моториз. картоприемника.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'FeederTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие сканера штрихкода', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'BarcodeExist';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличия банковского модуля', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'BankModuleExist';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наличие дисплея', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = 'DisplayExists';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип дисплея', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'DisplayTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Com порт дисплей', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = 'ComDisplay';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Com порт Slave', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'ComSlave';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Com порт ридер внешний', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'ComReadOut';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Com порт ридер внутри', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'ComReadIn';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Com порт диспенсер', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'ComDispens';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Com порт фидер', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'ComFeeder';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COM порт сканера штрихкода', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'ComBarcode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Номер порта 1-й платы дискрета', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'PortDiscret1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Номер порта 2-й платы дискрета', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'PortDiscret2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Номер порта 3-й платы дискрета (высотомер)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'PortDiscret3';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Режим использования распознавания гос. номера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'PlateNumberUse';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время карты в губах', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'TimeCardV';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Длительность импульса на шлагбаум', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'TimeBarrier';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Номер сектора', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'NumerSector';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID ТС разовый', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'TarifIdScheduleId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID ТП разовый', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'TarifPlanId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Группа клиента для записи разового', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'ClientGroupidToC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Маневрирование перед стойкой', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'TimeNotLoopA';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ожидание ИК перед закртием', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'TimeNotIr';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Инверсия loopА', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'LoopA';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Инверсия loopВ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'LoopB';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Инверсия ИК', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'IR';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Инверсия Нажмите', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'PUSH';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Отладка', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'Debug';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Имиитация Slave', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'SlaveDebug';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Имиитация ридера внешнего', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'ReaderOutDebug';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Имиитация ридера внутри', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'ReaderInDebug';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Имиитация диспенсера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'DispensDebug';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Имиитация транспортера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'FeederDebug';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Итог по клику', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'checkBoxItog';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Настройки лога (0-ошибки, 1-все изменения)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'logNastroy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ключ карты', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'CardKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IP камеры', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'WebCameraIP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Логин для подключения к камере', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'WebCameraLogin';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Пароль для подключения к камере', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'WebCameraPassword';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип проезда', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'TransitTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Блокировать=1, свободный=2, штатный=3 проезд', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'RackWorkingModeId';

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL сервера', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DeviceRackModel', @level2type = N'COLUMN', @level2name = N'ServerURL';


GO
CREATE TRIGGER [dbo].[SyncDeviceRackModel]
ON [dbo].[DeviceRackModel]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [DeviceRackModel] set Sync=@dt	where DeviceId in (select DeviceId from inserted)
	update SyncTables set [DateTime]=@dt where TableName='DeviceRackModel'
end
