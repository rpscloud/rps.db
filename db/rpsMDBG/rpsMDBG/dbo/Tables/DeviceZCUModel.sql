﻿CREATE TABLE [dbo].[DeviceZCUModel] (
    [DeviceId]   UNIQUEIDENTIFIER NOT NULL,
    [_IsDeleted] BIT              NOT NULL,
    CONSTRAINT [PK_dbo.DeviceZCUModel] PRIMARY KEY CLUSTERED ([DeviceId] ASC),
    CONSTRAINT [FK_dbo.DeviceZCUModel_dbo.DeviceModel_DeviceId] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[DeviceModel] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_DeviceId]
    ON [dbo].[DeviceZCUModel]([DeviceId] ASC);

