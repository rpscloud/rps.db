﻿CREATE TABLE [dbo].[TariffModel] (
    [Id]         UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Name]       NVARCHAR(MAX)    NULL,
    [TimeTypeId] INT              NOT NULL,
    [_IsDeleted] BIT              NOT NULL,
    [Color]      NVARCHAR(MAX)    NULL,
    [Sync]       DATETIME         NULL,
    CONSTRAINT [PK_dbo.TariffModel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.TariffModel_dbo.TimeTypeModel_TimeTypeId] FOREIGN KEY ([TimeTypeId]) REFERENCES [dbo].[TimeTypeModel] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_TimeTypeId]
    ON [dbo].[TariffModel]([TimeTypeId] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наименование', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffModel', @level2type = N'COLUMN', @level2name = N'Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время тарифа в', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffModel', @level2type = N'COLUMN', @level2name = N'TimeTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Цвет', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffModel', @level2type = N'COLUMN', @level2name = N'Color';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Время последнего изменения строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TariffModel', @level2type = N'COLUMN', @level2name = N'Sync';

GO
CREATE TRIGGER [dbo].[SyncTariffModel]
ON [dbo].[TariffModel]
AFTER INSERT,UPDATE
AS
BEGIN
SET NOCOUNT ON;
declare @dt datetime;
	set @dt=GetDate();
	update [TariffModel] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='TariffModel'
end
