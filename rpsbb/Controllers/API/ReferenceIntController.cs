﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using rps.Data;
using ASE.MVC;

namespace rpsbb.Controllers
{
    [Authorize]
    public class ReferenceIntController : ApiController
    {
        [System.Web.Http.HttpGet]
        public IReferenceInt Get(int id, string type)
        {
            return (IReferenceInt) ContextPerRequest.Db.Set(Type.GetType("rps.Data." + type)).Find(id);
        }

        [System.Web.Http.HttpGet]
        public List<IReferenceInt> Get(string type)
        {
            var ret = new List<IReferenceInt>();
            var result = ContextPerRequest.Db.Set(Type.GetType("rps.Data." + type + ", RPS.Shared, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null")).AsQueryable().GetEnumerator();
            while (result.MoveNext())
                ret.Add(result.Current as IReferenceInt);

            return ret;
        }

        [System.Web.Http.HttpGet]
        public List<IReferenceInt> Get(string type, int? typeId)
        {
            var ret = new List<IReferenceInt>();
            var result = ContextPerRequest.Db.Set(Type.GetType("rps.Data." + type + ", RPS.Shared, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null")).AsQueryable().GetEnumerator();
            while (result.MoveNext())
                if (!typeId.HasValue)
                    ret.Add(result.Current as IReferenceInt);
                else if ((result.Current as IReferenceIntType).Type == typeId.Value)
                    ret.Add(result.Current as IReferenceInt);

            return ret;
        }
    }
}
