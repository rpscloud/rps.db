
CREATE TABLE [dbo].[VirtualCard] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [MiFareCardId] BIGINT           NOT NULL,
    [TagId]        NVARCHAR (20)    NOT NULL,
    [TagType]      INT              NOT NULL,
    [Description]  VARCHAR (MAX)    NULL,
    [Sync]         DATETIME         NULL,
    [IsDeleted]    BIT              NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Тип метки в фомате варчар (Номер ТС, Номер тэга, Номер карты СКУД)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VirtualCard', @level2type = N'COLUMN', @level2name = N'TagType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id идентификатор', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VirtualCard', @level2type = N'COLUMN', @level2name = N'TagId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Поле для синхронизации', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VirtualCard', @level2type = N'COLUMN', @level2name = N'Sync';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Номер виртуальной MiFare карты в БД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VirtualCard', @level2type = N'COLUMN', @level2name = N'MiFareCardId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Флаг удаления строки', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VirtualCard', @level2type = N'COLUMN', @level2name = N'IsDeleted';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Комментарий', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VirtualCard', @level2type = N'COLUMN', @level2name = N'Description';

