﻿CREATE TABLE [dbo].[ExecutableDevice] (
    [Id]                     INT           NOT NULL,
    [ExecutableDeviceTypeId] INT           NULL,
    [Name]                   VARCHAR (MAX) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [ExecutableDevice_fk] FOREIGN KEY ([ExecutableDeviceTypeId]) REFERENCES [dbo].[ExecutableDeviceTypeModel] ([Id])
);

