﻿CREATE TABLE [dbo].[CardsTransaction](
	[Id] [uniqueidentifier] DEFAULT (newid()) NOT NULL,
	[Blocked] [tinyint] NOT NULL,
	[_IsDeleted] [bit] NOT NULL,
	[Sync] [datetime] NULL,
	[CardId] BIGINT NOT NULL,
	[ParkingEnterTime] [datetime] NULL,
	[LastRecountTime] [datetime] NULL,
	[LastPaymentTime] [datetime] NULL,
	[TSidFC] [tinyint] NULL,
	[TPidFC] [tinyint] NULL,
	[ZoneidFC] [tinyint] NULL,
	[ClientGroupidFC] [tinyint] NULL,
	[SumOnCard] [int] NULL,
	[Nulltime1] [datetime] NULL,
	[Nulltime2] [datetime] NULL,
	[Nulltime3] [datetime] NULL,
	[TVP] [datetime] NULL,
	[TKVP] [int] NULL,
	[ClientTypidFC] INT NULL,
	[DateSaveCard]	 [DATETIME]	NULL,
	[LastPlate] NVARCHAR(MAX) NULL, 
    CONSTRAINT [PK_dbo.CardsTransaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CardsTransaction]  WITH CHECK ADD  CONSTRAINT [Foreign_key01aadfh_Transaction] FOREIGN KEY([ClientGroupidFC])
REFERENCES [dbo].[Group] ([idFC])
GO

ALTER TABLE [dbo].[CardsTransaction] CHECK CONSTRAINT [Foreign_key01aadfh_Transaction]
GO
ALTER TABLE [dbo].[CardsTransaction]  WITH CHECK ADD  CONSTRAINT [Foreign_key01asdg_Transaction] FOREIGN KEY([ZoneidFC])
REFERENCES [dbo].[ZoneModel] ([IdFC])
GO

ALTER TABLE [dbo].[CardsTransaction] CHECK CONSTRAINT [Foreign_key01asdg_Transaction]
GO
CREATE TRIGGER [dbo].[SyncCardsTransaction]
ON [dbo].[CardsTransaction]
AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [CardsTransaction] set Sync=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='CardsTransaction'
	declare @level int;

	if(Exists(select top 1 CurrentLevel from ConfigSync))
	begin
		set @level=(select top 1 CurrentLevel from ConfigSync);
		if(@level=2)
		begin
			update Cards set 
				Blocked=i.Blocked,
				_IsDeleted=i._IsDeleted,
				ParkingEnterTime=i.ParkingEnterTime,
				LastRecountTime=i.LastRecountTime,
				LastPaymentTime=i.LastPaymentTime,
				TSidFC=i.TSidFC,
				TPidFC=i.TPidFC,
				ZoneidFC=i.ZoneidFC,
				ClientGroupidFC=i.ClientGroupidFC,
				SumOnCard=i.SumOnCard,
				Nulltime1=i.Nulltime1,
				Nulltime2=i.Nulltime2,
				Nulltime3=i.Nulltime3,
				TVP=i.TVP,
				TKVP=i.TKVP,
				ClientTypidFC=i.ClientTypidFC,
				DateSaveCard=i.DateSaveCard,
				LastPlate=i.LastPlate
			from Cards c,inserted i
			where c.CardId=i.CardId and c.DateSaveCard<i.DateSaveCard;

			insert into Cards (Blocked,_IsDeleted,CardId,ParkingEnterTime,LastRecountTime,LastPaymentTime,
						TSidFC,TPidFC,ZoneidFC,ClientGroupidFC,SumOnCard,Nulltime1,Nulltime2,Nulltime3,TVP,TKVP,
						ClientTypidFC,DateSaveCard,LastPlate)
			select Blocked,_IsDeleted,CardId,ParkingEnterTime,LastRecountTime,LastPaymentTime,
					TSidFC,TPidFC,ZoneidFC,ClientGroupidFC,SumOnCard,Nulltime1,Nulltime2,Nulltime3,TVP,TKVP,
					ClientTypidFC,DateSaveCard,LastPlate
			from inserted
			where CardId not in (select CardId from Cards)
		end
	end
end