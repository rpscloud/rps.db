﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ASE.Data;

using rps.Data;

namespace rpsbb.Controllers
{
    public class MenuItem
    {        
        public Guid Id { get; set; }

        public int Num { get; set; }
        
        public string Image_url { get; set; }

    }

    [Authorize]
    public class Adm_menuController : ApiController
    {

        public static List<MenuItem> MenuItems = new List<MenuItem>
        {
            new MenuItem { Id = Guid.Parse("8A572822-4E29-4974-945D-A5AB7261BCFC"), Num = 1, Image_url = "/style/img/l1.png"  },
            new MenuItem { Id = Guid.Parse("A62B34E0-410C-4584-8425-C11143429F39"), Num = 2, Image_url = "/style/img/l2.png"  },
            new MenuItem { Id = Guid.Parse("7D5C39AB-4745-4626-BED1-28B90B1DAC13"), Num = 3, Image_url = "/style/img/l3.png"  },
            new MenuItem { Id = Guid.Parse("7AFC6248-32BA-417C-8B1C-D891EA840B8F"), Num = 4, Image_url = "/style/img/l4.png"  },
            new MenuItem { Id = Guid.Parse("657D3AFC-4EE7-4D7E-9EE5-144226C0E02D"), Num = 5, Image_url = "/style/img/l5.png"  },
        };

        DB db = new DB();

        [System.Web.Http.HttpGet]
        public IEnumerable<MenuItem> List()
        {
            return MenuItems;
        }
    }
}
