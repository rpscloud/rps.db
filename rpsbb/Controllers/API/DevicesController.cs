﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

using ASE.MVC;
using ASE.Data;

using rps.Data;
using rpsbb.Code;

namespace rpsbb.Controllers
{
    [Authorize]
    public class DevicesController : ApiControllerAdv<DB>
    {
        [System.Web.Http.HttpGet]
        public DeviceModelJson Get(Guid id)
        {
            return db.Devices.Find(id).Get();
        }

        [System.Web.Http.HttpGet]
        public IEnumerable<DeviceModelJson> List()
        {
            var q = db.Devices;

            return q.ToList().Select(x => x.Get());
        }

        [System.Web.Http.HttpPost]
        public DeviceModelJson Add(DeviceModelJson model)
        {
            model.Id = Guid.Empty;
            return model.DALSave().Get();
        }

        [System.Web.Http.HttpPut]
        public DeviceModelJson Put(DeviceModelJson model)
        {
            return model.DALSave().Get();
        }

        [System.Web.Http.HttpPatch]
        public DeviceModelJson Patch(DeviceModelJson model)
        {
            return model.DALSave(true).Get();
        }

        [System.Web.Http.HttpDelete]
        public DeviceModelJson Delete([FromUri] DeviceModelJson model)
        {
            return model.DALDelete().Get();
        }
    }
}
