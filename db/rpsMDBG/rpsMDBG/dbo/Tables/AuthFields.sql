﻿CREATE TABLE [dbo].[AuthFields] (
    [Id]             INT              IDENTITY (1, 1) NOT NULL,
    [ObjectClass]    VARCHAR (MAX)    NULL,
    [CanReadFields]  NVARCHAR (MAX)   NULL,
    [CanWriteFields] TEXT             NULL,
    [Updated]        DATETIME         NULL,
    [RightId]        UNIQUEIDENTIFIER NULL,
    [Module]         VARCHAR (MAX)    DEFAULT (NULL) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [AuthFields_RightModel_Id_fk] FOREIGN KEY ([RightId]) REFERENCES [dbo].[RightModel] ([Id])
);

