﻿CREATE TABLE [dbo].[RoleModel] (
    [Id]        UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Name]      VARCHAR (MAX)    NOT NULL,
    [ParkingId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_dbo.RoleModel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.RoleModel_dbo.ParkingModel_ParkingId] FOREIGN KEY ([ParkingId]) REFERENCES [dbo].[ParkingModel] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ParkingId]
    ON [dbo].[RoleModel]([ParkingId] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RoleModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Наименование', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RoleModel', @level2type = N'COLUMN', @level2name = N'Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RoleModel', @level2type = N'COLUMN', @level2name = N'ParkingId';

