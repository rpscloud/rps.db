﻿CREATE TABLE [dbo].[ZoneModel] (
    [Id]         UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Name]       VARCHAR (MAX)    NULL,
    [Capacity]   INT              NULL,
    [Reserved]   INT              NULL,
    [Sync]       DATETIME         NULL,
    [_IsDeleted] BIT              NULL,
    [IdFC]       TINYINT          NULL,
    [FreeSpace]  INT              NULL,
    [OccupId]    INT              NULL,
    CONSTRAINT [PK_dbo.ZoneModel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [ZoneModel_uq] UNIQUE NONCLUSTERED ([IdFC] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ZoneModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Название', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ZoneModel', @level2type = N'COLUMN', @level2name = N'Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Кол-во машинамест', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ZoneModel', @level2type = N'COLUMN', @level2name = N'Capacity';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'?', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ZoneModel', @level2type = N'COLUMN', @level2name = N'Reserved';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД для карты', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ZoneModel', @level2type = N'COLUMN', @level2name = N'IdFC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Кол-во свободных м/м', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ZoneModel', @level2type = N'COLUMN', @level2name = N'FreeSpace';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Занято мест', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ZoneModel', @level2type = N'COLUMN', @level2name = N'OccupId';


GO

CREATE TRIGGER [dbo].[SyncZoneModel]
ON [dbo].[ZoneModel]
AFTER INSERT,UPDATE
AS
BEGIN
SET NOCOUNT ON;
	declare @dt datetime;
	set @dt=GetDate();
	update [ZoneModel] set [Sync]=@dt	where Id in (select Id from inserted)
	update SyncTables set [DateTime]=@dt where TableName='ZoneModel'
end
GO

--CREATE TRIGGER [dbo].[CarParkZoneModel_Insert]
--ON [dbo].[ZoneModel]
--AFTER INSERT
--AS
--BEGIN
--	SET NOCOUNT ON;
--	declare @level int;
--	if(Exists(select top 1 CurrentLevel from ConfigSync))
--	begin
--		set @level=(select top 1 CurrentLevel from ConfigSync);
--		if(@level=2)
--		begin
--			INSERT INTO CarParkCapacity(Id,TimeDate, ZoneId,NumberOfCars)
--				SELECT NEWID(),GETDATE(),i1.Id,(i1.Capacity-i1.FreeSpace) FROM inserted i1
--				where i1.Name!='Вне парковки'
--		end
--	end
--end
--go

--CREATE TRIGGER [dbo].[CarParkZoneModel_UPDATE]
--ON [dbo].[ZoneModel]
--AFTER UPDATE
--AS
--BEGIN
--	SET NOCOUNT ON;
--	declare @level int;
--	if(Exists(select top 1 CurrentLevel from ConfigSync))
--	begin
--		set @level=(select top 1 CurrentLevel from ConfigSync);
--		if(@level=2)
--		begin
--			INSERT INTO CarParkCapacity(Id,TimeDate, ZoneId,NumberOfCars)
--				SELECT NEWID(),GETDATE(),i1.Id,(i1.Capacity-i1.FreeSpace) FROM inserted i1
--				,deleted d1
--				--left join ZoneModel zm on zm.Id=i1.Id
--				where d1.FreeSpace!=i1.FreeSpace and d1.Id=i1.Id and
--				i1.Name!='Вне парковки'
--		end
--	end
--end
--go


--Изминение триггера ZoneModel

CREATE TRIGGER [dbo].[AFTER_UPDATE_ZONE]
ON [dbo].[ZoneModel]
AFTER UPDATE
AS
  BEGIN
    SET NOCOUNT ON;
    declare @oldFree int;
    declare @newFree int;
    declare @Id UNIQUEIDENTIFIER;
    DECLARE @S varchar(max);

    set @Id = (select Id from inserted);
    set @oldFree = (SELECT FreeSpace FROM deleted);
    set @newFree = (SELECT FreeSpace FROM inserted);


    --IF (UPDATE(FreeSpace))
    --  BEGIN
    --    SET @S = CONCAT('?T=ZoneModel', '&A=U', '&I=' , CAST(@Id AS varchar(max)), '&V=', @newFree, '&O=', @oldFree)
    --    EXEC SPRemoteNotifierL @S
    --  END

  END