﻿CREATE TABLE [dbo].[OrganizationParkingModel] (
    [Id]             UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [OrganizationId] UNIQUEIDENTIFIER NOT NULL,
    [ParkingId]      UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_dbo.OrganizationParkingModel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.OrganizationParkingModel_dbo.OrganizationModel_OrganizationId] FOREIGN KEY ([OrganizationId]) REFERENCES [dbo].[OrganizationModel] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.OrganizationParkingModel_dbo.ParkingModel_ParkingId] FOREIGN KEY ([ParkingId]) REFERENCES [dbo].[ParkingModel] ([Id]) ON DELETE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_OrganizationId_ParkingId]
    ON [dbo].[OrganizationParkingModel]([OrganizationId] ASC, [ParkingId] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ИД', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'OrganizationParkingModel', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Пользователь', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'OrganizationParkingModel', @level2type = N'COLUMN', @level2name = N'OrganizationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Парковка', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'OrganizationParkingModel', @level2type = N'COLUMN', @level2name = N'ParkingId';

