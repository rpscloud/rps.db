﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RPS_Shared;

namespace rps.Sync
{
    public static partial class Synchronize
    {
        private static bool PriousePingOk = false;

        public static void StartPing(int type, Func<int> onRestore)
        {
            Task perdiodicTask = PeriodicTaskFactory.Start(() =>
                {
                    var device = GetDevice(type);
                    if (device == null)
                        return;

                    var cs = String.Format("data source={0};Initial Catalog={1};User ID={2};Password={3};{4}", device.Host, device.DataBase, device.User, device.Password, device.Advanced);
                    if (cs.ToLower().IndexOf(";connection timeout=") == -1)
                        cs += ";Connection Timeout=3";

                    try
                    {
                        using (var dbR = new Entities(cs))
                        {
                            var test = dbR.DeviceModel.FirstOrDefault();
                            if (!PriousePingOk)
                            {
                                PriousePingOk = onRestore() == 0;
                            }
                        }
                    }
                    catch
                    {
                        PriousePingOk = false;
                    }
                }, 
                intervalInMilliseconds: 5000, 
                maxIterations: -1
            );           
        }

        private static void Copy<TEntity>(Entities from, Entities to, TEntity model, bool add) where TEntity : class
        {
            if (add)
                to.Entry<TEntity>(model).State = System.Data.Entity.EntityState.Added;
            else
                to.Entry<TEntity>(model).State = System.Data.Entity.EntityState.Modified;

            to.SaveChanges();
            to.Entry<TEntity>(model).State = System.Data.Entity.EntityState.Detached;
        }

        private static DeviceModel GetDevice(int type)
        {
            using (var db = new Entities())
            {
                return db.DeviceModel.FirstOrDefault(x => x.Type == type);
            }
        }

        private static Semaphore _lock = new Semaphore(1, 1);

        public static int L1_To_L23()
        {
            int result = 0;
            _lock.WaitOne();

            try
            {
                ASE.Log.L("------L1_To_L23 start");

                result += DeviceCMModel();
                result += DeviceRackModel();
                result += AlarmModel();                

                ASE.Log.L("L1_To_L23 stop");
            }
            catch(Exception exc)
            {
                result = -1;
                ASE.Log.L(exc);
            }

            _lock.Release();

            return result;
        }

        public static int DeviceCMModel()
        {
            int result = DeviceCMModel<DeviceCMModel>(GetDevice(-1), x => !x.C_IsSync2, z => new DeviceCMModel { C_IsSync2 = true });
            result += DeviceCMModel<DeviceCMModel>(GetDevice(-2), x => !x.C_IsSync3, z => new DeviceCMModel { C_IsSync3 = true });
            
            return result;
        }

        public static int DeviceRackModel()
        {
            int result = DeviceRackModel<DeviceRackModel>(GetDevice(-1), x => !x.C_IsSync2, z => new DeviceRackModel { C_IsSync2 = true });
            result += DeviceRackModel<DeviceRackModel>(GetDevice(-2), x => !x.C_IsSync3, z => new DeviceRackModel { C_IsSync3 = true });

            return result;
        }
        public static int AlarmModel()
        {
            int result = AlarmModel<AlarmModel>(GetDevice(-1), x => !x.C_IsSync2, z => new AlarmModel { C_IsSync2 = true });
            result += AlarmModel<AlarmModel>(GetDevice(-2), x => !x.C_IsSync3, z => new AlarmModel { C_IsSync3 = true });

            return result;
        }
    }
}
