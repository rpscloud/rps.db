﻿CREATE TABLE [dbo].[AuthAssignment] (
    [itemname] VARCHAR (64) NOT NULL,
    [userid]   VARCHAR (64) NOT NULL,
    [bizrule]  TEXT         NULL,
    [data]     TEXT         NULL,
    CONSTRAINT [PK_AuthAssignment] PRIMARY KEY CLUSTERED ([itemname] ASC, [userid] ASC),
    CONSTRAINT [FK_AuthAssignment_AuthItem] FOREIGN KEY ([itemname]) REFERENCES [dbo].[AuthItem] ([name])
);

