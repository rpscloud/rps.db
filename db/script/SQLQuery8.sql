USE [rpsMDBG]

GO

INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (121, 3, N'Этот штрих-код уже используется');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (122, 3, N'Закончился срок действия штрих-кода');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (123, 3, N'Штрих-код не действителен');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (124, 3, N'Проезд запрещён по гос. номеру');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (125, 3, N'кража карты');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (126, 3, N'Проезд запрещён Карта заблокирована');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (127, 3, N'Проезд запрещён Нет карты в базе');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (128, 3, N'Проезд запрещён Нет мест для компании');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (129, 3, N'Проезд запрещён Не совпал гос.номер');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (130, 3, N'Проезд запрещён группе клиентов');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (131, 3, N'Проезд запрещён Несоответствие зоны');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (132, 3, N'проезд с отбоем стрелы');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (133, 3, N'отказ от проезда');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (134, 3, N'попытка неоплаченного проезда');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (135, 3, N'Разовая карта на внешнем считывателе');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (136, 3, N'ОШИБКА ЧТЕНИЯ КАРТЫ');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (137, 3, N'ОШИБКА КЛЮЧА КАРТЫ');
INSERT INTO dbo.MessageTypeModel (Id, Color, [Message]) VALUES  (138, 3, N'ОШИБКА ЗАПИСИ КАРТЫ');

GO