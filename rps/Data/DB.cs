﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using rps.Data;

namespace ASE.Data
{
    public class DB : DbContext
    {
        public DbSet<OrganizationModel> Organizations { get; set; }

        public DbSet<ParkingModel> Parkings { get; set; }

        public DbSet<OrganizationParkingModel> OrganizationParkings { get; set; }

        public DbSet<UserModel> Users { get; set; }

        public DbSet<RightModel> Rights { get; set; }

        public DbSet<RoleModel> Roles { get; set; }

        public DbSet<RightsInRoleModel> RightsInRoles { get; set; }

        public DbSet<UserParkingRoleModel> UserParkings { get; set; }


        public DbSet<Setting> Settings { get; set; }

        public DbSet<Map> Maps { get; set; }

        public DbSet<NavMapRect> NavMapRects { get; set; }

        public DbSet<NavMapRectReserv> NavMapRectReservs { get; set; }

        public DbSet<Device> Devices { get; set; }

        public DbSet<SyncItem> SyncItems { get; set; }        
        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<NavMapRectReserv>().HasRequired(x => x.NavMapRect).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<NavMapRect>().HasOptional(x => x.CurrentReserv).WithMany().WillCascadeOnDelete(false);

            //modelBuilder.Entity<UserParkingRoleModel>().HasOptional(x => x.CurrentReserv).WithMany().WillCascadeOnDelete(false);

            /*modelBuilder.Entity<Source>().HasKey(x => x.Id);
            modelBuilder.Entity<TermElement>().HasKey(x => x.Id);
            modelBuilder.Entity<TermElementValue>().HasKey(x => x.Id);
            modelBuilder.Entity<ValueHistory>().Property(x => x.ValueDecimal).HasPrecision(19, 4);

            modelBuilder.Entity<TermElementValue>().Property(e => e.TermElementId).HasColumnAnnotation("Index",
                new IndexAnnotation(new[] { 
                    new IndexAttribute("TermElementId"), 
                    new IndexAttribute("TermElementIdAndDateTime", 1)}));
            modelBuilder.Entity<TermElementValue>().Property(e => e.DateTime).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("TermElementIdAndDateTime", 2)));

            modelBuilder.Entity<HeaterElementState>().Property(e => e.HeaterElementId).HasColumnAnnotation("Index",
                new IndexAnnotation(new[] { 
                    new IndexAttribute("HeaterElementId"), 
                    new IndexAttribute("HeaterElementIdAndDateTime", 1)}));
            modelBuilder.Entity<HeaterElementState>().Property(e => e.DateTime).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("HeaterElementIdAndDateTime", 2)));

            modelBuilder.Entity<HeaterElementMode>().Property(e => e.HeaterElementId).HasColumnAnnotation("Index",
                new IndexAnnotation(new[] { 
                    new IndexAttribute("HeaterElementId"), 
                    new IndexAttribute("HeaterElementIdAndDateTime", 1)}));
            modelBuilder.Entity<HeaterElementMode>().Property(e => e.DateTime).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("HeaterElementIdAndDateTime", 2)));

            modelBuilder.Entity<ElementVoltage>().Property(e => e.ElementId).HasColumnAnnotation("Index",
                new IndexAnnotation(new[] { 
                    new IndexAttribute("ElementId"), 
                    new IndexAttribute("ElementIdAndDateTime", 1)}));
            modelBuilder.Entity<ElementVoltage>().Property(e => e.DateTime).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("ElementIdAndDateTime", 2)));
            */

            //modelBuilder.Ignore<UserParking>();
            //modelBuilder.Ignore<ParkingModel>();
            //modelBuilder.Ignore<NavMap>();
            //modelBuilder.Ignore<NavMapRect>();
            //modelBuilder.Ignore<NavMapRectReserv>();
            //modelBuilder.Ignore<OrganizationParking>();
            //modelBuilder.Entity<ParkingModel>().Property(x => x.OrganizationId).IsOptional();
        }

        public DB()
            : base("ConnectionString")
        {
        }
        public DB(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        public override int SaveChanges()
        {
            /*foreach(var item in this.ChangeTracker.Entries())
            {
                if (item.Entity is IChanged)
                {
                    (item.Entity as IChanged).ChangedDateTime = DateTime.Now;
                }
            }*/
            return base.SaveChanges();
        }
    }
}
