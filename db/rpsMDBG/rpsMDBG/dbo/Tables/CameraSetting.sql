﻿CREATE TABLE [dbo].[CameraSetting](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DeviceId] [uniqueidentifier] NOT NULL,
	[Channel] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_CameraSetting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[CameraSetting]  WITH CHECK ADD  CONSTRAINT [FK_CameraSetting_DeviceModel] FOREIGN KEY([DeviceId])
REFERENCES [dbo].[DeviceModel] ([Id])
GO

ALTER TABLE [dbo].[CameraSetting] CHECK CONSTRAINT [FK_CameraSetting_DeviceModel]